package com.dreamorbit.researchaware.api;

import android.util.Log;

import com.dreamorbit.researchaware.model.fit.weather.WeatherResponse;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by nareshkumar.reddy on 12/19/2017.
 */

public class WeatherApi {

    public void getLocation(double latitude, double longitude) {

        Retrofit retrofit = ApiClient.getGeoCodeClient("http://api.openweathermap.org/data/2.5/");
        IApiRepo service = retrofit.create(IApiRepo.class);

        HashMap<String, Object> geoData = new HashMap<>();
        geoData.put("lat",String.valueOf(latitude));
        geoData.put("lon",String.valueOf(longitude));
        geoData.put("appid","b56ef860ea8018ab2a94bc5f24f91bcf");
        Call<WeatherResponse> call = service.getWeather(geoData);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response != null && response.code() == 200 ) {
                    Log.e("Response:", "" + response.body());
                    Gson gson = new Gson();
                    String  weatherData = gson.toJson(response.body(), WeatherResponse.class);
                    SdkSharedPrefSingleton.getInstance().saveWeatherData(weatherData);
                } else {
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.e("", "" + t.toString());
            }
        });
    }

}
