package com.dreamorbit.researchaware.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mujasam.bn on 9/6/2017.
 */
public class ApiClient {

    private static Retrofit sRetrofit = null;
    private static long RETROFIT_TIMEOUT = 30;
    private static String contentType;

    /**
     * For AWS api call
     *
     * @return
     */
    public static Retrofit getGeoCodeClient(String baseUrl) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging)
                .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS);

        OkHttpClient client = httpClient.build();
        sRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return sRetrofit;
    }
}
