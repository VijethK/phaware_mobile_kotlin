package com.dreamorbit.researchaware.api;


import com.dreamorbit.researchaware.model.fit.weather.WeatherResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;


/**
 * Created by mujasam.bn on 9/6/2017.
 */

//https://guides.codepath.com/android/Consuming-APIs-with-Retrofit

public interface IApiRepo {

    @GET("weather")
    Call<WeatherResponse> getWeather(@QueryMap Map<String, Object> options);

}
