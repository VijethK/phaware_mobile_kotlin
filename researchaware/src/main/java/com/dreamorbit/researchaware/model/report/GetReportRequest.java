package com.dreamorbit.researchaware.model.report;

public class GetReportRequest {

    public GetReportRequest(){}
    private String username;
    private String from_date;
    private String to_date;

    public GetReportRequest(String username, String from_date, String to_date) {
        this.username = username;
        this.from_date = from_date;
        this.to_date = to_date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }
}
