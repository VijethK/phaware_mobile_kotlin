package com.dreamorbit.researchaware.model.fit.location;

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */

public class LocationItem {
    private Integer verticalAccuracy;
    private Double course;
    private Float speed;
    private Float horizontalAccuracy;
    private String timestamp;
    private Double altitude;
    private Coordinate coordinate;

    public Integer getVerticalAccuracy() {
        return verticalAccuracy;
    }

    public void setVerticalAccuracy(Integer verticalAccuracy) {
        this.verticalAccuracy = verticalAccuracy;
    }

    public Double getCourse() {
        return course;
    }

    public void setCourse(Double course) {
        this.course = course;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Float getHorizontalAccuracy() {
        return horizontalAccuracy;
    }

    public void setHorizontalAccuracy(Float horizontalAccuracy) {
        this.horizontalAccuracy = horizontalAccuracy;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

}
