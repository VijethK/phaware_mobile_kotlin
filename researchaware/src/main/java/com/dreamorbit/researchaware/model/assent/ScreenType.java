
package com.dreamorbit.researchaware.model.assent;

/**
 * enum to get screen flow type
 */

public enum ScreenType {
    VISUAL_SCREEN,
    SIGNATURE_SCREEN,
    REVIEW,
    USER_INFO_SCREEN,
    SURVEY_QUESTIONS_SCREEN
}