package com.dreamorbit.researchaware.model.report;

public class PostReportRequest {

    public PostReportRequest(){}
    public PostReportRequest(String userName, int avgHeartRate, int maxHeartRate, double distance, long totalSteps, String durationOfTest, String testTakenAt, int floorAscended) {
        this.username = userName;
        this.average_heart_rate = avgHeartRate;
        this.max_heart_rate = maxHeartRate;
        this.distance = distance;
        this.steps_count = totalSteps;
        this.duration_of_test = durationOfTest;
        this.test_taken_at = testTakenAt;
        this.total_floor_ascended =floorAscended;
    }

    private int id;
    private String username;
    private int average_heart_rate;
    private int max_heart_rate;
    private double distance;
    private long steps_count;
    private String duration_of_test;
    private String test_taken_at;
    private int total_floor_ascended;

    public int getFloorAscended() {
        return total_floor_ascended;
    }

    public void setFloorAscended(int floorAscended) {
        this.total_floor_ascended = floorAscended;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAverage_heart_rate() {
        return average_heart_rate;
    }

    public void setAverage_heart_rate(int average_heart_rate) {
        this.average_heart_rate = average_heart_rate;
    }

    public int getMax_heart_rate() {
        return max_heart_rate;
    }

    public void setMax_heart_rate(int max_heart_rate) {
        this.max_heart_rate = max_heart_rate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getSteps_count() {
        return steps_count;
    }

    public void setSteps_count(long steps_count) {
        this.steps_count = steps_count;
    }

    public String getDuration_of_test() {
        return duration_of_test;
    }

    public void setDuration_of_test(String duration_of_test) {
        this.duration_of_test = duration_of_test;
    }

    public String getTest_taken_at() {
        return test_taken_at;
    }

    public void setTest_taken_at(String test_taken_at) {
        this.test_taken_at = test_taken_at;
    }
}
