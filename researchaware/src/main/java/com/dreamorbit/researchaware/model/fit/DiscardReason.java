package com.dreamorbit.researchaware.model.fit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mujasam.bn on 12/28/2017.
 */

public class DiscardReason {

    @SerializedName("discardReason")
    @Expose
    private String discardReason;

    public String getDiscardReason() {
        return discardReason;
    }

    public void setDiscardReason(String discardReason) {
        this.discardReason = discardReason;
    }
}
