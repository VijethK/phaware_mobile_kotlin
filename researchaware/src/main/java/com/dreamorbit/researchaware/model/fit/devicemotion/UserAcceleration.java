package com.dreamorbit.researchaware.model.fit.devicemotion;

/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class UserAcceleration {

    private Float x;

    private Float y;

    private Float z;

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getZ() {
        return z;
    }

    public void setZ(Float z) {
        this.z = z;
    }
}
