package com.dreamorbit.researchaware.model.fit.location;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */

public class Coordinate {
    private Double longitude;
    private Double latitude;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
