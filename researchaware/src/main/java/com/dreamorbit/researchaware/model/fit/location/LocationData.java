package com.dreamorbit.researchaware.model.fit.location;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */

public class LocationData {
    private List<LocationItem> items = new ArrayList<>();

    public List<LocationItem> getItems() {
        return items;
    }

    public void addItems(LocationItem item) {
       items.add(item);
    }
}
