package com.dreamorbit.researchaware.model.survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mujasam.bn on 10/30/2017.
 */

public class QuestionList {

    @SerializedName("questions")
    @Expose
    private List<Questions> questions = null;

    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }
}

