
package com.dreamorbit.researchaware.model.assent;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConsentAssent implements Parcelable {

    @SerializedName("sections")
    @Expose
    private ArrayList<Section> sections = null;

    protected ConsentAssent(Parcel in) {
        sections = in.createTypedArrayList(Section.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(sections);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConsentAssent> CREATOR = new Creator<ConsentAssent>() {
        @Override
        public ConsentAssent createFromParcel(Parcel in) {
            return new ConsentAssent(in);
        }

        @Override
        public ConsentAssent[] newArray(int size) {
            return new ConsentAssent[size];
        }
    };

    public ArrayList<Section> getSections() {
        return sections;
    }

    public void setSections(ArrayList<Section> sections) {
        this.sections = sections;
    }

}
