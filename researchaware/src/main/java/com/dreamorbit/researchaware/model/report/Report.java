
package com.dreamorbit.researchaware.model.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Report {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("average_heart_rate")
    @Expose
    private Integer averageHeartRate;
    @SerializedName("max_heart_rate")
    @Expose
    private Integer maxHeartRate;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("steps_count")
    @Expose
    private Integer stepsCount;
    @SerializedName("duration_of_test")
    @Expose
    private String durationOfTest;
    @SerializedName("test_taken_at")
    @Expose
    private String testTakenAt;
    @SerializedName("study_id")
    @Expose
    private Integer studyId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getAverageHeartRate() {
        return averageHeartRate;
    }

    public void setAverageHeartRate(Integer averageHeartRate) {
        this.averageHeartRate = averageHeartRate;
    }

    public Integer getMaxHeartRate() {
        return maxHeartRate;
    }

    public void setMaxHeartRate(Integer maxHeartRate) {
        this.maxHeartRate = maxHeartRate;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getStepsCount() {
        return stepsCount;
    }

    public void setStepsCount(Integer stepsCount) {
        this.stepsCount = stepsCount;
    }

    public String getDurationOfTest() {
        return durationOfTest;
    }

    public void setDurationOfTest(String durationOfTest) {
        this.durationOfTest = durationOfTest;
    }

    public String getTestTakenAt() {
        return testTakenAt;
    }

    public void setTestTakenAt(String testTakenAt) {
        this.testTakenAt = testTakenAt;
    }

    public Integer getStudyId() {
        return studyId;
    }

    public void setStudyId(Integer studyId) {
        this.studyId = studyId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
