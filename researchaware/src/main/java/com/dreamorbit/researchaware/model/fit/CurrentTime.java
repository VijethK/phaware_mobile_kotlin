package com.dreamorbit.researchaware.model.fit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mujasam.bn on 12/21/2017.
 */

public class CurrentTime {
    @SerializedName("currentTime")
    @Expose
    private String currentTime;

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

}

