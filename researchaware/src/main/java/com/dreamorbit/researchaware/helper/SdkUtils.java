package com.dreamorbit.researchaware.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SdkUtils {
    /**
     * Get the custom drawables from application
     *
     * @param context
     * @param resource_name
     * @return
     */
    public static Drawable getDrawable(Context context, String resource_name) {
        try {
            int resId = context.getResources().getIdentifier(resource_name, "drawable", context.getPackageName());
            if (resId != 0) {
                return context.getResources().getDrawable(resId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Snackbar utility method
     */
    public static void showSnackBar(View view, String msg, int duration) {
        Snackbar mySnackbar = Snackbar.make(view,
                msg, duration);

        mySnackbar.show();
    }

    /***
     * Create a unique app folder to store 6 minute walk test data/files
     * @param context
     * @return
     */

    public static File createUniqueAppFolder(Context context, boolean isSurvey) {
        String prefix = isSurvey ? SdkConstant.SURVEY_PREFIX : SdkConstant.TEST_PREFIX;
        String folderName = prefix + getAndroidId(context) + "_" + getCurrentDateTimeUploadFileName();
        boolean isCreated = false;
        String file = context.getFilesDir().getAbsolutePath() + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + folderName;
        File projDir = new File(file);
        if (!projDir.exists())
            isCreated = projDir.mkdirs();

        if (isCreated)
            return projDir;
        else
            return null;
    }

    public static File createUniqueFile(Context context) {
        String folderName = SdkConstant.TEST_PREFIX + getAndroidId(context) + "_" + getCurrentDateTimeUploadFileName();
        boolean isCreated = false;
        File dir = new File(context.getFilesDir().getAbsolutePath() + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME);
        if (!dir.exists())
            dir.mkdirs();

        File newFile = new File(dir, folderName);
        if (!newFile.exists())
            try {
                isCreated = newFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

        if (isCreated)
            return newFile;
        else
            return null;
    }


    public static File getPdfDirectory(SdkConstant.PDF_DIRECTORY directory, Context context) {
        if (directory.equals(directory.External)) {
            return getExternalPdfDirectory();
        } else if (directory.equals(directory.App)) {
            return getAppPdfDirectory(context);
        }
        return null;
    }

    private static File getAppPdfDirectory(Context context) {
        return new File(context.getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_PDF_DOCUMENTS);
    }

    private static File getExternalPdfDirectory() {
        return new File(Environment.getExternalStorageDirectory() + File.separator + SdkConstant.PH_PDF_DOCUMENTS);
    }

    public static File[] getAllDirectories(Context context) {
        File myDirectory = new File(context.getFilesDir().getAbsolutePath() + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME);
        if (!myDirectory.exists())
            myDirectory.mkdir();
        File[] files = myDirectory.listFiles();
        return files;
    }

    public static String getAndroidId(Context context) {
        String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        return androidId;
    }

    public static String getCurrentDateTime() {
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    public static String getCurrentUTCDateTime() {
        SimpleDateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss:SSSZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    public static String getCurrentUTCDateTimeWithMillSeconds() {
        SimpleDateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSSZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    public static String getCurrentDateTimeUploadFileName() {
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSS");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @return String representing date in specified format
     */
    public static String getDateTime(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat dateFormatterR = new SimpleDateFormat("hh:mm:ss");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return dateFormatterR.format(calendar.getTime());
    }

    public static void grantVibration(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);// Vibrate for 500 milliseconds
    }

    /**
     * As simple wrapper around Log.d
     */
    public static void LOGD(final String tag, String message) {
        if (Log.isLoggable(tag, Log.DEBUG)) {
            Log.d(tag, message);
        }
    }

    /**
     * Convert 24 hour format to 12 hour Date format to show in Report Adapter each rows
     * It will show the Test taken at data
     *
     * @param createdAt
     * @return
     */
    //yyyy-MM-dd'T'hh:mm:ssZ
    public static String serverTolocalDateTimeTodayReport(String createdAt) {
        String finalString = null;
        try {
            SimpleDateFormat thedate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            thedate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = thedate.parse(createdAt);

            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            newFormat.setTimeZone(TimeZone.getDefault());
            finalString = newFormat.format(date);

            return finalString;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalString;
    }


    /**
     * Get today's date mid night. Morning time of the today
     * To call the todays report api
     *
     * @return
     */
    public static String getTodaysDates() {
        Calendar date = new GregorianCalendar();
        DateFormat dateFormatterEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormatterEnd.format(date.getTime());
    }

    /**
     * Get last weeks
     * Startdate and Enddate of week
     *
     * @return
     */
    public static String[] getLastWeekDates() {
        String[] lastWeekDates = new String[2];

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - 1));
        DateFormat dateFormatterStart = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterStart.setTimeZone(TimeZone.getTimeZone("UTC"));
        lastWeekDates[0] = dateFormatterStart.format(cal.getTime());

        //last day of week
        cal.add(Calendar.DATE, 6);
        DateFormat dateFormatterEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
        lastWeekDates[1] = dateFormatterEnd.format(cal.getTime());

        return lastWeekDates;
    }

    /**
     * Get Startdate and Enddate of Month from current day
     *
     * @return
     */
    public static String[] getLastMonthsDates() {
        String[] lastMonthDates = new String[2];

        //Get first date of the month
        Calendar startDate = Calendar.getInstance();   // this takes current date
        startDate.set(Calendar.DAY_OF_MONTH, 1);

        //Last date of month
        Calendar endDate = Calendar.getInstance();
        int lastDate = endDate.getActualMaximum(Calendar.DATE);
        endDate.set(Calendar.DATE, lastDate);

        DateFormat dateFormatterStart = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterStart.setTimeZone(TimeZone.getTimeZone("UTC"));
        lastMonthDates[0] = dateFormatterStart.format(startDate.getTime());

        DateFormat dateFormatterEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
        lastMonthDates[1] = dateFormatterEnd.format(endDate.getTime());

        return lastMonthDates;
    }

    /**
     * Get date range format
     *
     * @param date
     * @return
     */
    public static String getRangeDate(Date date) {
        DateFormat dateFormatterEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = dateFormatterEnd.format(date);
        return utcTime;
    }

    /**
     * Test taken at date time 24 format upload
     * As per Karthik no need to send UTC format. he is handling it in backend
     *
     * @return
     */
    public static String getTestTakenDateTime() {
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = dateFormatterR.format(new Date());
        return utcTime;
    }
}
