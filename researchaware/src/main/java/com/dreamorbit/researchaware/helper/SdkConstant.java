package com.dreamorbit.researchaware.helper;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SdkConstant {

    //Bundle key Name to pass assent doc details
    public final static String CONSENT_ASSENT_DATA ="consent_assent_data";
    public static final String ARGUMENT = "argument" ;
    //Header Font
    public static final String APP_FONT = "ChicaGogoNF.ttf";
    public static final String POSITION = "mPosition";
    public static final String LEARN_MORE_TEXT = "learn_more" ;
    public static final String LEARN_MORE_TITLE = "learn_more_title" ;

    //Survey Activity screen
    public static final java.lang.String SURVERY_TITLE = "survey_title";
    public static final String IS_SURVEY_COMPLETED = "is_survey_completed";
    public static final String SURVERY_ID = "survey_id";
    public static final String TEST_PREFIX = "6MWT_" ;
    public static final String SURVEY_PREFIX = "Survey_" ;

    public static String KEY = "ASDFGHJKL";

    //Survey screen
    public static final String SURVEY_QUESTION_LIST = "survey_question_list";
    public static final String QUESTION = "question";
    public static final String ACTIVITY_TITLE = "activity_title";

    public static final String ROOT_APP_FOLDER_NAME = "Phaware";
    public static final String PH_HTML_DOCUMENTS = "Ph_html";
    public static final String PH_PDF_DOCUMENTS = "Ph_PDF";

    //Emergency call
    public static final int REQUEST_CALL_PHONE = 100;
    public static final long EMERGENCY_NUMBER = 911;//919986790;

    public enum PDF_DIRECTORY{
        External,
        App
    }
}
