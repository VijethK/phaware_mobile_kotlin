package com.dreamorbit.researchaware.screentype.screens.test;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.dreamorbit.researchaware.api.WeatherApi;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.fit.location.Coordinate;
import com.dreamorbit.researchaware.model.fit.location.LocationData;
import com.dreamorbit.researchaware.model.fit.location.LocationItem;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by nareshkumar.reddy on 9/27/2017.
 * Updated by Mujasam on 07/03/2019.
 */

public class SdkLocationHelper {

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private Location mCurrentLocation;
    private LocationManager locationManager;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    /**
     * Provides access to the Location Settings API.
     */
    private SettingsClient mSettingsClient;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    private Boolean mRequestingLocationUpdates;
    private Activity activity;

    //Storing location details
    private LocationData locationData = new LocationData();

    public SdkLocationHelper(Activity activity) {
        this.activity = activity;
        // Kick off the process of building the LocationCallback, LocationRequest, and
        // LocationSettingsRequest objects.
        initLocationManager();
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    private void initLocationManager() {
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        mSettingsClient = LocationServices.getSettingsClient(activity);
        mRequestingLocationUpdates = false;
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                SdkSharedPrefSingleton.getInstance().saveLatitude(String.valueOf(mCurrentLocation.getLatitude()));
                SdkSharedPrefSingleton.getInstance().saveLongitude(String.valueOf(mCurrentLocation.getLongitude()));

                //Weather Api call if Weather is not fetched yet.
                String weatherData = SdkSharedPrefSingleton.getInstance().getWeatherData();
                if (TextUtils.isEmpty(weatherData)) {
                    Log.e("Weather Null: ","Calling Api");
                    new WeatherApi().getLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                }else{
                    Log.e("Weather Exist: ","Not Calling");
                }

                if (activity instanceof SurveyActivity) {
                    LocationItem item = new LocationItem();
                    Coordinate coordinate = new Coordinate();
                    coordinate.setLatitude(mCurrentLocation.getLatitude());
                    coordinate.setLongitude(mCurrentLocation.getLongitude());
                    item.setCoordinate(coordinate);
                    item.setAltitude(mCurrentLocation.getAltitude());
                    item.setHorizontalAccuracy(mCurrentLocation.getAccuracy());
                    item.setSpeed(mCurrentLocation.getSpeed());
                    item.setCourse(0.0);
                    item.setVerticalAccuracy(0);
                    item.setTimestamp(SdkUtils.getCurrentUTCDateTimeWithMillSeconds());
                    locationData.addItems(item);
                   /* Toast.makeText(activity, "Latitude: " + mCurrentLocation.getAltitude() + " Longitude: " + mCurrentLocation.getLongitude() +
                            "\n vertcal: " + mCurrentLocation.getAccuracy() + " horizontal: " + mCurrentLocation.getAccuracy() +
                            "\n speed: " + mCurrentLocation.getSpeed() + " altitude: " + mCurrentLocation.getAltitude()
                            + "\n date: " + dateFormatter.format(currentDate), Toast.LENGTH_SHORT).show();*/
                }
            }
        };
    }

    public LocationData getLocationDataOf6minWalk() {
        return locationData;
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    public void startLocationUpdates() {
        mRequestingLocationUpdates = true;
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                    }
                });
    }

    protected void stopLocationUpdates(Activity activity) {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
        Log.d("", "Location update stopped .......................");
    }

}
