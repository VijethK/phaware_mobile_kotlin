package com.dreamorbit.researchaware.screentype.screens.survey;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.survey.Options;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.test.WalkTestFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SurveyActivity extends AppCompatActivity implements OnNextListener, OnBackPressListener {

    public static int WALK_TEST_RESULT_CODE = 111;

    private static double mMeter;
    private static int mAvgHeartRate;
    private static int mMaxHeartRate;
    private static String mTestDuration;
    private static long totalStepsCount;

    protected final int REQUEST_PERMISSION_SETTING = 34;
    protected final int REQUEST_OAUTH_REQUEST_CODE = 1;
    private SurveyFlowFragment mSurveyFlowFragment;
    private List<Questions> mRootQuestionList;
    private String activityTitle;
    private Bundle mSavedInatsnce;
    private AlertDialog mAlertDialog = null;

    public static double getDistanceInMeter() {
        return mMeter;
    }

    public static void setDistanceInMeter(double meter) {
        mMeter = meter;
    }

    public static int getAvgHeartRate() {
        return mAvgHeartRate;
    }

    public static void setAvgHeartRate(int avgHeartRate) {
        mAvgHeartRate = avgHeartRate;
    }

    public static int getMaxHeartRate() {
        return mMaxHeartRate;
    }

    public static void setMaxHeartRate(int maxHeartRate) {
        mMaxHeartRate = maxHeartRate;
    }

    public static String getTestDuration() {
        return mTestDuration;
    }

    public static void setTestDuration(String testDuration) {
        mTestDuration = testDuration;
    }

    public static long getTotalStepsCount() {
        return totalStepsCount;
    }

    public static void setTotalStepsCount(long stepsCount) {
        totalStepsCount = stepsCount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mSavedInatsnce = savedInstanceState;
        if (mSavedInatsnce == null) {
            // withholding the previously created fragment from being created again
            // On orientation change, it will prevent fragment recreation
            // its necessary to reserving the fragment stack inside each tab
            mRootQuestionList = getSurveyQuestion();
            if (mRootQuestionList == null) {
                finish();
            }
            initScreen();
        } else {
            // restoring the previously created fragment
            // and getting the reference
            mSurveyFlowFragment = (SurveyFlowFragment) getSupportFragmentManager().getFragments().get(0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initScreen() {
        // Creating the ViewPager container fragment once
        mSurveyFlowFragment = new SurveyFlowFragment(mRootQuestionList, activityTitle);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mSurveyFlowFragment)
                .commit();
    }

    /**
     * @return
     */
    private List<Questions> getSurveyQuestion() {

        activityTitle = getIntent().getStringExtra(SdkConstant.ACTIVITY_TITLE);
        String surveyQuestion = getIntent().getStringExtra(SdkConstant.SURVEY_QUESTION_LIST);
        Type listType = new TypeToken<List<Questions>>() {
        }.getType();
        Gson gson = new Gson();
        List<Questions> screenDataRoot = gson.fromJson(surveyQuestion, listType);
        if (screenDataRoot.size() == 0) {
            finish();
            return null;
        }

        //check if activity is "Quality of life Survey"
        //Add medication confirmation object to the existing list
        if (!TextUtils.isEmpty(activityTitle) && activityTitle.equals(getString(R.string.quality_of_life))) {
            Questions questions = new Questions();
            questions.setQuestion(getString(R.string.medication_alert));

            List<Options> options = new ArrayList();
            options.add(new Options(1, "Yes", null));
            options.add(new Options(2, "No", null));

            questions.setOptions(options);
            screenDataRoot.add(screenDataRoot.size() - 1, questions); //add this object in last but one position.
        }

        return screenDataRoot;
    }

    /**
     * Hide keyboard global method
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isActive() && imm.isAcceptingText()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    @Override
    public void onBackPressed() {
        backNavigation();
    }

    @Override
    public void onNextPressed(int position) {
        mSurveyFlowFragment.mPager.setCurrentItem(position + 1);
    }

    @Override
    public void onFragmentBackPressed(int position) {
        backNavigation();
    }

    /**
     * Navigation back implementation depend on the
     * if 6 minute walk test dont allow to go back once Walk test has beens started
     */
    private void backNavigation() {
        if (mSurveyFlowFragment.mPager.getCurrentItem() != 0) {
            if (activityTitle.equalsIgnoreCase("6 Minute Walk Test") && mSurveyFlowFragment.mPager.getCurrentItem() == 5) { //dont show back button if walk test screen and in further survey question's screen.
                mSurveyFlowFragment.mPager.setCurrentItem(mSurveyFlowFragment.mPager.getCurrentItem() - 1, false);
            } else if (activityTitle.equalsIgnoreCase("6 Minute Walk Test") && mSurveyFlowFragment.mPager.getCurrentItem() > 2) {
                return;
            } else {
                mSurveyFlowFragment.mPager.setCurrentItem(mSurveyFlowFragment.mPager.getCurrentItem() - 1, false);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PERMISSION_SETTING) {
                if (!hasRuntimePermissions()) {
                    showAlertDialog(this, "Location Permission",
                            "Location permission is not enabled.\n\n" +
                                    "Please goto application setting and enable the permission to start 6 Minute Walk Test.");
                } else {
                    Fragment fragment = mSurveyFlowFragment.mAdapter.getRegisteredFragment(3);
                    if (fragment != null) {
                        ((WalkTestFragment) fragment).startWalkTest();
                    }
                }
            } else {
                Fragment fragment = mSurveyFlowFragment.mAdapter.getRegisteredFragment(3);
                if (fragment != null) {
                    ((WalkTestFragment) fragment).startWalkTest();
                }
            }
        }
    }

    /**
     * /*
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("Testing", "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (!hasRuntimePermissions()) {
                showAlertDialog(this, "Location Permission",
                        "Location permission is not enabled.\n\n" +
                                "Please goto application setting and enable the permission to start 6 Minute Walk Test.");
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                /*Fragment fragment = mSurveyFlowFragment.mAdapter.getRegisteredFragment(3);
                if (fragment != null) {
                    ((WalkTestFragment) fragment).startWalkTest();
                }*/
            }
        } else {
            //super.onRequestPermissionsResult(requestCode,permissions,grantResults);
            showAlertDialog(this, "Location Permission",
                    "Location permission is not enabled.\n\n" +
                            "Please goto application setting and enable the permission to start 6 Minute Walk Test.");
        }
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private boolean hasRuntimePermissions() {
        int permissionState =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void showAlertDialog(Context context, String title, String message) {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            return; //already showing
        } else if (mAlertDialog != null) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
        mAlertDialog = new AlertDialog.Builder(context).create();
        mAlertDialog.setCancelable(false);
        // Setting Dialog Title
        mAlertDialog.setTitle(title);

        // Setting Dialog Message
        mAlertDialog.setMessage(message);

        // Setting OK Button
        mAlertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        launchPermissionSetting();
                        mAlertDialog = null;
                    }
                });

        // Showing Alert Message
        mAlertDialog.show();
    }

    private void launchPermissionSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (activityTitle.equalsIgnoreCase("6 Minute Walk Test") && !hasRuntimePermissions()) {
            requestRuntimePermissions();
        }
    }

    private void requestRuntimePermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(
                        this, Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Snackbar.make(
                    findViewById(R.id.container),
                    "Enable location permission to start 6 Minute Walk Test.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(
                            R.string.cast_tracks_chooser_dialog_ok,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Request permission
                                    ActivityCompat.requestPermissions(
                                            SurveyActivity.this,
                                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                            REQUEST_PERMISSION_SETTING);
                                }
                            })
                    .show();
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSION_SETTING);
        }
    }
}
