package com.dreamorbit.researchaware.screentype.screens.survey.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.survey.Options;
import com.dreamorbit.researchaware.model.survey.Questions;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionViewHolder> {

    //Creating an arraylist of POJO objects
    private Questions mQuestions;
    private final LayoutInflater inflater;
    private View view;
    private QuestionViewHolder holder;
    private Context context;
    public int mSelectedItem = -1;


    public QuestionAdapter(Context context, Questions question) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        mQuestions = question;
    }

    //This method inflates view present in the RecyclerView
    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = inflater.inflate(R.layout.row_item_questions, parent, false);
        holder = new QuestionViewHolder(view, this, mQuestions.getOptions());
        return holder;
    }

    //Binding the data using get() method of POJO object
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final QuestionViewHolder holder, int position) {
        Options option = mQuestions.getOptions().get(position);
        holder.tvOption.setText(option.getName());
        holder.imgOption.setImageDrawable(SdkUtils.getDrawable(context, option.getImage()));
        holder.radio.setChecked(position == mSelectedItem);
        int color = position == mSelectedItem ? ContextCompat.getColor(context,R.color.colorAccent): ContextCompat.getColor(context,R.color.black);
        int typeface = position == mSelectedItem ? Typeface.BOLD: Typeface.NORMAL;
        holder.tvOption.setTypeface(null,typeface);
        holder.tvOption.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return mQuestions.getOptions() == null ? 0 : mQuestions.getOptions().size();
    }
}
