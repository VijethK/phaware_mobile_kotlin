package com.dreamorbit.researchaware.screentype.screens.survey.adapter;

import android.view.View;

/**
 * Created by mujasam.bn on 10/31/2017.
 */
public interface ClickListeners {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}