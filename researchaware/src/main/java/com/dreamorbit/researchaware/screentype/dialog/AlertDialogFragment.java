package com.dreamorbit.researchaware.screentype.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ReviewFragment;

/**
 * Created by mujasam.bn on 10/5/2017.
 */

public class AlertDialogFragment extends DialogFragment {

    private AlertDialogListener mListener;
    public void setDialogFragmentEvents(ReviewFragment dialogFragmentEvents) {
        this.mListener = dialogFragmentEvents;
    }

    // Defines the mListener interface
    public interface AlertDialogListener {
        void onAgreeDialog();
    }

    public AlertDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    public static AlertDialogFragment newInstance(String title) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        //alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(R.string.assentAgree);
        alertDialogBuilder.setPositiveButton("Agree",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onAgreeDialog();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

        });

        return alertDialogBuilder.create();
    }
}