package com.dreamorbit.researchaware.screentype.screens.survey;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.Answers;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by mujasam.bn on 11/6/2017.
 */

public class SurveyBaseDialogFragment extends DialogFragment {

    /**
     * Save answers in json format {"answer" :"Yes", "answer":"no"}
     */
    //Declare the Adapter, AecyclerView and our custom ArrayList
    protected TextView tvQuestion;
    protected List<Questions> mQuestionList;
    protected int mPosition;
    protected String mActivityTitle;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionList = getArguments().getParcelableArrayList(SdkConstant.QUESTION);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(SdkConstant.POSITION);
    }

    protected void saveAnswerJsonInAppFolder(String walkTestReasonString) {


        List<Answers> answersList = new ArrayList<>();
        Iterator<Questions> itr = mQuestionList.iterator();

        while (itr.hasNext()) {
            Questions question = itr.next();
            if (TextUtils.isEmpty(question.getQuestion())) {
                continue;
            } else {
                Answers answer = new Answers(); //root
                answer.setQuestions(question.getQuestion());
                List<String> answers = new ArrayList<>();
                answers.add(question.getAnswer());
                answer.setAnswers(answers);
                answer.setDate(question.getCurrentTime());
                answersList.add(answer);
            }
        }

        Type listType = new TypeToken<List<Answers>>() {
        }.getType();
        Gson gson = new Gson();
        String answerJason = gson.toJson(answersList, listType);

        //check if survey completed on 6 minute walk test then check for allready saved folder path
        //And save SurveyAnswers in same folder which allready contains pedometer,stepcount etc..
        File uniqueAppFolder;
        String folderPathWalkTest = SdkSharedPrefSingleton.getInstance().getWalkTestFolderPath();
        if (TextUtils.isEmpty(folderPathWalkTest)) {
            uniqueAppFolder = SdkUtils.createUniqueAppFolder(getActivity(), true);
        } else {
            uniqueAppFolder = new File(folderPathWalkTest);
        }
        //save this json in app folder
        saveProcess(uniqueAppFolder, answerJason, "SurveyResponse.json");

        //save the walk test stop reason in json and send it to backend
        if (!TextUtils.isEmpty(walkTestReasonString))
            saveProcess(uniqueAppFolder, walkTestReasonString, "discardReason.json");
    }

    protected void saveProcess(File uniqueAppFolder, String answerJason, String jsonName) {

        File fileName = new File(uniqueAppFolder, jsonName);
        try {
            FileWriter fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(answerJason);
            bw.close();
            Log.e("Saved: ", answerJason);
            Log.e("Save Path ", uniqueAppFolder.toString());
            SdkSharedPrefSingleton.getInstance().saveWalkTestFolderPath(null);

            String folderPathWalkTest = SdkSharedPrefSingleton.getInstance().getWalkTestFolderPath();
            Log.e("Save Path ", folderPathWalkTest);
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreen() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, true);
        i.putExtra(SdkConstant.SURVERY_ID, mQuestionList.get(0).getActivityId());
        getActivity().setResult(RESULT_OK, i);
        getActivity().finish();
    }

    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreenCancelled() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, false);
        i.putExtra(SdkConstant.SURVERY_ID, -1);
        getActivity().setResult(RESULT_CANCELED, i);
        getActivity().finish();
    }
}
