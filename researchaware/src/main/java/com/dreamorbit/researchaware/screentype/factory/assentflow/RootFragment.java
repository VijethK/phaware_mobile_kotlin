package com.dreamorbit.researchaware.screentype.factory.assentflow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.ScreenType;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ViewTaskActivity;

import static android.app.Activity.RESULT_OK;

public abstract class RootFragment extends Fragment {

    //Data to populate in screen
    protected int mPosition;
    protected Section mRoot;

    //listener for navigations next and back
    protected OnNextListener mIOnNextListener;
    protected OnBackPressListener mIOnBackPressListener;

    //toolbar_consent views
    protected Toolbar mToolbar;
    protected TextView mTvTitle;
    protected TextView mTvCancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(SdkConstant.POSITION, 0);
        mRoot = getArguments().getParcelable(SdkConstant.CONSENT_ASSENT_DATA);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((ViewTaskActivity) getActivity());
        mIOnBackPressListener = ((ViewTaskActivity) getActivity());
    }

    protected void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTvTitle.setText(mRoot == null ? "NA" : mRoot.getTitle());
    }


    /**
     * Overriding metod to perform ReviewScreen action Visual info type
     * #1 toolbar back navigation action
     * #2 toolbar Cancel action
     */

    protected void initNavigationListeners() {
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnBackPressListener.onFragmentBackPressed(mPosition);
            }
        });
        mTvCancel.setOnClickListener(view -> {
            if(mRoot.getTitle().equalsIgnoreCase(String.valueOf(ScreenType.REVIEW)) || mRoot.getTitle().equalsIgnoreCase(getString(R.string.signature))){
                Alert();
            }else {
                closeActivity();
            }
        });
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    protected void initNavigationListeners(final View rootView) {
        rootView.findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnNextListener.onNextPressed(mPosition);
            }
        });
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnBackPressListener.onFragmentBackPressed(mPosition);
            }
        });
        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mRoot.getTitle().equalsIgnoreCase(String.valueOf(ScreenType.REVIEW)) || mRoot.getTitle().equalsIgnoreCase(getString(R.string.signature))){
                    Alert();
                }else {
                    closeActivity();
                }
            }
        });
    }

    private void Alert() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.do_end_task).setPositiveButton(R.string.end_task, (dialog, which) -> {
           closeActivity();
        })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                })
                .setCancelable(false)
                .show();
    }

    private void closeActivity(){
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra("TAG", false);
        getActivity().setResult(RESULT_OK, i);
        getActivity().finish();
    }


    /**
     * Overriding metod to perform Signature Screen action
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #3 screen Done button action
     *
     * @param rootView
     */
    protected void initNavigationListeners(View rootView, ScreenType type) {
        switch (type) {
            case SIGNATURE_SCREEN:
            case USER_INFO_SCREEN:
                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mIOnBackPressListener.onFragmentBackPressed(mPosition);
                    }
                });
                mTvCancel.setOnClickListener(v -> {
                    if(mRoot.getTitle().equalsIgnoreCase(String.valueOf(ScreenType.REVIEW)) || mRoot.getTitle().equalsIgnoreCase(getString(R.string.signature))){
                        Alert();
                    }else {
                        closeActivity();
                    }
                });
                break;
        }
    }
}

