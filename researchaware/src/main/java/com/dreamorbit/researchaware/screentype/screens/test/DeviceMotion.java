package com.dreamorbit.researchaware.screentype.screens.test;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.fit.devicemotion.Gravity;
import com.dreamorbit.researchaware.model.fit.devicemotion.GyroResponse;
import com.dreamorbit.researchaware.model.fit.devicemotion.Item;
import com.dreamorbit.researchaware.model.fit.devicemotion.MagneticField;
import com.dreamorbit.researchaware.model.fit.devicemotion.RotationRate;
import com.dreamorbit.researchaware.model.fit.devicemotion.UserAcceleration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class DeviceMotion {

    SensorManager sensorManager;
    private ArrayList<Sensor> sensors = new ArrayList<>();
    GyroResponse gyroResponse;

    public DeviceMotion(Context context) {

        gyroResponse = new GyroResponse();

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
    }

    public void readDeviceMotion() {
        for (Sensor sensor : sensors) {
            sensorManager.registerListener(gyroListener, sensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void stopDeviceMotion() {
        sensorManager.unregisterListener(gyroListener);
    }

    public GyroResponse getDeviceMotionData() {
        if(gyroResponse.getItems().size() > 1000){
            List<Item> list = gyroResponse.getItems().subList(0, 1000);
            gyroResponse.setItems(list);
        }
        return gyroResponse;
    }

    public SensorEventListener gyroListener = new SensorEventListener() {

        private MagneticField magneticField = null;
        private Gravity gravity = null;
        private UserAcceleration userAcceleration = null;
        private RotationRate rotationRate = null;

        public void onAccuracyChanged(Sensor sensor, int acc) {

        }

        public void onSensorChanged(SensorEvent event) {
            Sensor sensor = event.sensor;
            if (sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                rotationRate = new RotationRate();
                rotationRate.setX(event.values[0]);
                rotationRate.setY(event.values[1]);
                rotationRate.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                userAcceleration = new UserAcceleration();
                userAcceleration.setX(event.values[0]);
                userAcceleration.setY(event.values[1]);
                userAcceleration.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_GRAVITY) {
                gravity = new Gravity();
                gravity.setX(event.values[0]);
                gravity.setY(event.values[1]);
                gravity.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                magneticField = new MagneticField();
                magneticField.setX(event.values[0]);
                magneticField.setY(event.values[1]);
                magneticField.setZ(event.values[2]);

            }

            if (magneticField != null && gravity != null && userAcceleration != null && rotationRate != null) {

                Item item = new Item();
                item.setTimestamp(SdkUtils.getCurrentUTCDateTimeWithMillSeconds());
                item.setGravity(gravity);
                item.setMagneticField(magneticField);
                item.setRotationRate(rotationRate);
                item.setUserAcceleration(userAcceleration);

                gyroResponse.addItems(item);


/*
                //TODO: Remove after tested
                Log.i("Time", "" + dateFormatter.format(currentDate));

                Log.i("MF", "" + magneticField.getX());
                Log.i("MF", "" + magneticField.getY());
                Log.i("MF", "" + magneticField.getZ());

                Log.i("UA", "" + userAcceleration.getX());
                Log.i("UA", "" + userAcceleration.getY());
                Log.i("UA", "" + userAcceleration.getZ());

                Log.i("GY", "" + gravity.getX());
                Log.i("GY", "" + gravity.getY());
                Log.i("GY", "" + gravity.getZ());

                Log.i("RV", "" + rotationRate.getX());
                Log.i("RV", "" + rotationRate.getY());
                Log.i("RV", "" + rotationRate.getZ());*/
            }


        }
    };

}
