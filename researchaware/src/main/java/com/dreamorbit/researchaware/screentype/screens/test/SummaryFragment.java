package com.dreamorbit.researchaware.screentype.screens.test;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyBaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment extends SurveyBaseFragment {

    protected TextView mTvTitle;
    protected TextView mTvDone;
    private TextView distanceText;
    private Toolbar mToolbar;

    public SummaryFragment() {
        // Required empty public constructor
    }

    public static SummaryFragment newInstance(List<Questions> question, int position, ViewPager viewPager, String activityTitle) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(SdkConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fit_summary, container, false);
        initToolBar(view);
        distanceText = view.findViewById(R.id.tv_distance_walk);
        return view;
    }


    public void updateDistance() {
        distanceText.setText(String.format("%.0f", ((SurveyActivity) getActivity()).getDistanceInMeter()) + " meters");
    }

    protected void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvDone = mToolbar.findViewById(R.id.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText(" " + getString(R.string.summary));
        mTvDone.setText(R.string.done);

        mTvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToActivityScreen();
            }
        });
    }
}
