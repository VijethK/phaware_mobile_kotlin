package com.dreamorbit.researchaware.screentype.screens.assentconsent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.screentype.dialog.AlertDialogFragment;
import com.dreamorbit.researchaware.screentype.factory.assentflow.RootFragment;

import java.io.File;

public class ReviewFragment extends RootFragment implements AlertDialogFragment.AlertDialogListener {

    //Layout views
    private WebView mWebView;

    public ReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static ReviewFragment newInstance(Section assentData, int position) {
        ReviewFragment reviewFragment = new ReviewFragment();
        Bundle args = new Bundle();
        args.putParcelable(SdkConstant.CONSENT_ASSENT_DATA, assentData);
        args.putInt(SdkConstant.POSITION, position);
        reviewFragment.setArguments(args);
        return  reviewFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_review, container, false);

        initViews(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);

        return rootView;
    }

    private void initViews(View rootView) {
        mWebView = rootView.findViewById(R.id.webView);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        File directory = new File(getActivity().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS);
        File file  = new File(directory,  mRoot.getHtmlContent());
        mWebView.loadUrl("file:///"+ file);
    }

    @Override
    protected void initNavigationListeners(View rootView) {
        super.initNavigationListeners();
        rootView.findViewById(R.id.btnAgree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });
        rootView.findViewById(R.id.btnDisAgree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    /**
     * Showing AlertDialogFragment to confirm Agree or DisAgree
     * Initializing the AlertDialogFragments interface callback
     */
    private void showAlertDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        AlertDialogFragment alertDialog = AlertDialogFragment.newInstance("Some title");
        alertDialog.setCancelable(false);
        alertDialog.setDialogFragmentEvents(ReviewFragment.this);
        alertDialog.show(fm, "fragment_alert");
    }

    @Override
    public void onAgreeDialog() {
        mIOnNextListener.onNextPressed(mPosition);
    }
}

