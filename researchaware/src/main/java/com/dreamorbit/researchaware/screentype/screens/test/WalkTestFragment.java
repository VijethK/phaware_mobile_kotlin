package com.dreamorbit.researchaware.screentype.screens.test;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.SessionInsertRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.IPedometerCallback;
import com.dreamorbit.pedometer.SensorChecker;
import com.dreamorbit.pedometer.accelerometer.AcceleroMeterService;
import com.dreamorbit.pedometer.accelerometer.PedometerSettings;
import com.dreamorbit.pedometer.accelerometer.PedometerUtils;
import com.dreamorbit.pedometer.stepsensor.StepSensorService;
import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.fit.CurrentTime;
import com.dreamorbit.researchaware.model.fit.TotalStepCount;
import com.dreamorbit.researchaware.model.fit.devicemotion.GyroResponse;
import com.dreamorbit.researchaware.model.fit.location.LocationData;
import com.dreamorbit.pedometer.pojo.pedometer.Item;
import com.dreamorbit.pedometer.pojo.pedometer.Pedometer;
import com.dreamorbit.researchaware.model.report.PostReportRequest;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.BaseFragment;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity;
import com.dreamorbit.researchaware.utilities.PermissionsUtility;
import com.dreamorbit.researchaware.utilities.ResearchUtility;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class WalkTestFragment extends BaseFragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "WalkTestFragment";
    private static final int STEPS_MSG = 1;
    private static final int DISTANCE_MSG = 2;
    private static final int HEART_RATE_MSG = 3;
    protected final String AUTH_PENDING = "auth_state_pending";

    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private Button mTvEmergencyCall;

    //Timer
    private CountDownTimer mCountDownTimer;
    private long remainingTime;
    private int minute = 6;
    private int seconds = 60000;

    //Views
    private Button btnStop;
    private TextView tvTimer;
    private TextView tvDistance;
    private boolean authInProgress = false;

    //Data to populate in screen
    private int mPosition;
    private String mActivityTitle;

    //Location Fetch
    private SdkLocationHelper mLocationHelper;

    //DeviceMotion
    private DeviceMotion mDeviceMotion;
    private List<Questions> mQuestionList;

    //walk test values
    private double mFinalDistance = 0;

    //listener for navigations next and back
    private OnNextListener mIOnNextListener;

    //Pedometer
    private Pedometer mPedometer = new Pedometer();
    private List<Item> mPedoItems = new ArrayList<>();

    //Step Count
    private boolean isCounterFinished;

    //Pedometer
    private PedometerUtils mUtils;
    private PedometerSettings mPedometerSettings;
    private boolean mIsRunning;
    private long startTime, stopTime;
    private String lastFetchedDate = "";
    private GoogleApiClient mGoogleApiClient;
    private long mTotalStepForDay;

    private Class<?> activeService;
    private SensorChecker.SensorType nSensorType;
    private BaseSensorService mBaseService;
    private int mFinalSteps;

    //Handler - update
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STEPS_MSG:
                    int mStepValue = msg.arg1;
                    mFinalSteps = mStepValue;
                    break;

                case DISTANCE_MSG:
                    Float mDistanceValue = Float.parseFloat(msg.obj.toString());
                    if (mDistanceValue <= 0) {
                        tvDistance.setText(mDistanceValue + "(m)");
                    } else {
                        String finalDistance = String.format("%.0f", mDistanceValue);
                        mFinalDistance = Double.parseDouble(finalDistance);
                        tvDistance.setText(finalDistance + "(m)");
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    };

    // TODO: unite all into 1 type of message
    private IPedometerCallback mCallback = new IPedometerCallback() {
        @Override
        public void stepsChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }

        @Override
        public void distanceChanged(float value) {
            Message msg = new Message();
            msg.what = DISTANCE_MSG;
            msg.obj = value;
            mHandler.sendMessage(msg);
            //mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG, (int) (value), 0));
        }

        @Override
        public void onHearBeatValueChanged(int hearRate) {
            mHandler.sendMessage(mHandler.obtainMessage(HEART_RATE_MSG, hearRate, 0));
        }

        @Override
        public void onDistanceListFetched(List<Item> items) {
            mPedoItems = items;
            new FetchTotalStepsAsync().execute();
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBaseService = ((BaseSensorService.StepBinder) service).getService();
            mBaseService.registerCallback(mCallback);
            //mBaseService.reloadSettings();
        }

        public void onServiceDisconnected(ComponentName className) {
            if (mBaseService != null) {
                mBaseService.unregisterListener();
            }
            mBaseService = null;
        }
    };


    //region Views Starts here
    public static WalkTestFragment newInstance(List<Questions> question, int position, ViewPager viewPager, String activityTitle) {
        WalkTestFragment fragment = new WalkTestFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(SdkConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(SdkConstant.POSITION, 0);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mQuestionList = getArguments().getParcelableArrayList(SdkConstant.QUESTION);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_walktest, container, false);
        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        } else {
            initToolBar(rootView);
            initViews(rootView);
            mUtils = PedometerUtils.getInstance();
        }
        return rootView;
    }

    /**
     * Init toolbar and ncavigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        mTvCancel.setVisibility(View.GONE);
        mTvTitle.setText("  6 Minute Walk Test");
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
    }

    private void initViews(View rootView) {
        tvDistance = rootView.findViewById(R.id.tvDistance);
        tvTimer = rootView.findViewById(R.id.tvTimer);
        btnStop = rootView.findViewById(R.id.btnStop);
        mTvEmergencyCall = rootView.findViewById(R.id.tvEmergencyCall);

        btnStop.setOnClickListener(this);
        mTvEmergencyCall.setOnClickListener(this);
    }

    //endregion Views Stops Here

    //region Click Events

    /**
     * Show dialog for Complete fragment
     * List<Questions> question, int position, String activityTitle
     */
    private void showActivityConfirmDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        StopFragment surveyConfirmDialogFragment = StopFragment.newInstance(mQuestionList, mPosition, mActivityTitle);
        surveyConfirmDialogFragment.show(fm, "ACTIVITY_STOP_FRAGMENT");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnStop) {
            SdkSharedPrefSingleton.getInstance().saveWalkTestActive(false);
            showActivityConfirmDialog();
            stopAllListeners();
        } else if (v.getId() == R.id.tvEmergencyCall) {
            ResearchUtility.permissionCheckEmergecnyCall(getActivity());
        }
    }

    /**
     * Check Call phone permission
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionsUtility.PERMISSIONS_REQUEST) {
            for (int result : grantResults) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                    ResearchUtility.triggerCall(getActivity());
                } else {
                    ResearchUtility.permissionCheckEmergecnyCall(getActivity());
                }
            }
        }
    }

    //endregion Click Events

    //region Walk Test implementation

    public void startWalkTest() {
        initLocationHelper();
        initDeviceMotion();
        startPedometerService();
    }

    private long getStartTime() {
        Calendar mCalendar = Calendar.getInstance();
        Date now = new Date();
        mCalendar.setTime(now);
        startTime = mCalendar.getTimeInMillis();
        Log.e("6MWT Start Time", " " + startTime);
        return startTime;
    }

    private long getStopTime() {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        stopTime = cal.getTimeInMillis();
        return stopTime;
    }

    private void startPedometerService() {
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPedometerSettings = new PedometerSettings(mSettings);
        mIsRunning = mPedometerSettings.isServiceRunning();

        // Start the service if this is considered to be an application start (last onPause was long ago)
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Started First time");
            checkSensor();
            startStepService();
            bindStepService();
            initFitApiClient();
            getStartTime();
            startTimer();
        } else if (mIsRunning) {
            Log.i(TAG, "[SERVICE] Running already");
            bindStepService();
        }
    }

    private void checkSensor() {
        SensorChecker sensorChecker = new SensorChecker(getActivity());
        nSensorType = sensorChecker.getPedometerSensor();
        switch (nSensorType) {
            case ACCELEROMETER:
                activeService = AcceleroMeterService.class;
                break;
            case STEP:
                activeService = StepSensorService.class;
                break;
        }
    }

    private void startStepService() {
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Start");
            mLocationHelper.startLocationUpdates(); // start the location fetch task
            mDeviceMotion.readDeviceMotion(); // start device motion
            mIsRunning = true;
            getActivity().startService(new Intent(getActivity(), activeService));
        }
    }

    private void bindStepService() {
        Log.i(TAG, "[SERVICE] Bind");
        getActivity().bindService(new Intent(getActivity(), activeService), mConnection, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }

    private void stopStepService() {
        Log.i(TAG, "[SERVICE] Stop");
        if (mBaseService != null) {
            Log.i(TAG, "[SERVICE] stopService");
            //mBaseService.resetValues();
            unbindStepService();
            getActivity().stopService(new Intent(getActivity(), activeService));
        }
        mIsRunning = false;
        mPedometerSettings.saveServiceRunningWithTimestamp(mIsRunning);
    }

    private void unbindStepService() {
        Log.i(TAG, "[SERVICE] Unbind");
        getActivity().unbindService(mConnection);
    }

    private void initDeviceMotion() {
        mDeviceMotion = new DeviceMotion(getActivity());
    }

    private void initLocationHelper() {
        mLocationHelper = new SdkLocationHelper(getActivity());
    }

    /*
     *  Step Count Subscription and Reading the Data
     */
    protected void startTimer() {

        mCountDownTimer = new CountDownTimer(minute * seconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                remainingTime = millisUntilFinished;
                SdkSharedPrefSingleton.getInstance().saveWalkTestActive(true);
                long secondsLimit = millisUntilFinished / 1000;
                String minute = String.format("%02d", secondsLimit / 60) + ":" + String.format("%02d", secondsLimit % 60);

                Log.e("Time ", minute);
                tvTimer.setText(minute);
            }

            @Override
            public void onFinish() {
                isCounterFinished = true;
                stopAllListeners();
            }
        };

        mCountDownTimer.start();
    }

    private void insertDataToGoogleFit(long step) {
        // Create a data source
        DataSource dataSource =
                new DataSource.Builder()
                        .setAppPackageName(getActivity())
                        .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .setStreamName("Steps")
                        .setType(DataSource.TYPE_RAW)
                        .build();

        // Create a data set
        DataSet dataSet = DataSet.create(dataSource);
        // For each data point, specify a start time, end time, and the data value -- in this case,
        // the number of new steps.
        DataPoint dataPoint =
                dataSet.createDataPoint().setTimeInterval(startTime, getStopTime(), TimeUnit.MILLISECONDS);
        dataPoint.getValue(Field.FIELD_STEPS).setInt((int) step);
        dataSet.add(dataPoint);
        Task response = Fitness.getHistoryClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity())).insertData(dataSet);
        Log.e(TAG, "**Mobile Step Pushed to Fit**");
    }
    //endregion Walk Test

    //region Save Walk Test Data In Json

    /*
     *Save jsons in folder and keep this folder name in golbal to to store SurveyAnswer.json in same folder
     */
    private void saveWalkTestData(String[] data) {

        File uniqueAppFolder = SdkUtils.createUniqueAppFolder(getActivity(), false);
        SdkSharedPrefSingleton.getInstance().saveWalkTestFolderPath(uniqueAppFolder.toString());
        int i = 0;
        for (String content : data) {
            ++i;
            saveProcess(uniqueAppFolder, content, i);
        }
    }

    protected Boolean saveProcess(File uniqueAppFolder, String answerJason, int pos) {

        String fileName = "";
        if (pos == 1) {
            fileName = "Location.json";
        } else if (pos == 2) {
            fileName = "DeviceMotion.json";
        } else if (pos == 3) {
            fileName = "Weather.json";
        } else if (pos == 4) {
            fileName = "CurrentTime.json";
        } else if (pos == 5) {
            fileName = "StepCountOfDay.json";
        } else if (pos == 6) {
            fileName = "Pedometer.json";
        } else if (pos == 7) {
            fileName = "Summary.json";
        }

        File filePath = new File(uniqueAppFolder, fileName);
        try {
            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(answerJason);
            bw.close();
            Log.e(fileName, " Saved");
            Log.e("Save Path ", filePath.toString());
            return true;
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            return false;
        }
    }

    private void saveSummaryData() {
        //Get summary data
        double distance = ((SurveyActivity) getActivity()).getDistanceInMeter();
        String testDuration = ((SurveyActivity) getActivity()).getTestDuration();
        long totalStepsCount = ((SurveyActivity) getActivity()).getTotalStepsCount();
        int avgHeartRate = 0;
        int maxHeartRate = 0;
        int floorAscended = 0;

        //get previous summary data
        Gson gsonObj = new Gson();
        List<PostReportRequest> summaryList = new ArrayList<>();
        String summary = SdkSharedPrefSingleton.getInstance().getTestSummary();
        if (!TextUtils.isEmpty(summary)) {
            TypeToken<List<PostReportRequest>> type = new TypeToken<List<PostReportRequest>>() {
            };
            summaryList = gsonObj.fromJson(summary, type.getType());
        }

        //prepare summary object
        PostReportRequest request = new PostReportRequest(null, avgHeartRate, maxHeartRate, distance, totalStepsCount, testDuration, SdkUtils.getTestTakenDateTime(), floorAscended);
        summaryList.add(request);

        //Save the summary data in json
        String summaryString = gsonObj.toJson(summaryList);
        JSONArray testObject = null;
        JSONObject jsonObj = null;
        try {
             testObject = new JSONArray(summaryString);
            for (int i = 0; i < testObject.length(); i++)
            {
                jsonObj = testObject.getJSONObject(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonObj.remove("id");
        //Log.d(TAG,testObject.toString());

      /*  try {
           *//* jsonObj.put("Total floor ascended",jsonObj.get("floorAscended"));
            jsonObj.remove("floorAscended");*//*
            jsonObj.put("Average heart rate",jsonObj.get("average_heart_rate"));
            jsonObj.remove("average_heart_rate");
            jsonObj.put("Total distance",jsonObj.get("distance"));
            jsonObj.remove("distance");
            jsonObj.put("Test duration",jsonObj.get("duration_of_test"));
            jsonObj.remove("duration_of_test");
            jsonObj.put("Max heart rate",jsonObj.get("max_heart_rate"));
            jsonObj.remove("max_heart_rate");
            jsonObj.put("Total number of steps",jsonObj.get("steps_count"));
            jsonObj.remove("steps_count");
            jsonObj.put("Test taken time",jsonObj.get("test_taken_at"));
            jsonObj.remove("test_taken_at");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
       // Log.d(TAG,testObject.toString());
        summaryString = testObject.toString();
        SdkSharedPrefSingleton.getInstance().saveTestSummary(summaryString);

        //insertDataToGoogleFit(totalStepsCount);
        insertSessionToGoogleFit(request);
    }

    private void insertSessionToGoogleFit(PostReportRequest castingData) {

        Log.i(TAG, "Creating a new session for an 6 minute walk");
        Session mSession = new Session.Builder()
                .setName("6 minute walk test Mobile")
                .setIdentifier(getString(R.string.app_name) + " " + System.currentTimeMillis())
                .setDescription("Walking Session Details")
                .setStartTime(startTime, TimeUnit.MILLISECONDS)
                .setEndTime(getStopTime(), TimeUnit.MILLISECONDS)
                .setActivity(FitnessActivities.WALKING)
                .build();

        //Distance
        DataSource distanceSegmentDataSource = new DataSource.Builder()
                .setAppPackageName(getActivity().getPackageName())
                .setDataType(DataType.AGGREGATE_DISTANCE_DELTA)
                .setName("Total Distance Covered")
                .setType(DataSource.TYPE_RAW)
                .build();
        DataSet distanceDataSet = DataSet.create(distanceSegmentDataSource);

        DataPoint firstRunSpeedDataPoint = distanceDataSet.createDataPoint()
                .setTimeInterval(startTime, stopTime, TimeUnit.MILLISECONDS);
        firstRunSpeedDataPoint.getValue(Field.FIELD_DISTANCE).setFloat((float) (castingData.getDistance()));
        distanceDataSet.add(firstRunSpeedDataPoint);

        //Steps
        DataSource stepsDataSource = new DataSource.Builder()
                .setAppPackageName(getActivity().getPackageName())
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setName("Total Steps")
                .setType(DataSource.TYPE_RAW)
                .build();
        DataSet stepsDataSet = DataSet.create(stepsDataSource);
        DataPoint stepsDataPoint = stepsDataSet.createDataPoint()
                .setTimeInterval(startTime, stopTime, TimeUnit.MILLISECONDS);
        stepsDataPoint.getValue(Field.FIELD_STEPS).setInt((int) castingData.getSteps_count());
        stepsDataSet.add(stepsDataPoint);

        //Build SessionInsertRequest
        SessionInsertRequest insertRequest = new SessionInsertRequest.Builder()
                .setSession(mSession)
                .addDataSet(distanceDataSet)
                .addDataSet(stepsDataSet)
                .build();

        //Inserting to Fit
        Fitness.getSessionsClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity()))
                .insertSession(insertRequest)
                .addOnSuccessListener(aVoid -> {
                    // At this point, the session has been inserted and can be read.
                    Log.i(TAG, "Session insert was successful!");
                })
                .addOnFailureListener(e -> {
                    //failure
                    Log.i(TAG, "There was a problem inserting the session: " + e.getLocalizedMessage());
                });
        // [END insert_session]

        Log.e(TAG, "**Test Data Pushed to Fit Mobile**");
    }

    protected void stopAllListeners() {
        //Stop the counter and stop the listeners
        mLocationHelper.stopLocationUpdates(getActivity());
        mDeviceMotion.stopDeviceMotion();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mBaseService.mCountDownTimerJson.cancel();
            stopStepService();
        } else {
            getActivity().finish();
        }
    }

    private void initFitApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .useDefaultAccount() //Use the default account
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed" + connectionResult.toString());
        Log.i(TAG, "onConnectionFailed:" + connectionResult.getErrorCode() + "," + connectionResult.getErrorMessage());
    }

    private class FetchTotalStepsAsync extends AsyncTask<Object, Object, Long> {
        protected Long doInBackground(Object... params) {
            long total = 0;
            PendingResult<DailyTotalResult> result = Fitness.HistoryApi.readDailyTotal(mGoogleApiClient, DataType.TYPE_STEP_COUNT_DELTA);
            DailyTotalResult totalResult = result.await(30, TimeUnit.SECONDS);
            if (totalResult.getStatus().isSuccess()) {
                DataSet totalSet = totalResult.getTotal();
                if (totalSet != null) {
                    total = totalSet.isEmpty()
                            ? 0
                            : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();

                    //Log.w(TAG, "Total Dyas Step." + total);
                    mTotalStepForDay = total;
                }
            } else {
                Log.w(TAG, "There was a problem getting the step count.");
            }
            return total;
        }

        @Override
        protected void onPreExecute() {
            showLoader("Collecting data. Please wait...");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            Log.i(TAG, "Total steps: " + aLong);
            fetchTestData();
        }
    }

    /**
     * Reading all listeners data in the background
     */
    private void fetchTestData() {

        Observable.fromCallable(() -> {
            showLoader("Saving 6 minute walk test data. Please wait...");

            //todo remove after test
/*            for (Item item : mPedoItemsTemp) {
                Log.e("Item",item.toString());
            }*/

            //Get total steps for a day
            Gson gson = new Gson();
            //Get Pedometer
            //If Pedometer object is 0 then add atlease one dummy object to it
            if (mPedoItems.size() == 0) {
                Item item = new Item();
                item.setNumberOfSteps(0);
                item.setDistance(0d);
                mPedoItems.add(item);
            }
            mPedometer.setItems(mPedoItems);
            String pedoStepsJson = gson.toJson(mPedometer, Pedometer.class);

            //Get Location data
            LocationData locationData = mLocationHelper.getLocationDataOf6minWalk();
            String locationJson = gson.toJson(locationData, LocationData.class);

            //Get Device Motion
            GyroResponse deviceMotion = mDeviceMotion.getDeviceMotionData();
            String deviceJson = gson.toJson(deviceMotion, GyroResponse.class);

            //Get Weather
            String weatherJson = SdkSharedPrefSingleton.getInstance().getWeatherData();

            //Get CurrentTime
            String dateTime = SdkUtils.getCurrentUTCDateTimeWithMillSeconds();
            CurrentTime curentTime = new CurrentTime();
            curentTime.setCurrentTime(dateTime);
            String curentTimeJson = gson.toJson(curentTime, CurrentTime.class);

            //Get stepCount of the day
            TotalStepCount totalStepCount = new TotalStepCount();
            totalStepCount.setTotalStepsInDay("" + mTotalStepForDay);
            String totalStepCountJson = gson.toJson(totalStepCount, TotalStepCount.class);

           /* //Get Summary
            String summary = SdkSharedPrefSingleton.getInstance().getTestSummary();
            Log.e(TAG,"test summary >> "+summary);
            String[] data = {locationJson, deviceJson, weatherJson, curentTimeJson, totalStepCountJson, pedoStepsJson, summary};*/
            ((SurveyActivity) getActivity()).setDistanceInMeter(mFinalDistance);

            long myTestStepCount = mFinalSteps;//getMyTestStepsCount(mPedoItems);

            //Saving the summary data
            long secondsLimit = (360000 - remainingTime) / 1000;
            String timDuration = String.format("%02d", secondsLimit / 60) + "m : " + String.format("%02d", secondsLimit % 60) + "s";
            ((SurveyActivity) getActivity()).setTestDuration(timDuration);
            ((SurveyActivity) getActivity()).setMaxHeartRate(0);
            ((SurveyActivity) getActivity()).setAvgHeartRate(0);
            ((SurveyActivity) getActivity()).setTotalStepsCount(myTestStepCount);

            saveSummaryData();

            //Get Summary
            String summary = SdkSharedPrefSingleton.getInstance().getTestSummary();
            Log.e(TAG,"test summary >> "+summary);

            String[] data = {locationJson, deviceJson, weatherJson, curentTimeJson, totalStepCountJson, pedoStepsJson, summary};
            saveWalkTestData(data);
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    hideLoader();
                    SdkSharedPrefSingleton.getInstance().saveWalkTestActive(false);
                    SdkUtils.grantVibration(getActivity());
                    //Goto to next question after completion of 6MWT
                    if (isCounterFinished) {
                        mIOnNextListener.onNextPressed(mPosition);
                        isCounterFinished = false;
                    }
                });

    }

    //endregion Save Data
}
