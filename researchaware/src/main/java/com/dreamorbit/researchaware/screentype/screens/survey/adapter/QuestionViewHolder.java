package com.dreamorbit.researchaware.screentype.screens.survey.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;


import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.model.survey.Options;

import java.util.List;

/**
 * Created by Mujas on 22-10-2017.
 */

//View holder class, where all view components are defined
public class QuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvOption;
    public ImageView imgOption;
    public RadioButton radio;
    private QuestionAdapter adapter;
    private List<Options> mOptions;

    public QuestionViewHolder(View itemView, final QuestionAdapter adapter, List<Options> options) {
        super(itemView);
        this.mOptions = options;
        this.adapter = adapter;

        tvOption = itemView.findViewById(R.id.tvOption);
        imgOption = itemView.findViewById(R.id.imgOption);
        radio = itemView.findViewById(R.id.radio);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.mSelectedItem = getAdapterPosition();
                adapter.notifyItemRangeChanged(0, mOptions.size());
            }
        };

        itemView.setOnClickListener(clickListener);
        radio.setOnClickListener(clickListener);
        imgOption.setOnClickListener(clickListener);
    }

    @Override
    public void onClick(View v) {

    }
}
