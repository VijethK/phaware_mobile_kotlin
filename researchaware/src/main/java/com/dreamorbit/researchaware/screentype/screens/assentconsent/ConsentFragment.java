package com.dreamorbit.researchaware.screentype.screens.assentconsent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.screentype.dialog.LearnMoreDialogFragment;
import com.dreamorbit.researchaware.screentype.factory.assentflow.RootFragment;

public class ConsentFragment extends RootFragment {

    private static final String TAG = "ConsentFragment";

    //consent views
    private TextView mTitle;
    private TextView mTvLearnMore;
    private TextView mTvDescription;
    private ImageView mImageView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static ConsentFragment newInstance(Section root, int position) {
        ConsentFragment consentFragment = new ConsentFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putParcelable(SdkConstant.CONSENT_ASSENT_DATA, root);
        consentFragment.setArguments(args);
        return consentFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_consent, container, false);
        initToolBar(rootView);
        initViews(rootView);
        initNavigationListeners(rootView);

        return rootView;
    }

    /**
     * Initilialize the views
     * Hide the Learn More button if content is empty
     * @param rootView
     */
    private void initViews(View rootView) {
        mTitle = rootView.findViewById(R.id.tvTtle);
        mTvCancel = rootView.findViewById(R.id.cancel);
        mTvDescription = rootView.findViewById(R.id.tvDescription);
        mTvLearnMore = rootView.findViewById(R.id.tvLearnMore);
        mImageView = rootView.findViewById(R.id.imageView);
        mImageView.setBackgroundDrawable(SdkUtils.getDrawable(getActivity(), mRoot.getImage()));
        mTitle.setText(mRoot == null ? "NA" : mRoot.getTitle());
        mTvDescription.setText(mRoot == null ? "NA" : mRoot.getSummary());

        //Hide the Learn More button if content is empty
        if(TextUtils.isEmpty(mRoot.getContent()))
            mTvLearnMore.setVisibility(View.INVISIBLE);
        else
            mTvLearnMore.setVisibility(View.VISIBLE);

        if("Welcome".equalsIgnoreCase(mRoot.getTitle()))
            mTitle.setVisibility(View.INVISIBLE);
        else
            mTitle.setVisibility(View.VISIBLE);
    }

    private void showLearnDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        LearnMoreDialogFragment learnMoreDialogFragment = LearnMoreDialogFragment.newInstance(mRoot.getTitle(), mRoot.getContent());
        learnMoreDialogFragment.show(fm, "LEARN_MORE_FRAGMENT");
    }

    @Override
    protected void initNavigationListeners(View rootView) {
        super.initNavigationListeners(rootView);
        mTvLearnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLearnDialog();
            }
        });
    }
}
