package com.dreamorbit.researchaware.screentype.screens.survey;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.BaseFragment;

import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by mujasam.bn on 11/6/2017.
 */

public class SurveyBaseFragment extends BaseFragment {

    /**
     * Save answers in json format {"answer" :"Yes", "answer":"no"}
     */

    //Declare the Adapter, AecyclerView and our custom ArrayList
    protected TextView tvQuestion;
    protected List<Questions> mQuestionList;
    protected int mPosition;
    protected String mActivityTitle;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionList = getArguments().getParcelableArrayList(SdkConstant.QUESTION);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(SdkConstant.POSITION);
    }

    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreenCancelled() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, false);
        i.putExtra(SdkConstant.SURVERY_ID, -1);
        getActivity().setResult(RESULT_CANCELED, i);
        getActivity().finish();
    }


    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreen() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, true);
        i.putExtra(SdkConstant.SURVERY_ID, mQuestionList.get(0).getActivityId());
        getActivity().setResult(RESULT_OK, i);
        getActivity().finish();
    }
}
