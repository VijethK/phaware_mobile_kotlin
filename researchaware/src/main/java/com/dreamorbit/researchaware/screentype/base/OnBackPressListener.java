package com.dreamorbit.researchaware.screentype.base;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public interface OnBackPressListener {
    void onFragmentBackPressed(int position);
}
