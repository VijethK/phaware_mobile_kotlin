package com.dreamorbit.researchaware.screentype.screens.assentconsent;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.ConsentAssent;
import com.dreamorbit.researchaware.network.ConnectivityReceiver;
import com.dreamorbit.researchaware.screentype.base.FlowFragment;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.google.gson.Gson;

public class ViewTaskActivity extends AppCompatActivity implements OnNextListener, OnBackPressListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private FlowFragment mFlowFragment;
    private ConsentAssent mRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);

        if (savedInstanceState == null) {
            // withholding the previously created fragment from being created again
            // On orientation change, it will prevent fragment recreation
            // its necessary to reserving the fragment stack inside each tab
            mRoot = getConsentDocument();
            initScreen();
        } else {
            // restoring the previously created fragment
            // and getting the reference
            mFlowFragment = (FlowFragment) getSupportFragmentManager().getFragments().get(0);
        }
    }

    private void initScreen() {
        // Creating the ViewPager container fragment once
        mFlowFragment = FlowFragment.createInstance(mRoot);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mFlowFragment)
                .commit();
    }

    /**
     * Fetching the data from main application
     * data will be in json fomat related to all data regarding ConsentAssent and consent flow
     *
     * @return screenDataRoot
     */
    private ConsentAssent getConsentDocument() {
        String data = getIntent().getStringExtra(SdkConstant.CONSENT_ASSENT_DATA);
        Gson gson = new Gson();
        ConsentAssent screenDataRoot = gson.fromJson(data, ConsentAssent.class);
        return screenDataRoot;
    }

    /**
     * Hide keyboard global method
     */
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isActive() && imm.isAcceptingText()) {
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    @Override
    public void onBackPressed() {
        backNavigation();
    }



    @Override
    public void onNextPressed(int position) {
        mFlowFragment.mPager.setCurrentItem(position + 1);
    }

    @Override
    public void onFragmentBackPressed(int position) {
        backNavigation();
    }

    /**
     * Navigation back implementation depend on the
     */
    private void backNavigation() {
        if (mFlowFragment.mPager.getCurrentItem() != 0) {
            mFlowFragment.mPager.setCurrentItem(mFlowFragment.mPager.getCurrentItem() - 1,false);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setConnectivityListener(this);
    }

    /**
     * Setting the listener for the broadcast reciver
     * to receive updates {@link
     *
     * @param listener (activity or fragment which implements the interface)
     */
    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }
}
