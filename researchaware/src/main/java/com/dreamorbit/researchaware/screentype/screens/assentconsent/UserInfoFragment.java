package com.dreamorbit.researchaware.screentype.screens.assentconsent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.ScreenType;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.screentype.factory.assentflow.RootFragment;
import com.dreamorbit.researchaware.utilities.Cryptography;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;

public class UserInfoFragment extends RootFragment {

    private EditText mFirstName;
    private EditText mLastName;
    private SdkSharedPrefSingleton mSdkSharedPrefSingleton;

    public UserInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static UserInfoFragment newInstance(Section assentData, int position) {
        UserInfoFragment userInfoFragment = new UserInfoFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putParcelable(SdkConstant.CONSENT_ASSENT_DATA, assentData);
        userInfoFragment.setArguments(args);
        return  userInfoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);

        initViews(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);
        mSdkSharedPrefSingleton = SdkSharedPrefSingleton.getInstance().initialize(getActivity(), SdkSharedPrefSingleton.ASSENT_PREFERENCE);

        return rootView;
    }

    private void initViews(View rootView) {
        mFirstName = rootView.findViewById(R.id.firstName);
        mLastName = rootView.findViewById(R.id.lastName);
    }


    protected void initNavigationListeners(View rootView) {
        super.initNavigationListeners(rootView,ScreenType.valueOf(mRoot.getType().toUpperCase()));
        rootView.findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUserInfo();
            }
        });
    }

    private void validateUserInfo() {

        if (mFirstName != null && mLastName != null) {
            if (mFirstName.getText().toString().trim().isEmpty()) {
                mFirstName.setError(getString(R.string.firstName_validate));
            } else if (mLastName.getText().toString().trim().isEmpty()) {
                mLastName.setError(getString(R.string.lastName_validate));
            } else {
                //Shared Preferences to store firstname, lastname
                String encryptedFirstName = Cryptography.encrypt(SdkConstant.KEY, mFirstName.getText().toString());
                String encryptedLastName = Cryptography.encrypt(SdkConstant.KEY, mLastName.getText().toString());
                mSdkSharedPrefSingleton.saveFirstName(encryptedFirstName);
                mSdkSharedPrefSingleton.saveLastName(encryptedLastName);
                mIOnNextListener.onNextPressed(mPosition);
            }
        }
    }
}

