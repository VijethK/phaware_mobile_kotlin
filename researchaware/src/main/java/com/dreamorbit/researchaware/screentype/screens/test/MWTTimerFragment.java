package com.dreamorbit.researchaware.screentype.screens.test;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyBaseDialogFragment;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class MWTTimerFragment extends SurveyBaseDialogFragment {

    private static OnNextListener mIOnNextListener;
    protected int mPosition;
    protected String mActivityTitle;
    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private CountDownTimer mCountDownTimer;

    public MWTTimerFragment() {
        // Required empty public constructor
    }

    public static MWTTimerFragment newInstance(int position, String activityTitle, OnNextListener IOnNextListener) {
        MWTTimerFragment MWTTimerFragment = new MWTTimerFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        MWTTimerFragment.setArguments(args);
        return MWTTimerFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(SdkConstant.POSITION);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppDialogThemeNoAnim);
        mIOnNextListener = ((SurveyActivity) getActivity());
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_timer, container, false);

        initTimerView(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    /**
     * ON a count of 5 launch the 6 minute walk test
     *
     * @param rootView
     */
    private void initTimerView(View rootView) {
        ProgressBar pb = rootView.findViewById(R.id.progressBarToday);
        TextView textTimer = rootView.findViewById(R.id.tvTimer);
        mCountDownTimer = new CountDownTimer(7000, 1000) {
            // 1000 means, onTick function will be called at every 1000 milliseconds

            @Override
            public void onTick(long leftTimeInMilliseconds) {
                Log.e("TICK ", "" + leftTimeInMilliseconds);
                long seconds = (leftTimeInMilliseconds / 1000) - 1;
                pb.setProgress((int) seconds);

                if ((int) seconds < 0) {
                    return;
                }

                textTimer.setText("" + (int) seconds);

                if (seconds == 0) {
                    try {
                        dismiss();
                    }catch (IllegalStateException f){}

                    mIOnNextListener.onNextPressed(mPosition);
                }
            }

            @Override
            public void onFinish() {

            }
        };
    }

    /**
     * Init toolbar and navigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        mTvCancel.setText(R.string.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText("  " + mActivityTitle);
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        //Done button
        mTvCancel.setOnClickListener(view -> {
                    mCountDownTimer.cancel();
                    navigateToActivityScreenCancelled();
                }
        );
    }

    @Override
    public void onPause() {
        super.onPause();
        mCountDownTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCountDownTimer.start();
    }
}