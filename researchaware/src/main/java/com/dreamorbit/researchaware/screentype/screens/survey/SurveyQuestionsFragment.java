package com.dreamorbit.researchaware.screentype.screens.survey;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.helper.SdkUtils;
import com.dreamorbit.researchaware.model.survey.Options;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.survey.adapter.ClickListeners;
import com.dreamorbit.researchaware.screentype.screens.survey.adapter.QuestionAdapter;
import com.dreamorbit.researchaware.screentype.screens.survey.adapter.RecyclerTouchListener;
import com.dreamorbit.researchaware.screentype.screens.survey.adapter.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class SurveyQuestionsFragment extends SurveyBaseFragment {

    private static final String TAG = "SurveyQuestionsFragment";
    //listener for navigations next and back
    protected OnNextListener mIOnNextListener;
    protected OnBackPressListener mIOnBackPressListener;
    //Declare the Adapter, RecyclerView and our custom ArrayList
    private TextView tvQuestion;
    private RecyclerView recyclerView;
    private QuestionAdapter mQuestionAdapter;
    private int mOptionPosition;

    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private TextView tvSkip;

    public SurveyQuestionsFragment() {
        // Required empty public constructor
    }

    public static SurveyQuestionsFragment newInstance(List<Questions> question, int position, String activityTitle) {
        SurveyQuestionsFragment consentFragment = new SurveyQuestionsFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(SdkConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        consentFragment.setArguments(args);
        return consentFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_survey_list, container, false);
        initViews(rootView);
        setupRecyclerView(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    /**
     * Init screen views
     *
     * @param rootView
     */
    private void initViews(View rootView) {
        tvSkip = rootView.findViewById(R.id.tvSkip);
        tvQuestion = rootView.findViewById(R.id.tvQuestion);
        tvQuestion.setText(mQuestionList == null ? "NA" : mQuestionList.get(mPosition).getQuestion());
    }

    /**
     * Setup questionaire Recyclerview
     *
     * @param rootView
     */
    private void setupRecyclerView(View rootView) {
        recyclerView = rootView.findViewById(R.id.recycleView);
        //As explained in the tutorial, LineatLayoutManager tells the RecyclerView that the view
        //must be arranged in linear fashion
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        /**
         * RecyclerView: Implementing single item click and long press (Part-II)
         * */
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerView, new ClickListeners() {
            @Override
            public void onClick(View view, final int position) {
                mOptionPosition = position;
                updateAnswerForThisQuestion(position);
            }

            @Override
            public void onLongClick(View view, int position) {
                mOptionPosition = position;
                updateAnswerForThisQuestion(position);
            }
        }));

        mQuestionAdapter = new QuestionAdapter(getActivity(), mQuestionList.get(mPosition));
        recyclerView.setAdapter(mQuestionAdapter);
    }

    /**
     * Init toolbar and ncavigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText(" "+mActivityTitle);

        //dont show back button if walk test screen and in further survey question's screen.

        if (mActivityTitle.equalsIgnoreCase("6 Minute Walk Test") && mPosition == 5) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }else  if (mActivityTitle.equalsIgnoreCase("6 Minute Walk Test") && mPosition > 2) {
            return;
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        rootView.findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 *If Yes close the activity else if No then go to next question
                 * first condition check if you are in last but one screen (Questionaire's last but one screen)
                 * second condition to check whether user has selected "no"
                 */
                if (mQuestionList.size() - 2 == mPosition && checkIsNoSelected() && mActivityTitle.equalsIgnoreCase("Quality of Life Survey")) {
                    showActivityConfirmDialog();
                } else if (mQuestionList.size() - 1 == mPosition && checkIsAnswerSelectedforThisQuestion()) { //if last question screen
                    showActivityConfirmDialog();
                }else if (mPosition == 5 && checkIsAnswerSelectedforThisQuestion() && mActivityTitle.equalsIgnoreCase("6 Minute Walk Test")) {
                    showActivityConfirmDialog();
                }  else if (checkIsAnswerSelectedforThisQuestion()) {
                    mIOnNextListener.onNextPressed(mPosition);
                } else {
                    SdkUtils.showSnackBar(rootView.findViewById(R.id.fragment_mainLayout), getString(R.string.please_select_answer), Snackbar.LENGTH_LONG);
                }
            }
        });

        //Yes or No Screen
        if (mPosition == mQuestionList.size() - 2 && mActivityTitle.equalsIgnoreCase("Quality of Life Survey")) {
            tvSkip.setVisibility(View.INVISIBLE);
        }
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAnswerSkipped(); // update the answer to null if allready selected any
                if (mQuestionList.size() - 1 == mPosition) {
                    showActivityConfirmDialog();
                } else {
                    mIOnNextListener.onNextPressed(mPosition);
                }
            }
        });


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnBackPressListener.onFragmentBackPressed(mPosition);
            }
        });
        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.are_you_sure_cancel)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                navigateToActivityScreenCancelled();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                dialog.cancel();
                            }
                        });
                // Create the AlertDialog object and return it
                builder.setCancelable(false);
                builder.show();
            }
        });
    }

    /**
     * Upadate the answer for each questions
     *
     * @param position
     */
    private void updateAnswerForThisQuestion(int position) {
        List<Options> optionsList = mQuestionList.get(mPosition).getOptions(); // get Option list fof this question from main list
        mQuestionList.get(mPosition).setAnswer(optionsList.get(position).getName()); // select the answer from options and update in in main mQuestionList
        String dateTime = SdkUtils.getCurrentUTCDateTimeWithMillSeconds();
        optionsList.get(position).setDateTime(dateTime);
        mQuestionList.get(mPosition).setCurrentTime(optionsList.get(position).getDateTime());
    }

    /**
     * Upadate the answer for each questions
     */
    private void updateAnswerSkipped() {
        mQuestionList.get(mPosition).setAnswer(null); // select the answer from options and update in in main mQuestionList
        String dateTime = SdkUtils.getCurrentUTCDateTimeWithMillSeconds();
        mQuestionList.get(mPosition).setCurrentTime(dateTime);
        mQuestionAdapter.mSelectedItem = -1; //deselecting the last selected position
        mQuestionAdapter.notifyItemChanged(mOptionPosition); // refresh the radio button selection to uncheck
    }

    /**
     * on next click check whether answer is selected allready
     *
     * @return
     */
    private boolean checkIsAnswerSelectedforThisQuestion() {
        String answer = mQuestionList.get(mPosition).getAnswer();
        return !TextUtils.isEmpty(answer);
    }

    /**
     * on next click check if you are in Meication screen
     * If Yes got to next questionaire
     * else if No finish the activity and mark it as completed.
     *
     * @return
     */
    private boolean checkIsNoSelected() {
        if (mQuestionList.get(mPosition).getAnswer() != null) {
            String answer = mQuestionList.get(mPosition).getAnswer().toLowerCase();
            return !TextUtils.isEmpty(answer) && answer.equalsIgnoreCase("no");
        } else {
            return false;
        }
    }

    /**
     * Create final answer json to send to server
     * save this in preference for offline use
     */
    private void fetchAllAnswers() {
    }

    /**
     * Show dialog for Complete fragment
     * List<Questions> question, int position, String activityTitle
     */
    private void showActivityConfirmDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ActivityCompleteFragment surveyConfirmDialogFragment = ActivityCompleteFragment.newInstance(mQuestionList, mPosition, mActivityTitle);
        surveyConfirmDialogFragment.show(fm, "ACTIVITY_COMPLETE_FRAGMENT");
    }
}
