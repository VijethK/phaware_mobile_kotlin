package com.dreamorbit.researchaware.screentype.screens.assentconsent;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.ScreenType;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.network.ConnectivityReceiver;
import com.dreamorbit.researchaware.screentype.factory.assentflow.RootFragment;
import com.dreamorbit.researchaware.utilities.ResearchUtility;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;


public class SignatureFragment extends RootFragment {

    //Signature View
    private SignaturePad mSignaturePad;

    //Shared preference intilization
    private SdkSharedPrefSingleton mSdkSharedPrefSingleton;
    private Button mBtnDone;
    private SignaturePad nSignaturePad;
    private TextView mTvClear;
    private Bitmap mBitmap;

    public SignatureFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static SignatureFragment newInstance(Section root, int position) {
        SignatureFragment signatureFragment = new SignatureFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putParcelable(SdkConstant.CONSENT_ASSENT_DATA, root);
        signatureFragment.setArguments(args);
        return signatureFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_signature, container, false);
        initSignatureView(rootView);
        initToolBar(rootView);
        initViews(rootView);
        initNavigationListeners(rootView);
        mSdkSharedPrefSingleton = SdkSharedPrefSingleton.getInstance().initialize(getActivity(), SdkSharedPrefSingleton.ASSENT_PREFERENCE);

        return rootView;
    }

    private void initViews(View rootView) {
        nSignaturePad = rootView.findViewById(R.id.signature_pad);
        mBtnDone = rootView.findViewById(R.id.btnDone);
        mBtnDone.setEnabled(false);
        mBtnDone.setAlpha(0.5f);
        mTvClear = rootView.findViewById(R.id.clear);
    }

    private void initSignatureView(final View rootView) {

        mSignaturePad = rootView.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                mBitmap = mSignaturePad.getSignatureBitmap();
                validateSignature();
            }

            @Override
            public void onClear() {
            }
        });
    }


    protected void initNavigationListeners(View rootView) {
        super.initNavigationListeners(rootView, ScreenType.valueOf(mRoot.getType().toUpperCase()));

        mTvClear.setOnClickListener(view -> {
            nSignaturePad.clear();
            validateSignature();
        });

        mBtnDone.setOnClickListener(v -> {
            if (ConnectivityReceiver.isConnected(getActivity())) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 5, baos);
                byte[] b = baos.toByteArray();

                String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                if (encodedImage != null) {
                    mSdkSharedPrefSingleton.saveSignature(encodedImage);
                }

                Intent i = new Intent();  // or // Intent i = getIntent()
                i.putExtra("TAG", true);
                getActivity().setResult(RESULT_OK, i);
                getActivity().finish();
            } else {
                ResearchUtility.showDialogOK(getActivity(), getString(R.string.check_internet_connection),
                        (dialog, which) -> {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    break;
                            }
                        });
            }
        });
    }

    private void validateSignature() {
        if (nSignaturePad.isEmpty()) {
            mBtnDone.setEnabled(false);
            mBtnDone.setAlpha(0.5f);
            mTvClear.setVisibility(View.INVISIBLE);
        } else {
            mBtnDone.setEnabled(true);
            mBtnDone.setBackgroundResource(R.drawable.roundbordr);
            mBtnDone.setAlpha(1.0f);
            mTvClear.setVisibility(View.VISIBLE);
        }
    }
}
