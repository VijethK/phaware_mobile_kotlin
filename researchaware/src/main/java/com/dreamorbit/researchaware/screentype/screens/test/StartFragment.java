package com.dreamorbit.researchaware.screentype.screens.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity;

import static android.app.Activity.RESULT_CANCELED;


public class StartFragment extends Fragment {

    private static ViewPager viewPager;
    //Data to populate in screen
    protected int mPosition;
    protected String mActivityTitle;
    //toolbar_consent views
    protected Toolbar mToolbar;
    protected TextView mTvTitle;
    protected TextView mTvCancel;
    //listener for navigations next and back
    private OnNextListener mIOnNextListener;
    private OnBackPressListener mIOnBackPressListener;

    public StartFragment() {
        // Required empty public constructor
    }

    public static StartFragment newInstance(int position, ViewPager viewPager, String activityTitle) {
        StartFragment fragment = new StartFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        StartFragment.viewPager = viewPager;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(SdkConstant.POSITION, 0);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fit, container, false);
        initToolBar(view);
        initNavigationListeners(view);
        return view;
    }

    protected void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        mTvTitle.setText(mActivityTitle);
    }

    /**
     * Overriding metod to perform ReviewScreen action Visual info type
     * #1 toolbar back navigation action
     * #2 toolbar Cancel action
     * @param rootView
     */

    protected void initNavigationListeners(View rootView) {
        mToolbar.setNavigationOnClickListener(v -> mIOnBackPressListener.onFragmentBackPressed(mPosition));
        mTvCancel.setOnClickListener(view -> {
            Alert();
        });
        rootView.findViewById(R.id.btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimerDialog();
                //mIOnNextListener.onNextPressed(mPosition);
            }
        });
    }

    private void Alert() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.do_end_task).setPositiveButton(R.string.end_task, (dialog, which) -> {
            closeActivity();
        })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                })
                .setCancelable(false)
                .show();
    }

    /**
     * Show dialog for Complete fragment
     * List<Questions> question, int position, String activityTitle
     */
    private void showTimerDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        MWTTimerFragment timerFragment = MWTTimerFragment.newInstance(mPosition, mActivityTitle,mIOnNextListener);
        timerFragment.show(fm, "TIMER_FRAGMENT");
    }

    private void closeActivity() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, false);
        i.putExtra(SdkConstant.SURVERY_ID, -1);
        getActivity().setResult(RESULT_CANCELED, i);
        getActivity().finish();
    }

}
