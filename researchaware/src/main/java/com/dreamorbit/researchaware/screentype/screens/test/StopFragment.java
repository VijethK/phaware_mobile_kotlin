package com.dreamorbit.researchaware.screentype.screens.test;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.fit.DiscardReason;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyBaseDialogFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class StopFragment extends SurveyBaseDialogFragment {

    //Declare the Adapter, AecyclerView and our custom ArrayList
    protected TextView tvQuestion;
    protected List<Questions> mQuestionList;
    protected int mPosition;
    protected String mActivityTitle;
    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private EditText etReason;

    public StopFragment() {
        // Required empty public constructor
    }

    public static StopFragment newInstance(List<Questions> mQuestionList, int position, String activityTitle) {
        StopFragment activityCompleteFragment = new StopFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(SdkConstant.QUESTION, (ArrayList<? extends Parcelable>) mQuestionList);
        activityCompleteFragment.setArguments(args);
        return activityCompleteFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionList = getArguments().getParcelableArrayList(SdkConstant.QUESTION);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(SdkConstant.POSITION);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_walk_stop, container, false);
        initDialog(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    private void initDialog(View rootView) {
        // Get field from view
        etReason = rootView.findViewById(R.id.etReason);
        // Fetch arguments from bundle and set title
        //getDialog().setTitle("Please enter reason.");
        setCancelable(false);
        // Show soft keyboard automatically and request focus to field
        etReason.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        //Done button
        rootView.findViewById(R.id.btnOk).setOnClickListener(view -> {
            if (!TextUtils.isEmpty(etReason.getText().toString())) {
                Gson gson = new Gson();
                DiscardReason discardReason = new DiscardReason();
                discardReason.setDiscardReason(etReason.getText().toString());
                String reasonJson = gson.toJson(discardReason, DiscardReason.class);
                saveAnswerJsonInAppFolder(reasonJson);
                navigateToActivityScreen();
            } else {
                etReason.setError("Please enter the reason");
                etReason.requestFocus();
            }
        });
    }
}