package com.dreamorbit.researchaware.screentype.factory.assentflow;

import com.dreamorbit.researchaware.model.assent.ScreenType;
import com.dreamorbit.researchaware.model.assent.Section;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ConsentFragment;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ReviewFragment;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.SignatureFragment;
import com.dreamorbit.researchaware.screentype.screens.assentconsent.UserInfoFragment;

/**
 * Created by mujasam.bn on 10/4/2017.
 */

public class ScreenFactory {
    public static RootFragment getScreenFactory(ScreenType type, Section assentData, int position) {
        switch (type) {
            case VISUAL_SCREEN:
                return ConsentFragment.newInstance(assentData, position);
            case REVIEW:
                return ReviewFragment.newInstance(assentData, position);
            case USER_INFO_SCREEN:
                return UserInfoFragment.newInstance(assentData, position);
            case SIGNATURE_SCREEN:
                return SignatureFragment.newInstance(assentData, position);
            default:
                return UserInfoFragment.newInstance(assentData, position);
        }
    }
}
