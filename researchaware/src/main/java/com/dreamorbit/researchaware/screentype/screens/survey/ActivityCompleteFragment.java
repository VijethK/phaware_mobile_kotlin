package com.dreamorbit.researchaware.screentype.screens.survey;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.OnBackPressListener;
import com.dreamorbit.researchaware.screentype.base.OnNextListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class ActivityCompleteFragment extends SurveyBaseDialogFragment {

    //Declare the Adapter, AecyclerView and our custom ArrayList
    protected TextView tvQuestion;
    protected List<Questions> mQuestionList;
    protected int mPosition;
    protected String mActivityTitle;
    //listener for navigations next and back
    protected OnNextListener mIOnNextListener;
    protected OnBackPressListener mIOnBackPressListener;
    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;

    public ActivityCompleteFragment() {
        // Required empty public constructor
    }

    public static ActivityCompleteFragment newInstance(List<Questions> question, int position, String activityTitle) {
        ActivityCompleteFragment activityCompleteFragment = new ActivityCompleteFragment();
        Bundle args = new Bundle();
        args.putInt(SdkConstant.POSITION, position);
        args.putString(SdkConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(SdkConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        activityCompleteFragment.setArguments(args);
        return activityCompleteFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionList = getArguments().getParcelableArrayList(SdkConstant.QUESTION);
        mActivityTitle = getArguments().getString(SdkConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(SdkConstant.POSITION);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppDialogThemeNoAnim);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.setCancelable(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_activity_complete, container, false);

        initToolBar(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    /**
     * Init toolbar and navigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        mTvCancel.setText(R.string.done);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText(mActivityTitle);
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        mTvCancel.setOnClickListener(new View.OnClickListener() { //Done button
            @Override
            public void onClick(View view) {
                saveAnswerJsonInAppFolder(null);
                if (mActivityTitle.equalsIgnoreCase("6 Minute Walk Test")) {
                    dismiss();
                    mIOnNextListener.onNextPressed(mPosition);
                } else {
                    navigateToActivityScreen();
                }
            }
        });
    }
}