package com.dreamorbit.researchaware.screentype.factory.assentflow;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.dreamorbit.researchaware.model.assent.ConsentAssent;
import com.dreamorbit.researchaware.model.assent.ScreenType;
import com.dreamorbit.researchaware.model.assent.Section;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private final Resources resources;
    private ConsentAssent root;

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    /**
     * Create mPager mAdapter
     *  @param resources
     * @param fm
     * @param root
     */
    public ViewPagerAdapter(final Resources resources, FragmentManager fm, ConsentAssent root) {
        super(fm);
        this.resources = resources;
        this.root = root;
    }

    /**
     * Call a factory to initialize the fragment.
     * Show the screen depend on the ui
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        final RootFragment result;
        final ScreenType type =  ScreenType.valueOf(root.getSections().get(position).getType().toUpperCase()); // Converting string type to enum ScreenType
        final Section screenDataObj = root.getSections().get(position);
        result = ScreenFactory.getScreenFactory(type,screenDataObj,position); //Factory pattern to get the fragment view depend on the type we got from json
        return result;
    }

    /**
     * ViewPagerAdapter will loop depend on this count.
     * @return
     */
    @Override
    public int getCount() {
        int count = root.getSections().size();
        return count;
    }

    /**
     * Title for the fragment
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(final int position) {
        return root.getSections().get(position).getTitle();
    }

    /**
     * On each Fragment instantiation we are saving the reference of that Fragment in a Map
     * It will help us to retrieve the Fragment by mPosition
     *
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Remove the saved reference from our Map on the Fragment destroy
     *
     * @param container
     * @param position
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }


    /**
     * Get the Fragment by mPosition
     *
     * @param position tab mPosition of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}