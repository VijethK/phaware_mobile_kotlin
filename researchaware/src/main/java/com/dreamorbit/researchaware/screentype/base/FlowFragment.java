package com.dreamorbit.researchaware.screentype.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;
import com.dreamorbit.researchaware.model.assent.ConsentAssent;
import com.dreamorbit.researchaware.screentype.factory.assentflow.ViewPagerAdapter;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class FlowFragment extends Fragment {


    public CustomViewPager mPager;
    public ViewPagerAdapter mAdapter;
    private ConsentAssent mRoot;

    public static FlowFragment createInstance(ConsentAssent mRoot) {
        FlowFragment fragment = new FlowFragment();
        Bundle args = new Bundle();
        args.putParcelable(SdkConstant.CONSENT_ASSENT_DATA, mRoot);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_root, container, false);
        mPager = rootView.findViewById(R.id.vp_pages);
        mRoot = getArguments().getParcelable(SdkConstant.CONSENT_ASSENT_DATA);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Note that we are passing childFragmentManager, not FragmentManager
        mAdapter = new ViewPagerAdapter(getResources(), getChildFragmentManager(), mRoot);
        mPager.setPagingEnabled(false);
        mPager.setAdapter(mAdapter);
    }

}