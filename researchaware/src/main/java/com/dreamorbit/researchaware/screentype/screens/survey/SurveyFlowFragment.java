package com.dreamorbit.researchaware.screentype.screens.survey;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.base.CustomViewPager;
import com.dreamorbit.researchaware.screentype.screens.test.SummaryFragment;
import com.dreamorbit.researchaware.screentype.screens.test.WalkTestFragment;
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton;

import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SurveyFlowFragment extends Fragment {

    private String activityTitle;
    public CustomViewPager mPager;
    public SurveyFragmentPagerAdapter mAdapter;
    private List<Questions> mRoot;

    public SurveyFlowFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public SurveyFlowFragment(List<Questions> root, String activityTitle) {
        this.mRoot = root;
        this.activityTitle = activityTitle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_root, container, false);
        mPager = rootView.findViewById(R.id.vp_pages);


        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       // Note that we are passing childFragmentManager, not FragmentManager
        mAdapter = new SurveyFragmentPagerAdapter(mPager, getResources(), getChildFragmentManager(), mRoot, activityTitle);
        mPager.setPagingEnabled(false);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(mRoot.size());
        mPager.setCurrentItem(0);

        initPageChangeListener();
    }

    /**
     * Start the 6 minute walk test timer when the fragment is visible
     * Handle the viewpager onload lifecycler (timer was starting before the screen was visible)
     *
     */
    public void initPageChangeListener() {
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (activityTitle.equals("6 Minute Walk Test") && position == 3  && !SdkSharedPrefSingleton.getInstance().isWalkTestActive()) {
                    Fragment fragment =  mAdapter.getRegisteredFragment(position);
                    if ( fragment != null) {
                        ((WalkTestFragment)fragment).startWalkTest();
                    }
                }else if(activityTitle.equals("6 Minute Walk Test") && position == 6 ){
                    Fragment fragment =  mAdapter.getRegisteredFragment(position);
                    if ( fragment != null) {
                        ((SummaryFragment)fragment).updateDistance();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}