package com.dreamorbit.researchaware.screentype.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.helper.SdkConstant;

/**
 * Created by mujasam.bn on 10/5/2017.
 */

public class LearnMoreDialogFragment extends DialogFragment {

    private TextView mText;
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    public LearnMoreDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static LearnMoreDialogFragment newInstance(String title, String learnMore) {
        LearnMoreDialogFragment frag = new LearnMoreDialogFragment();
        Bundle args = new Bundle();
        args.putString(SdkConstant.LEARN_MORE_TEXT, learnMore);
        args.putString(SdkConstant.LEARN_MORE_TITLE, title);
        frag.setArguments(args);


        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppDialogTheme);
    }

    private void initToolbarListeners() {
        // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_learn_more) {
                            dismiss();
                        }
                        return true;
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_learn_more, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Inflate a menu to be displayed in the mToolbar
        initViews(view);
        mToolbar.inflateMenu(R.menu.menu_learn_more);
        initToolbarListeners();
    }

    private void initViews(View view) {
        //Toolbar view
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = mToolbar.findViewById(R.id.title);
        mToolbarTitle.setText(getArguments().getString(SdkConstant.LEARN_MORE_TITLE));

        //Dialog View
        mText = view.findViewById(R.id.tvLearnMore);
        mText.setText(getArguments().getString(SdkConstant.LEARN_MORE_TEXT));
    }
}