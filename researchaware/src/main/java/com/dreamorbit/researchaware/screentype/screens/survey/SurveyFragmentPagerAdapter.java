package com.dreamorbit.researchaware.screentype.screens.survey;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.dreamorbit.researchaware.R;
import com.dreamorbit.researchaware.model.survey.Questions;
import com.dreamorbit.researchaware.screentype.screens.test.StartFragment;
import com.dreamorbit.researchaware.screentype.screens.test.SummaryFragment;
import com.dreamorbit.researchaware.screentype.screens.test.WalkTestFragment;

import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SurveyFragmentPagerAdapter extends FragmentPagerAdapter {

    private final Resources resources;
    private final String activityTitle;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    private List<Questions> root;
    private ViewPager viewPager;

    /**
     * Create mPager mAdapter
     *
     * @param resources
     * @param fm
     * @param root
     * @param activityTitle
     */
    public SurveyFragmentPagerAdapter(ViewPager viewPager, final Resources resources, FragmentManager fm, List<Questions> root, String activityTitle) {
        super(fm);
        this.resources = resources;
        this.root = root;
        this.activityTitle = activityTitle;
        this.viewPager = viewPager;
        if(this.activityTitle.equals("6 Minute Walk Test")){
            root.add(2,new Questions()); //Fit initial start screen
            root.add(3,new Questions()); //Fit Data fetching screen
            root.add(root.size(),new Questions()); //Review screen
        }
    }

    /**
     * Call a factory to initialize the fragment.
     * Show the screen depend on the ui
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        Fragment result = null;  //getRegisteredFragment(position);

        if(activityTitle.equals(resources.getString(R.string.quality_of_life)) && position == root.size() - 1){
            result = SurveyQuestionsFragment.newInstance(root, position, activityTitle);
        }else if(activityTitle.equals("6 Minute Walk Test") && position == 2){
            result = StartFragment.newInstance(position, viewPager,activityTitle);
        }else if(activityTitle.equals("6 Minute Walk Test") && position == 3){
            result = WalkTestFragment.newInstance(root,position, viewPager,activityTitle);
        }else if(activityTitle.equals("6 Minute Walk Test") && position == root.size()-1){
            result = SummaryFragment.newInstance(root,position, viewPager,activityTitle);
        } else { // Show questions fragment
            result = SurveyQuestionsFragment.newInstance(root, position, activityTitle);
        }

        return result;
    }

    /**
     * ViewPagerAdapter will loop depend on this count.
     *
     * @return
     */
    @Override
    public int getCount() {
        //int count = root.size() + 1; //adding one fragment to show Activity completed
        int count = root.size(); //adding one fragment to show Activity completed
        return count;
    }

    /**
     * Title for the fragment
     *
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(final int position) {
        return root.get(position).getQuestion();
    }

    /**
     * On each Fragment instantiation we are saving the reference of that Fragment in a Map
     * It will help us to retrieve the Fragment by mPosition
     *
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Get the Fragment by mPosition
     *
     * @param position tab mPosition of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}