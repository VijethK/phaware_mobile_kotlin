package com.dreamorbit.researchaware.emergency;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dreamorbit.researchaware.utilities.ResearchUtility;

public class EmergencyBrodcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ResearchUtility.triggerCall(context);
    }
}
