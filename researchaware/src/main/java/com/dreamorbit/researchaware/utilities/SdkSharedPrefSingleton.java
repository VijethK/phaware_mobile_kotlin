package com.dreamorbit.researchaware.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mujasam on 6/28/2016.
 */

public class SdkSharedPrefSingleton {

    //Mujasam

    public static final String ASSENT_PREFERENCE = "assent_preference";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String SIGNATURE = "signature";
    public static final String ANSWERS = "answers";
    public static final String WALK_TEST_ACTIVE = "mwt_active";
    public static final String WEATHER_DATA = "weather_data";
    public static final String WALK_TEST_FOLDER_PATH = "walk_folder_path";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String SUMMARY = "summary";

    private static SdkSharedPrefSingleton sInstance;
    private SharedPreferences mSharedPreferences;

    @Retention(RetentionPolicy.SOURCE)
    public @interface TAG {
    }

    private SdkSharedPrefSingleton() {
    }

    public static SdkSharedPrefSingleton getInstance() {
        if(sInstance == null){
            sInstance = new SdkSharedPrefSingleton();
        }
        return sInstance;
    }

    public SdkSharedPrefSingleton initialize(Context context, String sharedPreferenceName) {
        mSharedPreferences = context.getSharedPreferences(sharedPreferenceName, MODE_PRIVATE);
        return sInstance;
    }

    private SharedPreferences.Editor getEditor() {
        return mSharedPreferences.edit();
    }

    public void saveSignature(String value) {
        getEditor().putString(SIGNATURE, value).apply();
    }

    public String getSignature() {
        return mSharedPreferences.getString(SIGNATURE,"");
    }

    public void saveFirstName(String value) {
        getEditor().putString(FIRST_NAME, value).apply();
    }

    public String getFirstName() {
        return mSharedPreferences.getString(FIRST_NAME,"");
    }

    public void saveLastName(String value) {
        getEditor().putString(LAST_NAME, value).apply();
    }

    public String getLastName() {
        return mSharedPreferences.getString(LAST_NAME,"");
    }

    public void saveWeatherData(String weatherData) {
        getEditor().putString(WEATHER_DATA, weatherData).apply();
    }

    public String getWeatherData() {
        return mSharedPreferences.getString(WEATHER_DATA,"");
    }

    public void saveWalkTestActive(boolean value) {
        getEditor().putBoolean(WALK_TEST_ACTIVE, value).apply();
    }

    public boolean isWalkTestActive() {
        return mSharedPreferences.getBoolean(WALK_TEST_ACTIVE,false);
    }


    public void saveWalkTestFolderPath(String path) {
        getEditor().putString(WALK_TEST_FOLDER_PATH, path).apply();
    }

    public String getWalkTestFolderPath() {
        return mSharedPreferences.getString(WALK_TEST_FOLDER_PATH,"");
    }

    public void saveLatitude(String latitude) {
        getEditor().putString(LATITUDE, latitude).apply();
    }

    public String getLatitude() {
        return mSharedPreferences.getString(LATITUDE,"");
    }

    public void saveLongitude(String longitude) {
        getEditor().putString(LONGITUDE, longitude).apply();
    }

    public String getLongitude() {
        return mSharedPreferences.getString(LONGITUDE,"");
    }

    public void saveTestSummary(String summary) {
        getEditor().putString(SUMMARY, summary).apply();
    }

    public String getTestSummary() {
        return mSharedPreferences.getString(SUMMARY,"");
    }

    public void saveQuestionAnswers(String value) {
        getEditor().putString(ANSWERS, value).apply();
    }

    public String getQuestionAnswers() {
        return mSharedPreferences.getString(ANSWERS,"");
    }

    public void clearActiveResearchPrefs(@TAG String... tags) {
        for(String tag: tags) {
            getEditor().remove(tag).apply();
        }
    }
}
