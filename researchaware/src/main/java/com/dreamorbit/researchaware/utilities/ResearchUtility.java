package com.dreamorbit.researchaware.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.dreamorbit.researchaware.emergency.MyPhoneCallListener;
import com.dreamorbit.researchaware.helper.SdkConstant;

public class ResearchUtility {

    private static AlertDialog dialog = null;

    /**
     * Check permission before triggering the call
     */
    public static void permissionCheckEmergecnyCall(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                new PermissionsUtility().requestForCallPermission(activity);
            } else {
                triggerCall(activity);
            }
        } else {
            triggerCall(activity);
        }
    }

    /**
     * Trigger a emrgency call from phone
     * Show alert dialog for the confirmation
     *
     * @param context
     */
    public static void triggerCall(Context context) {
        if (!((Activity) context).isFinishing()) {
            showDialogOKCancel(context, "Do you want to make a emergency call?",
                    (dialog, which) -> {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                MyPhoneCallListener phoneCallListener = new MyPhoneCallListener();
                                TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                                telManager.listen(phoneCallListener, PhoneStateListener.LISTEN_CALL_STATE);
                                Intent phoneCallIntent = new Intent(Intent.ACTION_CALL);
                                phoneCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                phoneCallIntent.setData(Uri.parse("tel:+" + SdkConstant.EMERGENCY_NUMBER));
                                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                context.startActivity(phoneCallIntent);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    });
        }
    }

    /***********************************************
     * Show alert dialog with Ok and Cancel
     * @param message
     * @param okListener
     **********************************************/
    public static void showDialogOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {

        if (dialog != null) {
            dialog.dismiss();
        }
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        mBuilder.setMessage(message);
        mBuilder.setPositiveButton("Call", okListener);
        mBuilder.setNegativeButton("Cancel", okListener);
        dialog = mBuilder.create();
        dialog.show();
    }

    /**
     * Show alert dialog with Ok
     *
     * @param message
     * @param okListener
     */
    public static void showDialogOK(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setCancelable(false)
                .create()
                .show();
    }
}
