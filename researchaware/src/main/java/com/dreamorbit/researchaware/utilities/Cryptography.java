package com.dreamorbit.researchaware.utilities;

import java.security.GeneralSecurityException;

/**
 * Created by nareshkumar.reddy on 10/9/2017.
 */

public class Cryptography {

    private Cryptography(){

    }

    /**
     * encrpt a string using AESencrypt
     *
     * @param key
     * @return encryptString
     */
    public static String encrypt(String key, String encryptString){
        //Encrypt
        try {
            String encryptedMsg = AESCrypt.encrypt(key, encryptString);
            return encryptedMsg;
        }catch (GeneralSecurityException e){
            //handle error
            return null;
        }
    }


    /**
     * decrypt a string using AESencrypt
     *
     * @param key
     * @return decryptString
     */
    public static String decrypt(String key, String decryptString){
        try {
            String messageAfterDecrypt = AESCrypt.decrypt(key, decryptString);
            return messageAfterDecrypt;
        }catch (GeneralSecurityException e){
            //handle error - could be due to incorrect password or tampered encryptedMsg
            return null;
        }
    }

}
