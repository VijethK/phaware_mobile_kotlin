# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\D-Drive\Android\SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-dontwarn android.databinding.**
-dontwarn com.amazonaws.**
-dontwarn okhttp3.**
-dontwarn retrofit2.**
-dontwarn okio.**
-dontwarn com.squareup.okhttp3.**
-dontwarn javax.annotation.Nullable
-keep class android.databinding.** { *; }
-libraryjars <java.home>/lib/rt.jar (javax/xml/stream/** )

#-keep class javax.xml.crypto.dsig.** { *; }
#-dontwarn javax.xml.crypto.dsig.**
#-keep class javax.xml.crypto.** { *; }
#-dontwarn javax.xml.crypto.**
#-keep class org.spongycastle.** { *; }
#-dontwarn org.spongycastle.**
#-dontwarn com.itextpdf.**