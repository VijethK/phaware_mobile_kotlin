package com.dreamorbit.walktalktrack.shared

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.os.Environment
import android.os.Vibrator
import android.provider.Settings
import android.util.Patterns
import java.io.*
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by mujasam.bn on 9/18/2017.
 */
object SharedUtility {
    var receiver: BroadcastReceiver? = null
    private var mProgressBar: ProgressDialog? = null
    fun playDefaultSound(context: Context?) {
        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(context, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getAndroidId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    val currentDateTime: String
        get() {
            val dateFormatterR: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ")
            val currentDate = Date()
            return dateFormatterR.format(currentDate)
        }

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @return String representing date in specified format
     */
    fun getDateTime(milliSeconds: Long): String { // Create a DateFormatter object for displaying date in specified format.
        val dateFormatterR: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ")
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return dateFormatterR.format(calendar.time)
    }

    fun grantVibration(context: Context) {
        val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        v.vibrate(500) // Vibrate for 500 milliseconds
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    /**
     * Returns a hexadecimal encoded SHA-256 hash for the input String.
     */
    fun getSHA256Hash(data: String): String? {
        val result: String? = null
        try {
            val digest = MessageDigest.getInstance("SHA-256")
            val hash = digest.digest(data.toByteArray(StandardCharsets.UTF_8))
            return bytesToHex(hash) // make it printable
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return result
    }

    /**
     * convert byte array
     * to a hexadecimal string.
     *
     * @return string
     */
    private fun bytesToHex(hash: ByteArray): String {
        val hexString = StringBuffer()
        for (i in hash.indices) {
            val hex = Integer.toHexString(0xff and hash[i].toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        return hexString.toString()
    }

    fun unRegisterReciverForN(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.unregisterReceiver(receiver)
            receiver = null
        }
    }

    /**
     * Show progress bar
     */
    fun showProgressBar(context: Context?, message: String?) {
        mProgressBar = ProgressDialog(context)
        mProgressBar!!.setCancelable(false)
        mProgressBar!!.setMessage(message)
        mProgressBar!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressBar!!.isIndeterminate = true
        mProgressBar!!.show()
    }

    /**
     * Dismiss progress bar
     */
    fun dismissProgressBar() {
        if (mProgressBar != null && mProgressBar!!.isShowing) {
            mProgressBar!!.dismiss()
        }
    }

    fun pullDatabase() {
        try {
            val file = Environment.getExternalStorageDirectory()
            val data = Environment.getDataDirectory()
            if (file.canWrite()) {
                val currentPath = "/data/data/com.dreamorbit.walktalktrack/databases/Tasks.db"
                val copyPath = "Tasks.db"
                val currentDB = File(currentPath)
                val backupDB = File(file, copyPath)
                if (currentDB.exists()) {
                    val src = FileInputStream(currentDB).channel
                    val dst = FileOutputStream(backupDB).channel
                    dst.transferFrom(src, 0, src.size())
                    src.close()
                    dst.close()
                }
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    enum class AgeCheck {
        assent, adult, invalid
    }

    enum class BackgroundAction {
        upload_survey, reset_survey, upload_walk_test, upload_all_pending, upload_wear_data
    }
}