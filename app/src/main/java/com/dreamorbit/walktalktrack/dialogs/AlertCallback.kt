package com.dreamorbit.walktalktrack.dialogs

import android.content.DialogInterface

/**
 * Created by mujasam.bn on 11/14/2017.
 */
interface AlertCallback {
    fun PositiveMethod(dialog: DialogInterface?, id: Int)
    fun NegativeMethod(dialog: DialogInterface?, id: Int)
}