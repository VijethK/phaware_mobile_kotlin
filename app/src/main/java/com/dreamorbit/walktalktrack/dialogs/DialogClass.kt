package com.dreamorbit.walktalktrack.dialogs

import android.content.Context
import android.support.v7.app.AlertDialog

/**
 * Created by mujasam.bn on 11/14/2017.
 */
object DialogClass {
    @JvmStatic
    fun getAlertDialogOk(mContext: Context?, title: String?, msg: String?, positiveBtnCaption: String?, isCancelable: Boolean, target: AlertCallback) {
        val builder = AlertDialog.Builder(mContext!!)
        builder.setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(positiveBtnCaption) { dialog, id -> target.PositiveMethod(dialog, id) }
        val alert = builder.create()
        alert.setCancelable(isCancelable)
        alert.show()
        if (isCancelable) {
            alert.setOnCancelListener { target.NegativeMethod(null, 0) }
        }
    } /*public static void getAlertDialogOkCancel(Context mContext,String title, String msg, String positiveBtnCaption, String negativeBtnCaption, boolean isCancelable, final AlertCallback target) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        builder.setTitle(title).setMessage(msg).setCancelable(false).setPositiveButton(positiveBtnCaption, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                target.PositiveMethod(dialog, id);
            }
        }).setNegativeButton(negativeBtnCaption, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                target.NegativeMethod(dialog, id);
            }
        });

        AlertDialog alert = builder.create();
        alert.setCancelable(isCancelable);
        alert.show();
        if (isCancelable) {
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface arg0) {
                    target.NegativeMethod(null, 0);
                }
            });
        }
    }*/
}