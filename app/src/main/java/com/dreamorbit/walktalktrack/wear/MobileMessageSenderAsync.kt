package com.dreamorbit.walktalktrack.wear

import android.content.Intent
import android.os.AsyncTask
import android.support.annotation.WorkerThread
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.APP_LOGINED
import com.dreamorbit.walktalktrack.application.AppConstants.APP_LOGOUT
import com.dreamorbit.walktalktrack.application.AppConstants.LAUNCH_WEAR
import com.dreamorbit.walktalktrack.application.AppConstants.TEST_SCHEDULER_ENABLE
import com.dreamorbit.walktalktrack.application.AppConstants.ZIP_FILE_RECEIVED
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.Wearable
import java.util.*
import java.util.concurrent.ExecutionException

/**
 * Created by mujasam.bn on 2/22/2018.
 *
 *
 * 1 Ackonoledge Login success in Mobile
 * 2 Acknoledge Phone receiverd the zip file
 */
/*********************************************************************************
 * *
 * *
 * Wearable Communication Channel                                   *
 * *
 * *
 */
class MobileMessageSenderAsync : AsyncTask<Any?, Any?, Any?>() {
    override fun doInBackground(objects: Array<Any?>): Any? {
        val task = objects[0] as Int
        val nodes = nodes
        for (node in nodes) {
            if (task == APP_LOGINED) {
                sendStartActivityMessage(node)
            } else if (task == APP_LOGOUT) {
                sendStopActivityMessage(node)
            } else if (task == ZIP_FILE_RECEIVED) {
                val fileName = objects[1] as String?
                sendFileReceivedMessage(node, fileName)
            } else if (task == LAUNCH_WEAR) {
                sendStartWear(node)
            } else if (task == TEST_SCHEDULER_ENABLE) {
                val schedulerModel = objects[1] as String?
                sendTestEnableDisableStatus(node, schedulerModel)
            }
        }
        return null
    }

    private fun sendTestEnableDisableStatus(node: String, schedulerModel: String?) {
        SdkUtils.LOGD(TAG, "Node: Test Wear $node")
        val sendMessageTask = Wearable.getMessageClient(PhAwareApplication().getMyAppContext()).sendMessage(node, TEST_ENABLE_DISABLE, schedulerModel!!.toByteArray())
        try { // Block on a task and get the result synchronously (because this is on a background
// thread).
            val result = Tasks.await(sendMessageTask)
            SdkUtils.LOGD(TAG, "Test Wear sent: $result")
        } catch (exception: ExecutionException) {
            Log.e(TAG, "Test Wear failed: $exception")
        } catch (exception: InterruptedException) {
            Log.e(TAG, "Test Wear occurred: $exception")
        }
    }

    private fun sendFileReceivedMessage(node: String, fileName: String?) {
        SdkUtils.LOGD(TAG, "Node: $node File: $fileName")
        val sendMessageTask = Wearable.getMessageClient(PhAwareApplication().getMyAppContext()).sendMessage(node, FILE_RECEIVED, fileName!!.toByteArray())
        try { // Block on a task and get the result synchronously (because this is on a background
// thread).
            val result = Tasks.await(sendMessageTask)
            SdkUtils.LOGD(TAG, "Message sent: $result")
            //Show rating screen
//Send LocalBroadcast
//sendRatingAlert();
        } catch (exception: ExecutionException) {
            Log.e(TAG, "Task failed: $exception")
        } catch (exception: InterruptedException) {
            Log.e(TAG, "Interrupt occurred: $exception")
        }
    }

    /**
     * Once file received in phone from Wear app send
     * Broadcast message to local asking to show the Rating screen
     */
    private fun sendRatingAlert() {
        Log.d(TAG, "sendRatingAlert")
        val intent = Intent(AppConstants.RATING_ALERT)
        LocalBroadcastManager.getInstance(PhAwareApplication().getMyAppContext()).sendBroadcast(intent)
    }

    @WorkerThread
    protected fun sendStartActivityMessage(node: String) {
        SdkUtils.LOGD(TAG, "Node: $node")
        val sendMessageTask = Wearable.getMessageClient(PhAwareApplication().getMyAppContext()).sendMessage(node, START_ACTIVITY_PATH, ByteArray(0))
        try { // Block on a task and get the result synchronously (because this is on a background
// thread).
            val result = Tasks.await(sendMessageTask)
            SdkUtils.LOGD(TAG, "Message sent: $result")
        } catch (exception: ExecutionException) {
            Log.e(TAG, "Task failed: $exception")
        } catch (exception: InterruptedException) {
            Log.e(TAG, "Interrupt occurred: $exception")
        }
    }

    protected fun sendStartWear(node: String) {
        SdkUtils.LOGD(TAG, "Node: Launch Wear $node")
        val sendMessageTask = Wearable.getMessageClient(PhAwareApplication().getMyAppContext()).sendMessage(node, LAUNCH_WEAR_PATH, ByteArray(0))
        try { // Block on a task and get the result synchronously (because this is on a background
// thread).
            val result = Tasks.await(sendMessageTask)
            SdkUtils.LOGD(TAG, "Launch Wear sent: $result")
        } catch (exception: ExecutionException) {
            Log.e(TAG, "Launch Wear failed: $exception")
        } catch (exception: InterruptedException) {
            Log.e(TAG, "Launch Wear occurred: $exception")
        }
    }

    @WorkerThread
    fun sendStopActivityMessage(node: String) {
        SdkUtils.LOGD(TAG, "Node: $node")
        val sendMessageTask = Wearable.getMessageClient(PhAwareApplication().getMyAppContext()).sendMessage(node, STOP_ACTIVITY_PATH, ByteArray(0))
        try { // Block on a task and get the result synchronously (because this is on a background
// thread).
            val result = Tasks.await(sendMessageTask)
            SdkUtils.LOGD(TAG, "Message sent: $result")
        } catch (exception: ExecutionException) {
            Log.e(TAG, "Task failed: $exception")
        } catch (exception: InterruptedException) {
            Log.e(TAG, "Interrupt occurred: $exception")
        }
    }

    // Block on a task and get the result synchronously (because this is on a background
// thread).
    @get:WorkerThread
    protected val nodes: Collection<String>
        protected get() {
            val results = HashSet<String>()
            val nodeListTask = Wearable.getNodeClient(PhAwareApplication().getMyAppContext()).connectedNodes
            try { // Block on a task and get the result synchronously (because this is on a background
// thread).
                val nodes = Tasks.await(nodeListTask)
                for (node in nodes) {
                    if (node.isNearby) {
                        results.add(node.id)
                    }
                }
            } catch (exception: ExecutionException) {
                Log.e(TAG, "Task failed: $exception")
            } catch (exception: InterruptedException) {
                Log.e(TAG, "Interrupt occurred: $exception")
            }
            return results
        }

    companion object {
        const val FILE_RECEIVED = "/file-received"
        const val CASTING_MESSAGE = "/casting_message"
        const val TAG = "MobileMessageAsync"
        /*Wear*/
        protected const val START_ACTIVITY_PATH = "/start-activity"
        protected const val LAUNCH_WEAR_PATH = "/launch-wear"
        protected const val TEST_ENABLE_DISABLE = "/test_enable_disable"
        protected const val STOP_ACTIVITY_PATH = "/stop-activity"
    }
}