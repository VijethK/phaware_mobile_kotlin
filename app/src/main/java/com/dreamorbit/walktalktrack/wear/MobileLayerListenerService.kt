package com.dreamorbit.walktalktrack.wear

import android.content.Intent
import android.net.Uri
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.dreamorbit.pedometer.PedoUtils
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.ZIP_FILE_RECEIVED
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.report.ReportRepository
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.screens.casting.CastingActivity
import com.dreamorbit.walktalktrack.service.BackGroundService
import com.dreamorbit.walktalktrack.service.BackGroundService.Companion.enqueueBackgroundWork
import com.dreamorbit.walktalktrack.shared.CastingData
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.google.android.gms.wearable.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by mujasam.bn on 1/30/2018.
 */
class MobileLayerListenerService : WearableListenerService() {
    override fun onDataChanged(dataEvents: DataEventBuffer) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDataChanged: $dataEvents")
        }
        for (event in dataEvents) {
            if (event.type == DataEvent.TYPE_CHANGED) { // DataItem changed
                val item = event.dataItem
                if (item.uri.path.compareTo("/count") == 0) {
                    val dataMap = DataMapItem.fromDataItem(item).dataMap
                }
            } else if (event.type == DataEvent.TYPE_DELETED) { // DataItem deleted
            }
        }
        // Loop through the events and send a message
// to the node that created the data item.
        for (event in dataEvents) {
            val uri = event.dataItem.uri
            // Get the node id from the host value of the URI
            val nodeId = uri.host
            // Set the data of the message to be the bytes of the URI
            val payload = uri.toString().toByteArray()
            // Send the RPC
            Wearable.getMessageClient(this).sendMessage(
                    nodeId, SERVICE_CALLED_WEAR, payload)
        }
    }

    override fun onMessageReceived(messageEvent: MessageEvent) {
        super.onMessageReceived(messageEvent)
        if (messageEvent.path == MobileMessageSenderAsync.FILE_RECEIVED) {
            sendAcknoledgementWearToDeleteFile(String(messageEvent.data))
        } else if (messageEvent.path == MobileMessageSenderAsync.CASTING_MESSAGE) {
            sendCastingData(String(messageEvent.data))
        } else if (messageEvent.path == EMERGENCY_CALL_MESSAGE) {
            sendEmergencyCall() //trigering emergency call
        } else if (messageEvent.path == REPORT_SUMMARY_MESSAGE) {
            Log.e("Phone Summary reciv! ", String(messageEvent.data))
            saveSummaryDataInDatabaseFromPreference(String(messageEvent.data)) //Received Wear test summary data
        } else if (messageEvent.path == TEST_ENABLE_DISABLE) {
            val schedulerString = String(messageEvent.data)
            val gson = Gson()
            val token: TypeToken<List<SchedulerModel>> = object : TypeToken<List<SchedulerModel>>() {}
            val model = gson.fromJson<List<SchedulerModel>>(schedulerString, token.type)
            SchedulerRepository.instance?.saveScheduler(model)
            sendSchedulerStatus()
            Log.e(TAG, "MobileLayer updated model received from Wear: $schedulerString")
        }
    }

    private fun saveSummaryDataInDatabaseFromPreference(summary: String) {
        val gson = Gson()
        val type: TypeToken<List<PostReportRequest>> = object : TypeToken<List<PostReportRequest>>() {}
        val summaryList = gson.fromJson<List<PostReportRequest?>>(summary, type.type)
        if (summaryList != null && summaryList.size > 0) {
            saveSummary(summaryList)
            startBackgroundServiceUploadSummary()
        }
    }

    /**
     * Save summary object in database
     *
     * @param reportSummary
     * @return
     */
    fun saveSummary(reportSummary: List<PostReportRequest?>?): Long {
        val emailID = SharedPrefSingleton.instance!!.userEmail
        return ReportRepository.instance!!.saveReport(reportSummary, emailID)
    }

    override fun onChannelOpened(channel: ChannelClient.Channel) {
        super.onChannelOpened(channel)
        if (channel.path == PATH) {
            val file = SdkUtils.createUniqueFile(this)
            Wearable.getChannelClient(this).receiveFile(channel, Uri.fromFile(file), false)
                    .addOnSuccessListener { task: Void? -> Log.e("File received! ", "OnSuccess") }
        }
    }

    /***********************************************
     * once Mobile recieved the file send the message
     * to delete that received file
     * @param fileName
     */
    private fun sendAcknoledgementWearToDeleteFile(fileName: String) {
        MobileMessageSenderAsync().execute(ZIP_FILE_RECEIVED, fileName)
    }

    override fun onChannelClosed(channel: ChannelClient.Channel, i: Int, i1: Int) {
        super.onChannelClosed(channel, i, i1)
        Log.e("File received! ", "onChannelClosed")
        //startBackGroundService();
    }

    override fun onInputClosed(channel: ChannelClient.Channel, i: Int, i1: Int) {
        super.onInputClosed(channel, i, i1)
        Log.e("File received! ", "onInputClosed")
        startBackGroundService()
        sendOfflineAlert()
    }

    private fun startBackGroundService() {
        val intent = Intent(PhAwareApplication().getMyAppContext(), BackGroundService::class.java)
        intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_wear_data.toString())
        enqueueBackgroundWork(PhAwareApplication().getMyAppContext(), intent)
    }

    /**
     * Start the upload survey background task
     */
    private fun startBackgroundServiceUploadSummary() {
        val intent = Intent(PhAwareApplication().getMyAppContext(), BackGroundService::class.java)
        intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_summary_data.toString())
        enqueueBackgroundWork(PhAwareApplication().getMyAppContext(), intent)
    }

    /**
     * Send an Intent with an action named "custom-event-name". The Intent sent should
     * be received by the ReceiverActivity.
     */
    private fun sendCastingData(message: String) {
        Log.d(TAG, message)
        val gson = Gson()
        val castingData = gson.fromJson(message, CastingData::class.java)
        if (castingData != null) {
            SharedPrefSingleton.instance!!.saveCastData(message)
            PedoUtils.insertHeartRateDataToGoogleFit(castingData.heartRate, this)
            PedoUtils.insertStepDataToGoogleFit(castingData.step, this)
        }
        val intent = Intent(CastingActivity.WEAR_MESSAGE_INTENT_FILTER)
        intent.putExtra(CastingActivity.WEAR_MESSAGE, message)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * Send an Intent with an action named "custom-event-name". The Intent sent should
     * be received by the ReceiverActivity.
     */
    private fun sendEmergencyCall() {
        Log.d(TAG, "Emergency Call")
        val intent = Intent(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * Send an Intent with an action named "custom-event-name". The Intent sent should
     * be received by the ReceiverActivity.
     */
    private fun sendSchedulerStatus() {
        Log.d(TAG, "Send Scheduler Status ")
        val intent = Intent(AppConstants.WEAR_MESSAGE_SCHEDULER_STATUS)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    /**
     * Send message to show offline alert dialog
     */
    private fun sendOfflineAlert() {
        Log.d(TAG, "Send Offline Status ")
        val intent = Intent(AppConstants.OFFLINE_ALERT)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    companion object {
        const val REPORT_SUMMARY_MESSAGE = "/report_summary_message"
        const val EMERGENCY_CALL_MESSAGE = "/emergency_call_message"
        const val TEST_ENABLE_DISABLE = "/test_enable_disable"
        private const val TAG = "MobileLayerListener"
        private const val PATH = "/path"
        var SERVICE_CALLED_WEAR = "WearWalkTestCompleted"
    }
}