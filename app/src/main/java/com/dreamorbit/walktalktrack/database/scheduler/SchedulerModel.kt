package com.dreamorbit.walktalktrack.database.scheduler

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SchedulerModel {
    @SerializedName("schedule_type")
    @Expose
    var scheduleType: String? = null
    @SerializedName("schedule_days")
    @Expose
    val scheduleDays: List<String>? = null
    @SerializedName("study_token")
    @Expose
    private val studyToken: String? = null
    /*public void setScheduleDays(List<String> scheduleDays) {
        this.scheduleDays = scheduleDays;
    }

    public String getStudyToken() {
        return studyToken;
    }

    public void setStudyToken(String studyToken) {
        this.studyToken = studyToken;
    }*/
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("scheduled_at")
    @Expose
    var scheduledAt: String? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    //DB
    private var testTakenAt: String? = null
    var status: String? = null
    var day: String? = null
    /*public String getTestTakenAt() {
        return testTakenAt;
    }*/
    fun setTestTakenAt(testTakenAt: String?) {
        this.testTakenAt = testTakenAt
    }

    companion object {
        const val TEST_TAKEN = "test_taken"
        const val TEST_PENDING = "pending"
    }
}