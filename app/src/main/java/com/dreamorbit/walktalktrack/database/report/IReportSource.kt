package com.dreamorbit.walktalktrack.database.report

import com.dreamorbit.researchaware.model.report.PostReportRequest

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
interface IReportSource {
    fun getReport(userName: String?): List<PostReportRequest?>?
    fun saveReport(report: List<PostReportRequest?>?, userName: String?): Long
    fun deleteReport(reportID: PostReportRequest?)
    fun deleteReportData()
}