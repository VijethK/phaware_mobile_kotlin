package com.dreamorbit.walktalktrack.database.scheduler

interface ISchedulerSource {
    fun getScheduler(activityId: Int): SchedulerModel?
    val scheduler: List<SchedulerModel?>?
    fun saveScheduler(schedulerModel: List<SchedulerModel?>?): Long
    fun saveSchedulerStatus(schedulerStatus: String?, testTakenAt: String?, surveyID: Int): Long
    fun deleteSchedulerData()
    fun getActivityStatus(activityId: Int): Boolean
}