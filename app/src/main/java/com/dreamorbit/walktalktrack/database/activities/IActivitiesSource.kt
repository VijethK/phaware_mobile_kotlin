package com.dreamorbit.walktalktrack.database.activities

import com.dreamorbit.walktalktrack.pojo.activities.ActivityList

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
interface IActivitiesSource {
    fun saveData(activities: List<ActivityList?>?): Long
    fun deleteActivities()
    val activities: List<ActivityList?>?
    fun updateSurveyCompleted(surveyId: Int): Long
}