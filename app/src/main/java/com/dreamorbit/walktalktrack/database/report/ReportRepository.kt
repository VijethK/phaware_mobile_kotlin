package com.dreamorbit.walktalktrack.database.report

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import android.util.Log
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns
import java.util.*

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
class ReportRepository @SuppressLint("RestrictedApi") private constructor(context: Context) : IReportSource {
    private val mDbHelper: DbHelper
    override fun getReport(userName: String?): List<PostReportRequest?>? {
        val list: MutableList<PostReportRequest?> = ArrayList()
        val db = mDbHelper.readableDatabase
        val query = "SELECT * FROM " + DbTableColumns.ReportSummary.TABLE_NAME + " WHERE " + DbTableColumns.ReportSummary.USER_NAME + "='" + userName + "' "
        val cursor = db.rawQuery(query, null)
        if (cursor != null && cursor.count > 0) {
            while (cursor.moveToNext()) {
                val summary = PostReportRequest()
                summary.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.ReportSummary._ID))
                summary.username = cursor.getString(cursor.getColumnIndex(DbTableColumns.ReportSummary.USER_NAME))
                summary.max_heart_rate = cursor.getInt(cursor.getColumnIndex(DbTableColumns.ReportSummary.MAX_HEART_RATE))
                summary.average_heart_rate = cursor.getInt(cursor.getColumnIndex(DbTableColumns.ReportSummary.AVERAGE_HEART_RATE))
                summary.distance = cursor.getDouble(cursor.getColumnIndex(DbTableColumns.ReportSummary.DISTANCE))
                summary.duration_of_test = cursor.getString(cursor.getColumnIndex(DbTableColumns.ReportSummary.DURATION_OF_TEST))
                summary.test_taken_at = cursor.getString(cursor.getColumnIndex(DbTableColumns.ReportSummary.TEST_TAKEN_AT))
                summary.steps_count = cursor.getLong(cursor.getColumnIndex(DbTableColumns.ReportSummary.STEPS_COUNT))
                list.add(summary)
            }
        }
        cursor?.close()
        db.close()
        return list
    }

    @SuppressLint("RestrictedApi")
    override fun saveReport(reportList: List<PostReportRequest?>?, userName: String?): Long {
        var row: Long = 0
        Preconditions.checkNotNull(reportList)
        val db = mDbHelper.writableDatabase
        for (report in reportList!!) {
            val values = ContentValues()
            values.put(DbTableColumns.ReportSummary.USER_NAME, userName)
            values.put(DbTableColumns.ReportSummary.MAX_HEART_RATE, report!!.max_heart_rate)
            values.put(DbTableColumns.ReportSummary.AVERAGE_HEART_RATE, report.average_heart_rate)
            values.put(DbTableColumns.ReportSummary.DISTANCE, report.distance)
            values.put(DbTableColumns.ReportSummary.DURATION_OF_TEST, report.duration_of_test)
            values.put(DbTableColumns.ReportSummary.TEST_TAKEN_AT, report.test_taken_at)
            values.put(DbTableColumns.ReportSummary.STEPS_COUNT, report.steps_count)
            row = db.insert(DbTableColumns.ReportSummary.TABLE_NAME, null, values)
        }
        db.close()
        return row
    }

    override fun deleteReport(reportRequest: PostReportRequest?) {
        val db = mDbHelper.writableDatabase
        val deleted = db.delete(DbTableColumns.ReportSummary.TABLE_NAME, DbTableColumns.ReportSummary.USER_NAME + "=? AND " + DbTableColumns.ReportSummary._ID + "=? ", arrayOf(reportRequest!!.username, reportRequest.id.toString()))
        Log.e("Report Deleted ", "" + deleted)
        db.close()
    }

    override fun deleteReportData() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.ReportSummary.TABLE_NAME, null, null)
        db.close()
    }

    companion object {
        private var INSTANCE: ReportRepository? = null
        @JvmStatic
        val instance: ReportRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = ReportRepository(PhAwareApplication.context)
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        Preconditions.checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}