package com.dreamorbit.walktalktrack.database.scheduler

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import android.text.TextUtils
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns
import java.util.*

class SchedulerRepository @SuppressLint("RestrictedApi") private constructor(context: Context) : ISchedulerSource {
    private val mDbHelper: DbHelper
    /**
     * Saving scheduler data from FCM notification
     *
     * @param schedulerModel
     * @return
     */
    override fun saveScheduler(schedulerModel: List<SchedulerModel?>?): Long {
        var row: Long = 0
        val db = mDbHelper.readableDatabase
        var status: String? = null
        for (model in schedulerModel!!) {
            val query = "SELECT * FROM " + DbTableColumns.Scheduler.TABLE_NAME + " WHERE " + DbTableColumns.Scheduler.SURVEY_ID + " = " + model!!.id
            val cursor = db.rawQuery(query, null)
            if (cursor.count > 0 && cursor.moveToFirst()) {
                status = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_STATUS)) //initially save empty or null in status so that user can take any number of test
            }
            status = if (TextUtils.isEmpty(model.status)) status else model.status //insert previous status
            val values = ContentValues()
            values.put(DbTableColumns.Scheduler.SCHEDULER_SEQUENCE, model.scheduleType)
            values.put(DbTableColumns.Scheduler.SCHEDULER_TIME, model.scheduledAt)
            values.put(DbTableColumns.Scheduler.SCHEDULER_STATUS, status) //initially save empty or null in status
            values.put(DbTableColumns.Scheduler.SCHEDULER_DAY, if (model.scheduleDays != null && model.scheduleDays.size > 0) model.scheduleDays.get(0) else model.day)
            values.put(DbTableColumns.Scheduler.SURVEY_ID, model.id)
            values.put(DbTableColumns.Scheduler.SURVEY_NAME, model.name)
            row = if (cursor.count > 0) {
                db.update(DbTableColumns.Scheduler.TABLE_NAME, values, DbTableColumns.Scheduler.SURVEY_ID + " = ?", arrayOf(model.id.toString())).toLong()
            } else {
                db.insert(DbTableColumns.Scheduler.TABLE_NAME, null, values)
            }
        }
        //db.close();
        return row
    }

    override fun saveSchedulerStatus(schedulerStatus: String?, statusUpdatedAt: String?, surveyID: Int): Long {
        val row: Long
        val db = mDbHelper.readableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Scheduler.SCHEDULER_STATUS, schedulerStatus)
        values.put(DbTableColumns.Scheduler.TEST_TAKEN_AT, statusUpdatedAt)
        row = db.update(DbTableColumns.Scheduler.TABLE_NAME, values, DbTableColumns.Scheduler.SURVEY_ID + " = ?", arrayOf(surveyID.toString())).toLong()
        return row
    }

    override fun deleteSchedulerData() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.Scheduler.TABLE_NAME, null, null)
        db.close()
    }

    override fun getActivityStatus(activityId: Int): Boolean {
        val db = mDbHelper.readableDatabase
        val query = "SELECT * FROM " + DbTableColumns.Scheduler.TABLE_NAME + " WHERE " + DbTableColumns.Scheduler.SURVEY_ID + " = " + activityId + " AND " +
                DbTableColumns.Scheduler.SCHEDULER_STATUS + " = '" + SchedulerModel.TEST_TAKEN + "'"
        val cursor = db.rawQuery(query, null)
        return cursor.count > 0
    }

    override fun getScheduler(activityId: Int): SchedulerModel? {
        val db = mDbHelper.readableDatabase
        val query = "SELECT * FROM " + DbTableColumns.Scheduler.TABLE_NAME + " WHERE " + DbTableColumns.Scheduler.SURVEY_ID + " = " + activityId
        val cursor = db.rawQuery(query, null)
        var model: SchedulerModel? = null
        while (cursor.moveToNext()) {
            model = SchedulerModel()
            model.scheduledAt = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_TIME))
            model.scheduleType = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_SEQUENCE))
            model.status = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_STATUS))
            model.day = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_DAY))
            model.setTestTakenAt(cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.TEST_TAKEN_AT)))
            model.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Scheduler.SURVEY_ID))
            model.name = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SURVEY_NAME))
        }
        //db.close();
        return model
    }

    //db.close();
    override val scheduler: List<SchedulerModel>
        get() {
            val db = mDbHelper.readableDatabase
            val query = "SELECT * FROM " + DbTableColumns.Scheduler.TABLE_NAME
            val cursor = db.rawQuery(query, null)
            val fcmSchedulerDataList: MutableList<SchedulerModel> = ArrayList()
            while (cursor.moveToNext()) {
                val model = SchedulerModel()
                model.scheduledAt = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_TIME))
                model.scheduleType = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_SEQUENCE))
                model.status = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_STATUS))
                model.day = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SCHEDULER_DAY))
                model.setTestTakenAt(cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.TEST_TAKEN_AT)))
                model.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Scheduler.SURVEY_ID))
                model.name = cursor.getString(cursor.getColumnIndex(DbTableColumns.Scheduler.SURVEY_NAME))
                fcmSchedulerDataList.add(model)
            }
            //db.close();
            return fcmSchedulerDataList
        }

    companion object {
        private var INSTANCE: SchedulerRepository? = null
        val instance: SchedulerRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = SchedulerRepository(PhAwareApplication().getMyAppContext())
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        Preconditions.checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}