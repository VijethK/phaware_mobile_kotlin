package com.dreamorbit.walktalktrack.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_JOIN_STUDY)
        db.execSQL(SQL_CREATE_REGISTRATION)
        db.execSQL(SQL_CREATE_FLOW)
        db.execSQL(SQL_ADDITIONALINFORMATION_FLOW)
        db.execSQL(SQL_CREATE_ACTIVITIES)
        db.execSQL(SQL_CREATE_QUESTIONS)
        db.execSQL(SQL_CREATE_QUESTIONS_OPTIONS)
        db.execSQL(SQL_CREATE_REPORT_SUMMARY)
        db.execSQL(SQL_CREATE_SCHEDULER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) { // Not required as at version 1
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) { // Not required as at version 1
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "Tasks.db"
        private const val TEXT_TYPE = " TEXT"
        private const val INTEGER_TYPE = " INTEGER"
        private const val LONG_TYPE = " LONG"
        private const val DOUBLE_TYPE = " DOUBLE"
        private const val BOOLEAN_TYPE = " INTEGER"
        private const val COMMA_SEP = ","
        private const val SQL_CREATE_JOIN_STUDY = "CREATE TABLE " + DbTableColumns.JoinStudy.TABLE_NAME + " (" +
                DbTableColumns.JoinStudy.COLUMN_NAME_ID + TEXT_TYPE + " PRIMARY KEY," +
                DbTableColumns.JoinStudy.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_MONTH + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_DAY + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_AGE + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_STATUS + TEXT_TYPE +
                " )"
        private const val SQL_CREATE_REGISTRATION = "CREATE TABLE " + DbTableColumns.Registartion.TABLE_NAME + " (" +
                DbTableColumns.Registartion._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.Registartion.COLUMN_NAME_EMAIL + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Registartion.COLUMN_NAME_USERNAME + INTEGER_TYPE +
                " )"
        private const val SQL_CREATE_FLOW = "CREATE TABLE " + DbTableColumns.Flow.TABLE_NAME + " (" +
                DbTableColumns.Flow.SCREEN_NAME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Flow.STATUS + BOOLEAN_TYPE +
                " )"
        private const val SQL_ADDITIONALINFORMATION_FLOW = "CREATE TABLE " + DbTableColumns.AdditionalInformation.TABLE_NAME + " (" +
                DbTableColumns.AdditionalInformation._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.AdditionalInformation.GENDER + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.AdditionalInformation.HEIGHT + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.AdditionalInformation.WEIGHT + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.AdditionalInformation.DIAGNOISED_PH + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.AdditionalInformation.MEDICATION_PH + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.AdditionalInformation.WEARABLE + TEXT_TYPE +
                " )"
        private const val SQL_CREATE_ACTIVITIES = "CREATE TABLE " + DbTableColumns.Activities.TABLE_NAME + " (" +
                DbTableColumns.Activities._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.Activities.NAME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Activities.SCHEDULE_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Activities.ACTIVITY_TYPE_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Activities.CREATED_AT + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Activities.UPDATED_AT + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Activities.USER_GROUP_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Activities.ACTIVITY_ANSWERED + INTEGER_TYPE +
                " )"
        private const val SQL_CREATE_QUESTIONS = "CREATE TABLE " + DbTableColumns.Questions.TABLE_NAME + " (" +
                DbTableColumns.Questions._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.Questions.IDENTIFIER + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Questions.FIELD_TYPE_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Questions.ACTIVITY_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Questions.QUESTION + TEXT_TYPE +
                " )"
        private const val SQL_CREATE_QUESTIONS_OPTIONS = "CREATE TABLE " + DbTableColumns.QuestionOptions.TABLE_NAME + " (" +
                DbTableColumns.QuestionOptions._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.QuestionOptions.NAME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.QuestionOptions.IMAGE + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.QuestionOptions.QUESTION_ID + INTEGER_TYPE +
                " )"
        private const val SQL_CREATE_REPORT_SUMMARY = "CREATE TABLE " + DbTableColumns.ReportSummary.TABLE_NAME + " (" +
                DbTableColumns.ReportSummary._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.ReportSummary.USER_NAME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.AVERAGE_HEART_RATE + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.MAX_HEART_RATE + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.DISTANCE + DOUBLE_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.STEPS_COUNT + LONG_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.DURATION_OF_TEST + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.ReportSummary.TEST_TAKEN_AT + TEXT_TYPE +
                " )"
        private const val SQL_CREATE_SCHEDULER = "CREATE TABLE " + DbTableColumns.Scheduler.TABLE_NAME + " (" +
                DbTableColumns.Scheduler._ID + INTEGER_TYPE + " PRIMARY KEY," +
                DbTableColumns.Scheduler.SCHEDULER_SEQUENCE + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_TIME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_DAY + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_STATUS + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.SURVEY_ID + INTEGER_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.SURVEY_NAME + TEXT_TYPE + COMMA_SEP +
                DbTableColumns.Scheduler.TEST_TAKEN_AT + TEXT_TYPE +
                " )"
    }
}