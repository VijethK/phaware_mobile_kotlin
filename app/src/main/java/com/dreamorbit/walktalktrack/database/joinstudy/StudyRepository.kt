/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dreamorbit.walktalktrack.database.joinstudy

import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns

/**
 * Concrete implementation to load tasks from the data sources into a cache.
 *
 *
 * For simplicity, this implements a dumb synchronisation between locally persisted data and data
 * obtained from the server, by using the remote data source only if the local database doesn't
 * exist or is empty.
 */
class StudyRepository private constructor(context: Context) : IStudyDataSource {
    private val mDbHelper: DbHelper// This will be called if the table is new or just empty.//String Id = c.getSignature(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_ID));
    //DbTableColumns.JoinStudy.COLUMN_NAME_ID,
    override val studyData: StudyData?
        get() {
            var studyData: StudyData? = null
            val db = mDbHelper.readableDatabase
            val projection = arrayOf( //DbTableColumns.JoinStudy.COLUMN_NAME_ID,
                    DbTableColumns.JoinStudy.COLUMN_NAME_YEAR,
                    DbTableColumns.JoinStudy.COLUMN_NAME_MONTH,
                    DbTableColumns.JoinStudy.COLUMN_NAME_DAY,
                    DbTableColumns.JoinStudy.COLUMN_NAME_AGE,
                    DbTableColumns.JoinStudy.COLUMN_NAME_STATUS
            )
            val c = db.query(
                    DbTableColumns.JoinStudy.TABLE_NAME, projection, null, null, null, null, null)
            if (c != null && c.count > 0) {
                while (c.moveToNext()) { //String Id = c.getSignature(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_ID));
                    val year = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_YEAR))
                    val month = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_MONTH))
                    val day = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_DAY))
                    val age = c.getString(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_AGE))
                    val status = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_STATUS)) == 1
                    studyData = StudyData(year, month, day, age, status)
                }
            }
            c?.close()
            db.close()
            return studyData
        }

    override fun saveStudyData(studyData: StudyData) {
        checkNotNull(studyData)
        val db = mDbHelper.writableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_YEAR, studyData.year)
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_MONTH, studyData.month)
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_DAY, studyData.day)
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_AGE, studyData.age)
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_STATUS, studyData.status)
        db.insert(DbTableColumns.JoinStudy.TABLE_NAME, null, values)
        db.close()
    }

    override fun deleteStudyData() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.JoinStudy.TABLE_NAME, null, null)
        db.close()
    }

    companion object {
        private var INSTANCE: StudyRepository? = null
        /**
         * Singleton class to create a single instance for this class
         *
         * @return INSTANCE
         */
        @JvmStatic
        val instance: StudyRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = StudyRepository(PhAwareApplication.context)
                }
                return INSTANCE
            }
    }

    /**
     * Prevent direct instantiation.
     */
    init {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}