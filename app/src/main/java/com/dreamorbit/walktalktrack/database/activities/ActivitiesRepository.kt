package com.dreamorbit.walktalktrack.database.activities

import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns
import com.dreamorbit.walktalktrack.pojo.activities.ActivityList
import java.util.*

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
 abstract class ActivitiesRepository private constructor(context: Context) : IActivitiesSource {
    private val mDbHelper: DbHelper
    override fun saveData(activities: List<ActivityList?>?): Long {
        var row: Long = 0
        val db = mDbHelper.writableDatabase
        for (activity in activities!!.iterator()) {  // added !!.iterator()
            val values = ContentValues()
            //values.put(DbTableColumns.Activities._ID, activity.id)
            values.put(DbTableColumns.Activities.NAME, activity?.name)
            values.put(DbTableColumns.Activities.SCHEDULE_ID, activity?.scheduleId)
            values.put(DbTableColumns.Activities.ACTIVITY_TYPE_ID, activity?.activityTypeId)
            values.put(DbTableColumns.Activities.CREATED_AT, activity?.createdAt)
            values.put(DbTableColumns.Activities.UPDATED_AT, activity?.updatedAt)
            values.put(DbTableColumns.Activities.USER_GROUP_ID, activity?.userGroupId)
            values.put(DbTableColumns.Activities.ACTIVITY_ANSWERED, false)
            row = db.insert(DbTableColumns.Activities.TABLE_NAME, null, values)
        }
        db.close()
        return row
    }

    override fun deleteActivities() {
        val db = mDbHelper.readableDatabase
        db.execSQL("delete from " + DbTableColumns.Activities.TABLE_NAME)
    }

    //Kotlin error
    /*override fun getActivities(): List<ActivityList> {
        val db = mDbHelper.readableDatabase
        val query = "SELECT * FROM " + DbTableColumns.Activities.TABLE_NAME
        val cursor = db.rawQuery(query, null)
        val activityList: MutableList<ActivityList> = ArrayList()
        while (cursor.moveToNext()) {
            val activities = ActivityList()
            //activities.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Activities._ID))
            activities.name = cursor.getString(cursor.getColumnIndex(DbTableColumns.Activities.NAME))
            activities.scheduleId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Activities.SCHEDULE_ID))
            activities.activityTypeId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Activities.ACTIVITY_TYPE_ID))
            activities.createdAt = cursor.getString(cursor.getColumnIndex(DbTableColumns.Activities.CREATED_AT))
            activities.updatedAt = cursor.getString(cursor.getColumnIndex(DbTableColumns.Activities.UPDATED_AT))
            activities.userGroupId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Activities.USER_GROUP_ID))
            activities.isActivityAnswered = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Activities.ACTIVITY_ANSWERED))
            activityList.add(activities)
        }
        db.close()
        return activityList
    }*/

    override fun updateSurveyCompleted(activityId: Int): Long {
        val db = mDbHelper.readableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Activities.ACTIVITY_ANSWERED, true)
        return db.update(DbTableColumns.Activities.TABLE_NAME, values,   null, null).toLong()
    }

    companion object {
        private var INSTANCE: ActivitiesRepository? = null
        @JvmStatic
        val instance: ActivitiesRepository?
            get() {
                if (INSTANCE == null) {
                   // INSTANCE = ActivitiesRepository(PhAwareApplication.getMyAppContext())
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}