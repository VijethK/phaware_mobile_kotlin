package com.dreamorbit.walktalktrack.database.flow

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
 class FlowRepository private constructor(context: Context) : IFlowSource {
    private val mDbHelper: DbHelper
    override fun getAssentStatus(screenName: String?): Boolean {
        var status = false
        val db = mDbHelper.readableDatabase
        val projection = arrayOf(
                DbTableColumns.Flow.STATUS
        )
        val selectionArgs = arrayOf(
                screenName
        )
        val c = db.query(
                DbTableColumns.Flow.TABLE_NAME, projection, DbTableColumns.Flow.SCREEN_NAME + "=?", selectionArgs, null, null, null)
        if (c != null && c.count > 0) {
            while (c.moveToNext()) {
                status = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.Flow.STATUS)) != 0
            }
        }
        c?.close()
        db.close()
        return status
    }
    @SuppressLint("RestrictedApi")
    override fun saveFlow(flow_status: String?, status: Boolean): Long {
        Preconditions.checkNotNull(status)
        val db = mDbHelper.writableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Flow.SCREEN_NAME, flow_status)
        values.put(DbTableColumns.Flow.STATUS, status)
        val row = db.insert(DbTableColumns.Flow.TABLE_NAME, null, values)
        db.close()
        return row
    }

    override fun deleteFlow() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.Flow.TABLE_NAME, null, null)
        db.close()
    }

    companion object {
        private var INSTANCE: FlowRepository? = null
        @JvmStatic
        val instance: FlowRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = FlowRepository(PhAwareApplication.context)
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}