package com.dreamorbit.walktalktrack.database.registration

/**
 * Created by nareshkumar.reddy on 10/10/2017.
 */
class RegistartionData {
    var email: String? = null
    var email_sha: String? = null
    var userName: String? = null

    constructor() {}
    constructor(email: String?, email_sha: String?, userName: String?) {
        this.email = email
        this.email_sha = email_sha
        this.userName = userName
    }

}