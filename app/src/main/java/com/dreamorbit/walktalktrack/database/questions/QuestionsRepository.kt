package com.dreamorbit.walktalktrack.database.questions

import android.content.ContentValues
import android.content.Context
import android.support.v4.util.Preconditions
import com.dreamorbit.researchaware.model.survey.Options
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.researchaware.model.survey.Questions
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns
import java.util.*

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
class QuestionsRepository private constructor(context: Context) : IQuestionsSource {
    private val mDbHelper: DbHelper
    override fun deleteQuestions() {
        val db = mDbHelper.readableDatabase
        db.execSQL("delete from " + DbTableColumns.Questions.TABLE_NAME)
        db.execSQL("delete from " + DbTableColumns.QuestionOptions.TABLE_NAME)
    }

    //Query Question answer options
    override val questions: List<Questions>
        get() {
            val db = mDbHelper.readableDatabase
            val query = "SELECT * FROM " + DbTableColumns.Questions.TABLE_NAME
            val cursor = db.rawQuery(query, null)
            val questionList: MutableList<Questions> = ArrayList()
            while (cursor.moveToNext()) {
                val questions = Questions()
                questions.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions._ID))
                questions.question = cursor.getString(cursor.getColumnIndex(DbTableColumns.Questions.QUESTION))
                questions.identifier = cursor.getString(cursor.getColumnIndex(DbTableColumns.Questions.IDENTIFIER))
                questions.fieldTypeId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions.FIELD_TYPE_ID))
                questions.activityId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions.ACTIVITY_ID))
                //Query Question answer options
                val queryOptions = "SELECT * FROM " + DbTableColumns.QuestionOptions.TABLE_NAME + " WHERE " + DbTableColumns.QuestionOptions.QUESTION_ID + " = " + questions.id
                val cursorOption = db.rawQuery(queryOptions, null)
                val optionList: MutableList<Options> = ArrayList()
                while (cursorOption.moveToNext()) {
                    val questionOptions = Options()
                    questionOptions.id = cursorOption.getInt(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions._ID))
                    questionOptions.name = cursorOption.getString(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions.NAME))
                    questionOptions.image = cursorOption.getString(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions.IMAGE))
                    optionList.add(questionOptions)
                }
                questions.options = optionList
                questionList.add(questions)
            }
            db.close()
            return questionList
        }

    override fun saveQuestions(questions: QuestionList?): Long {
        var row: Long = 0
        val db = mDbHelper.writableDatabase
        val questionsList = questions!!.questions
        for (quest in questionsList) {
            val values = ContentValues()
            values.put(DbTableColumns.Questions._ID, quest.id)
            values.put(DbTableColumns.Questions.QUESTION, quest.question)
            values.put(DbTableColumns.Questions.IDENTIFIER, quest.identifier)
            values.put(DbTableColumns.Questions.FIELD_TYPE_ID, quest.fieldTypeId)
            values.put(DbTableColumns.Questions.ACTIVITY_ID, quest.activityId)
            row = db.insert(DbTableColumns.Questions.TABLE_NAME, null, values)
            for (options in quest.options) {
                val valuesOption = ContentValues()
                valuesOption.put(DbTableColumns.QuestionOptions._ID, options.id)
                valuesOption.put(DbTableColumns.QuestionOptions.NAME, options.name)
                valuesOption.put(DbTableColumns.QuestionOptions.IMAGE, options.image)
                valuesOption.put(DbTableColumns.QuestionOptions.QUESTION_ID, quest.id)
                db.insert(DbTableColumns.QuestionOptions.TABLE_NAME, null, valuesOption)
            }
        }
        db.close()
        return row
    }

    override fun getSurveyQuestions(activityId: Int): List<Questions?>? {
        val db = mDbHelper.readableDatabase
        val query = "SELECT * FROM " + DbTableColumns.Questions.TABLE_NAME + " WHERE " + DbTableColumns.Questions.ACTIVITY_ID + " = " + activityId
        val cursor = db.rawQuery(query, null)
        val questionList: MutableList<Questions?> = ArrayList()
        while (cursor.moveToNext()) {
            val questions = Questions()
            questions.id = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions._ID))
            questions.question = cursor.getString(cursor.getColumnIndex(DbTableColumns.Questions.QUESTION))
            questions.identifier = cursor.getString(cursor.getColumnIndex(DbTableColumns.Questions.IDENTIFIER))
            questions.fieldTypeId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions.FIELD_TYPE_ID))
            questions.activityId = cursor.getInt(cursor.getColumnIndex(DbTableColumns.Questions.ACTIVITY_ID))
            //Query Question answer options
            val queryOptions = "SELECT * FROM " + DbTableColumns.QuestionOptions.TABLE_NAME + " WHERE " + DbTableColumns.QuestionOptions.QUESTION_ID + " = " + questions.id
            val cursorOption = db.rawQuery(queryOptions, null)
            val optionList: MutableList<Options> = ArrayList()
            while (cursorOption.moveToNext()) {
                val questionOptions = Options()
                questionOptions.id = cursorOption.getInt(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions._ID))
                questionOptions.name = cursorOption.getString(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions.NAME))
                questionOptions.image = cursorOption.getString(cursorOption.getColumnIndex(DbTableColumns.QuestionOptions.IMAGE))
                optionList.add(questionOptions)
            }
            questions.options = optionList
            questionList.add(questions)
        }
        db.close()
        return questionList
    }

    companion object {
        private var INSTANCE: QuestionsRepository? = null
        @JvmStatic
        val instance: QuestionsRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = QuestionsRepository(PhAwareApplication().getMyAppContext())
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
    }
}