package com.dreamorbit.walktalktrack.database.flow

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
interface IFlowSource {
    fun getAssentStatus(screenName: String?): Boolean
    fun saveFlow(screenName: String?, status: Boolean): Long
    fun deleteFlow()
}