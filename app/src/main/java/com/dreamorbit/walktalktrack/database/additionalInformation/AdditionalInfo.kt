package com.dreamorbit.walktalktrack.database.additionalInformation

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by nareshkumar.reddy on 10/24/2017.
 */
class AdditionalInfo : Parcelable {
    @JvmField
    var gender: String? = null
    var height: String? = null
    var weigh: String? = null
    var diagnosed: String? = null
    var medication: String? = null
    var wear: String? = null

    constructor() {}
    constructor(gender: String?, height: String?, weigh: String?, diagnosed: String?, medication: String?, wear: String?) {
        this.gender = gender
        this.height = height
        this.weigh = weigh
        this.diagnosed = diagnosed
        this.medication = medication
        this.wear = wear
    }

    protected constructor(`in`: Parcel) {
        gender = `in`.readString()
        height = `in`.readString()
        weigh = `in`.readString()
        diagnosed = `in`.readString()
        medication = `in`.readString()
        wear = `in`.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeString(gender)
        parcel.writeString(height)
        parcel.writeString(weigh)
        parcel.writeString(diagnosed)
        parcel.writeString(medication)
        parcel.writeString(wear)
    }

    /*companion object {
        val CREATOR: Parcelable.Creator<AdditionalInfo> = object : Parcelable.Creator<AdditionalInfo?> {
            override fun createFromParcel(`in`: Parcel): AdditionalInfo? {
                return AdditionalInfo(`in`)
            }

            override fun newArray(size: Int): Array<AdditionalInfo?> {
                return arrayOfNulls(size)
            }
        }
    }*/

    companion object CREATOR : Parcelable.Creator<AdditionalInfo> {
        override fun createFromParcel(parcel: Parcel): AdditionalInfo {
            return AdditionalInfo(parcel)
        }

        override fun newArray(size: Int): Array<AdditionalInfo?> {
            return arrayOfNulls(size)
        }
    }
}