package com.dreamorbit.walktalktrack.database.joinstudy

/**
 * Main entry point for accessing tasks data.
 *
 *
 * For simplicity, only getTasks() and getTask() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new task is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */
interface IStudyDataSource {
    val studyData: StudyData?
    fun saveStudyData(studyData: StudyData)
    fun deleteStudyData()
}