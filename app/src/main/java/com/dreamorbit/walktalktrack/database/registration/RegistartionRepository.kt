package com.dreamorbit.walktalktrack.database.registration

import android.content.ContentValues
import android.content.Context
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns

/**
 * Created by nareshkumar.reddy on 10/10/2017.
 */
class RegistartionRepository private constructor(context: Context) : IRegistrationDataSource {
    private val mDbHelper: DbHelper
    // This will be called if the table is new or just empty.
    override val registrationEmailSha: String?
        get() {
            var emailSha: String? = null
            val db = mDbHelper.readableDatabase
            val projection = arrayOf(
                    DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA
            )
            val c = db.query(
                    DbTableColumns.Registartion.TABLE_NAME, projection, null, null, null, null, null)
            if (c != null && c.count > 0) {
                while (c.moveToNext()) {
                    emailSha = c.getString(c.getColumnIndexOrThrow(DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA))
                }
            }
            c?.close()
            db.close()
            return emailSha
        }

    // This will be called if the table is new or just empty.
    override val registrationEmail: String?
        get() {
            var email: String? = null
            val db = mDbHelper.readableDatabase
            val projection = arrayOf(
                    DbTableColumns.Registartion.COLUMN_NAME_EMAIL
            )
            val c = db.query(
                    DbTableColumns.Registartion.TABLE_NAME, projection, null, null, null, null, null)
            if (c != null && c.count > 0) {
                while (c.moveToNext()) {
                    email = c.getString(c.getColumnIndexOrThrow(DbTableColumns.Registartion.COLUMN_NAME_EMAIL))
                }
            }
            c?.close()
            db.close()
            return email
        }

    override fun saveRegistrationData(registrationData: RegistartionData): Long {
        val db = mDbHelper.writableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Registartion.COLUMN_NAME_EMAIL, registrationData.email)
        values.put(DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA, registrationData.email_sha)
        values.put(DbTableColumns.Registartion.COLUMN_NAME_USERNAME, registrationData.userName)
        val row = db.insert(DbTableColumns.Registartion.TABLE_NAME, null, values)
        db.close()
        return row
    }

    override fun deleteRegistrationData() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.Registartion.TABLE_NAME, null, null)
        db.close()
    }

    companion object {
        private var INSTANCE: RegistartionRepository? = null
        @JvmStatic
        val instance: RegistartionRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = RegistartionRepository(PhAwareApplication.context)
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        mDbHelper = DbHelper(context)
    }
}