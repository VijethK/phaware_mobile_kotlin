package com.dreamorbit.walktalktrack.database.registration

/**
 * Created by nareshkumar.reddy on 10/10/2017.
 */
interface IRegistrationDataSource {
    val registrationEmailSha: String?
    val registrationEmail: String?
    fun saveRegistrationData(studyData: RegistartionData): Long
    fun deleteRegistrationData()
}