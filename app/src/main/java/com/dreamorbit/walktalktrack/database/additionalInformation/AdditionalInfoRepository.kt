package com.dreamorbit.walktalktrack.database.additionalInformation

import android.content.ContentValues
import android.content.Context
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.DbHelper
import com.dreamorbit.walktalktrack.database.DbTableColumns

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
class AdditionalInfoRepository private constructor(context: Context) : IAdditionalInfoSource {
    private val mDbHelper: DbHelper
    override fun deleteAdditionalInfo() {
        val db = mDbHelper.writableDatabase
        db.delete(DbTableColumns.AdditionalInformation.TABLE_NAME, null, null)
        db.close()
    }

    override fun saveGender(gender: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.GENDER, gender)
    }

    override fun saveHeight(height: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.HEIGHT, height)
    }

    override fun saveWeight(weight: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.WEIGHT, weight)
    }

    override fun saveDaignoised(diagnoised: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.DIAGNOISED_PH, diagnoised)
    }

    override fun saveMedicated(medicated: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.MEDICATION_PH, medicated)
    }

    override fun saveWearable(wear: String?): Long {
        return saveData(DbTableColumns.AdditionalInformation.WEARABLE, wear)
    }

    override val additionalInfo: AdditionalInfo
        get() {
            val db = mDbHelper.readableDatabase
            val query = "SELECT * FROM " + DbTableColumns.AdditionalInformation.TABLE_NAME + " WHERE " + DbTableColumns.AdditionalInformation._ID + " = 1"
            val cursor = db.rawQuery(query, null)
            val additionalInfo = AdditionalInfo()
            while (cursor.moveToNext()) {
                additionalInfo.gender = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.GENDER))
                additionalInfo.height = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.HEIGHT))
                additionalInfo.weigh = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.WEIGHT))
                additionalInfo.diagnosed = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.DIAGNOISED_PH))
                additionalInfo.medication = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.MEDICATION_PH))
                additionalInfo.wear = cursor.getString(cursor.getColumnIndex(DbTableColumns.AdditionalInformation.WEARABLE))
            }
            db.close()
            return additionalInfo
        }

    private fun saveData(columnName: String, value: String?): Long {
        var row: Long = 0
        val db = mDbHelper.writableDatabase
        val query = "SELECT * FROM " + DbTableColumns.AdditionalInformation.TABLE_NAME + " WHERE " + DbTableColumns.AdditionalInformation._ID + " = 1"
        val cursor = db.rawQuery(query, null)
        val values = ContentValues()
        values.put(columnName, value)
        row = if (cursor.count > 0) {
            db.update(DbTableColumns.AdditionalInformation.TABLE_NAME, values, DbTableColumns.AdditionalInformation._ID + " =1", null).toLong()
        } else {
            db.insert(DbTableColumns.AdditionalInformation.TABLE_NAME, null, values)
        }
        db.close()
        return row
    }

    companion object {
        private var INSTANCE: AdditionalInfoRepository? = null
        @JvmStatic
        val instance: AdditionalInfoRepository?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = AdditionalInfoRepository(PhAwareApplication().getMyAppContext())
                }
                return INSTANCE
            }
    }

    // Prevent direct instantiation.
    init {
        mDbHelper = DbHelper(context)
    }
}