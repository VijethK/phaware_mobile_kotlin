package com.dreamorbit.walktalktrack.database.additionalInformation

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
interface IAdditionalInfoSource {
    fun deleteAdditionalInfo()
    fun saveGender(gender: String?): Long
    fun saveHeight(height: String?): Long
    fun saveWeight(weight: String?): Long
    fun saveDaignoised(diagnoised: String?): Long
    fun saveMedicated(medicated: String?): Long
    fun saveWearable(wear: String?): Long
    val additionalInfo: AdditionalInfo?
}