package com.dreamorbit.walktalktrack.database.questions

import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.researchaware.model.survey.Questions

/**
 * Created by nareshkumar.reddy on 10/11/2017.
 */
interface IQuestionsSource {
    fun deleteQuestions()
    val questions: List<Questions?>?
    fun saveQuestions(questions: QuestionList?): Long
    fun getSurveyQuestions(activityId: Int): List<Questions?>?
}