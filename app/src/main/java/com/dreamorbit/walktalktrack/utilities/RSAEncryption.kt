package com.dreamorbit.walktalktrack.utilities

import android.text.TextUtils
import android.util.Base64
import android.util.Log
import java.io.*
import java.security.*
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException

/**
 * Created by mujasam.bn on 11/16/2017.
 */
class RSAEncryption {
    /**
     * New Encryption
     */
    @Throws(IOException::class, GeneralSecurityException::class)
    fun encryptFile(input: ByteArray?, output: File) {
        cipher = Cipher.getInstance(RSA_ALGORITHM)
        val pemString = SharedPrefSingleton.instance?.publicKey
        val publicKey = getPublicKeyNew(pemString)
        cipher?.init(Cipher.ENCRYPT_MODE, publicKey)
        writeToFile(output, cipher!!.doFinal(input))
    }

    //Append exsting file
    @Throws(IOException::class)
    private fun writeToFile(output: File, toWrite: ByteArray) {
        val fos = FileOutputStream(output, true)
        fos.write(toWrite)
        fos.flush()
        fos.close()
    }

    @Throws(InvalidKeyException::class, UnsupportedEncodingException::class, IllegalBlockSizeException::class, BadPaddingException::class)
    fun decryptText(msg: String?): String {
        try {
            cipher = Cipher.getInstance(RSA_ALGORITHM)
            cipher?.init(Cipher.DECRYPT_MODE, privateKey)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return String(cipher!!.doFinal(Base64.decode(msg, Base64.DEFAULT)))
    }

    /**
     * New Encryption
     */
    @Throws(IOException::class, GeneralSecurityException::class)
    fun decryptFile(input: ByteArray?, output: File) {
        try {
            cipher = Cipher.getInstance(RSA_ALGORITHM)
            cipher?.init(Cipher.DECRYPT_MODE, privateKey)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val bai = ByteArrayInputStream(input)
        val os = ByteArrayOutputStream()
        val buffer = ByteArray(245)
        var len: Int
        // read bytes from the input stream and store them in buffer
        while (bai.read(buffer).also { len = it } != -1) { // write bytes from the buffer into output stream
            os.write(buffer, 0, len)
            writeToDecryptFile(output, cipher!!.doFinal(buffer))
        }
    }

    //Append exsting file
    @Throws(IOException::class)
    private fun writeToDecryptFile(output: File, toWrite: ByteArray) {
        val fos = FileOutputStream(output, true)
        fos.write(toWrite)
        fos.flush()
        fos.close()
    }

    companion object {
        const val RSA_ALGORITHM = "RSA/ECB/PKCS1Padding"
        //public static final String PROVIDER = "BC";
        const val BASE64_FLAG = Base64.DEFAULT
        private var cipher: Cipher? = null
        @get:Throws(Exception::class)
        val privateKey: PrivateKey
            get() {
                val privateKeyString = "MIIEowIBAAKCAQEAnjXagsibhz8ycXZEGSPplWVrt9klBy1pg3hfMYKUKFIfuCTa\n" +
                        "0OtwxOIThK0NKXSdh1UWtuFtJ/BXLXMn55N/KmvX98/KZOOdp9uZvIWVxr8pjd7f\n" +
                        "iJ+Nnvy3DZqKCpCdwjUjtLP7RJsaZInRxEiY6MBntAdliaDBR4N1fIURtrtoTXpS\n" +
                        "8kGRfcPBjlfsboZFTsTqdz+wN/xLuujpXDg6MrQ2ELCIP9Q9fdn3vxkoDTga+tjp\n" +
                        "maEvEw0lXKTjkgmMy4T5/WMfqlEJb876APrMTrAFYfrnqHDINQOIPoD8j81qcuZ4\n" +
                        "hiDSDzgUXBEPYi/tU9qcscbCLhgFkbLTrYBotwIDAQABAoIBAEbvTnGlEFardyp4\n" +
                        "JEBBYdGiD5H2witdqRlRNx0ztsn9SEt/+rwBVCZyVt392RDlQFwVQoHswMxPbkbq\n" +
                        "gtHJyBJ1F/f8KnrhsyY3x85G9siNnUwGyaKwJwdJt6lZyNrDzHfu63tx1w8Dm+nI\n" +
                        "AU1IX4a7/cMyLcxvTzmJL3wNDcwntENoSAxzp26kF/scPh0Ip3dDA2BR8cKzG1+X\n" +
                        "yi2jHqbm9tyX35eExIDWnb0C4Hl/XfUt+lqLjEO8++92XGUsaNIzsVJy/soyTCX/\n" +
                        "g7fBULfk3zQUii5PzrVCl/DGHk+xzGNrQZjvkavZgt67Vv4/XkU/CWZpZxiI2jEj\n" +
                        "fDdZl4ECgYEA6Uv9rDNp4MvCGiVPfbLTHyWn+0PJ0tSTNRgFcpOSa6m+pH+6I8K0\n" +
                        "yJ+sGgSbnbnO2vHHe1ndPJDuChyUNizZWSAs9TsqOLraVnvsx0R8habncljEGvX0\n" +
                        "N6EK+ik+gq335dXnIzl4AB2KNzb1BJaCUAvx8OrMWm+TzAeh63JhgH8CgYEArZtF\n" +
                        "NiRt/pj0SLKqrajAs3BSlCWLUEePBVPktAA4ad+vH6YpX/a1IeDwnTn2WThwgOJr\n" +
                        "1TAr4IhQUOD6M3aJOTACVEzQRmBLgDXJKWc1geNTj0OWlFtG/jz6CwWYNl0yvPQN\n" +
                        "k9n/FU/iAkqxNxwN0+I+RY2F0pL0sXjNHT8K+8kCgYAD+QNM12NKLK4Ve+Uqoszt\n" +
                        "jtqZcMITEQOgt9P/63tm0fEpI/OrgDlL4Uww9TshKHTZTZy0AQe/bEhha7MqaQEa\n" +
                        "f23Ej0lcxfHJ3zpc+lGUhQ8T52cPfni2KcviDqm91/CMvWO+OdBNUDN2VT0+4sE9\n" +
                        "U7a+/4CTWasOomKje8MlSQKBgHnj8L5I6am+PpXkTA3N1C6oiHJXwe7Fr1cwRf1T\n" +
                        "wkgjs1NnWNl14Es/IfFiRuLxy5vSXZ+7SLNHQgznSQRlD4fFe30wyXMAHUKRgQG+\n" +
                        "40SlFYAbFlhnlfrfWTfv36Ub3onoWLgccQrHoYm2Y6Kr4dGIoew3p2PnpkIM7QbB\n" +
                        "wrZxAoGBAOK99kzIuA4mcl4ht5GDPKRJ+m+Jvq4lr5bbdGIy+MnNxw190Lgs78jj\n" +
                        "WIIsHy10OFIUE8mN1LYdtT6ZT3gzaGdE/P/CYLu+dosQ2KKLts+/0StK1pDFZo4N\n" +
                        "uhBM9hFVozO5L3tQdLEAtf4kty62M6MNb7evgEfKr5G6njQFatZK".replace("\\s".toRegex(), "").trim { it <= ' ' }
                Log.e("Private Key ", privateKeyString)
                val keyBytes = Base64.decode(privateKeyString, Base64.DEFAULT)
                val spec = PKCS8EncodedKeySpec(keyBytes)
                val kf = KeyFactory.getInstance("RSA", "BC")
                return kf.generatePrivate(spec)
            }

        /**
         * Create and return the PublicKey object from the public key bytes
         */
        @Throws(NoSuchAlgorithmException::class, InvalidKeySpecException::class)
        private fun getPublicKeyNew(certificateString: String?): PublicKey? {
            var certificateString = certificateString
            val keyFactory: KeyFactory
            if (!TextUtils.isEmpty(certificateString)) {
                certificateString = certificateString
                        ?.replace("-----BEGIN PUBLIC KEY-----\n", "")
                        ?.replace("-----END PUBLIC KEY-----", "")
                        ?.replace("\\s".toRegex(), "")
                        ?.replace("-----BEGINPUBLICKEY-----", "")?.trim { it <= ' ' }
                Log.e("Public Key ", certificateString)
                val certificateData = Base64.decode(certificateString, Base64.DEFAULT)
                keyFactory = KeyFactory.getInstance("RSA")
                val keySpec = X509EncodedKeySpec(certificateData)
                return keyFactory.generatePublic(keySpec)
            }
            return null
        }
        /* //Encryption large byte by byte
    public void encryptLargeBytesByBytes(byte[] input, File output)
            throws IOException, GeneralSecurityException {
        this.cipher = Cipher.getInstance(RSA_ALGORITHM);
        String pemString = SharedPrefSingleton.getInstance().getPublicKey();
        PublicKey publicKey = getPublicKeyNew(pemString);
        this.cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        writeToFile(output, this.cipher.doFinal(input));
    }*/
/*//Encrypting large file
    public void encryptLargeFile(File input, File output)
            throws IOException, GeneralSecurityException {
        this.cipher = Cipher.getInstance(RSA_ALGORITHM);
        String pemString = SharedPrefSingleton.getInstance().getPublicKey();
        PublicKey publicKey = getPublicKeyNew(pemString);
        this.cipher.init(Cipher.ENCRYPT_MODE, publicKey);

        byte[] data = new byte[32]; //(allowing 256 - 11 = 245 bytes of storage). (11 for padding)
        int i = 0;
        FileInputStream fileIn = new FileInputStream(input);
        FileOutputStream fileOut = new FileOutputStream(output);
        //CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher);
        //Read in the data from the file and encrypt it

        while ((i = fileIn.read(data)) != -1) {
            //cipherOut.write(data, 0, i);
            //fileOut.write(data, 0, i);
            fileOut.write(this.cipher.doFinal(data,0,i));
        }
        if (fileIn != null) fileIn.close();
        if (fileOut != null) fileOut.close();

        //writeToFile(output, this.cipher.doFinal(input,0,256));
    }*/
/*------------------------------------------------------------
                Text Encription and Decryption
    -------------------------------------------------------------*/
        /***
         *
         *
         * @param msg
         * @return
         * @throws NoSuchAlgorithmException
         * @throws NoSuchPaddingException
         * @throws UnsupportedEncodingException
         * @throws IllegalBlockSizeException
         * @throws BadPaddingException
         * @throws InvalidKeyException
         * @throws NoSuchProviderException
         */
        @JvmStatic
        @Throws(NoSuchAlgorithmException::class, NoSuchPaddingException::class, UnsupportedEncodingException::class, IllegalBlockSizeException::class, BadPaddingException::class, InvalidKeyException::class, NoSuchProviderException::class)
        fun encryptText(msg: String?): String {
            if (!TextUtils.isEmpty(msg)) {
                cipher = Cipher.getInstance(RSA_ALGORITHM)
                val pemString = SharedPrefSingleton.instance?.publicKey
                try {
                    cipher?.init(Cipher.ENCRYPT_MODE, getPublicKeyNew(pemString))
                } catch (e: InvalidKeySpecException) {
                    e.printStackTrace()
                }
                val encrypted = cipher?.doFinal(msg?.toByteArray())
                return Base64.encodeToString(encrypted, Base64.DEFAULT).replace("\\s".toRegex(), "")
            }
            return null!!
        }
    }
}