package com.dreamorbit.walktalktrack.utilities

import android.util.Log
import java.io.*
import java.security.GeneralSecurityException
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

/**
 * Created by mujasam.bn on 11/14/2017.
 */
object FileHelper {
    private const val BUFFER_SIZE = 8192 //2048;
    private val TAG = FileHelper::class.java.name
    private const val parentPath = ""
    /**
     * Ziping single json file
     *
     * @param files
     * @param zipFileName
     * @return
     */
    fun zip(files: String, zipFileName: String?): Boolean {
        try {
            var origin: BufferedInputStream? = null
            val dest = FileOutputStream(zipFileName)
            val out = ZipOutputStream(BufferedOutputStream(
                    dest))
            val data = ByteArray(BUFFER_SIZE)
            Log.v("Compress", "Adding: $files")
            val fi = FileInputStream(files)
            origin = BufferedInputStream(fi, BUFFER_SIZE)
            val entry = ZipEntry(files.substring(files.lastIndexOf("/") + 1))
            out.putNextEntry(entry)
            var count: Int
            while (origin.read(data, 0, BUFFER_SIZE).also { count = it } != -1) {
                out.write(data, 0, count)
            }
            origin.close()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    /**
     * Zip multiple files
     * For 6 Minute Walk Test json files
     *
     * @param files
     * @param zipFileName
     */
    fun zipFiles(files: Array<File>, zipFileName: String?) {
        var fos: FileOutputStream? = null
        var zipOut: ZipOutputStream? = null
        var fis: FileInputStream? = null
        try {
            fos = FileOutputStream(zipFileName)
            zipOut = ZipOutputStream(BufferedOutputStream(fos))
            for (filePath in files) {
                val input = File(filePath.toString())
                fis = FileInputStream(input)
                val ze = ZipEntry(input.name)
                println("Zipping the file: " + input.name)
                zipOut.putNextEntry(ze)
                val tmp = ByteArray(BUFFER_SIZE)
                var size = 0
                while (fis.read(tmp).also { size = it } != -1) {
                    zipOut.write(tmp, 0, size)
                }
                zipOut.flush()
                fis.close()
            }
            zipOut.close()
            println("Done... Zipped the files...")
        } catch (e: FileNotFoundException) { // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) { // TODO Auto-generated catch block
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (ex: Exception) {
            }
        }
    }

    /**
     * Encrypt Raw zip and save as filename_Encrypted.zip
     *
     * @param zipFileRaw
     * @param encryptedZip
     * @return
     */
    fun encryptedZip(zipFileRaw: String?, encryptedZip: String?): Boolean {
        try {
            RSAEncryption().encryptFile(getFileInBytes(File(zipFileRaw)), File(encryptedZip))
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
        return true
    }

    /**
     * Encrypting large file byte by byte
     * allowing 256 - 11 = 245 bytes of storage). (11 for padding)
     *
     * @param zipFileRaw
     * @param encryptedZip
     * @return
     */
    @JvmStatic
    fun encrypLargeZip(zipFileRaw: String?, encryptedZip: String?): Boolean {
        val input = File(zipFileRaw)
        val fbytes = ByteArray(245) //allowing 256 - 11 = 245 bytes of storage). (11 for padding)
        var i = 0
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(input)
            while (fis.read(fbytes).also { i = it } != -1) {
                RSAEncryption().encryptFile(fbytes, File(encryptedZip))
            }
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                fis?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return true
    }

    /*public static boolean decryptedZip(String zipFileEncrypted, String decyptedZip) {
        try {
            new RSAEncryption().decryptFile(new File(decyptedZip),getFileInBytes(new File(zipFileEncrypted)) );
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        return true;
    }*/
    fun unzip(sourceFile: String?, destinationFolder: String?): Boolean {
        var zis: ZipInputStream? = null
        try {
            zis = ZipInputStream(BufferedInputStream(FileInputStream(sourceFile)))
            var ze: ZipEntry
            var count: Int
            val buffer = ByteArray(BUFFER_SIZE)
            while (zis.nextEntry.also { ze = it } != null) {
                var fileName = ze.name
                fileName = fileName.substring(fileName.indexOf("/") + 1)
                val file = File(destinationFolder, fileName)
                val dir = if (ze.isDirectory) file else file.parentFile
                if (!dir.isDirectory && !dir.mkdirs()) throw FileNotFoundException("Invalid path: " + dir.absolutePath)
                if (ze.isDirectory) continue
                val fout = FileOutputStream(file)
                try {
                    while (zis.read(buffer).also { count = it } != -1) fout.write(buffer, 0, count)
                } finally {
                    fout.close()
                }
            }
        } catch (ioe: IOException) {
            Log.d(TAG, ioe.message)
            return false
        } finally {
            if (zis != null) try {
                zis.close()
            } catch (e: IOException) {
            }
        }
        return true
    }

    @Throws(IOException::class)
    fun getFileInBytes(f: File): ByteArray {
        val fis = FileInputStream(f)
        val fbytes = ByteArray(f.length().toInt())
        fis.read(fbytes)
        fis.close()
        return fbytes
    }

    @JvmStatic
    fun deleteRecursive(fileOrDirectory: File?) {
        if (fileOrDirectory!!.isDirectory) for (child in fileOrDirectory.listFiles()) deleteRecursive(child)
        fileOrDirectory?.delete()
    }
}