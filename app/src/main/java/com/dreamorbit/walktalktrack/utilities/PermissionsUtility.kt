package com.dreamorbit.walktalktrack.utilities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import java.util.*

/**
 */
class PermissionsUtility {
    fun hasPermissions(activity: Activity?): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        val permissionsNeeded = ArrayList<String>()
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.READ_CONTACTS)
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            permissionsNeeded.add(Manifest.permission.READ_CALENDAR)
        }
        return if (permissionsNeeded.size > 0) {
            getPermissions(activity, permissionsNeeded.toTypedArray())
            false
        } else {
            true
        }
    }

    fun hasPermissions(activity: Activity?, vararg permissions: String?): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(activity!!, permission!!) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun shouldShowRationale(activity: Activity?, vararg permissions: String?): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        for (permission in permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission!!)) {
                return true
            }
        }
        return false
    }

    fun hasPermissions(activity: Context?, vararg permissions: String?): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(activity!!, permission!!) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun getPermissions(activity: Activity?, permissions: Array<String>?) {
        ActivityCompat.requestPermissions(activity!!, permissions!!, PERMISSIONS_REQUEST)
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray): Boolean {
        return if (requestCode == PERMISSIONS_REQUEST) {
            for (result in grantResults) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    return false
                }
            }
            true
        } else {
            false
        }
    }

    fun checkForCalendarPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            getPermissions(activity, arrayOf(Manifest.permission.READ_CALENDAR))
        }
    }

    fun checkForCameraPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            getPermissions(activity, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE))
        }
    }

    fun checkForContactPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            getPermissions(activity, arrayOf(Manifest.permission.READ_CONTACTS))
        }
    }

    fun requestForLocationPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            getPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
        }
    }

    fun requestForCallPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            getPermissions(activity, arrayOf(Manifest.permission.CALL_PHONE))
        }
    }

    companion object {
        const val PERMISSIONS_REQUEST = 154
    }
}