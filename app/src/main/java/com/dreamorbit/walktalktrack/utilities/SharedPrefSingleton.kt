package com.dreamorbit.walktalktrack.utilities

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.dreamorbit.walktalktrack.pojo.register.StudyConfiguration

import com.google.gson.Gson
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy


class SharedPrefSingleton private constructor() {
    var sharedPreferences: SharedPreferences? = null
    fun initialize(context: Context?) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    private val editor: SharedPreferences.Editor
        private get() = sharedPreferences!!.edit()

    fun saveBoolean(@TAG tag: String?, state: Boolean) {
        editor.putBoolean(tag, state).apply()
    }

    fun getBoolean(@TAG tag: String?, defaultValue: Boolean): Boolean {
        return sharedPreferences!!.getBoolean(tag, defaultValue)
    }

    fun saveString(@TAG tag: String?, value: String?) {
        editor.putString(tag, value).apply()
    }

    fun getString(@TAG tag: String?, defaultValue: String?): String {
        return sharedPreferences!!.getString(tag, defaultValue)
    }

    fun saveLong(@TAG tag: String?, value: Long) {
        editor.putLong(tag, value).apply()
    }

    fun getLong(@TAG tag: String?, defaultValue: Long): Long {
        return sharedPreferences!!.getLong(tag, defaultValue)
    }

    fun saveInt(@TAG tag: String?, value: Int) {
        editor.putInt(tag, value).apply()
    }

    fun getInt(@TAG tag: String?, defaultValue: Int): Int {
        return sharedPreferences!!.getInt(tag, defaultValue)
    }

    fun clearPhAwarePrefs(@TAG vararg tags: String?) {
        for (tag in tags) {
            editor.remove(tag).apply()
        }
    }

    fun clearCalendarPrefs(@TAG vararg tags: String?) {
        for (tag in tags) {
            editor.remove(tag).apply()
        }
    }

    fun clearStateTamadPrefs(@TAG vararg tags: String?) {
        for (tag in tags) {
            editor.remove(tag).apply()
        }
    }

    fun savePublicKeyUrl(value: String?) {
        editor.putString(PUBLIC_KEY_AWS_URL, value).apply()
    }

    val publicKeyUrl: String
        get() = sharedPreferences!!.getString(PUBLIC_KEY_AWS_URL, "")

    fun savePublicKey(value: String?) {
        editor.putString(PUBLIC_KEY, value).apply()
    }

    val publicKey: String
        get() = sharedPreferences!!.getString(PUBLIC_KEY, "")

    fun saveCastData(message: String?) {
        editor.putString(CASTED_DATA, message).apply()
    }

    val castedData: String
        get() = sharedPreferences!!.getString(CASTED_DATA, "")

    fun saveRateCount(count: Int) {
        editor.putInt(RATING_COUNT, count).apply()
    }

    val rateCount: Int
        get() = sharedPreferences!!.getInt(RATING_COUNT, 0)

    val userEmail: String
        get() = sharedPreferences!!.getString(EMAIL, "")

    fun saveUserEmail(userEmail: String?) {
        editor.putString(EMAIL, userEmail).apply()
    }

    fun saveFcmToken(refreshedToken: String?) {
        editor.putString(FCM_TOKEN, refreshedToken).apply()
    }

    val fcmToken: String
        get() = sharedPreferences!!.getString(FCM_TOKEN, "")

    fun saveStudyConfig(studyConfiguration: StudyConfiguration?) {
        val gson = Gson()
        val studyConfig: String = gson.toJson(studyConfiguration, StudyConfiguration::class.java)
        editor.putString(STUDY_CONFIG, studyConfig).apply()
    }

    val studyConfig: StudyConfiguration
        get() {
            val gson = Gson()
            return gson.fromJson(sharedPreferences!!.getString(STUDY_CONFIG, ""), StudyConfiguration::class.java)
        }

    @Retention(RetentionPolicy.SOURCE)
    annotation class TAG

    companion object {
        //Mujasam
        const val EMAIL = "email"
        //AccessToken
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val REFRESH_TOKEN = "REFRESH_TOKEN"
        //Public Key Aws url
        const val PUBLIC_KEY_AWS_URL = "aws_url"
        //Public Key
        const val PUBLIC_KEY = "public_key"
        //IS USA country region
        const val IS_USA = "IS_USA"
        const val COUNTRY = "COUNTRY"
        const val CASTED_DATA = "casted_data"
        //public static final String TEMP_TOKN = "eyJraWQiOiJZd3J3Z3VoTXhTUWM2djVob1FGVWRFZVRcL2dZN3UyTU5hWjVzM1ZRZGZCND0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJmMjVjNTAwMy1kNTk0LTRjNDgtYTkyMy1jYmNmZjJiNmIyMjkiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfNW14SzRHZDN1IiwiZXhwIjoxNTA5NzA3NTgyLCJpYXQiOjE1MDk3MDM5ODIsImp0aSI6IjgxMGFkYjBhLTkzNzYtNGUwZC1hNjMyLTRlMTJkMzhjN2QzYyIsImNsaWVudF9pZCI6Im8zNGZoZm1wdWVxM3RuOWluMGJ1bGEwNHEiLCJ1c2VybmFtZSI6IjM2bGdhNTBzNjc4NzgyNmI0dDQ2NzAwMHBlOHBjMDg1MGFmNzczNGUzZDczZWNjMWE5YTUxMjBmOTVmNDFjZDRzaEBwaGF3YXJlLm9yZyJ9.IIR8WA63UrrwVlm8noWs0EP_R59KpzjHfzf4TGWrchxwC73xZkRAqMbUNTjHJX0-cQsR0KwTFwO4fRTA-n7PjsvWxRMQEKiqsu9z11fTxiR8H3QzJFdDJw_jkSkWa6c8s0A5uPJZEYoRiD6_sapyZs2whhAT8Ke6QKHyy8jUrK4kbhrqT9Z67wX3WYZfSentm2EBA-RFIP3hPFgfLQ1-_ON-MlMDc3fFCTGL5pmKfk4JFBqf2g3zih9SjKSs29y6Kl_tnk5l9QHksTjCrpZ-9GvwSZ5LMN9_nV54EROimVPXiPLcGPsTzHiKh7UNQsMwVN3L-T_uso2_ApKrdOIR_g";
        const val RATING_COUNT = "RATING_COUNT"
        private const val FCM_TOKEN = "fcm_token"
        private const val STUDY_CONFIG = "study_config"
        private var ourInstance: SharedPrefSingleton? = null
        val instance: SharedPrefSingleton?
            get() {
                if (ourInstance == null) {
                    ourInstance = SharedPrefSingleton()
                }
                return ourInstance
            }
    }
}