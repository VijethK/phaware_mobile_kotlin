package com.dreamorbit.walktalktrack.application

/**
 * Created by mujasam.bn on 9/15/2017.
 */
object AppConstants {
    const val TASK_TAG_PERIODIC = "periodic_task"
    //Header Font
//public static final String APP_FONT = "ChicaGogoNF.ttf";
    const val APP_FONT_DEFAULT = "proximaNova-Regular.ttf"
    //Bundle Key Names
    const val IS_ELIGIBLE = "is_eligible"
    const val YEAR = "year"
    const val MONTH = "month"
    const val DAY = "day"
    const val AGE = "age"
    //Screen Flow Names
    const val REGISTER_FLOW = "registerFlow"
    //public static final String ASSENT_FLOW = "assentFlow";
//public static final String CONSENT_FLOW = "consentFlow";
    const val CONSENT_ONE_FLOW = "consent_one_flow"
    const val CONSENT_TWO_FLOW = "consent_two_flow"
    const val ADDITIONAL_INFO_FLOW = "additionalInfoFlow"
    //public static final String ACTIVITY_FLOW = "activityFlow";
//public static final String ADDITIONAL_INFO = "additionalInfo";
    const val ACTIVITY_ID_ARRAY = "activity_id_array"
    const val HTML_FILE = "html_file"
    const val PAGE_TITLE = "page_title"
    //Intent service
    const val BACKGROUND_ACTION = "background_action"
    //Alarm to reset Activities everyday morning 7am
    const val REPEAT_HOUR = 7
    const val REPEAT_MIN = 0
    //public static final int REPEAT_APM_PM = Calendar.AM;
    const val PROFILE_ABOUT = "profile_about"
    const val LOGIN = "login"
    //public static final String IMMEDIATELY = "immediately";
//Wear start/stop constant values
    const val APP_LOGINED = 0
    const val APP_LOGOUT = 1
    const val ZIP_FILE_RECEIVED = 2
    const val LAUNCH_WEAR = 3
    const val TEST_SCHEDULER_ENABLE = 4
    const val WEAR_MESSAGE_EMERGENCY_CALL = "WEAR_MESSAGE_EMERGENCY_CALL"
    const val RATING_ALERT = "RATING_ALERT"
    const val DEVICE_TYPE = "android"
    const val TEST_SEQUENCE = "test_sequence"
    const val SURVEY_ID = "survey_id"
    const val SURVEY_NAME = "survey_name"
    const val WEAR_MESSAGE_SCHEDULER_STATUS = "scheduler_test_status"
    const val ACTIVITY_HAS_RESET = "activity_has_reset"
    const val RATE_APP = "rate_app"
    const val OFFLINE_ALERT = "offline_alert"
}