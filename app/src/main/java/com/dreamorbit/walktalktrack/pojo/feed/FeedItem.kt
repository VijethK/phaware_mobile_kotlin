package com.dreamorbit.walktalktrack.pojo.feed

import org.simpleframework.xml.*
import java.io.Serializable

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
@Root(name = "item", strict = false)
class FeedItem : Serializable {
    @Element(name = "pubDate")
    var mpubDate: String? = null
    @Path("title")
    @Text(required = false)
    var mtitle: String? = null
        get() = field
        set(mtitle) {
            field = mtitle
        }
    @Element(name = "link")
    var mlink: String? = null
    @Element(name = "description")
    var mdescription: String? = null
    @Namespace(reference = "http://www.itunes.com/dtds/podcast-1.0.dtd", prefix = "itunes")
    @Element
    var image: Image? = null

    constructor() {}
    constructor(mdescription: String?, mlink: String?, mtitle: String?, mpubDate: String?) {
        this.mdescription = mdescription
        this.mlink = mlink
        this.mtitle = mtitle
        this.mpubDate = mpubDate
    }

}