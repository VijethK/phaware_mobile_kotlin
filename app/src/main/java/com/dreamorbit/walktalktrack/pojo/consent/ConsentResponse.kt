package com.dreamorbit.walktalktrack.pojo.consent

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
class ConsentResponse {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("consent_created_at")
    @Expose
    var consentCreatedAt: String? = null
    @SerializedName("consent_signature")
    @Expose
    var consentSignature: String? = null
    @SerializedName("user_id")
    @Expose
    var userId: String? = null
    @SerializedName("study_id")
    @Expose
    var studyId: Any? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("withdraw")
    @Expose
    var withdraw: Boolean? = null
    @SerializedName("error")
    @Expose
    var errorMessage: String? = null

}