package com.dreamorbit.walktalktrack.pojo.consent

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
class ConsentCreateRequest {
    var name: String? = null
    var consent_signature: String? = null
    var consent_document: String? = null

}