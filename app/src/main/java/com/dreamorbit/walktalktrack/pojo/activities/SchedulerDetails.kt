package com.dreamorbit.walktalktrack.pojo.activities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SchedulerDetails {
    @SerializedName("schedule_days")
    @Expose
    var scheduleDays: List<String>? = null
    @SerializedName("schedule_type")
    @Expose
    var scheduleType: String? = null
    @SerializedName("scheduled_at")
    @Expose
    var scheduledAt: String? = null

}