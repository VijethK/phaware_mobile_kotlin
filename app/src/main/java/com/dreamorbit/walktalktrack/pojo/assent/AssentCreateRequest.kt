package com.dreamorbit.walktalktrack.pojo.assent

/**
 * Created by mujasam.bn on 10/6/2017.
 */
class AssentCreateRequest {
    var name: String? = null
    var assent_signature: String? = null
    var assent_document: String? = null

}