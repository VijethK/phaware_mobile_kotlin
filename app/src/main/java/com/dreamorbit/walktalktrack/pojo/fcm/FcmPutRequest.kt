package com.dreamorbit.walktalktrack.pojo.fcm

class FcmPutRequest {
    var email: String? = null
    var device_token: String? = null
    var device_type: String? = null

}