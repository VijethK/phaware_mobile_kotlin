package com.dreamorbit.walktalktrack.pojo.userProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nareshkumar.reddy on 11/29/2017.
 */

public class ProfileResponse {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("given_name")
    @Expose
    private String givenName;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("sub")
    @Expose
    private String sub;
    @SerializedName("email_verified")
    @Expose
    private String emailVerified;
    @SerializedName("medication_for_ph")
    @Expose
    private String medicationForPh;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("actual_email")
    @Expose
    private String actualEmail;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("sync_wearable")
    @Expose
    private String syncWearable;
    @SerializedName("survey_start_date")
    @Expose
    private String surveyStartDate;
    @SerializedName("survey_end_date")
    @Expose
    private String surveyEndDate;
    @SerializedName("diagnosed_with_ph")
    @Expose
    private String diagnosedWithPh;
    @SerializedName("height")
    @Expose
    private String height;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(String emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getMedicationForPh() {
        return medicationForPh;
    }

    public void setMedicationForPh(String medicationForPh) {
        this.medicationForPh = medicationForPh;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getActualEmail() {
        return actualEmail;
    }

    public void setActualEmail(String actualEmail) {
        this.actualEmail = actualEmail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSyncWearable() {
        return syncWearable;
    }

    public void setSyncWearable(String syncWearable) {
        this.syncWearable = syncWearable;
    }

    public String getSurveyStartDate() {
        return surveyStartDate;
    }

    public void setSurveyStartDate(String surveyStartDate) {
        this.surveyStartDate = surveyStartDate;
    }

    public String getSurveyEndDate() {
        return surveyEndDate;
    }

    public void setSurveyEndDate(String surveyEndDate) {
        this.surveyEndDate = surveyEndDate;
    }

    public String getDiagnosedWithPh() {
        return diagnosedWithPh;
    }

    public void setDiagnosedWithPh(String diagnosedWithPh) {
        this.diagnosedWithPh = diagnosedWithPh;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }
}
