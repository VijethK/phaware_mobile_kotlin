package com.dreamorbit.walktalktrack.pojo.additionalInfo

/**
 * Created by nareshkumar.reddy on 10/25/2017.
 */
class AdditionalInfoRequest {
    var actual_email: String? = null
    var gender: String? = null
    var height: String? = null
    var weight: String? = null
    var diagnosed_with_ph: String? = null
    var medication_for_ph: String? = null
    var sync_wearable: String? = null
    var survey_start_date: String? = null
    var survey_end_date: String? = null
    var birthdate: String? = null

}