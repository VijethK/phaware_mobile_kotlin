package com.dreamorbit.walktalktrack.pojo.feed

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import java.io.Serializable

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
@Root(name = "channel", strict = false)
class Channel : Serializable {
    @ElementList(inline = true, name = "item")
    var feedItems: List<FeedItem>? = null
        private set

    constructor() {}
    constructor(mFeedItems: List<FeedItem>?) {
        feedItems = mFeedItems
    }
}