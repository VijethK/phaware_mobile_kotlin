package com.dreamorbit.walktalktrack.pojo.upload

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 11/17/2017.
 */
class PhUploadResponse {
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("id")
    @Expose
    var id = 0
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("expires")
    @Expose
    var expires: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: Any? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: Any? = null
    @SerializedName("study_id")
    @Expose
    var studyId: Int? = null

}