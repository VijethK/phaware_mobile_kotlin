package com.dreamorbit.walktalktrack.pojo.geocode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 12/4/2017.
 */
class GeoResponse {
    @SerializedName("results")
    @Expose
    var results: List<Result>? = null
    @SerializedName("status")
    @Expose
    var status: String? = null

}