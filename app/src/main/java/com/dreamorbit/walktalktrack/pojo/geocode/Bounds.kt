package com.dreamorbit.walktalktrack.pojo.geocode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 12/4/2017.
 */
class Bounds {
    @SerializedName("northeast")
    @Expose
    var northeast: Northeast_? = null
    @SerializedName("southwest")
    @Expose
    var southwest: Southwest_? = null

}