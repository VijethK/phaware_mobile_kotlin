package com.dreamorbit.walktalktrack.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 10/26/2017.
 */
abstract class IError {
    @SerializedName("error")
    @Expose
    var errorMessage: String? = null

}