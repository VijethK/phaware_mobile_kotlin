package com.dreamorbit.walktalktrack.pojo.login

/**
 * Created by nareshkumar.reddy on 11/20/2017.
 */
class SignInRequest(var email: String, var password: String, var device_token: String?, var device_type: String)