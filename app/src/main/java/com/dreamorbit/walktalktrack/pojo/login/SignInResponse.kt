package com.dreamorbit.walktalktrack.pojo.login

import com.dreamorbit.walktalktrack.pojo.register.StudyConfiguration
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SignInResponse {
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null
    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null
    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null
    @SerializedName("id_token")
    @Expose
    var idToken: String? = null
    @SerializedName("registration_status")
    @Expose
    var registrationStatus: String? = null
    @SerializedName("error")
    @Expose
    var errorMessage: String? = null
    @SerializedName("study_configuration")
    @Expose
    var studyConfiguration: StudyConfiguration? = null

}