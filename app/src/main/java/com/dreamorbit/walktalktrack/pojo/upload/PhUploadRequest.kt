package com.dreamorbit.walktalktrack.pojo.upload

/**
 * Created by mujasam.bn on 11/17/2017.
 */
class PhUploadRequest(var name: String, var contentLength: Long, var contentMd5: String, var contentType: String, var type: String?) {

    fun setContentLength(contentLength: Int) {
        this.contentLength = contentLength.toLong()
    }

}