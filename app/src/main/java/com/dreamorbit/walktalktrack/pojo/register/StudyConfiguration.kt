package com.dreamorbit.walktalktrack.pojo.register

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StudyConfiguration {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("study_consent")
    @Expose
    var studyConsent: Boolean? = null
    @SerializedName("study_assent")
    @Expose
    var studyAssent: Boolean? = null
    @SerializedName("additional_user_details")
    @Expose
    var additionalUserDetails: Boolean? = null
    @SerializedName("study_id")
    @Expose
    var studyId: Int? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null

}