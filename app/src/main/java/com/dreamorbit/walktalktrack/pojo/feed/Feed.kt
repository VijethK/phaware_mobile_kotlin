package com.dreamorbit.walktalktrack.pojo.feed

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.io.Serializable

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
@Root(name = "rss", strict = false)
class Feed : Serializable {
    @Element(name = "channel")
    private var mChannel: Channel? = null

    fun getmChannel(): Channel? {
        return mChannel
    }

    constructor() {}
    constructor(mChannel: Channel?) {
        this.mChannel = mChannel
    }
}