package com.dreamorbit.walktalktrack.pojo.document

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DocumentResponse {
    @SerializedName("link")
    @Expose
    var link: String? = null

}