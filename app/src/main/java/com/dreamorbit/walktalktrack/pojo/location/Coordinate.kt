package com.dreamorbit.walktalktrack.pojo.location

import java.util.*

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */
class Coordinate {
    var longitude: Double? = null
    var latitude: Double? = null
    private val additionalProperties: MutableMap<String, Any> = HashMap()

    fun getAdditionalProperties(): Map<String, Any> {
        return additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}