package com.dreamorbit.walktalktrack.pojo.location

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */
class Item {
    var verticalAccuracy: Int? = null
    var course: Double? = null
    var speed: Float? = null
    var horizontalAccuracy: Float? = null
    var timestamp: String? = null
    var altitude: Double? = null
    var coordinate: Coordinate? = null

}