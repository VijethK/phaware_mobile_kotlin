package com.dreamorbit.walktalktrack.pojo.register

/**
 * Created by nareshkumar.reddy on 9/18/2017.
 */
class RegisterRequest {
    var email: String? = null
    var password: String? = null
    var name: String? = null
    var role: String? = null
    var device_token: String? = null
    var device_type: String? = null
    var actual_email: String? = null
    var survey_start_date: String? = null
    var survey_end_date: String? = null

}