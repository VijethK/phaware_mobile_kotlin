package com.dreamorbit.walktalktrack.pojo.location

import java.util.*

/**
 * Created by nareshkumar.reddy on 12/20/2017.
 */
class LocationData {
    private val items: MutableList<Item> = ArrayList()
    fun getItems(): List<Item> {
        return items
    }

    fun addItems(item: Item) {
        items.add(item)
    }
}