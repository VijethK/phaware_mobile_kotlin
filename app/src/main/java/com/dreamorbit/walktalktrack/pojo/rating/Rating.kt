package com.dreamorbit.walktalktrack.pojo.rating

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Rating {
    @SerializedName("rate")
    @Expose
    var rate = 0f
    @SerializedName("comment")
    @Expose
    var comment: String? = null
    @SerializedName("username")
    @Expose
    var username: String? = null

}