package com.dreamorbit.walktalktrack.pojo.assent

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 10/9/2017.
 */
class AssentCreatedResponse {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("assent_created_at")
    @Expose
    var assentCreatedAt: String? = null
    @SerializedName("assent_signature")
    @Expose
    var assentSignature: String? = null
    @SerializedName("assent_for_id")
    @Expose
    var assentForId: String? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("study_consent_id")
    @Expose
    var studyConsentId: Any? = null
    @SerializedName("withdraw")
    @Expose
    var withdraw: Boolean? = null
    @SerializedName("error")
    @Expose
    var errorMessage: String? = null

}