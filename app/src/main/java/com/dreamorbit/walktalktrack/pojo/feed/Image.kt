package com.dreamorbit.walktalktrack.pojo.feed

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
@Root
class Image {
    @Attribute
    var href: String? = null

}