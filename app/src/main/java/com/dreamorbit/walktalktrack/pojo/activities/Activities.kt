package com.dreamorbit.walktalktrack.pojo.activities

import com.dreamorbit.walktalktrack.pojo.IError
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 10/26/2017.
 */
class Activities : IError() {
    /*public void setActivityList(List<ActivityList> activityList) {
        this.activityList = activityList;
    }

    public SchedulerDetails getOldSchedulerDetails() {
        return schedulerDetails;
    }

    public void setOldSchedulerDetails(SchedulerDetails schedulerDetails) {
        this.schedulerDetails = schedulerDetails;
    }*/
    @JvmField
    @SerializedName("activity_list")
    @Expose
    var activityList: List<ActivityList>? = null
    @SerializedName("scheduler_details")
    @Expose
    private val schedulerDetails: SchedulerDetails? = null

}