package com.dreamorbit.walktalktrack.pojo.invite

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class InviteToggleResponse {
    @SerializedName("message")
    @Expose
    var message = false

}