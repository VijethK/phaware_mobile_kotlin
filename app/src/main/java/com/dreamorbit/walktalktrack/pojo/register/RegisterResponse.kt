package com.dreamorbit.walktalktrack.pojo.register

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 9/20/2017.
 */
class RegisterResponse {
    @SerializedName("birthdate")
    @Expose
    var birthdate: String? = null
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null
    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null
    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null
    @SerializedName("id_token")
    @Expose
    var idToken: String? = null
    @SerializedName("new_device_metadata")
    @Expose
    var newDeviceMetadata: Any? = null
    @SerializedName("registration_status")
    @Expose
    var registrationStatus: String? = null
    @SerializedName("error")
    @Expose
    var errorMessage: String? = null
    @SerializedName("study_configuration")
    @Expose
    var studyConfiguration: StudyConfiguration? = null

    constructor(accessToken: String?, expiresIn: Int?, tokenType: String?, refreshToken: String?, idToken: String?, newDeviceMetadata: Any?, errorMessage: String?) {
        this.accessToken = accessToken
        this.expiresIn = expiresIn
        this.tokenType = tokenType
        this.refreshToken = refreshToken
        this.idToken = idToken
        this.newDeviceMetadata = newDeviceMetadata
        this.errorMessage = errorMessage
    }

    constructor() {}

}