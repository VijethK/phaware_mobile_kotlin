package com.dreamorbit.walktalktrack.pojo.refreshToken

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 10/10/2017.
 */
class RefreshTokenReponse {
    @SerializedName("access_token")
    @Expose
    var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    var expiresIn: Int? = null
    @SerializedName("token_type")
    @Expose
    var tokenType: String? = null
    @SerializedName("refresh_token")
    @Expose
    var refreshToken: String? = null
    @SerializedName("id_token")
    @Expose
    var idToken: String? = null
    @SerializedName("new_device_metadata")
    @Expose
    var newDeviceMetadata: Any? = null

}