package com.dreamorbit.walktalktrack.pojo.geocode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 12/4/2017.
 */
class Geometry {
    @SerializedName("location")
    @Expose
    var location: Location? = null
    @SerializedName("location_type")
    @Expose
    var locationType: String? = null
    @SerializedName("viewport")
    @Expose
    var viewport: Viewport? = null
    @SerializedName("bounds")
    @Expose
    var bounds: Bounds? = null

}