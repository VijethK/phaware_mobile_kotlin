package com.dreamorbit.walktalktrack.pojo.geocode

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by nareshkumar.reddy on 12/4/2017.
 */
class AddressComponent {
    @SerializedName("long_name")
    @Expose
    var longName: String? = null
    @SerializedName("short_name")
    @Expose
    var shortName: String? = null
    @SerializedName("types")
    @Expose
    var types: List<String>? = null

}