package com.dreamorbit.walktalktrack.pojo.activities

import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 10/30/2017.
 */
class ActivityList {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("schedule_id")
    @Expose
    var scheduleId: Int? = null
    @SerializedName("activity_type_id")
    @Expose
    var activityTypeId: Int? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("scheduler_details")
    @Expose
    var schedulerDetails: SchedulerModel? = null
    var isActivityAnswered = 0

    var userGroupId: Int? = null

}