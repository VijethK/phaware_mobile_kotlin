package com.dreamorbit.walktalktrack.screens.bottombar.profile

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.APP_LOGOUT
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfo
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository
import com.dreamorbit.walktalktrack.database.flow.FlowRepository
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoView
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.screens.bottombar.fithistory.GoogleFitHistoryActivity
import com.dreamorbit.walktalktrack.screens.bottombar.learn.DescriptionActivity
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.wear.MobileMessageSenderAsync

class ProfileActivity : BaseActivity(), AdditionalInfoView, View.OnClickListener, ProfileView {
    private var additionalInfo: AdditionalInfo? = null
    private val profile = false
    private val encrypted = "***"
    private var profilePresenter: ProfilePresenter? = null
    private var androidWatch: Switch? = null
    private var fitbit: Switch? = null
    private var mParent: LinearLayout? = null
    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL, ignoreCase = true)) {
                requestCallPermission(this@ProfileActivity)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        profilePresenter = ProfilePresenter(this, SignOutApi(), WithdrawApi(), GetProfileApi())
        initToolBar()
        initViews()
    }

    private fun initToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.setText(R.string.menu_bottom_about_me)
    }

    //initializing views
    private fun initViews() { //getting data from AdditionalInfoRepository and assign it to views
        additionalInfo = AdditionalInfoRepository.instance!!.additionalInfo
        val age = findViewById<TextView>(R.id.tvAge)
        val gender = findViewById<TextView>(R.id.tvGender)
        val height = findViewById<TextView>(R.id.tvHeight)
        val weight = findViewById<TextView>(R.id.tvWeight)
        val diagnoised = findViewById<TextView>(R.id.tvDiagnosed)
        val medicated = findViewById<TextView>(R.id.tvMedication)
        androidWatch = findViewById(R.id.watch_switch)
        fitbit = findViewById(R.id.fitbit_switch)
        age.text = if (profile) StudyRepository.instance!!.studyData!!.age else encrypted
        gender.text = if (profile) additionalInfo!!.gender else encrypted
        height.text = if (profile) additionalInfo!!.height else encrypted
        weight.text = if (profile) additionalInfo!!.weigh else encrypted
        diagnoised.text = if (profile) additionalInfo!!.diagnosed else encrypted
        medicated.text = if (profile) additionalInfo!!.medication else encrypted
        androidWatch?.setClickable(false)
        fitbit?.setClickable(false)
        setWearble(additionalInfo!!)
        val userName = findViewById<RelativeLayout>(R.id.user_layout)
        userName.visibility = View.VISIBLE
        val tvUserName = findViewById<TextView>(R.id.tvUserName)
        tvUserName.text = "***"
        tvUserName.setOnClickListener(this)
        val email = findViewById<RelativeLayout>(R.id.email_layout)
        email.visibility = View.VISIBLE
        val tvEmail = findViewById<TextView>(R.id.tvEmail)
        tvEmail.visibility = View.VISIBLE
        tvEmail.text = "***"
        tvUserName.setOnClickListener(this)
        //Google Fit
        val googlefitLayout = findViewById<RelativeLayout>(R.id.googlefit_layout)
        googlefitLayout.visibility = View.VISIBLE
        val tvHeartRate = findViewById<TextView>(R.id.tvHeartRate)
        val tvStepCount = findViewById<TextView>(R.id.tvStepCount)
        tvHeartRate.setOnClickListener(this)
        tvStepCount.setOnClickListener(this)
        val policy = findViewById<RelativeLayout>(R.id.policy_layout)
        policy.visibility = View.VISIBLE
        val tvPolicy = findViewById<TextView>(R.id.policy)
        tvPolicy.setOnClickListener(this)
        val feedback = findViewById<RelativeLayout>(R.id.feedback_layout)
        feedback.visibility = View.VISIBLE
        val tvFeedBack = findViewById<TextView>(R.id.feedBack)
        tvFeedBack.setOnClickListener(this)
        val withDraw = findViewById<RelativeLayout>(R.id.withdraw_layout)
        withDraw.visibility = View.VISIBLE
        val tvWithDraw = findViewById<TextView>(R.id.withDraw)
        tvWithDraw.setOnClickListener(this)
        val signOut = findViewById<RelativeLayout>(R.id.signout_layout)
        signOut.visibility = View.VISIBLE
        val tvSignOut = findViewById<TextView>(R.id.signOut)
        tvSignOut.setOnClickListener(this)
        findViewById<View>(R.id.tvAge).setOnClickListener(this)
        findViewById<View>(R.id.tvGender).setOnClickListener(this)
        findViewById<View>(R.id.tvHeight).setOnClickListener(this)
        findViewById<View>(R.id.tvWeight).setOnClickListener(this)
        findViewById<View>(R.id.tvDiagnosed).setOnClickListener(this)
        findViewById<View>(R.id.tvMedication).setOnClickListener(this)
        mParent = findViewById(R.id.main_content)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setWearble(additionalInfo: AdditionalInfo) {
        if (additionalInfo.wear != null) {
            if (additionalInfo.wear.equals(getString(R.string.watch), ignoreCase = true)) {
                androidWatch!!.isChecked = true
                fitbit!!.isChecked = false
            } else if (additionalInfo.wear.equals(getString(R.string.fitbit), ignoreCase = true)) {
                androidWatch!!.isChecked = false
                fitbit!!.isChecked = true
            } else {
                androidWatch!!.isChecked = false
                fitbit!!.isChecked = false
            }
        } else {
            androidWatch!!.isChecked = false
            fitbit!!.isChecked = false
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.tvHeartRate -> callGoogleFitActivity(true)
            R.id.tvStepCount -> callGoogleFitActivity(false)
            R.id.policy -> {
                /*Intent intents = new Intent(this, PolicyPdfViewActivity.class);
                startActivity(intents);*/
                val intents = Intent(this, DescriptionActivity::class.java)
                intents.putExtra(AppConstants.PAGE_TITLE, getString(R.string.privacy_policy))
                intents.putExtra(AppConstants.HTML_FILE, "https://www.phaware.global/phaware-privacy-policy")
                startActivity(intents)
            }
            R.id.feedBack -> {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:info@phAware.com"))
                //To add subject
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "There are no apps to support your actions on your mobile", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.withDraw -> withDrawAlert(this)
            R.id.signOut -> {
                val pendingFiles = isPendingUpload
                if (pendingFiles) {
                    Utility.showSnackBar(mParent, getString(R.string.survey_upload_pending), Snackbar.LENGTH_LONG)
                    return
                }
                signOutAlert(this)
            }
            R.id.tvAge, R.id.tvGender, R.id.tvHeight, R.id.tvWeight, R.id.tvDiagnosed, R.id.tvMedication, R.id.tvUserName, R.id.tvEmail -> encryptAlertt(this)
        }
    }

    private fun callGoogleFitActivity(isHeartRate: Boolean) {
        val intent = Intent(this, GoogleFitHistoryActivity::class.java)
        intent.putExtra(AppConstants.PAGE_TITLE, isHeartRate)
        startActivity(intent)
    }

    private val isPendingUpload: Boolean
        private get() {
            var pendingFiles = false
            val directories = SdkUtils.getAllDirectories(this)
            for (dir in directories) {
                if (dir.isFile && dir.name.endsWith(".zip")) {
                    pendingFiles = true
                } else if (dir.isDirectory) {
                    pendingFiles = true
                }
            }
            return pendingFiles
        }

    override fun onSuccess() {
        Utility.dismissProgressBar()
        if (profile) {
            AdditionalInfoRepository.instance!!.saveWearable(AdditionalInfoRepository.instance!!.additionalInfo.wear)
            FlowRepository.instance!!.saveFlow(AppConstants.ADDITIONAL_INFO_FLOW, true)
            val intent = Intent(this, BottomBarActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        } else {
            setWearble(AdditionalInfoRepository.instance!!.additionalInfo)
        }
    }

    /**
     * Show withDraw related message in dialog
     *
     * @param context
     */
    private fun withDrawAlert(context: Context) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.withdraw_study)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                    if (ConnectivityReceiver.isConnected) {
                        Utility.showProgressBar(this, getString(R.string.please_wait))
                        profilePresenter!!.callWithDrawApi(RegistartionRepository.instance!!.registrationEmailSha)
                    } else {
                        Utility.showSnackBar(mParent, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    private fun signOutAlert(context: Context) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.signout_msg)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                    if (ConnectivityReceiver.isConnected) {
                        Utility.showProgressBar(this, getString(R.string.please_wait))
                        profilePresenter!!.callSignOutApi(RegistartionRepository.instance!!.registrationEmailSha)
                    } else {
                        Utility.showSnackBar(mParent, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    private fun encryptAlertt(context: Context) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.encrypt_msg)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    override fun onFailure() {
        Utility.dismissProgressBar()
        Toast.makeText(this, R.string.profile_failed, Toast.LENGTH_SHORT).show()
    }

    override fun onError() {
        Utility.dismissProgressBar()
        Toast.makeText(this, R.string.profile_failed, Toast.LENGTH_SHORT).show()
    }

    override fun signOutSuccess() {
        Utility.dismissProgressBar()
        Utility.signOut()
        Utility.clearAlarms()
        MobileMessageSenderAsync().execute(APP_LOGOUT)
    }

    override fun signOutFailure(failure: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, failure, Toast.LENGTH_SHORT).show()
    }

    override fun signOutError(error: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun withDrawSuccess() {
        Utility.dismissProgressBar()
        MobileMessageSenderAsync().execute(APP_LOGOUT) // Stop Wear 6mwt and navigate to Main screen.
        Utility.signOut()
        Utility.clearAlarms()
    }

    override fun getProfileSuccess(profileResponse: ProfileResponse?) {
        Utility.dismissProgressBar()
        AdditionalInfoRepository.instance!!.saveDaignoised(profileResponse?.diagnosedWithPh)
        AdditionalInfoRepository.instance!!.saveMedicated(profileResponse?.medicationForPh)
        AdditionalInfoRepository.instance!!.saveGender(profileResponse?.gender)
        AdditionalInfoRepository.instance!!.saveHeight(profileResponse?.height)
        AdditionalInfoRepository.instance!!.saveWeight(profileResponse?.weight)
        AdditionalInfoRepository.instance!!.saveWearable(profileResponse?.syncWearable)
        setWearble(AdditionalInfoRepository.instance!!.additionalInfo)
    }

    override fun getProfileFailure(failure: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, failure, Toast.LENGTH_SHORT).show()
    }

    override fun getProfileError(error: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onCertificateDownloadSuccess() {}
    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL))
    }
}