package com.dreamorbit.walktalktrack.screens.invite

import okhttp3.ResponseBody
import retrofit2.Response

class InvitePresenter(private val inviteView: InviteView, private val inviteApi: InviteApi) {
    fun performInviteCodeApiCall(token: String?) {
        inviteApi.validateInviteCode(this, token)
    }

    /**
     * If entered token is valid
     * @param response
     */
    fun getValidateSuccess(response: Response<ResponseBody?>) {
        inviteView.onValidationSuccess(response.body().toString())
    }

    /**
     * If entered token is not valid
     * @param response
     */
    fun onFaiure(response: Response<ResponseBody?>?) {
        inviteView.onValidationFail(null)
    }

    /**
     * If api call fails
     * @param error
     */
    fun getError(error: String?) {
        inviteView.onApiError(error)
    }

}