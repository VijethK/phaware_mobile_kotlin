package com.dreamorbit.walktalktrack.screens.casting

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.screens.casting.CastingReviewActivity
import com.dreamorbit.walktalktrack.shared.CastingData
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson

/**
 * Created by mujasam.bn on 16/7/2018.
 */
class CastRestingHeartRateActivity : BaseActivity() {
    private var mTvTimer: TextView? = null
    private var mTvHearRate: TextView? = null
    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "message_receiver" is broadcasted.
    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(WEAR_MESSAGE_INTENT_FILTER, ignoreCase = true)) {
                val message = intent.getStringExtra(WEAR_MESSAGE)
                bindRestingData(message)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resting_cast)
        initViews()
        initToolBar()
    }

    private val lastCastingData: String?
        private get() {
            val castedValue = SharedPrefSingleton.instance?.castedData
            return if (TextUtils.isEmpty(castedValue)) {
                null
            } else {
                castedValue
            }
        }

    /**
     * Initialize the mToolbar components
     * dynamic left margin to mToolbar title. Reusing the existing mToolbar
     */
    private fun initToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val mTvTitle = toolbar.findViewById<TextView>(R.id.title)
        val mTvCancel = toolbar.findViewById<TextView>(R.id.done)
        mTvTitle.setText(R.string.resting)
        mTvCancel.setText(R.string.done)
        mTvCancel.setOnClickListener { view: View? -> navigateToActivityScreen() }
    }

    private fun initViews() {
        mTvTimer = findViewById(R.id.tvDuration)
        mTvHearRate = findViewById(R.id.tvHeartRate)
    }

    private fun bindRestingData(message: String?) {
        val gson = Gson()
        val restingData = gson.fromJson(message, CastingData::class.java)
        if (restingData != null && !restingData.isResting && restingData.castStop) {
            navigateToReviewScreen()
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        } else {
            mTvTimer!!.text = restingData!!.timer
            mTvHearRate!!.text = "" + restingData.heartRate
        }
    }

    private fun navigateToReviewScreen() {
        val intent = Intent(this, CastingReviewActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun navigateToActivityScreen() {
        val intent = Intent(this, BottomBarActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(WEAR_MESSAGE_INTENT_FILTER))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL))
        //Check if 6MWT still in process and not stoped
//get the last state data if its paused in Wear and show that
        val lastCastedData = lastCastingData
        if (!TextUtils.isEmpty(lastCastedData)) {
            bindRestingData(lastCastedData)
        } else {
        }
    }

    override fun onDestroy() { // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() { //super.onBackPressed();
    }

    companion object {
        const val WEAR_MESSAGE_INTENT_FILTER = "message_receiver"
        const val WEAR_MESSAGE = "message"
    }
}