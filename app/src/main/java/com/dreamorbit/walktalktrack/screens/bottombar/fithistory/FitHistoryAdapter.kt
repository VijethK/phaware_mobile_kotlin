package com.dreamorbit.walktalktrack.screens.bottombar.fithistory

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.researchaware.model.report.Report
import com.dreamorbit.walktalktrack.R
import java.util.*

class FitHistoryAdapter(context: Context?, isHeartRateData: Boolean) : RecyclerView.Adapter<FitViewHolder>() {
    private val inflater: LayoutInflater
    private var reportList: List<Report>? = ArrayList()
    private var view: View? = null
    private var holder: FitViewHolder? = null
    private val isHeartRate: Boolean
    //This method inflates view present in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FitViewHolder {
        view = inflater.inflate(R.layout.row_item_fit_history, parent, false)
        holder = FitViewHolder(view!!)
        return holder!!
    }

    //Binding the data using get() method of POJO object
    override fun onBindViewHolder(holder: FitViewHolder, position: Int) {
        val report = reportList!![position]
        holder.tvDateTime.text = report.testTakenAt
        holder.tvValue.text = if (isHeartRate) "" + report.averageHeartRate else "" + report.stepsCount
    }

    //Setting the arraylist
    fun setListContent(list: List<Report>?) {
        reportList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (reportList == null) 0 else reportList!!.size
    }

    init {
        inflater = LayoutInflater.from(context)
        isHeartRate = isHeartRateData
    }
}