package com.dreamorbit.walktalktrack.screens.assent

import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
interface AssentViewContract {
    fun onErrorResponse(assentResponse: AssentCreatedResponse?)
    fun onSuccessResponse(assentResponse: AssentCreatedResponse?)
    fun onDownloadComplete()
}