package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.util.Log
import com.dreamorbit.walktalktrack.pojo.additionalInfo.AdditionalInfoRequest
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody

/**
 * Created by nareshkumar.reddy on 10/25/2017.
 */
class AdditionalInfoPreseneter(private val additionalInfoView: AdditionalInfoView, private val aditionalApi: AdditionalInfoApi) {
    /*
     * method to call update the AdditionalInfo API to server
     * @param additionalInfoRequest
     * */
    fun callAdditionalInfoService(additionalInfoRequest: AdditionalInfoRequest?) {
        aditionalApi.sendAdditionalInfo(additionalInfoRequest)!!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ResponseBody?> {
                    override fun onSubscribe(disposable: Disposable) {}
                    override fun onSuccess(additionalInfoResponse: ResponseBody) { //Removed ? from ResponseBody?
                        if (additionalInfoResponse != null) {
                            Log.e("Response:", "" + additionalInfoResponse)
                            addInforResult("Success")
                        }
                    }

                    override fun onError(throwable: Throwable) {
                        addInfoErrorResponse()
                    }
                })
    }

    /*
     * method to update the view with AdditionalInfo response
     * @param body
     * */
    fun addInforResult(body: String?) {
        additionalInfoView.onSuccess()
    }

    /*
     * method to update the view with AdditionalInfo errorResponse
     * @param errorResponse
     * */
    fun addInfoErrorResponse() {
        additionalInfoView.onFailure()
    }

    /*
     * method to update the view with AdditionalInfo failureResponse
     * @param failurResponse
     * */
    fun setAddInfoFailureResponse(failurResponse: String?) {
        additionalInfoView.onError()
    }

}