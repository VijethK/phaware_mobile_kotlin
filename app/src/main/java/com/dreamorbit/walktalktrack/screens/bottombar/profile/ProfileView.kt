package com.dreamorbit.walktalktrack.screens.bottombar.profile

import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse

/**
 * Created by nareshkumar.reddy on 11/16/2017.
 */
interface ProfileView {
    fun signOutSuccess()
    fun signOutFailure(failure: String?)
    fun signOutError(error: String?)
    fun withDrawSuccess()
    fun getProfileSuccess(profileResponse: ProfileResponse?)
    fun getProfileFailure(failure: String?)
    fun getProfileError(error: String?)
    fun onCertificateDownloadSuccess()
}