package com.dreamorbit.walktalktrack.screens.bottombar.profile

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by nareshkumar.reddy on 11/28/2017.
 */
class GetProfileApi {
    fun getProfile(profilePresenter: ProfilePresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.userProfile
        call!!.enqueue(object : Callback<ProfileResponse?> {
            override fun onResponse(call: Call<ProfileResponse?>, response: Response<ProfileResponse?>) {
                if (response != null && response.code() == 200) {
                    Log.e("Response:", "" + response.body())
                    profilePresenter.getProfileSuccess(response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), RegisterResponse::class.java)
                        val failure = errorResponse.errorMessage
                        profilePresenter.faiure(failure)
                    } catch (e: Exception) {
                        profilePresenter.getProfileFailure("Please try again.")
                    }
                }
            }

            override fun onFailure(call: Call<ProfileResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val error = "Profile failed. Please try again."
                profilePresenter.getProfileError(error)
            }
        })
    }
}