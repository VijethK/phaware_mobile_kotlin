package com.dreamorbit.walktalktrack.screens.bottombar.learn

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ScrollView
import android.widget.TextView
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.utilities.Utility
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import com.google.android.gms.fitness.data.DataSource
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterLoginButton
import com.twitter.sdk.android.tweetcomposer.ComposerActivity


class DescriptionActivity : AppCompatActivity(), View.OnClickListener {
    private var scrollView: ScrollView? = null
    private var loginButton: TwitterLoginButton? = null
    private var simpleExoPlayerView: SimpleExoPlayerView? = null
    private var player: SimpleExoPlayer? = null
   // Kotlin error
  //  private var mediaDataSourceFactory: DataSource.Factory? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var shouldAutoPlay = false
    private var bandwidthMeter: BandwidthMeter? = null
    private val progressDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)
        //Exoplayer initialization
        shouldAutoPlay = true
        bandwidthMeter = DefaultBandwidthMeter()
        //Kotlin error
       // mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"), bandwidthMeter as TransferListener<in DataSource?>?)
        initToolbar()
        initView()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.text = intent.getStringExtra(AppConstants.PAGE_TITLE)
    }

    private fun initView() {
        val webView = findViewById<WebView>(R.id.webView)
        if (intent.getStringExtra(AppConstants.PAGE_TITLE) != getString(R.string.spread_the_word) && intent.getStringExtra(AppConstants.PAGE_TITLE) != getString(R.string.life_with_pulmonary_hypertension)) {
            Utility.showProgressBar(this@DescriptionActivity, getString(R.string.please_wait))
            webView.webViewClient = MyWebClient()
            webView.settings.javaScriptEnabled = true
            webView.settings.builtInZoomControls = true
            webView.settings.displayZoomControls = false
            webView.loadUrl(intent.getStringExtra(AppConstants.HTML_FILE))
        } else if (intent.getStringExtra(AppConstants.PAGE_TITLE) == getString(R.string.life_with_pulmonary_hypertension)) {
            initializePlayer()
        } else {
            scrollView = findViewById(R.id.spread_layout)
            scrollView?.setVisibility(View.VISIBLE)
        }
        findViewById<View>(R.id.twitter).setOnClickListener(this)
        findViewById<View>(R.id.facebook).setOnClickListener(this)
        findViewById<View>(R.id.email).setOnClickListener(this)
        findViewById<View>(R.id.sms).setOnClickListener(this)
        //twitter login
        loginButton = findViewById(R.id.login_button)
        loginButton?.setCallback(object : Callback<TwitterSession?>() {
            override fun success(result: Result<TwitterSession?>) { // Do something with result, which provides a TwitterSession for making API calls
                val session = TwitterCore.getInstance().sessionManager.activeSession
                val twitterintent = ComposerActivity.Builder(this@DescriptionActivity)
                        .session(session)
                        .text("http://www.phaware.global/")
                        .hashtags("#twitter")
                        .createIntent()
                startActivity(twitterintent)
            }

            override fun failure(exception: TwitterException) { // Do something on failure
                Snackbar.make(scrollView!!, "Please try again", Snackbar.LENGTH_SHORT).show()
            }
        })
    }

    inner class MyWebClient : WebViewClient() {
        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) { // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon)
        }

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean { // TODO Auto-generated method stub
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) { // TODO Auto-generated method stub
            super.onPageFinished(view, url)
            Utility.dismissProgressBar()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        // Pass the activity result to the login button.
        loginButton!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // handle arrow click here
        if (item.itemId == android.R.id.home) {
            finish() // close this activity and return to preview activity
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.email -> {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                } else {
                    Snackbar.make(scrollView!!, "There are no apps to support your actions on your mobile", Snackbar.LENGTH_SHORT).show()
                }
            }
            R.id.sms -> {
                val smsIntent = Intent(Intent.ACTION_VIEW)
                smsIntent.data = Uri.parse("sms:")
                if (smsIntent.resolveActivity(packageManager) != null) {
                    startActivity(smsIntent)
                } else {
                    Snackbar.make(scrollView!!, "There are no apps to support your actions on your mobile", Snackbar.LENGTH_SHORT).show()
                }
            }
            R.id.facebook -> {
                //for generating hash for facebbok
/* try {
                    PackageInfo info = getPackageManager().getPackageInfo(
                            "com.dreamorbit.walktalktrack", PackageManager.GET_SIGNATURES);
                    for (Signature signature : info.signatures) {
                        MessageDigest md = MessageDigest.getInstance("SHA");
                        md.update(signature.toByteArray());
                        Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                    }
                } catch (PackageManager.NameNotFoundException e) {

                } catch (NoSuchAlgorithmException e) {

                }*/
                val content = ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("http://www.phaware.global/"))
                        .build()
                val shareDialog = ShareDialog(this)
                shareDialog.show(content, ShareDialog.Mode.AUTOMATIC)
            }
            R.id.twitter -> loginButton!!.performClick()
        }
    }

    private fun initializePlayer() {
        simpleExoPlayerView = findViewById(R.id.player_view)
        simpleExoPlayerView?.setVisibility(View.VISIBLE)
        simpleExoPlayerView?.requestFocus()
        val videoTrackSelectionFactory: TrackSelection.Factory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
        simpleExoPlayerView?.setPlayer(player)
        player?.setPlayWhenReady(shouldAutoPlay)
        val extractorsFactory = DefaultExtractorsFactory()

        //Kotlin error
    /*    val mediaSource: MediaSource = ExtractorMediaSource(Uri.parse("file:///android_asset/info.mp4"),
                mediaDataSourceFactory, extractorsFactory, null, null)
        player?.prepare(mediaSource)*/
    }

    private fun releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }

    public override fun onStop() {
        releasePlayer()
        super.onStop()
    }
}