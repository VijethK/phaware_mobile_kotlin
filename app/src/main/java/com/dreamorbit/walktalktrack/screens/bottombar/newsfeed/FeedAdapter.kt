package com.dreamorbit.walktalktrack.screens.bottombar.newsfeed

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.pojo.feed.FeedItem
import com.dreamorbit.walktalktrack.screens.bottombar.learn.DescriptionActivity
import com.dreamorbit.walktalktrack.screens.bottombar.newsfeed.FeedAdapter.FeedHolder
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
class FeedAdapter(private val context: Context, private val feedArrayList: List<FeedItem>) : RecyclerView.Adapter<FeedHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): FeedHolder {
        val myInflater = LayoutInflater.from(context)
        // Inflate the view for this view holder
        val thisItemsView = myInflater.inflate(R.layout.feed_item,
                viewGroup, false)
        // Call the view holder's constructor, and pass the view to it;
// return that new view holder
        return FeedHolder(thisItemsView)
    }

    override fun onBindViewHolder(feedHolder: FeedHolder, i: Int) {
        feedHolder.title.text = feedArrayList[i].mtitle
        feedHolder.time.text = feedArrayList[i].mpubDate
        Picasso.with(context).load(feedArrayList[i].image!!.href).into(feedHolder.circleView)
    }

    override fun getItemCount(): Int {
        return feedArrayList.size
    }

    inner class FeedHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var circleView: CircleImageView
        var title: TextView
        var time: TextView
        override fun onClick(view: View) {
            val intent = Intent(context, DescriptionActivity::class.java)
            intent.putExtra(AppConstants.PAGE_TITLE, context.getString(R.string.back))
            intent.putExtra(AppConstants.HTML_FILE, feedArrayList[adapterPosition].mlink)
            context.startActivity(intent)
        }

        init {
            itemView.setOnClickListener(this)
            circleView = itemView.findViewById(R.id.logo)
            title = itemView.findViewById(R.id.title)
            time = itemView.findViewById(R.id.time)
        }
    }

}