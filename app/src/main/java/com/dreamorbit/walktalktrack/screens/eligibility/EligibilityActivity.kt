package com.dreamorbit.walktalktrack.screens.eligibility

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.joinstudy.StudyData
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository.Companion.instance
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.invite.InviteActivity
import com.dreamorbit.walktalktrack.screens.invite.InviteApi
import com.dreamorbit.walktalktrack.screens.registration.RegisterActivity
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.welcome.WelcomeActivity

class EligibilityActivity : BaseActivity(), View.OnClickListener, EligibilityView {
    //Option menu
    private val mMenu: Menu? = null
    private var mImgEligibility: ImageView? = null
    private var mTvDescrption: TextView? = null
    //budle data
    private var mIsEligible = false
    private var mAge: String? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    //Toolbar
    private var mTvTitle: TextView? = null
    private var mTvDone: TextView? = null
    private var mEligibilityPresenter: EligibilityPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eligibility)
        initViews()
        initToolbar()
        mEligibilityPresenter = EligibilityPresenter(this, InviteApi())
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        mTvTitle = toolbar.findViewById(R.id.title)
        mTvDone = toolbar.findViewById(R.id.done)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mTvTitle?.setText(R.string.eligibility_title)
        if (mIsEligible) mTvDone?.setText(R.string.next) else mTvDone?.setText(R.string.quit)
        mTvDone?.setOnClickListener(View.OnClickListener { view: View? ->
            if (mIsEligible) { //Save study details in local database
                performEligibleCondition()
            } else {
                val intent = Intent(this@EligibilityActivity, WelcomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        })
    }

    /**
     * If eligibility critieria matches
     * Check the Invite toggle is On or Off at portal end
     */
    private fun performEligibleCondition() {
        val data = StudyData(mYear, mMonth, mDay, mAge!!, mIsEligible)
        instance!!.deleteStudyData()
        instance!!.saveStudyData(data)
        if (ConnectivityReceiver.isConnected) {
            showLoader("Verifying invite code setting")
            mEligibilityPresenter!!.performInviteCodeToggleApiCall()
        } else {
            Utility.showSnackBar(findViewById(R.id.parent), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    private fun initViews() {
        mImgEligibility = findViewById(R.id.imgEligibility)
        mTvDescrption = findViewById(R.id.tvDescrption)
        mIsEligible = intent.getBooleanExtra(AppConstants.IS_ELIGIBLE, false)
        mYear = intent.getIntExtra(AppConstants.YEAR, 0)
        mMonth = intent.getIntExtra(AppConstants.MONTH, 0)
        mDay = intent.getIntExtra(AppConstants.DAY, 0)
        mAge = intent.getStringExtra(AppConstants.AGE)
        if (mIsEligible) {
            mImgEligibility?.setBackgroundResource(R.drawable.eligibility)
            mTvDescrption?.setText(R.string.eligibility_allowed)
        } else {
            mImgEligibility?.setBackgroundResource(R.drawable.eligibility_denied)
            mTvDescrption?.setText(R.string.eligibility_denied)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
        }
    }

    override fun onValidationSuccess(isToggleOn: Boolean) {
        hideLoader()
        navigateToNextScreen(isToggleOn)
    }

    override fun onValidationFail(response: String?) {
        hideLoader()
    }

    override fun onApiError(error: String?) {
        hideLoader()
    }

    /**
     * If toggle is off at portal end directly go to InviteActivity screen else
     *
     * @param isToggleOn
     */
    private fun navigateToNextScreen(isToggleOn: Boolean) {
        if (isToggleOn) startActivity(Intent(this@EligibilityActivity, InviteActivity::class.java)) else startActivity(Intent(this@EligibilityActivity, RegisterActivity::class.java))
    }
}