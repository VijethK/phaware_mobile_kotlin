package com.dreamorbit.walktalktrack.screens.rating

import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository.Companion.instance
import com.dreamorbit.walktalktrack.pojo.rating.Rating
import retrofit2.Response

class RatingPresenter(private val ratingView: RatingView, private val ratingApi: RatingApi) {
    fun onSubmit(rating: Float, description: String?) {
        val rate = Rating()
        rate.comment = description
        rate.rate = rating
        rate.username = instance!!.registrationEmailSha
        ratingApi.submitRating(rate, this)
    }

    fun onRatingChange(rating: Float) {
        ratingView.onRateChange(rating)
    }

    fun onApiSuccess(response: Response<*>?) {
        ratingView.onSuccess(response)
    }

    fun onApiError(response: Response<*>?) {
        ratingView.onError(response)
    }

}