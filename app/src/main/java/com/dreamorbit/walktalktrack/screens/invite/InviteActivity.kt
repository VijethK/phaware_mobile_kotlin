package com.dreamorbit.walktalktrack.screens.invite

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.custom.PinInviteEditText
import com.dreamorbit.walktalktrack.dialogs.AlertCallback
import com.dreamorbit.walktalktrack.dialogs.DialogClass.getAlertDialogOk
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.registration.RegisterActivity
import com.dreamorbit.walktalktrack.utilities.Utility

class InviteActivity : BaseActivity(), InviteView {
    private var mEtPinEntry: PinInviteEditText? = null
    private var mInvitePresenter: InvitePresenter? = null
    var pinValidationWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        override fun afterTextChanged(s: Editable) {
            if (s.length == 5) {
                Utility.hideKeyboard(mEtPinEntry)
                if (ConnectivityReceiver.isConnected) {
                    showLoader(getString(R.string.verifying_token))
                    mInvitePresenter!!.performInviteCodeApiCall(s.toString())
                } else {
                    mEtPinEntry!!.text = null
                    Utility.showSnackBar(findViewById(R.id.main_content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite)
        initToolbar()
        initViews()
        initData()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.setText(R.string.invite_code)
    }

    private fun initViews() {
        mEtPinEntry = findViewById(R.id.txt_pin_entry)
        mEtPinEntry?.addTextChangedListener(pinValidationWatcher)
    }

    private fun initData() {
        mInvitePresenter = InvitePresenter(this, InviteApi())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_invite, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.action_reset -> mEtPinEntry!!.text = null
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onValidationSuccess(response: String?) {
        hideLoader()
        startActivity(Intent(this, RegisterActivity::class.java))
        finish()
    }

    override fun onValidationFail(response: String?) {
        hideLoader()
        mEtPinEntry!!.text = null
        getAlertDialogOk(this, getString(R.string.invite_failed), getString(R.string.incorrect_code), getString(R.string.ok), false,
                object : AlertCallback {
                    override fun PositiveMethod(dialog: DialogInterface?, id: Int) {}
                    override fun NegativeMethod(dialog: DialogInterface?, id: Int) {}
                })
    }

    override fun onApiError(error: String?) {
        hideLoader()
        Utility.showSnackBar(findViewById(R.id.main_content), getString(R.string.unknown_error), Snackbar.LENGTH_LONG)
    }
}