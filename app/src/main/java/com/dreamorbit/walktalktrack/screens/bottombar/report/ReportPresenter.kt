package com.dreamorbit.walktalktrack.screens.bottombar.report

import com.dreamorbit.researchaware.model.report.GetReportRequest
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton

class ReportPresenter(private val mReportView: ReportView, private val mReportApi: ReportApi) {
    fun getReportApi(fromDate: String?, toDate: String?) {
        val userName: String? = SharedPrefSingleton.instance?.userEmail
        val reportRequest = GetReportRequest(userName, fromDate, toDate)
        mReportApi.getReport(reportRequest, this)
    }

    fun onSuccessReports(body: ReportResponse?) {
        mReportView.onReportSuccess(body)
    }

    fun onFailureReports() {
        mReportView.onReportFailure()
    }

}