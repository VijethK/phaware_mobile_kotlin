package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository.Companion.instance
import com.dreamorbit.walktalktrack.utilities.Utility
import com.rd.PageIndicatorView

class AdditionalInfoActivity : AppCompatActivity(), PerformAction {
    //ViewPager
    private var mViewPager: ViewPager? = null
    //Pager Adapter
    private var additionalInfoPagerAdapter: AdditionalInfoPagerAdapter? = null
    //pass this flag in all fragment to check its partial flow or normal flow
    @JvmField
    var isPartialRegistrationFlow = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_additional_info)
        isPartialRegistrationFlow = isPartialRegistered
        initViewPager()
    }

    private val isPartialRegistered: Boolean
        private get() {
            val dob = instance!!.studyData
            return if (dob == null) true else false
        }

    //initializing view pager
    private fun initViewPager() {
        mViewPager = findViewById(R.id.container)
        additionalInfoPagerAdapter = AdditionalInfoPagerAdapter(supportFragmentManager)
        additionalInfoPagerAdapter!!.addpages(GenderFragment())
        additionalInfoPagerAdapter!!.addpages(HeightFragment())
        additionalInfoPagerAdapter!!.addpages(WeightFragment())
        additionalInfoPagerAdapter!!.addpages(DiagnosedFragment())
        additionalInfoPagerAdapter!!.addpages(MedicationFragment())
        if (isPartialRegistrationFlow) additionalInfoPagerAdapter!!.addpages(DOBFragment())
        //additionalInfoPagerAdapter.addpages(new WearableFragment());
        mViewPager?.setAdapter(additionalInfoPagerAdapter) //? is added
        //to keep all fragments in memory at a time
        mViewPager?.setOffscreenPageLimit(additionalInfoPagerAdapter!!.count) //? is added
        val pageIndicatorView = findViewById<PageIndicatorView>(R.id.pageIndicatorView)
        pageIndicatorView.setViewPager(mViewPager)
        mViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {  //? is added
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Utility.hideKeyboard(mViewPager)
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    /*
     * Method which will handle the navigation from one fragment to another
     * @param  position
     * */
    override fun moveToNext(position: Int) {
        mViewPager!!.currentItem = position + 1
    }

    /*
     * Method which will handle the back navigation from one fragment to another
     * @param  position
     * */
    override fun moveBack(position: Int) {
        when (position) {
            0 -> finish()
            else -> mViewPager!!.currentItem = position - 1
        }
    }
}