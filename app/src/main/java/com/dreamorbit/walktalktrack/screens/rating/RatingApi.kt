package com.dreamorbit.walktalktrack.screens.rating

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.rating.Rating
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RatingApi {
    fun submitRating(ratingRequest: Rating?, ratingPresenter: RatingPresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.ratings(ratingRequest)
        call!!.enqueue(object : Callback<Any?> {
            override fun onResponse(call: Call<Any?>, response: Response<Any?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    ratingPresenter.onApiSuccess(response)
                } else {
                    ratingPresenter.onApiError(response)
                }
            }

            override fun onFailure(call: Call<Any?>, t: Throwable) {
                ratingPresenter.onApiError(null)
            }
        })
    }
}