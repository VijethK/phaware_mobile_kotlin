package com.dreamorbit.walktalktrack.screens.login

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.walktalktrack.BuildConfig
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.flow.FlowRepository
import com.dreamorbit.walktalktrack.database.registration.RegistartionData
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.login.SignInRequest
import com.dreamorbit.walktalktrack.pojo.login.SignInResponse
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.service.BackGroundService
import com.dreamorbit.walktalktrack.service.PhGCMTaskService
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.utilities.Utility.PARTIAL_SCREEN

class LoginActivity : BaseActivity(), LoginView {
    private var email: EditText? = null
    private var password: EditText? = null
    private var loginPresenter: LoginPresenter? = null
    private var mRegistartionData: RegistartionData? = null
    private var emailSHA: String? = null
    private var parent: LinearLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginPresenter = LoginPresenter(this, LoginApi())
        initToolbar()
        initViews()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.setText(R.string.sign_in)
    }

    private fun initViews() {
        email = findViewById(R.id.et_email)
        password = findViewById(R.id.et_password)
        mRegistartionData = RegistartionData()
        parent = findViewById(R.id.login_parent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                loginPresenter!!.validateSignIn(Utility.isValidEmail(email!!.text.toString().trim { it <= ' ' }), password!!.text.toString().trim { it <= ' ' })
                Utility.hideKeyboard(email)
            }
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setEmailError(error: String?) {
        email!!.error = error
    }

    override fun setPasswordError(error: String?) {
        password!!.error = error
    }

    override fun isValid() {
        val emailEncrypt = Utility.getSHA256Hash(email!!.text.toString().trim { it <= ' ' })
        emailSHA = StringBuilder().append(emailEncrypt).append("@phaware.org").toString()
        val fcmToken = SharedPrefSingleton.instance?.fcmToken
        if (ConnectivityReceiver.isConnected) {
            Utility.showProgressBar(this, getString(R.string.please_wait))
            loginPresenter!!.callSignInApi(SignInRequest(emailSHA!!, password!!.text.toString().trim { it <= ' ' }, fcmToken, AppConstants.DEVICE_TYPE))
        } else {
            Utility.showSnackBar(parent, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    override fun onSuccess(registerResponse: SignInResponse?) { //To store in db
//RegistartionRepository.getInstance().deleteRegistrationData();
        mRegistartionData!!.email = email!!.text.toString().trim { it <= ' ' }
        mRegistartionData!!.email_sha = emailSHA
        if (registerResponse != null) {
            loginPresenter!!.saveRegistartionDetails(mRegistartionData)
            SharedPrefSingleton.instance?.saveString(SharedPrefSingleton.ACCESS_TOKEN, registerResponse.accessToken)
            SharedPrefSingleton.instance?.saveUserEmail(emailSHA)
            SharedPrefSingleton.instance?.saveString(SharedPrefSingleton.REFRESH_TOKEN, registerResponse.refreshToken)
            //Immediate start the Public key api
            val intent = Intent(PhAwareApplication().getMyAppContext(), BackGroundService::class.java)
            intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_survey.toString())
            BackGroundService.enqueueBackgroundWork(PhAwareApplication().getMyAppContext(), intent)
            PhGCMTaskService.startPeriodicTask()
            val registartionData = RegistartionData()
            registartionData.email = email!!.text.toString().trim { it <= ' ' }
            registartionData.email_sha = emailSHA
            RegistartionRepository.instance!!.saveRegistrationData(registartionData)
            SharedPrefSingleton.instance?.saveBoolean(SharedPrefSingleton.IS_USA, true)
        } else {
            Utility.showSnackBar(parent, getString(R.string.try_again), Snackbar.LENGTH_LONG)
        }
        /**
         * If partial registration made previously for this user
         * Navigate to pending signup process
         */
        Utility.dismissProgressBar()
        when (PARTIAL_SCREEN.valueOf(registerResponse?.registrationStatus!!)) {
            PARTIAL_SCREEN.complete -> {
                FlowRepository.instance!!.saveFlow(AppConstants.ADDITIONAL_INFO_FLOW, true)
                val intent = Intent(this, BottomBarActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            }
            else -> loginPresenter!!.onPartialRegistration(registerResponse)
        }
    }

    override fun onFailure(failureMessage: SignInResponse?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, failureMessage?.errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onError(errorMessage: SignInResponse?) {
        Utility.dismissProgressBar()
        Toast.makeText(this, errorMessage?.errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onPartialRegistration(body: SignInResponse?) {
        Utility.dismissProgressBar()
        if (BuildConfig.IS_DENVER) {
            navigateToPartialRegisteredScreenDenver(PARTIAL_SCREEN.valueOf(body?.registrationStatus!!))
        } else {
            navigateToPartialRegisteredScreenStanford(PARTIAL_SCREEN.valueOf(body?.registrationStatus!!))
        }
    }
}