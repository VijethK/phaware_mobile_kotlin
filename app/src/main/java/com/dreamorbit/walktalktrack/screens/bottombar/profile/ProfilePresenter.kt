package com.dreamorbit.walktalktrack.screens.bottombar.profile

import android.util.Log
import com.dreamorbit.walktalktrack.api.PhPublicKey
import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse
import com.dreamorbit.walktalktrack.service.PublicKeyApi
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by nareshkumar.reddy on 11/16/2017.
 */
class ProfilePresenter(private val profileView: ProfileView, private val signOutApi: SignOutApi, private val withdrawApi: WithdrawApi, private val getProfileApi: GetProfileApi) {
    fun callSignOutApi(email: String?) {
        signOutApi.signOut(email, this)
    }

    fun callWithDrawApi(email: String?) {
        withdrawApi.withdraw(email, this)
    }

    fun downloadCertificate() {
        val publicKeyApi = PublicKeyApi()
        publicKeyApi.presendUrl?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(object : SingleObserver<PhPublicKey?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(phPublicKey: PhPublicKey) {
                        if (phPublicKey != null) {
                            Log.e("Response:", "" + phPublicKey)
                            publicKeyApi.getPublicKey(phPublicKey.url)?.subscribeOn(Schedulers.io())
                                    ?.observeOn(AndroidSchedulers.mainThread())
                                    ?.subscribe(object : SingleObserver<String?> {
                                        override fun onSubscribe(disposable: Disposable) {}
                                        override fun onSuccess(s: String) {
                                            profileView.onCertificateDownloadSuccess()
                                        }

                                        override fun onError(throwable: Throwable) {}
                                    })
                        }
                    }

                    override fun onError(e: Throwable) {}
                })
    }

    fun success() {
        profileView.signOutSuccess()
    }

    fun withDrawSuccess() {
        profileView.withDrawSuccess()
    }

    fun faiure(failure: String?) {
        profileView.signOutFailure(failure)
    }

    fun error(error: String?) {
        profileView.signOutError(error)
    }

    val userProfileApi: Unit
        get() {
            getProfileApi.getProfile(this)
        }

    fun getProfileSuccess(profileResponse: ProfileResponse?) {
        profileView.getProfileSuccess(profileResponse)
    }

    fun getProfileFailure(failure: String?) {
        profileView.getProfileFailure(failure)
    }

    fun getProfileError(error: String?) {
        profileView.getProfileError(error)
    }

}