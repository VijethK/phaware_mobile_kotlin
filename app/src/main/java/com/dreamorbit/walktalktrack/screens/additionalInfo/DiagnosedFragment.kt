package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance
import com.dreamorbit.walktalktrack.utilities.Utility

/**
 * A simple [Fragment] subclass.
 */
class DiagnosedFragment : Fragment() {
    private var performAction: PerformAction? = null
    private var next: TextView? = null
    private var diagnoised: String? = null
    private var back: ImageView? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_diagnosed, container, false)
        initToolbar(view)
        isDiagnoised(view)
        return view
    }

    //initialize toolbar
    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.diagnoised_ph_title)
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)  //? is added
        back?.setOnClickListener(View.OnClickListener { view12: View? -> performAction!!.moveBack(3) })  //? is added
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))  //? is added
        next?.setEnabled(false) //? is added
        next?.setAlpha(0.5f) //? is added
        next?.setOnClickListener(View.OnClickListener { view1: View? -> performAction!!.moveToNext(3) })  //? is added
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    /*
    * To check whether user is diagnoised or not by using radioGroup
    * @layout
    * */
    private fun isDiagnoised(layout: View) {
        val radioGroup = layout.findViewById<RadioGroup>(R.id.radioGroup)
        val radioYes = layout.findViewById<RadioButton>(R.id.radioYes)
        val radioNo = layout.findViewById<RadioButton>(R.id.radioNo)
        Utility.hideKeyboard(radioNo)
        radioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            next!!.isEnabled = true
            next!!.alpha = 1.0f
            if (checkedId == R.id.radioYes) {
                diagnoised = "Yes"
                instance!!.saveDaignoised(diagnoised)
            } else if (checkedId == R.id.radioNo) {
                diagnoised = "No"
                instance!!.saveDaignoised(diagnoised)
            }
        }
    }
}