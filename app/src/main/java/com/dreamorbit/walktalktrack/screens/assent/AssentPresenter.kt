package com.dreamorbit.walktalktrack.screens.assent

import com.dreamorbit.walktalktrack.pojo.assent.AssentCreateRequest
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse
import com.dreamorbit.walktalktrack.screens.registration.DocumentDownloadApi

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
class AssentPresenter
/**
 * Constructor to initilize the contractor and api service
 */(private val assentViewContract: AssentViewContract, private val mAssentPostApi: CreateAssentApi, private val mDocumentDownloadApi: DocumentDownloadApi) {
    /**
     * Api call to create assent in server
     */
    fun createAssentService(createRequest: AssentCreateRequest?) {
        mAssentPostApi.createAssent(createRequest, this)
    }

    /**
     * craete api Callback result will be navigated to Controller.
     */
    fun postResult(assentResponse: AssentCreatedResponse?) {
        if (assentResponse != null && assentResponse.assentForId != null) {
            assentViewContract.onSuccessResponse(assentResponse)
        } else if (assentResponse != null) {
            assentViewContract.onErrorResponse(assentResponse)
        }
    }

    fun downloadDocument() {
        val docsType = arrayOf("assent")
        for (type in docsType) {
            mDocumentDownloadApi.downloadDocument(null, this, type)
        }
    }

    fun onDownloadComplete() {
        assentViewContract.onDownloadComplete()
    }

}