package com.dreamorbit.walktalktrack.screens.bottombar.fithistory

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.dreamorbit.researchaware.model.report.Report
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.custom.SimpleDividerItemDecoration
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.common.api.Scope
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.request.DataReadRequest
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class GoogleFitHistoryActivity : BaseActivity(), ConnectionCallbacks, OnConnectionFailedListener {
    //Adapter
    protected var mFitHistoryAdapter: FitHistoryAdapter? = null
    //RecyclerView
    protected var mListParent: RelativeLayout? = null
    protected var recyclerView: RecyclerView? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private val mReportList: MutableList<Report>? = ArrayList()
    private var isHeartRateData = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fit_history)
        initViews()
        initToolbar()
        initFit()
    }

    private fun initViews() {
        isHeartRateData = intent.getBooleanExtra(AppConstants.PAGE_TITLE, false)
        recyclerView = findViewById(R.id.fitRecycler)
        recyclerView?.setLayoutManager(LinearLayoutManager(this))
        recyclerView?.addItemDecoration(SimpleDividerItemDecoration(this))
    }

    /**
     * Initialise the toolbar
     */
    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        val titleString = if (isHeartRateData == true) "Heart Rate" else "Step Count"
        title.text = titleString
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initFit() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .addScope(Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addConnectionCallbacks(this)
                .enableAutoManage(this, 0, this)
                .build()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onConnected(bundle: Bundle?) {
        Log.e("HistoryAPI", "onConnected")
        if (isHeartRateData) ViewHeartHistoryTask().execute() else ViewStepsHistoryTask().execute()
    }

    /**
     * Heart Rate
     */
    private fun readFitHeartHistoryData() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime = cal.timeInMillis
        cal.add(Calendar.YEAR, -1)
        val startTime = cal.timeInMillis
        val readRequest = DataReadRequest.Builder()
                .read(DataType.TYPE_HEART_RATE_BPM)
                .enableServerQueries()
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()
        val dataReadResult = Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(30, TimeUnit.SECONDS)
        //Used for aggregated data
        if (dataReadResult.buckets.size > 0) {
            Log.e("History", "Number of buckets: " + dataReadResult.buckets.size)
            for (bucket in dataReadResult.buckets) {
                val dataSets = bucket.dataSets
                for (dataSet in dataSets) {
                    showDataSet(dataSet)
                }
            }
        } else if (dataReadResult.dataSets.size > 0) {
            Log.e("History", "Number of returned DataSets: " + dataReadResult.dataSets.size)
            for (dataSet in dataReadResult.dataSets) {
                showDataSet(dataSet)
            }
        }
    }

    /**
     * Steps count
     */
    private fun readFitStepsHistoryData() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime = cal.timeInMillis
        cal.add(Calendar.YEAR, -1)
        val startTime = cal.timeInMillis
        val readRequest = DataReadRequest.Builder() //.read(DataType.TYPE_STEP_COUNT_DELTA)
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA) //.aggregate(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY)
                .bucketByTime(1, TimeUnit.DAYS)
                .enableServerQueries()
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build()
        val dataReadResult = Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).await(30, TimeUnit.SECONDS)
        //Used for aggregated data
        if (dataReadResult.buckets.size > 0) {
            Log.e("History", "Number of buckets: " + dataReadResult.buckets.size)
            for (bucket in dataReadResult.buckets) {
                val dataSets = bucket.dataSets
                for (dataSet in dataSets) {
                    showDataSet(dataSet)
                }
            }
        } else if (dataReadResult.dataSets.size > 0) {
            Log.e("History", "Number of returned DataSets: " + dataReadResult.dataSets.size)
            for (dataSet in dataReadResult.dataSets) {
                showDataSet(dataSet)
            }
        }
    }

    private fun showDataSet(dataSet: DataSet) {
        Log.e("History", "Data returned for Data type: " + dataSet.dataType.name)
        val dateFormat = DateFormat.getDateInstance()
        val timeFormat = DateFormat.getTimeInstance()
        for (dp in dataSet.dataPoints) {
            for (field in dp.dataType.fields) {
                val report = Report()
                report.testTakenAt = dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS))
                if (!isHeartRateData && field.name.equals("steps", ignoreCase = true)) {
                    report.stepsCount = dp.getValue(field).asInt()
                } else {
                    report.averageHeartRate = dp.getValue(field).asFloat().toInt()
                }
                mReportList?.add(report)
                Log.e("History", "\tField: " + field.name + " Value: " + dp.getValue(field) + "\n")
            }
        }
    }

    /**
     * Bind the survey activity adapter
     */
    protected fun bindAdapter() {
        if (mReportList!!.size <= 0) findViewById<View>(R.id.tvNodata).visibility = View.VISIBLE
        Collections.reverse(mReportList)
        mFitHistoryAdapter = FitHistoryAdapter(this, isHeartRateData)
        mFitHistoryAdapter!!.setListContent(mReportList)
        recyclerView!!.adapter = mFitHistoryAdapter
    }

    override fun onConnectionSuspended(i: Int) {
        Log.e("HistoryAPI", "onConnectionSuspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("HistoryAPI", "onConnectionFailed")
    }

    private inner class ViewHeartHistoryTask : AsyncTask<Void?, Void?, Void?>() {
        override fun onPreExecute() {
            super.onPreExecute()
            showLoader(getString(R.string.fetching_heart_rate))
        }

        protected override fun doInBackground(vararg params: Void?): Void? {
            readFitHeartHistoryData()
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            hideLoader()
            bindAdapter()
        }
    }

    private inner class ViewStepsHistoryTask : AsyncTask<Void?, Void?, Void?>() {
        override fun onPreExecute() {
            super.onPreExecute()
            showLoader(getString(R.string.fetching_steps_count))
        }

        protected override fun doInBackground(vararg params: Void?): Void? {
            readFitStepsHistoryData()
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            hideLoader()
            bindAdapter()
        }
    }
}