package com.dreamorbit.walktalktrack.screens.login

import com.dreamorbit.walktalktrack.database.registration.RegistartionData
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository.Companion.instance
import com.dreamorbit.walktalktrack.pojo.login.SignInRequest
import com.dreamorbit.walktalktrack.pojo.login.SignInResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton

/**
 * Created by nareshkumar.reddy on 11/20/2017.
 */
class LoginPresenter(private val loginView: LoginView, private val loginApi: LoginApi) {
    fun validateSignIn(email: Boolean, password: String) {
        if (!email) {
            loginView.setEmailError("Please enter valid email id")
        } else if (password.isEmpty()) {
            loginView.setPasswordError("Please enter valid password")
        } else {
            loginView.isValid()
        }
    }

    fun saveRegistartionDetails(registartionData: RegistartionData?) {
        instance!!.saveRegistrationData(registartionData!!)
    }

    fun callSignInApi(signInRequest: SignInRequest?) {
        loginApi.login(signInRequest, this)
    }

    fun onSuccess(body: SignInResponse?) {
        saveStudyConfiguration(body)
        loginView.onSuccess(body)
    }

    fun onFail(failureResponse: SignInResponse?) {
        loginView.onFailure(failureResponse)
    }

    fun onError(errorResponse: SignInResponse?) {
        loginView.onError(errorResponse)
    }

    fun onPartialRegistration(registerResponse: SignInResponse?) {
        loginView.onPartialRegistration(registerResponse)
    }

    private fun saveStudyConfiguration(regResponse: SignInResponse?) {
        SharedPrefSingleton.instance?.saveStudyConfig(regResponse?.studyConfiguration)
    }

}