package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.util.Log
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.researchaware.model.survey.Questions
import com.dreamorbit.walktalktrack.database.activities.ActivitiesRepository
import com.dreamorbit.walktalktrack.database.questions.QuestionsRepository
import com.dreamorbit.walktalktrack.database.report.ReportRepository
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.firebase.MyFirebaseMessagingService
import com.dreamorbit.walktalktrack.pojo.activities.Activities
import com.dreamorbit.walktalktrack.pojo.activities.ActivityList
import com.dreamorbit.walktalktrack.screens.bottombar.activities.ActivitiesPresenterContract.ActivityView
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import java.util.*

/**
 * Created by mujasam.bn on 10/26/2017.
 */
class ActivitiesPresenter(private val mAcactivityView: ActivityView, private val mActivitiesApi: ActivitiesApi, private val mSurveyQuestionsApi: SurveyQuestionsApi) {
    val activitiesApi: Unit
        get() {
            mActivitiesApi.getActivities(this)
        }

   /* val localDBActivities: List<ActivityList>
        get() = ActivitiesRepository.instance!!.getActivities()*/

    fun apiActivitiesSuccessResult(activityList: Activities?) {
        mAcactivityView.onLoad(activityList)
    }

    fun saveActivitiesLocally(activityList: List<ActivityList?>?): Long {
        return ActivitiesRepository.instance!!.saveData(activityList)
    }

    fun getSurveyQuestionsApi(id: Int) {
        mSurveyQuestionsApi.getSurveyQuestions(this, id)
    }

    fun getSurveyQuestionsDb(activityId: Int): List<Questions?>? {
        return QuestionsRepository.instance!!.getSurveyQuestions(activityId)
    }

    fun onSurveySuccessResponse(questions: QuestionList?) {
        mAcactivityView.onSurveyLoaded(questions)
    }

    fun updateSurveyAsComplted(activityId: Int): Long {
        return ActivitiesRepository.instance!!.updateSurveyCompleted(activityId)
    }

    fun HideProgressBar() {
        mAcactivityView.hideProgressBar()
    }

    fun saveSummary(reportSummary: List<PostReportRequest?>?): Long {
        val emailID = SharedPrefSingleton.instance!!.userEmail
        return ReportRepository.instance!!.saveReport(reportSummary, emailID)
    }

    fun saveSchedulerData(activityList: Activities) {
        val scheduler = SchedulerRepository.instance?.scheduler
        val schedulerList: MutableList<SchedulerModel?> = ArrayList() //to save the updated scheduler data into db
        for (activity in activityList.activityList!!) {
            val schedulerDetails = activity.schedulerDetails
            val time = Utility.convert24To12Format(schedulerDetails!!.scheduledAt)
            Log.e("12hr Time:", time)
            val dbModel = SchedulerRepository.instance?.getScheduler(activity.activityTypeId!!)
            if (schedulerDetails != null) {
                schedulerDetails.day = if (schedulerDetails.scheduleDays?.size!! > 0) schedulerDetails.scheduleDays!![0] else ""
                schedulerDetails.scheduledAt = time
                schedulerDetails.scheduleType = schedulerDetails.scheduleType
                schedulerDetails.status = if (dbModel != null) dbModel.status else "" //initially save empty or null in status so that user can take any number of test
                schedulerDetails.name = activity.name
                schedulerDetails.id = activity.id
            }
            schedulerList.add(schedulerDetails)
        }
        SchedulerRepository.instance?.saveScheduler(schedulerList)
        //First time launch so init alarm
        if (scheduler?.size == 0) {
            MyFirebaseMessagingService().initSchedulerAlarm(-1)
        }
    }

}