package com.dreamorbit.walktalktrack.screens.registration

import com.dreamorbit.walktalktrack.database.joinstudy.StudyData
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse

/**
 * Created by nareshkumar.reddy on 9/18/2017.
 */
interface RegisterViewContract {
    fun onRegistrationFailure(registerResponse: RegisterResponse?)
    fun onRegistrationSuccess(registerResponse: RegisterResponse?)
    fun onStudyDataLoaded(studyData: StudyData?)
    fun setValidationErrors(errors: String?)
    fun registerWebRequest()
    fun partialRegistration(response: RegisterResponse?)
    fun onCertificateDownloadSuccess()
}