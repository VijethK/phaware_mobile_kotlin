package com.dreamorbit.walktalktrack.screens.rating

import android.content.DialogInterface
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.*
import android.widget.RatingBar.OnRatingBarChangeListener
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.dialogs.AlertCallback
import com.dreamorbit.walktalktrack.dialogs.DialogClass.getAlertDialogOk
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import retrofit2.Response

class RatingActivity : BaseActivity(), RatingView, View.OnClickListener {
    private var ratingPresenter: RatingPresenter? = null
    private var mImgSmiley: ImageView? = null
    private var mEtDescription: EditText? = null
    private var mRatingBar: RatingBar? = null
    private var mBtnSubmit: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)
        initView()
        initToolbar()
    }

    /**
     * Initilising the views
     */
    private fun initView() {
        ratingPresenter = RatingPresenter(this, RatingApi())
        mImgSmiley = findViewById(R.id.imgSmiley)
        mRatingBar = findViewById(R.id.ratingBar)
        mEtDescription = findViewById(R.id.etRateDescription)
        mBtnSubmit = findViewById(R.id.btnSubmit)
        mBtnSubmit?.setOnClickListener(this)
        mRatingBar?.setOnRatingBarChangeListener(OnRatingBarChangeListener { ratingBar: RatingBar, v: Float, b: Boolean ->
            //ratingPresenter.onRatingChange(ratingBar.getRating()); // no need to change the icons
            if (ratingBar.rating > 0) mBtnSubmit?.setEnabled(true) else mBtnSubmit?.setEnabled(false)
        })
        if (Build.VERSION.SDK_INT <= 22) {
            val stars = mRatingBar?.getProgressDrawable() as LayerDrawable
            stars.getDrawable(2).setColorFilter(resources.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
        }
    }

    /**
     * Initialise the toolbar
     */
    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        title.text = "Rating"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                SharedPrefSingleton.instance?.saveRateCount(0) //show rating after taking one more test
                navigateToBottomBarActivity()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Change the image depend on the rate value
     *
     * @param rate
     */
    override fun onRateChange(rate: Float) {
        mRatingBar!!.rating = rate
        when (Math.round(rate)) {
            1 -> mImgSmiley!!.setBackgroundResource(R.drawable.verysad)
            2 -> mImgSmiley!!.setBackgroundResource(R.drawable.sad)
            3 -> mImgSmiley!!.setBackgroundResource(R.drawable.neutral)
            4 -> mImgSmiley!!.setBackgroundResource(R.drawable.happy)
            5 -> mImgSmiley!!.setBackgroundResource(R.drawable.veryhappy)
        }
    }

    /**
     * on press of submit button call the api
     */
    override fun onSubmitPressed() {
        if (ConnectivityReceiver.isConnected) {
            showLoader(getString(R.string.please_wait))
            ratingPresenter!!.onSubmit(mRatingBar!!.rating, mEtDescription!!.text.toString())
        } else {
            Utility.showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    /**
     * api response if succss
     *
     * @param response
     */
    override fun onSuccess(response: Response<*>?) {
        SharedPrefSingleton.instance?.saveRateCount(-1)
        hideLoader()
        showDialogOK(getString(R.string.thanks_rating), getString(R.string.ok), DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> navigateToBottomBarActivity()
            }
        })
    }

    /**
     * Navigate to bottom bar
     */
    private fun navigateToBottomBarActivity() {
        val i = Intent(this, BottomBarActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

    /**
     * api response if any failure occurs
     *
     * @param failureMessage
     */
    override fun onFailure(failureMessage: Response<*>?) {
        hideLoader()
    }

    /**
     * api response if any error occurs
     *
     * @param errorMessage
     */
    override fun onError(errorMessage: Response<*>?) {
        hideLoader()
    }

    /**
     * onclick of any buttons this methid will triggers
     *
     * @param view
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnSubmit -> if (ConnectivityReceiver.isConnected) {
                showLoader(getString(R.string.please_wait))
                ratingPresenter!!.onSubmit(mRatingBar!!.rating, mEtDescription!!.text.toString())
            } else {
                getAlertDialogOk(this@RatingActivity, getString(R.string.not_connected), getString(R.string.internet_disconnected), getString(R.string.ok), false,
                        object : AlertCallback {
                            override fun PositiveMethod(dialog: DialogInterface?, id: Int) {
                                finish()
                            }

                            override fun NegativeMethod(dialog: DialogInterface?, id: Int) {
                                finish()
                            }
                        })
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        SharedPrefSingleton.instance?.saveRateCount(0) //show rating after taking one more test
        navigateToBottomBarActivity()
    }
}