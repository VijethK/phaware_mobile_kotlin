package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import java.util.*

/**
 * Created by nareshkumar.reddy on 10/18/2017.
 * Fragment apger adpter to store all the fragments in pager adapter
 */
class AdditionalInfoPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private val additinalInfoFragments = ArrayList<Fragment>()
    override fun getItem(position: Int): Fragment {
        return additinalInfoFragments[position]
    }

    fun addpages(fragment: Fragment) {
        additinalInfoFragments.add(fragment)
    }

    override fun getCount(): Int {
        return additinalInfoFragments.size
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }
}