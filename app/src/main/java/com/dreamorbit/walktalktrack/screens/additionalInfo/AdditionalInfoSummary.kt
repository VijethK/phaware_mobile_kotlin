package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.screens.bottombar.profile.ProfileFragment

class AdditionalInfoSummary : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_additional_info_summary)
        val profileFragment = ProfileFragment()
        val bundledArgs = Bundle()
        bundledArgs.putBoolean(AppConstants.PROFILE_ABOUT, true)
        profileFragment.arguments = bundledArgs
        supportFragmentManager.beginTransaction().add(R.id.profile_container, profileFragment).commit()
        initToolbar()
    }

    //initializing toolbar
    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val title = toolbar.findViewById<TextView>(R.id.title)
        title.setText(R.string.additionalInfo)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(menuItem)
    }
}