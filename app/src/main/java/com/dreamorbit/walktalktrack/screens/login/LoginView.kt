package com.dreamorbit.walktalktrack.screens.login

import com.dreamorbit.walktalktrack.pojo.login.SignInResponse

/**
 * Created by nareshkumar.reddy on 11/20/2017.
 */
interface LoginView {
    fun setEmailError(error: String?)
    fun setPasswordError(error: String?)
    fun isValid() // Changed val isValid: Unit to  fun isValid()
    fun onSuccess(response: SignInResponse?)
    fun onFailure(failureMessage: SignInResponse?)
    fun onError(errorMessage: SignInResponse?)
    fun onPartialRegistration(body: SignInResponse?)
}