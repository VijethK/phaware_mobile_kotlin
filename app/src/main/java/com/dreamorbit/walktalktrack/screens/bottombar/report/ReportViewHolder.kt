package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.dreamorbit.walktalktrack.R

/**
 * Created by Mujas on 26-10-2017.
 */
//View holder class, where all view components are defined
class ReportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var tvTime: TextView
    var tvAvgHeartRate: TextView
    var tvMaxHeartRate: TextView
    var tvDistance: TextView
    var tvSteps: TextView
    var tvDuration: TextView
    override fun onClick(v: View) {}

    init {
        tvTime = itemView.findViewById(R.id.tvTime)
        tvAvgHeartRate = itemView.findViewById(R.id.tvAvgHeartRate)
        tvMaxHeartRate = itemView.findViewById(R.id.tvMaxHeartRate)
        tvDistance = itemView.findViewById(R.id.tvDistance)
        tvSteps = itemView.findViewById(R.id.tvSteps)
        tvDuration = itemView.findViewById(R.id.tvDuration)
    }
}