package com.dreamorbit.walktalktrack.screens.casting

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.dreamorbit.researchaware.model.fit.location.LocationData
import com.dreamorbit.researchaware.model.fit.location.LocationItem
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.shared.CastingData
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import java.util.*

/**
 * Created by mujasam.bn on 3/5/2018.
 */
class CastingReviewActivity : AppCompatActivity(), OnMapReadyCallback {
    private var mTvDistance: TextView? = null
    private var mTvAvgHearRate: TextView? = null
    private var mTvMaxHearRate: TextView? = null
    private var mTvDuration: TextView? = null
    private var mCastingData: CastingData? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_casting_review)
        initToolBar()
        initViews()
    }

    private fun initViews() {
        val castSummary = SharedPrefSingleton.instance?.castedData
        val gson = Gson()
        mCastingData = gson.fromJson(castSummary, CastingData::class.java)
        mTvDuration = findViewById(R.id.tvDuration)
        mTvDistance = findViewById(R.id.tvDistance)
        mTvAvgHearRate = findViewById(R.id.tvHeartRate)
        mTvMaxHearRate = findViewById(R.id.tvMaxHeartRate)
        if (mCastingData != null) {
            mTvDuration?.setText(mCastingData!!.timer)
            mTvDistance?.setText(String.format("%.0f", mCastingData!!.distance))
            mTvAvgHearRate?.setText("" + mCastingData!!.avgHeartRate)
            mTvMaxHearRate?.setText("" + mCastingData!!.heartRate)
        }
        initMap()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onBackPressed() { //super.onBackPressed();
    }

    /**
     * Initialize the mToolbar components
     * dynamic left margin to mToolbar title. Reusing the existing mToolbar
     */
    private fun initToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val mTvTitle = toolbar.findViewById<TextView>(R.id.title)
        val mTvCancel = toolbar.findViewById<TextView>(R.id.done)
        mTvTitle.setText(R.string.summary)
        mTvCancel.setText(R.string.done)
        mTvCancel.setOnClickListener { view: View? -> navigateToActivityScreen() }
    }

    private fun navigateToActivityScreen() {
        SharedPrefSingleton.instance?.saveCastData(null)
        val intent = Intent(this, BottomBarActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun onMapReady(map: GoogleMap) {
        val coordList = ArrayList<LatLng>()
        var maxLat: Double? = null
        var minLat: Double? = null
        var minLon: Double? = null
        var maxLon: Double? = null
        if (mCastingData != null && !TextUtils.isEmpty(mCastingData!!.locatons)) {
            val gson = Gson()
            val locationData = gson.fromJson(mCastingData!!.locatons, LocationData::class.java)
            if (locationData.items.size > 0) {
                val itr: Iterator<LocationItem> = locationData.items.iterator()
                while (itr.hasNext()) {
                    val item = itr.next()
                    coordList.add(LatLng(item.coordinate.latitude, item.coordinate.longitude))
                    // Find out the maximum and minimum latitudes & longitudes
// Latitude
                    maxLat = if (maxLat != null) Math.max(item.coordinate.latitude, maxLat) else item.coordinate.latitude
                    minLat = if (minLat != null) Math.min(item.coordinate.latitude, minLat) else item.coordinate.latitude
                    // Longitude
                    maxLon = if (maxLon != null) Math.max(item.coordinate.longitude, maxLon) else item.coordinate.longitude
                    minLon = if (minLon != null) Math.min(item.coordinate.longitude, minLon) else item.coordinate.longitude
                }
                val builder = LatLngBounds.Builder()
                builder.include(LatLng(maxLat!!, maxLon!!))
                builder.include(LatLng(minLat!!, minLon!!))
                map.setOnMapLoadedCallback {
                    map.addPolyline(PolylineOptions()
                            .addAll(coordList)
                            .width(INITIAL_STROKE_WIDTH_PX.toFloat())
                            .color(Color.BLUE)
                            .geodesic(true))
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 48))
                }
            }
        }
    }

    companion object {
        private const val INITIAL_STROKE_WIDTH_PX = 5
    }
}