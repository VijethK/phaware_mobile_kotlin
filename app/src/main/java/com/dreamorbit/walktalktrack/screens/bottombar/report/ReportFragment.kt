package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseFragment

class ReportFragment : BaseFragment() {
    private var mViewPager: ViewPager? = null
    private var mPagerAdapter: SectionsPagerAdapter? = null
    private var mTabLayout: TabLayout? = null
    private val tabTitle = arrayOf("Today", "Weekly", "Monthly", "Date Range")
    private val tabFragments = arrayOf<BaseReportFragment>(ReportTodayFragment(), ReportWeeklyFragment(), ReportMonthlyFragment(), ReportRangeFragment())
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_report, container, false)
        initToolbar(view)
        initViews(view)
        initPagerAdapter()
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val title = view.findViewById<TextView>(R.id.title)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        title.text = getString(R.string.menu_bottom_report)
        val imgProfile = toolbar.findViewById<TextView>(R.id.profile)
        imgProfile.setOnClickListener { v: View? -> loadProfileActivity() }
    }

    private fun initViews(view: View) {
        mViewPager = view.findViewById(R.id.container)
        mTabLayout = view.findViewById(R.id.tabLayout)
    }

    private fun initPagerAdapter() {
        var i = 0
        mPagerAdapter = SectionsPagerAdapter(activity!!.supportFragmentManager)
        for (fragment in tabFragments) {
            mPagerAdapter!!.addFragment(fragment, tabTitle[i])
            ++i
        }
        mViewPager!!.adapter = mPagerAdapter
        mTabLayout!!.setupWithViewPager(mViewPager)
        //to keep all the 4 fragments in memory at a time
        mViewPager!!.offscreenPageLimit = tabTitle.size
        mViewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivitiesFragment.
         */
        fun newInstance(): ReportFragment {
            return ReportFragment()
        }
    }
}