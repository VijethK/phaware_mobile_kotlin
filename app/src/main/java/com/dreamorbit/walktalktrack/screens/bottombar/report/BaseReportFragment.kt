package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager.BadTokenException
import android.widget.RelativeLayout
import android.widget.TextView
import com.dreamorbit.researchaware.model.report.Report
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.custom.SimpleDividerItemDecoration
import com.whiteelephant.monthpicker.MonthPickerDialog
import java.util.*

/**
 * Created by mujasam.bn on 6/29/2016.
 */
open class BaseReportFragment : Fragment() {
    protected var ploader: ProgressDialog? = null
    //Adapter
    protected var mReportAdapter: ReportAdapter? = null
    //RecyclerView
    @JvmField
    protected var mListParent: RelativeLayout? = null
    protected var recyclerView: RecyclerView? = null
    @JvmField
    protected var swipeRefreshLayout: SwipeRefreshLayout? = null
    @JvmField
    protected var mTvDate: TextView? = null
    protected var mTvTotalDistance: TextView? = null
    protected var mTvTotalStepsCount: TextView? = null
    @JvmField
    protected var tvNoData: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        return null
    }

    protected open fun initView(view: View) {
        mTvDate = view.findViewById(R.id.tvDate)
        mListParent = view.findViewById(R.id.listParent)
        tvNoData = view.findViewById(R.id.tvNoData)
        mTvTotalDistance = view.findViewById(R.id.totalDistance)
        mTvTotalStepsCount = view.findViewById(R.id.totalStepsCount)
        recyclerView = view.findViewById(R.id.reportRecycler)
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh)
        recyclerView?.setLayoutManager(LinearLayoutManager(this.activity))
        recyclerView?.addItemDecoration(SimpleDividerItemDecoration(this.activity))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    fun showLoader(message: String?) {
        activity!!.runOnUiThread(Runnable {
            if (ploader != null || activity!!.isFinishing) {
                return@Runnable
            }
            ploader = ProgressDialog(activity, R.style.MyProgressDialog)
            ploader!!.window.setBackgroundDrawable(ColorDrawable(0))
            ploader!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            ploader!!.setCancelable(false)
            ploader!!.isIndeterminate = true
            ploader!!.setMessage(message)
            try {
                ploader!!.show()
            } catch (e: BadTokenException) {
                ploader = null
            } catch (e: Exception) {
                ploader = null
            }
        })
    }

    fun hideLoader() {
        activity!!.runOnUiThread {
            if (ploader != null && ploader!!.isShowing) {
                ploader!!.dismiss()
            }
            ploader = null
        }
        swipeRefreshLayout!!.isRefreshing = false
    }

    protected fun dummyDataBind(): List<Report> {
        val mReportList: MutableList<Report> = ArrayList()
        for (i in 0..4) {
            val reportResponse = Report()
            mReportList.add(reportResponse)
        }
        return mReportList
    }

    /**
     * Bind the survey activity adapter
     *
     * @param reportList
     */
    protected fun bindAdapter(reportList: ReportResponse) {
        tvNoData!!.visibility = View.GONE
        mListParent!!.visibility = View.VISIBLE
       // mReportAdapter = ReportAdapter(this.activity)
        //Kotlin error above modified to below line
        mReportAdapter = ReportAdapter(activity!!.applicationContext)
        mReportAdapter!!.setListContent(reportList.reports)
        recyclerView!!.adapter = mReportAdapter
    }

    protected fun bindHeader(list: ReportResponse) {
        mTvTotalDistance!!.text = list.totalDistance.toString() + " m"
        mTvTotalStepsCount!!.text = list.totalSteps.toString() + ""
    }

    protected fun monthYearPicker(view: View) {
        val today = Calendar.getInstance()
        view.findViewById<View>(R.id.tvDate).setOnClickListener { v: View? ->
            val builder = MonthPickerDialog.Builder(context, MonthPickerDialog.OnDateSetListener { selectedMonth: Int, selectedYear: Int -> }, today[Calendar.YEAR], today[Calendar.MONTH])
            builder.setActivatedMonth(Calendar.JULY)
                    .setMinYear(2017)
                    .setActivatedYear(2018)
                    .setMaxYear(2018)
                    .setMinMonth(Calendar.FEBRUARY)
                    .setTitle("Select trading month")
                    .setMonthRange(Calendar.FEBRUARY, Calendar.NOVEMBER) // .setMaxMonth(Calendar.OCTOBER)
// .setYearRange(1890, 1890)
// .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
//.showMonthOnly()
// .showYearOnly()
                    .setOnMonthChangedListener { selectedMonth: Int -> }
                    .setOnYearChangedListener { selectedYear: Int -> }
                    .build()
                    .show()
        }
    }

    companion object {
        fun newInstance(param1: String?, param2: String?): BaseReportFragment {
            return BaseReportFragment()
        }
    }
}