package com.dreamorbit.walktalktrack.screens.login

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.login.SignInRequest
import com.dreamorbit.walktalktrack.pojo.login.SignInResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by nareshkumar.reddy on 11/20/2017.
 */
class LoginApi {
    fun login(signInRequest: SignInRequest?, loginPresenter: LoginPresenter) {
        val retrofit = getClient("")
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.signIn(signInRequest)
        call!!.enqueue(object : Callback<SignInResponse?> {
            override fun onResponse(call: Call<SignInResponse?>, response: Response<SignInResponse?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    loginPresenter.onSuccess(response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), SignInResponse::class.java)
                        if (response.code() == 412) { //loginPresenter.onPreConditionFail(errorResponse);
                        } else {
                            loginPresenter.onFail(errorResponse)
                        }
                    } catch (e: Exception) {
                        val SignInResponse = SignInResponse()
                        SignInResponse.errorMessage = "Please try again."
                        loginPresenter.onFail(SignInResponse)
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<SignInResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val registerResponse = SignInResponse()
                registerResponse.errorMessage = "Login failed. Please try again."
                loginPresenter.onError(registerResponse)
            }
        })
    }
}