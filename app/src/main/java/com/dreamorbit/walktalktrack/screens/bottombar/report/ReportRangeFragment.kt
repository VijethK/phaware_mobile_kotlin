package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import java.text.DateFormat
import java.util.*

class ReportRangeFragment : BaseReportFragment(), ReportView, View.OnClickListener {
    private val TAG = ReportRangeFragment::class.java.name
    private val mHasLoadedOnce = false // your boolean field
    private var mReportPresenter: ReportPresenter? = null
    private var etFromDate: EditText? = null
    private var etToDate: EditText? = null
    private var mCalendarFrom: Calendar? = null
    private var mCalendarTo: Calendar? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_ranges, container, false)
        mReportPresenter = ReportPresenter(this, ReportApi())
        initView(view)
        swipeRefreshLayout!!.setOnRefreshListener { setDateRangeReport() }
        return view
    }

    override fun initView(view: View) {
        super.initView(view)
        etFromDate = view.findViewById(R.id.etFromDate)
        etToDate = view.findViewById(R.id.etToDate)
        etFromDate?.setOnClickListener(this)
        etToDate?.setOnClickListener(this)
    }

    /**
     * Getting From and To date from Util class
     */
    private fun setDateRangeReport() {
        if (ConnectivityReceiver.isConnected) {
            if (mCalendarFrom != null && mCalendarTo != null) {
                val dateFrom: Date
                val dateTo: Date
                if (mCalendarFrom!!.time.after(mCalendarTo!!.time)) {
                    dateFrom = mCalendarTo!!.time
                    dateTo = mCalendarFrom!!.time
                } else {
                    dateFrom = mCalendarFrom!!.time
                    dateTo = mCalendarTo!!.time
                }
                showLoader(getString(R.string.please_wait))
                mReportPresenter!!.getReportApi(SdkUtils.getRangeDate(dateFrom), SdkUtils.getRangeDate(dateTo))
            } else { //Utility.showSnackBar(getActivity().findViewById(android.R.id.content), getString(R.string.please_select_date_range), Snackbar.LENGTH_LONG);
                swipeRefreshLayout!!.isRefreshing = false
            }
        } else {
            showSnackBar(activity!!.findViewById(android.R.id.content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    override fun onReportSuccess(list: ReportResponse) {
        if (list != null && list.reports.size > 0) {
            bindHeader(list)
            bindAdapter(list)
        } else {
            tvNoData!!.visibility = View.VISIBLE
            mListParent!!.visibility = View.GONE
        }
        hideLoader()
    }

    override fun onReportFailure() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    override fun onReportError() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.etFromDate -> pickerFromDialog()
            R.id.etToDate -> pickerToDialog(mCalendarFrom)
        }
    }

    private fun pickerFromDialog() {
        if (mCalendarFrom == null) {
            mCalendarFrom = Calendar.getInstance()
            mCalendarFrom?.set(Calendar.HOUR_OF_DAY, 0)
            mCalendarFrom?.set(Calendar.MINUTE, 59)
            mCalendarFrom?.set(Calendar.SECOND, 59)
        }
        val mYear = mCalendarFrom!![Calendar.YEAR]
        val mMonth = mCalendarFrom!![Calendar.MONTH]
        val mDay = mCalendarFrom!![Calendar.DAY_OF_MONTH]
        val datePickerDialog = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
            mCalendarFrom = Calendar.getInstance()
            mCalendarFrom?.set(year, monthOfYear, dayOfMonth)
            etFromDate!!.setText(DateFormat.getDateInstance(DateFormat.MEDIUM).format(mCalendarFrom?.getTime()))
            etToDate!!.isEnabled = true
            setDateRangeReport()
        }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.maxDate = currentDateTimeInMillis
        datePickerDialog.show()
    }

    /**
     * Samsung issue
     * Last day of DatePickerDialog is not selectable after setting max date
     *
     * @return
     */
    private val currentDateTimeInMillis: Long
        private get() {
            val maxDate = Calendar.getInstance()
            maxDate[Calendar.HOUR_OF_DAY] = 23
            maxDate[Calendar.MINUTE] = 59
            maxDate[Calendar.SECOND] = 59
            return maxDate.timeInMillis
        }

    private fun pickerToDialog(calendarFrom: Calendar?) {
        val CalendarTo = Calendar.getInstance()
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        if (mCalendarTo == null) {
            CalendarTo[Calendar.HOUR_OF_DAY] = 23
            CalendarTo[Calendar.MINUTE] = 59
            CalendarTo[Calendar.SECOND] = 59
            mYear = CalendarTo[Calendar.YEAR]
            mMonth = CalendarTo[Calendar.MONTH]
            mDay = CalendarTo[Calendar.DAY_OF_MONTH]
        } else {
            mYear = mCalendarTo!![Calendar.YEAR]
            mMonth = mCalendarTo!![Calendar.MONTH]
            mDay = mCalendarTo!![Calendar.DAY_OF_MONTH]
        }
        val datePickerDialog = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
            mCalendarTo = Calendar.getInstance()
            mCalendarTo?.set(year, monthOfYear, dayOfMonth)
            etToDate!!.setText(DateFormat.getDateInstance(DateFormat.MEDIUM).format(mCalendarTo?.getTime()))
            if (ConnectivityReceiver.isConnected) {
                setDateRangeReport() //call the api
            } else {
                showSnackBar(activity!!.findViewById(android.R.id.content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            }
        }, mYear, mMonth, mDay)
        //Assign the Fromcalendar to local variables
        //if current date is not more than 6 months then add the current datetime to max limit
//user should not able to select more than current date from calendar
        calendarFrom!!.add(Calendar.MONTH, 6)
        calendarFrom[Calendar.HOUR_OF_DAY] = 0
        calendarFrom[Calendar.MINUTE] = 59
        calendarFrom[Calendar.SECOND] = 59
        var maxDateTimeInMilli = calendarFrom.timeInMillis
        if (currentDateTimeInMillis < maxDateTimeInMilli) {
            maxDateTimeInMilli = currentDateTimeInMillis
        }
        datePickerDialog.datePicker.maxDate = maxDateTimeInMilli
        //disable previous date selection from today to 2 months
        calendarFrom.add(Calendar.MONTH, -6)
        datePickerDialog.datePicker.minDate = calendarFrom.timeInMillis
        datePickerDialog.show()
    }

    companion object {
        fun newInstance(): ReportRangeFragment {
            return ReportRangeFragment()
        }
    }
}