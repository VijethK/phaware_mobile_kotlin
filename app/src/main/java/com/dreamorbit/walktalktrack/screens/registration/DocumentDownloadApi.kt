package com.dreamorbit.walktalktrack.screens.registration

import android.util.Log
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.walktalktrack.api.ApiClient.awsDocumentClient
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.pojo.document.DocumentResponse
import com.dreamorbit.walktalktrack.screens.assent.AssentPresenter
import com.dreamorbit.walktalktrack.screens.consent.ConsentPresenter
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

/**
 * Created by mujasam.bn on 07/06/2019.
 */
class DocumentDownloadApi {
    private var mAwsDownloadURL: String? = null
    private val TAG = "DocumentDownloadApi"
    fun downloadDocument(consentPresenter: ConsentPresenter?, assentPresenter: AssentPresenter?, docType: String) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val retrofitAws = awsDocumentClient
        val serviceAws = retrofitAws.create(IApiRepo::class.java)
        try {
            service.getStudyDocument(docType)
                    ?.subscribeOn(Schedulers.io()) // "work" on io thread
                    ?.subscribe(object : SingleObserver<DocumentResponse?> {
                        override fun onSubscribe(disposable: Disposable) {}
                        override fun onSuccess(documentResponse: DocumentResponse) {
                            mAwsDownloadURL = documentResponse.link
                            serviceAws.downloadFromAws(mAwsDownloadURL)!!.enqueue(object : Callback<ResponseBody?> {
                                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                    writeResponseBodyToDisk(response.body(), docType, consentPresenter, assentPresenter)
                                }

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {}
                            })
                        }

                        override fun onError(throwable: Throwable) {}
                    })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun writeResponseBodyToDisk(body: ResponseBody?, docType: String, consentPresenter: ConsentPresenter?, assentPresenter: AssentPresenter?): Boolean {
        return try {
            val directory = File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath().toString() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
            if (!directory.exists()) directory.mkdir()
            val file = File(directory, "$docType.html")
            if (file.exists()) file.delete()
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                val fileSize = body!!.contentLength()
                var fileSizeDownloaded: Long = 0
                inputStream = body.byteStream()
                outputStream = FileOutputStream(file)
                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1) {
                        break
                    }
                    outputStream?.write(fileReader, 0, read)
                    fileSizeDownloaded += read.toLong()
                    Log.d(TAG, "file download: $fileSizeDownloaded of $fileSize")
                }
                outputStream?.flush()
                true
            } catch (e: IOException) {
                false
            } finally {
                inputStream?.close()
                outputStream?.close()
                if (consentPresenter != null) {
                    consentPresenter.onDownloadComplete()
                } else {
                    assentPresenter?.onDownloadComplete()
                }
            }
        } catch (e: IOException) {
            false
        }
    }
}