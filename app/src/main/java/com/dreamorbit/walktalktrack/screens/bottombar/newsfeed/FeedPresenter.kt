package com.dreamorbit.walktalktrack.screens.bottombar.newsfeed

import com.dreamorbit.walktalktrack.pojo.feed.FeedItem

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
class FeedPresenter(private val feedView: FeedView, private val rssApi: RssApi) {
    val feed: Unit
        get() {
            rssApi.getRss(this)
        }

    fun onSuccess(feedList: List<FeedItem?>?) {
        feedView.onSuccess(feedList)
    }

    fun onFailure(message: String?) {
        feedView.onFailure(message)
    }

    fun onError(message: String?) {
        feedView.onError(message)
    }

}