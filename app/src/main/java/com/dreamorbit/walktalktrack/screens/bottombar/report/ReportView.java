package com.dreamorbit.walktalktrack.screens.bottombar.report;

import com.dreamorbit.researchaware.model.report.ReportResponse;

/**
 * Created by mujasambn on 08/13/2018.
 */

public interface ReportView {
    void onReportSuccess(ReportResponse body);
    void onReportFailure();
    void onReportError();
}
