package com.dreamorbit.walktalktrack.screens.bottombar.newsfeed

import android.util.Log
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.api.RssApiClient.client
import com.dreamorbit.walktalktrack.pojo.feed.Feed
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
class RssApi {
    fun getRss(feedPresenter: FeedPresenter) {
        val retrofit = client
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.phAwareFeed
        call!!.enqueue(object : Callback<Feed?> {
            override fun onResponse(call: Call<Feed?>, response: Response<Feed?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response)
                    feedPresenter.onSuccess(response.body()!!.getmChannel()!!.feedItems)
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), RegisterResponse::class.java)
                        feedPresenter.onFailure(errorResponse.errorMessage)
                    } catch (e: Exception) {
                        val registerResponse = RegisterResponse()
                        registerResponse.errorMessage = "Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<Feed?>, t: Throwable) {
                Log.e("", "" + t.toString())
                feedPresenter.onError("Please try again.")
            }
        })
    }
}