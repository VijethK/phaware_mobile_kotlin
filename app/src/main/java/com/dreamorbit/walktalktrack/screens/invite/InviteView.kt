package com.dreamorbit.walktalktrack.screens.invite

interface InviteView {
    fun onValidationSuccess(response: String?)
    fun onValidationFail(response: String?)
    fun onApiError(error: String?)
}