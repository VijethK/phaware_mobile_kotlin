package com.dreamorbit.walktalktrack.screens.bottombar.profile

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.*
import android.widget.RelativeLayout
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.APP_LOGOUT
import com.dreamorbit.walktalktrack.database.activities.ActivitiesRepository
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfo
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository
import com.dreamorbit.walktalktrack.database.flow.FlowRepository
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository
import com.dreamorbit.walktalktrack.database.questions.QuestionsRepository
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.database.report.ReportRepository
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.additionalInfo.AdditionalInfoRequest
import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoApi
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoPreseneter
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoView
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.screens.bottombar.learn.DescriptionActivity
import com.dreamorbit.walktalktrack.screens.bottombar.profile.ProfileFragment
import com.dreamorbit.walktalktrack.utilities.RSAEncryption
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.wear.MobileMessageSenderAsync
import com.dreamorbit.walktalktrack.welcome.WelcomeActivity
import java.io.UnsupportedEncodingException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.BadPaddingException
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 */
class ProfileFragment : Fragment(), AdditionalInfoView, View.OnClickListener, ProfileView {
    private var additionalInfo: AdditionalInfo? = null
    private var additionalInfoPreseneter: AdditionalInfoPreseneter? = null
    private var profile = false
    private val encrypted = "***"
    private var profilePresenter: ProfilePresenter? = null
    private var androidWatch: Switch? = null
    private var fitbit: Switch? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        profile = arguments!!.getBoolean(AppConstants.PROFILE_ABOUT)
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        profilePresenter = ProfilePresenter(this, SignOutApi(), WithdrawApi(), GetProfileApi())
        initViews(view)
        return view
    }

    //initializing views
    private fun initViews(view: View) { //getting data from AdditionalInfoRepository and assign it to views
        additionalInfo = AdditionalInfoRepository.instance!!.additionalInfo
        val age = view.findViewById<TextView>(R.id.tvAge)
        val gender = view.findViewById<TextView>(R.id.tvGender)
        val height = view.findViewById<TextView>(R.id.tvHeight)
        val weight = view.findViewById<TextView>(R.id.tvWeight)
        val diagnoised = view.findViewById<TextView>(R.id.tvDiagnosed)
        val medicated = view.findViewById<TextView>(R.id.tvMedication)
        androidWatch = view.findViewById(R.id.watch_switch)
        fitbit = view.findViewById(R.id.fitbit_switch)
        age.text = if (profile) StudyRepository.instance!!.studyData!!.age else encrypted
        gender.text = if (profile) additionalInfo!!.gender else encrypted
        height.text = if (profile) additionalInfo!!.height else encrypted
        weight.text = if (profile) additionalInfo!!.weigh else encrypted
        diagnoised.text = if (profile) additionalInfo!!.diagnosed else encrypted
        medicated.text = if (profile) additionalInfo!!.medication else encrypted
        androidWatch?.setClickable(false)
        fitbit?.setClickable(false)
        setWearble(additionalInfo!!)
        //if navigates from BottonBarActivity profile will be false always
        if (!profile) { //Though we removed the Watch and Firbit screen selection option
//No need to call this api to get/sync the Updated wear option in some other phone using same credentials
/*
            Utility.showProgressBar(getActivity(), getString(R.string.please_wait));
            profilePresenter.getUserProfileApi();
            androidWatch.setClickable(true);
            fitbit.setClickable(true);
            androidWatch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean status) {
                    if (status) {
                        fitbit.setChecked(false);
                        AdditionalInfoRepository.getInstance().saveWearable(getString(R.string.watch));
                    } else {
                        androidWatch.setChecked(false);
                        AdditionalInfoRepository.getInstance().saveWearable("");
                    }
                }
            });
            fitbit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean status) {
                    if (status) {
                        androidWatch.setChecked(false);
                        AdditionalInfoRepository.getInstance().saveWearable(getString(R.string.fitbit));
                    } else {
                        fitbit.setChecked(false);
                        AdditionalInfoRepository.getInstance().saveWearable("");
                    }
                }
            });*/
            val userName = view.findViewById<RelativeLayout>(R.id.user_layout)
            userName.visibility = View.VISIBLE
            val tvUserName = view.findViewById<TextView>(R.id.tvUserName)
            tvUserName.text = "***"
            tvUserName.setOnClickListener(this)
            val email = view.findViewById<RelativeLayout>(R.id.email_layout)
            email.visibility = View.VISIBLE
            val tvEmail = view.findViewById<TextView>(R.id.tvEmail)
            tvEmail.visibility = View.VISIBLE
            tvEmail.text = "***"
            tvUserName.setOnClickListener(this)
            //view.findViewById(R.id.health_band).setVisibility(View.VISIBLE);
            val policy = view.findViewById<RelativeLayout>(R.id.policy_layout)
            policy.visibility = View.VISIBLE
            val tvPolicy = view.findViewById<TextView>(R.id.policy)
            tvPolicy.setOnClickListener(this)
            val feedback = view.findViewById<RelativeLayout>(R.id.feedback_layout)
            feedback.visibility = View.VISIBLE
            val tvFeedBack = view.findViewById<TextView>(R.id.feedBack)
            tvFeedBack.setOnClickListener(this)
            val withDraw = view.findViewById<RelativeLayout>(R.id.withdraw_layout)
            withDraw.visibility = View.VISIBLE
            val tvWithDraw = view.findViewById<TextView>(R.id.withDraw)
            tvWithDraw.setOnClickListener(this)
            val signOut = view.findViewById<RelativeLayout>(R.id.signout_layout)
            signOut.visibility = View.VISIBLE
            val tvSignOut = view.findViewById<TextView>(R.id.signOut)
            tvSignOut.setOnClickListener(this)
            view.findViewById<View>(R.id.tvAge).setOnClickListener(this)
            view.findViewById<View>(R.id.tvGender).setOnClickListener(this)
            view.findViewById<View>(R.id.tvHeight).setOnClickListener(this)
            view.findViewById<View>(R.id.tvWeight).setOnClickListener(this)
            view.findViewById<View>(R.id.tvDiagnosed).setOnClickListener(this)
            view.findViewById<View>(R.id.tvMedication).setOnClickListener(this)
        }
    }

    private fun setWearble(additionalInfo: AdditionalInfo) {
        if (additionalInfo.wear != null) {
            if (additionalInfo.wear.equals(getString(R.string.watch), ignoreCase = true)) {
                androidWatch!!.isChecked = true
                fitbit!!.isChecked = false
            } else if (additionalInfo.wear.equals(getString(R.string.fitbit), ignoreCase = true)) {
                androidWatch!!.isChecked = false
                fitbit!!.isChecked = true
            } else {
                androidWatch!!.isChecked = false
                fitbit!!.isChecked = false
            }
        } else {
            androidWatch!!.isChecked = false
            fitbit!!.isChecked = false
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.policy -> {
                val intents = Intent(activity, DescriptionActivity::class.java)
                intents.putExtra(AppConstants.PAGE_TITLE, getString(R.string.privacy_policy))
                intents.putExtra(AppConstants.HTML_FILE, "https://www.google.com")
                startActivity(intents)
            }
            R.id.feedBack -> {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:info@phAware.com"))
                //To add subject
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                if (intent.resolveActivity(activity!!.packageManager) != null) {
                    startActivity(intent)
                } else {
                    Toast.makeText(activity, "There are no apps to support your actions on your mobile", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.withDraw -> withDrawAlert(activity)
            R.id.signOut -> {
                val pendingFiles = isPendingUpload
                if (pendingFiles) {
                    Utility.showSnackBar(getView(), getString(R.string.survey_upload_pending), Snackbar.LENGTH_LONG)
                    return
                }
                signOutAlert(activity)
            }
            R.id.tvAge, R.id.tvGender, R.id.tvHeight, R.id.tvWeight, R.id.tvDiagnosed, R.id.tvMedication, R.id.tvUserName, R.id.tvEmail -> encryptAlertt(activity)
        }
    }

    private val isPendingUpload: Boolean
        private get() {
            var pendingFiles = false
            val directories = SdkUtils.getAllDirectories(activity)
            for (dir in directories) {
                if (dir.isFile && dir.name.endsWith(".zip")) {
                    pendingFiles = true
                } else if (dir.isDirectory) {
                    pendingFiles = true
                }
            }
            return pendingFiles
        }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (profile) {
            inflater.inflate(R.menu.menu_main, menu)
        } else {
            inflater.inflate(R.menu.menu_band, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                val publicKey = SharedPrefSingleton.instance?.publicKey
                if (TextUtils.isEmpty(publicKey)) {
                    profilePresenter!!.downloadCertificate()
                } else {
                    additionalInfoRequestEncrypt()
                }
            }
            R.id.action_save -> additionalInfoRequest()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun additionalInfoRequestEncrypt() {
        Utility.showProgressBar(activity, getString(R.string.please_wait))
        val additionalInfoRequest = AdditionalInfoRequest()
        try {
            val studyData = StudyRepository.instance!!.studyData
            additionalInfoRequest.actual_email = RSAEncryption.encryptText(RegistartionRepository.instance!!.registrationEmail)
            additionalInfoRequest.gender = RSAEncryption.encryptText(additionalInfo!!.gender)
            additionalInfoRequest.height = RSAEncryption.encryptText(additionalInfo!!.height)
            additionalInfoRequest.weight = RSAEncryption.encryptText(additionalInfo!!.weigh)
            additionalInfoRequest.diagnosed_with_ph = RSAEncryption.encryptText(additionalInfo!!.diagnosed)
            additionalInfoRequest.medication_for_ph = RSAEncryption.encryptText(additionalInfo!!.medication)
            additionalInfoRequest.birthdate = RSAEncryption.encryptText(studyData!!.year.toString() + "-" + String.format("%02d", studyData.month) + "-" + String.format("%02d", studyData.day))
            additionalInfoRequest.sync_wearable = AdditionalInfoRepository.instance!!.additionalInfo.wear
            val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd")
            val cal = Calendar.getInstance()
            //Assign present day to end Date
            additionalInfoRequest.survey_end_date = dateFormat.format(cal.time)
            //Assign yesterday day to start Date
            cal.add(Calendar.DATE, -1)
            additionalInfoRequest.survey_start_date = dateFormat.format(cal.time)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        additionalInfoPreseneter = AdditionalInfoPreseneter(this@ProfileFragment, AdditionalInfoApi())
        if (ConnectivityReceiver.isConnected) {
            additionalInfoPreseneter!!.callAdditionalInfoService(additionalInfoRequest)
        } else {
            Utility.dismissProgressBar()
            Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    private fun additionalInfoRequest() {
        additionalInfo = AdditionalInfoRepository.instance!!.additionalInfo
        Utility.showProgressBar(activity, getString(R.string.please_wait))
        val additionalInfoRequest = AdditionalInfoRequest()
        try {
            additionalInfoRequest.actual_email = RSAEncryption.encryptText(RegistartionRepository.instance!!.registrationEmail)
            additionalInfoRequest.gender = additionalInfo!!.gender
            additionalInfoRequest.height = additionalInfo!!.height
            additionalInfoRequest.weight = additionalInfo!!.weigh
            additionalInfoRequest.diagnosed_with_ph = additionalInfo!!.diagnosed
            additionalInfoRequest.medication_for_ph = additionalInfo!!.medication
            additionalInfoRequest.sync_wearable = AdditionalInfoRepository.instance!!.additionalInfo.wear
            val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd")
            val cal = Calendar.getInstance()
            //Assign present day to end Date
            additionalInfoRequest.survey_end_date = dateFormat.format(cal.time)
            //Assign yesterday day to start Date
            cal.add(Calendar.DATE, -1)
            additionalInfoRequest.survey_start_date = dateFormat.format(cal.time)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        additionalInfoPreseneter = AdditionalInfoPreseneter(this@ProfileFragment, AdditionalInfoApi())
        if (ConnectivityReceiver.isConnected) {
            additionalInfoPreseneter!!.callAdditionalInfoService(additionalInfoRequest)
        } else {
            Utility.dismissProgressBar()
            Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    override fun onSuccess() {
        Utility.dismissProgressBar()
        if (profile) {
            AdditionalInfoRepository.instance!!.saveWearable(AdditionalInfoRepository.instance!!.additionalInfo.wear)
            FlowRepository.instance!!.saveFlow(AppConstants.ADDITIONAL_INFO_FLOW, true)
            val intent = Intent(activity, BottomBarActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            activity!!.finish()
        } else {
            setWearble(AdditionalInfoRepository.instance!!.additionalInfo)
        }
    }

    /**
     * Show withDraw related message in dialog
     *
     * @param context
     */
    private fun withDrawAlert(context: Context?) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context!!)
        builder.setMessage(R.string.withdraw_study)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                    if (ConnectivityReceiver.isConnected) {
                        Utility.showProgressBar(activity, getString(R.string.please_wait))
                        profilePresenter!!.callWithDrawApi(RegistartionRepository.instance!!.registrationEmailSha)
                    } else {
                        Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    private fun signOutAlert(context: Context?) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context!!)
        builder.setMessage(R.string.signout_msg)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                    if (ConnectivityReceiver.isConnected) {
                        Utility.showProgressBar(activity, getString(R.string.please_wait))
                        profilePresenter!!.callSignOutApi(RegistartionRepository.instance!!.registrationEmailSha)
                    } else {
                        Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    private fun encryptAlertt(context: Context?) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context!!)
        builder.setMessage(R.string.encrypt_msg)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    override fun onFailure() {
        Utility.dismissProgressBar()
        Toast.makeText(activity, R.string.profile_failed, Toast.LENGTH_SHORT).show()
    }

    override fun onError() {
        Utility.dismissProgressBar()
        Toast.makeText(activity, R.string.profile_failed, Toast.LENGTH_SHORT).show()
    }

    override fun signOutSuccess() {
        Utility.dismissProgressBar()
        clearAppData()
        MobileMessageSenderAsync().execute(APP_LOGOUT)
    }

    private fun clearAppData() {
        val intent = Intent(activity, WelcomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        SharedPrefSingleton.instance?.clearPhAwarePrefs(SharedPrefSingleton.ACCESS_TOKEN, SharedPrefSingleton.ACCESS_TOKEN, SharedPrefSingleton.COUNTRY, SharedPrefSingleton.EMAIL, SharedPrefSingleton.IS_USA)
        SdkSharedPrefSingleton.getInstance().clearActiveResearchPrefs(SdkSharedPrefSingleton.SUMMARY)
        ActivitiesRepository.instance!!.deleteActivities()
        AdditionalInfoRepository.instance!!.deleteAdditionalInfo()
        FlowRepository.instance!!.deleteFlow()
        StudyRepository.instance!!.deleteStudyData()
        QuestionsRepository.instance!!.deleteQuestions()
        RegistartionRepository.instance!!.deleteRegistrationData()
        ReportRepository.instance!!.deleteReportData()
        startActivity(intent)
    }

    override fun signOutFailure(failure: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(activity, failure, Toast.LENGTH_SHORT).show()
    }

    override fun signOutError(error: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
    }

    override fun withDrawSuccess() {
        Utility.dismissProgressBar()
        MobileMessageSenderAsync().execute(APP_LOGOUT) // Stop Wear 6mwt and navigate to Main screen.
        clearAppData()
    }

    override fun getProfileSuccess(profileResponse: ProfileResponse?) {
        Utility.dismissProgressBar()
        AdditionalInfoRepository.instance!!.saveDaignoised(profileResponse?.diagnosedWithPh)
        AdditionalInfoRepository.instance!!.saveMedicated(profileResponse?.medicationForPh)
        AdditionalInfoRepository.instance!!.saveGender(profileResponse?.gender)
        AdditionalInfoRepository.instance!!.saveHeight(profileResponse?.height)
        AdditionalInfoRepository.instance!!.saveWeight(profileResponse?.weight)
        AdditionalInfoRepository.instance!!.saveWearable(profileResponse?.syncWearable)
        setWearble(AdditionalInfoRepository.instance!!.additionalInfo)
    }

    override fun getProfileFailure(failure: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(activity, failure, Toast.LENGTH_SHORT).show()
    }

    override fun getProfileError(error: String?) {
        Utility.dismissProgressBar()
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
    }

    override fun onCertificateDownloadSuccess() {
        additionalInfoRequestEncrypt()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivitiesFragment.
         */
// TODO: Rename and change types and number of parameters
        fun newInstance(): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()
            //args.putString(ARG_PARAM1, param1);
//args.putString(ARG_PARAM2, param2);
            fragment.arguments = args
            return fragment
        }
    }
}