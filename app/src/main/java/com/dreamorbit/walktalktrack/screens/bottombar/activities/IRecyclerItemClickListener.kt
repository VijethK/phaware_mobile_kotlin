package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.view.View

/**
 * Created by mujasam.bn on 10/27/2017.
 */
interface IRecyclerItemClickListener {
    fun onClick(view: View?, position: Int, id: Int)
    fun onLongClick(view: View?, position: Int)
}