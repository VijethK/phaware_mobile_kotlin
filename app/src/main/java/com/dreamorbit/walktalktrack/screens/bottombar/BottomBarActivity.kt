package com.dreamorbit.walktalktrack.screens.bottombar

import android.annotation.SuppressLint
import android.content.*
import android.os.Bundle
import android.os.Environment
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.MenuItem
import com.crashlytics.android.Crashlytics
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.APP_LOGINED
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_HOUR
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_MIN
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.dialogs.AlertCallback
import com.dreamorbit.walktalktrack.dialogs.DialogClass.getAlertDialogOk
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.bottombar.activities.ActivitiesFragment
import com.dreamorbit.walktalktrack.screens.bottombar.dashboard.DashBoardFragment
import com.dreamorbit.walktalktrack.screens.bottombar.learn.LearnFragment
import com.dreamorbit.walktalktrack.screens.bottombar.newsfeed.NewsFragment
import com.dreamorbit.walktalktrack.screens.bottombar.report.ReportFragment
import com.dreamorbit.walktalktrack.screens.casting.CastingActivity
import com.dreamorbit.walktalktrack.service.alarm.NotificationHelper
import com.dreamorbit.walktalktrack.utilities.FileHelper
import com.dreamorbit.walktalktrack.utilities.RSAEncryption
import com.dreamorbit.walktalktrack.utilities.RSAEncryption.Companion.encryptText
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.wear.MobileMessageSenderAsync
import java.io.File
import java.io.UnsupportedEncodingException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import javax.crypto.BadPaddingException
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException

/**
 * Created by mujasam.bn on 10/25/2017.
 */
class BottomBarActivity : BaseActivity(), ConnectivityReceiver.ConnectivityReceiverListener {
    private var bottomNavigationView: BottomNavigationView? = null
    private var selectedFragment: Fragment? = null
    private var mFragmentTag: String? = null
    private var isSurveyCompleted = false
    private var mActivityId = 0
    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "message_receiver" is broadcasted.
//to confirm from wear that App is logined already to start 6MWT in Wearables
    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action.equals(CastingActivity.WEAR_MESSAGE_INTENT_FILTER, ignoreCase = true)) {
                val message = intent.getStringExtra(CastingActivity.WEAR_MESSAGE)
                if (message.equals("null", ignoreCase = true)) {
                    MobileMessageSenderAsync().execute(APP_LOGINED)
                    Utility.sendTestSchedulerToWear() //send notification to Wearable to enable or disable test taking job
                }
            } else if (intent.action.equals(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL, ignoreCase = true)) {
                requestCallPermission(this@BottomBarActivity)
            } else if (intent.action.equals(AppConstants.WEAR_MESSAGE_SCHEDULER_STATUS, ignoreCase = true)) {
                val fragmentManager = this@BottomBarActivity.supportFragmentManager
                val visibleFragment = fragmentManager.findFragmentById(R.id.parent_frame)
                if (visibleFragment is ActivitiesFragment) {
                    visibleFragment.refreshAdapter() // just refresh the adapter once test has been taken in watch
                }
            } else if (intent.action.equals(AppConstants.ACTIVITY_HAS_RESET, ignoreCase = true)) {
                val fragmentManager = this@BottomBarActivity.supportFragmentManager
                val visibleFragment = fragmentManager.findFragmentById(R.id.parent_frame)
                if (visibleFragment is ActivitiesFragment) {
                    visibleFragment.refreshAdapter() //Activity has been reset
                }
            } else if (intent.action.equals(AppConstants.RATE_APP, ignoreCase = true)) {
                displayRatingAlert()
            } else if (intent.action.equals(AppConstants.OFFLINE_ALERT, ignoreCase = true)) { ///if 6MWT taken in Wear and Phone doesn't have internet connectivity
                if (!ConnectivityReceiver.isConnected) getAlertDialogOk(this@BottomBarActivity, getString(R.string.not_connected), getString(R.string.test_upload_alert), getString(R.string.ok), false,
                        object : AlertCallback {
                            override fun PositiveMethod(dialog: DialogInterface?, id: Int) {}
                            override fun NegativeMethod(dialog: DialogInterface?, id: Int) {}
                        })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottombar)
        onNewIntent(intent)
        initViews()
        initBottomNavigation()
        disableShiftMode()
        initAlarms()
        logUser()
        displayRatingAlert()
        //testTextEncryption();
    }

    private fun logUser() { // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("12345")
        Crashlytics.setUserEmail(SharedPrefSingleton.instance?.userEmail)
        Crashlytics.setUserName("Stanford Phone User")
        //throw new RuntimeException("This is a crash");
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        if (intent != null && intent.extras != null) {
            isSurveyCompleted = intent.getBooleanExtra(SdkConstant.IS_SURVEY_COMPLETED, false)
            mActivityId = intent.getIntExtra(SdkConstant.SURVERY_ID, -1)
        }
    }

    private fun initAlarms() { // Refresh the Survey activity everyday 7 a.m
//NotificationHelper.scheduleResetRTCNotification(this, false); // As discussed we will be resetting the task on Scheduler notification
// Rating screen weekly 8 a.m
        NotificationHelper.scheduleRatingRTCNotification(this, REPEAT_HOUR, REPEAT_MIN)
        //Enable Boot receiver
        NotificationHelper.enableBootReceiverReset(this)
    }

    private fun initViews() { //Initializing the bottomNavigationView
        bottomNavigationView = findViewById(R.id.bottom_navigation)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (bottomNavigationView != null) {
        }
    }

    private fun initBottomNavigation() {
        bottomNavigationView!!.setOnNavigationItemSelectedListener { item: MenuItem ->
            when (item.itemId) {
                R.id.action_activities -> selectedFragment = ActivitiesFragment.newInstance()
                R.id.action_dashboard -> selectedFragment = DashBoardFragment.newInstance()
                R.id.action_learn -> selectedFragment = LearnFragment.newInstance()
                R.id.action_newsfeed -> selectedFragment = NewsFragment.newInstance()
                R.id.action_report -> selectedFragment = ReportFragment.newInstance()
            }
            mFragmentTag = selectedFragment?.javaClass?.getSimpleName() // added ? ?
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.parent_frame, selectedFragment, mFragmentTag)
            transaction.commit()
            true
        }
        val activityFragment = ActivitiesFragment.newInstance()
        //Manually displaying the first fragment - one time only
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.parent_frame, activityFragment)
        transaction.commit()
    }

    /**
     * Once SCreen in visible we will get the Fragment instance
     * Check if activity navigation is from Survey completion
     */
    override fun onStart() {
        super.onStart()
        if (isSurveyCompleted) {
            val fragmentManager = this@BottomBarActivity.supportFragmentManager
            val visibleFragment = fragmentManager.findFragmentById(R.id.parent_frame)
            if (visibleFragment is ActivitiesFragment) {
                visibleFragment.updateSurveyStatusCompleted(mActivityId)
                //clear the old activity status data.
                isSurveyCompleted = false
                mActivityId = -1
            }
        }
    }

    //disable default shift mode on selection
    @SuppressLint("RestrictedApi")
    fun disableShiftMode() {
        val menuView = bottomNavigationView!!.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShiftingMode(false)
                // set once again checked value, so view will be updated
                item.setChecked(item.itemData.isChecked)
            }
        } catch (e: NoSuchFieldException) { //Timber.e(e, "Unable to get shift mode field");
        } catch (e: IllegalAccessException) { //Timber.e(e, "Unable to change value of shift mode");
        }
    }

    private fun testFileEncryption() {
        val zipPath = Environment.getExternalStorageDirectory().toString() + File.separator + "b9659e6f576e4819_1512020831831" + ".zip"
        val zipEncPath = Environment.getExternalStorageDirectory().toString() + File.separator + "b9659e6f576e4819_1512020831831" + "_Encrypted.zip"
        FileHelper.encryptedZip(zipPath, zipEncPath)
        //String zipPathDecrypted = Environment.getExternalStorageDirectory() + File.separator + "b9659e6f576e4819_1512020831831" +"_Decrypted.zip";
//FileHelper.decryptedZip(zipEncPath,zipPathDecrypted);
    }

    /**
     * For testing RSA test encryption
     *
     * @throws Exception
     */
    private fun testTextEncryption() {
        var enc: String? = null
        try {
            enc = encryptText("test4@gmail.com")
            Log.e("ENC ", enc)
            val dec = RSAEncryption().decryptText(enc)
            //String dec = new RSAEncryption().decryptText("43RVCLvmqyRHSmFf+aohiX9BUMh0F7V2JdRQW6xvHgYopKOfql/dxQwCYxU4\\n+6nB2aJfKMrQ1jmREkI6bp6nvZbEOGRySr0TFvjESwcpHoUdofPqCjrO4GA6\\nZTO286V2EP18HswXvH48AyGhMoALnX0OJ2f/FQECZ7KM/nUfBwSAxeHOlJl3\\nrzaHwpM7k/ED9mdpqSpmFN4ude1y7GTptydv+DUvIEYxN3pDuw2SzkeCFt6l\\nUMwrTuVBIlEBgJ99Npqu5JtS3LC582p/ZxRfucAfP58fx2otzHJK4Dq/HoIP\\n2U6jefATfCer494//0SzJHBR4k+9TwhMhzmKDRBnVQ==");
            Log.e("Dec ", dec)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        SharedPrefSingleton.instance?.saveCastData(null)
        MobileMessageSenderAsync().execute(APP_LOGINED)
        Utility.registerReciverForN(this)
        PhAwareApplication().getInstance()!!.setConnectivityListener(this)
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(CastingActivity.WEAR_MESSAGE_INTENT_FILTER))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.WEAR_MESSAGE_EMERGENCY_CALL))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.WEAR_MESSAGE_SCHEDULER_STATUS))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.ACTIVITY_HAS_RESET))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.RATE_APP))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                IntentFilter(AppConstants.OFFLINE_ALERT))
    }

    override fun onPause() {
        super.onPause()
        if (Utility.receiver != null) Utility.unRegisterReciverForN(this)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {}
    override fun onDestroy() { // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onDestroy()
    }

    companion object {
        const val SURVEY_REQUEST_CODE = 100
        private const val TAG = "BottomBarActivity"
    }
}