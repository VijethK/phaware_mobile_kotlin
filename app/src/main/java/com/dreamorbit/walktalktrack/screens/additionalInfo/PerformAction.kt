package com.dreamorbit.walktalktrack.screens.additionalInfo

/**
 * Created by nareshkumar.reddy on 10/19/2017.
 * Interface to perform to and fro navugation on AdditionalInfo
 */
internal interface PerformAction {
    fun moveToNext(position: Int)
    fun moveBack(position: Int)
}