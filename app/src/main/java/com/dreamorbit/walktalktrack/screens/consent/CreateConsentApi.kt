package com.dreamorbit.walktalktrack.screens.consent

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.consent.ConsentCreateRequest
import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
class CreateConsentApi {
    private var mPresenter: ConsentPresenter? = null
    fun createConsent(createRequest: ConsentCreateRequest?, mPresenter: ConsentPresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        this.mPresenter = mPresenter
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.createConsent(createRequest)
        call!!.enqueue(object : Callback<ConsentResponse?> {
            override fun onResponse(call: Call<ConsentResponse?>, response: Response<ConsentResponse?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    mPresenter.postResult(response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), ConsentResponse::class.java)
                        mPresenter.postResult(errorResponse)
                    } catch (e: Exception) {
                        val errorResponse = ConsentResponse()
                        errorResponse.errorMessage = "Please try again."
                        mPresenter.postResult(errorResponse)
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ConsentResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val errorResponse = ConsentResponse()
                errorResponse.errorMessage = "Registration failed. Please try again."
                mPresenter.postResult(errorResponse)
            }
        })
    }
}