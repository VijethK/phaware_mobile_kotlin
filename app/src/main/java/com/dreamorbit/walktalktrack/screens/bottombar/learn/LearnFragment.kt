package com.dreamorbit.walktalktrack.screens.bottombar.learn

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.base.BaseFragment

class LearnFragment : BaseFragment(), View.OnClickListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bottom_learn, container, false)
        initViews(view)
        initToolbar(view)
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val title = view.findViewById<TextView>(R.id.title)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        title.text = getString(R.string.menu_bottom_learn)
        val imgProfile = toolbar.findViewById<TextView>(R.id.profile)
        imgProfile.setOnClickListener { v: View? -> loadProfileActivity() }
    }

    private fun initViews(view: View) {
        view.findViewById<View>(R.id.about_this_study).setOnClickListener(this)
        view.findViewById<View>(R.id.how_study_works).setOnClickListener(this)
        view.findViewById<View>(R.id.who_can_participate).setOnClickListener(this)
        view.findViewById<View>(R.id.who_run_the_study).setOnClickListener(this)
        view.findViewById<View>(R.id.spread_the_word).setOnClickListener(this)
        view.findViewById<View>(R.id.life_with_ph).setOnClickListener(this)
        view.findViewById<View>(R.id.symptoms).setOnClickListener(this)
        view.findViewById<View>(R.id.online_resources).setOnClickListener(this)
    }

    override fun onClick(view: View) {
        var intent: Intent? = null
        when (view.id) {
            R.id.about_this_study -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.about_this_study))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/AboutStudy.html")
            }
            R.id.how_study_works -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.how_this_study_works))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/howstudyworks_intro.html")
            }
            R.id.who_can_participate -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.who_can_participate))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/participate.html")
            }
            R.id.who_run_the_study -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.who_is_running_this_study))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/WhoIsRunning.html")
            }
            R.id.spread_the_word -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.spread_the_word))
            }
            R.id.life_with_ph -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.life_with_pulmonary_hypertension))
            }
            R.id.symptoms -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.symptoms))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/symptoms.html")
            }
            R.id.online_resources -> {
                intent = Intent(activity, DescriptionActivity::class.java)
                intent.putExtra(AppConstants.PAGE_TITLE, getString(R.string.online_resources))
                intent.putExtra(AppConstants.HTML_FILE, "file:///android_asset/online_resources.html")
            }
        }
        intent?.let { startActivity(it) }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivitiesFragment.
         */
// TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(): LearnFragment {
            val fragment = LearnFragment()
            val args = Bundle()
            //args.putString(ARG_PARAM1, param1);
//args.putString(ARG_PARAM2, param2);
            fragment.arguments = args
            return fragment
        }
    }
}