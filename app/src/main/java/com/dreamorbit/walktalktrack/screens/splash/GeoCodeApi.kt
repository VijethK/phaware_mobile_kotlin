package com.dreamorbit.walktalktrack.screens.splash

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getGeoCodeClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.geocode.GeoResponse
import com.dreamorbit.walktalktrack.utilities.LocationHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by nareshkumar.reddy on 12/4/2017.
 */
class GeoCodeApi {
    fun getLocation(locationHelper: LocationHelper, latitude: Double, longitude: Double) {
        val retrofit = getGeoCodeClient("https://maps.googleapis.com/maps/api/geocode/")
        val service = retrofit!!.create(IApiRepo::class.java)
        val geoData = HashMap<String?, Any?>()
        geoData["latlng"] = "$latitude,$longitude"
        geoData["key"] = "AIzaSyDk14Ay1W3xEQAPaVcDO_cn1oPXW8H1I2I"
        val call = service.getGeoLocation(geoData)
        call!!.enqueue(object : Callback<GeoResponse?> {
            override fun onResponse(call: Call<GeoResponse?>, response: Response<GeoResponse?>) {
                if (response != null && response.code() == 200) {
                    Log.e("Response:", "" + response.body())
                    locationHelper.geoCodeResult(response.body())
                } else {
                    locationHelper.geoCodeFailure()
                }
            }

            override fun onFailure(call: Call<GeoResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                locationHelper.geoCodeError()
            }
        })
    }
}