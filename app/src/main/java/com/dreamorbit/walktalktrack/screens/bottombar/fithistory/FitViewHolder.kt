package com.dreamorbit.walktalktrack.screens.bottombar.fithistory

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.dreamorbit.walktalktrack.R

/**
 * Created by Mujas on 26-10-2017.
 */
//View holder class, where all view components are defined
class FitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var tvValue: TextView
    var tvDateTime: TextView
    override fun onClick(v: View) {}

    init {
        tvValue = itemView.findViewById(R.id.tvValue)
        tvDateTime = itemView.findViewById(R.id.tvDateTime)
    }
}