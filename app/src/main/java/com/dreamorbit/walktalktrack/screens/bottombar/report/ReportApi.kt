package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.util.Log
import com.dreamorbit.researchaware.model.report.GetReportRequest
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.database.report.ReportRepository.Companion.instance
import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by nareshkumar.reddy on 10/30/2017.
 */
class ReportApi {
    fun uploadSummary(reportRequest: PostReportRequest?) {
        //Kotlin error replaced
      /*  val accessToken: String = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)*/
        //Kotlin error above  replaced below line
        val retrofit = getClient(SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")) //? is added

        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.postReports(reportRequest)
        call!!.enqueue(object : Callback<Any?> {
            override fun onResponse(call: Call<Any?>, response: Response<Any?>) {
                if (response != null && response.code() == 200 || response.code() == 201) { //Delete that perticular record from local DB once uploaded
                    instance!!.deleteReport(reportRequest)
                    Log.e("Report Upload:", "" + response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), ConsentResponse::class.java)
                    } catch (e: Exception) {
                        val errorResponse = ConsentResponse()
                        errorResponse.errorMessage = "Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<Any?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val errorResponse = ConsentResponse()
                errorResponse.errorMessage = "Registration failed. Please try again."
            }
        })
    }

    fun getReport(reportRequest: GetReportRequest, reportPresenter: ReportPresenter) {
        /*val accessToken: String = SharedPrefSingleton.getInstance().getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)*/
        val retrofit = getClient(SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")) //? is added
        val service = retrofit!!.create(IApiRepo::class.java)
        val reportData = HashMap<String?, String?>()
        reportData["from_date"] = reportRequest.from_date
        reportData["to_date"] = reportRequest.to_date
        reportData["username"] = reportRequest.username
        val call = service.getReports(reportData)
        call!!.enqueue(object : Callback<ReportResponse?> {
            override fun onResponse(call: Call<ReportResponse?>, response: Response<ReportResponse?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    reportPresenter.onSuccessReports(response.body())
                } else {
                    reportPresenter.onFailureReports()
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), ConsentResponse::class.java)
                    } catch (e: Exception) {
                        val errorResponse = ConsentResponse()
                        errorResponse.errorMessage = "Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ReportResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val errorResponse = ConsentResponse()
                errorResponse.errorMessage = "Registration failed. Please try again."
            }
        })
    }
}