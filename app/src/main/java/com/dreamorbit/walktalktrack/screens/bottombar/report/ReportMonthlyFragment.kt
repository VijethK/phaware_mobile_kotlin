package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import java.text.SimpleDateFormat

class ReportMonthlyFragment : BaseReportFragment(), ReportView {
    private val TAG = ReportMonthlyFragment::class.java.name
    private var mReportPresenter: ReportPresenter? = null
    private var mHasLoadedOnce = false // your boolean field
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_monthly, container, false)
        mReportPresenter = ReportPresenter(this, ReportApi())
        initView(view)
        swipeRefreshLayout!!.setOnRefreshListener {
            if (ConnectivityReceiver.isConnected) {
                setMonthlyDateReport()
            } else {
                showSnackBar(getView(), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            }
        }
        return view
    }

    override fun setUserVisibleHint(isFragmentVisible: Boolean) {
        super.setUserVisibleHint(true)
        if (this.isVisible) { // we check that the fragment is becoming visible
            if (isFragmentVisible && !mHasLoadedOnce) {
                setMonthlyDateReport()
                mHasLoadedOnce = true
            }
        }
    }

    /**
     * Getting From and To date from Util class
     */
    private fun setMonthlyDateReport() {
        if (ConnectivityReceiver.isConnected) {
            val monthDates = SdkUtils.getLastMonthsDates()
            //Set date on UI
            try {
                val newFormat = SimpleDateFormat("MMMM yyyy")
                val date = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(monthDates[1])
                val month = newFormat.format(date)
                mTvDate!!.text = month
            } catch (e: Exception) {
                Log.e(TAG, e.message)
            }
            showLoader(getString(R.string.please_wait))
            mReportPresenter!!.getReportApi(monthDates[0], monthDates[1])
        } else {
            showSnackBar(activity!!.findViewById(android.R.id.content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    override fun onReportSuccess(list: ReportResponse) {
        if (list != null && list.reports.size > 0) {
            bindHeader(list)
            bindAdapter(list)
        } else {
            tvNoData!!.visibility = View.VISIBLE
            mListParent!!.visibility = View.GONE
        }
        hideLoader()
    }

    override fun onReportFailure() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    override fun onReportError() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    companion object {
        fun newInstance(): ReportMonthlyFragment {
            return ReportMonthlyFragment()
        }
    }
}