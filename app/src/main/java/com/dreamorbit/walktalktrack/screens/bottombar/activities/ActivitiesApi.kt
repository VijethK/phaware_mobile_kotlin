package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.activities.Activities
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Service access layer to call Register api
 * Created by nareshkumar.reddy on 9/26/2017.
 */
class ActivitiesApi {
    fun getActivities(activitiesPresenter: ActivitiesPresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "") // SharedPrefSingleton.TEMP_TOKN;
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.activities
        call!!.enqueue(object : Callback<Activities?> {
            override fun onResponse(call: Call<Activities?>, response: Response<Activities?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    activitiesPresenter.apiActivitiesSuccessResult(response.body())
                } else if (response.code() == 401) {
                    val errorResponse = Activities()
                    errorResponse.errorMessage = response.raw().message()
                    activitiesPresenter.apiActivitiesSuccessResult(errorResponse)
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), Activities::class.java)
                        activitiesPresenter.apiActivitiesSuccessResult(errorResponse)
                    } catch (e: Exception) {
                        val errorResponse = Activities()
                        errorResponse.errorMessage = "Server is down. Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<Activities?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val errorResponse = Activities()
                errorResponse.errorMessage = t.toString()
                activitiesPresenter.apiActivitiesSuccessResult(errorResponse)
            }
        })
    }
}