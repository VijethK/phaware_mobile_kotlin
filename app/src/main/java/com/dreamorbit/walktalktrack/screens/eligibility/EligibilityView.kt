package com.dreamorbit.walktalktrack.screens.eligibility

interface EligibilityView {
    fun onValidationSuccess(response: Boolean)
    fun onValidationFail(response: String?)
    fun onApiError(error: String?)
}