package com.dreamorbit.walktalktrack.screens.splash

import android.util.Log
import com.dreamorbit.researchaware.model.fit.weather.WeatherResponse
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton
import com.dreamorbit.walktalktrack.api.ApiClient.getGeoCodeClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by nareshkumar.reddy on 12/19/2017.
 */
class WeatherApi {
    fun getLocation(latitude: Double, longitude: Double) {
        val retrofit = getGeoCodeClient("http://api.openweathermap.org/data/2.5/")
        val service = retrofit!!.create(IApiRepo::class.java)
        val geoData = HashMap<String?, Any?>()
        geoData["lat"] = latitude.toString()
        geoData["lon"] = longitude.toString()
        geoData["appid"] = "b56ef860ea8018ab2a94bc5f24f91bcf"
        val call = service.getWeather(geoData)
        call!!.enqueue(object : Callback<WeatherResponse?> {
            override fun onResponse(call: Call<WeatherResponse?>, response: Response<WeatherResponse?>) {
                if (response != null && response.code() == 200) {
                    Log.e("Response:", "" + response.body())
                    val gson = Gson()
                    val weatherData = gson.toJson(response.body(), WeatherResponse::class.java)
                    SdkSharedPrefSingleton.getInstance().saveWeatherData(weatherData)
                } else {
                }
            }

            override fun onFailure(call: Call<WeatherResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
            }
        })
    }
}