package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import java.text.DateFormat
import java.text.SimpleDateFormat

class ReportWeeklyFragment : BaseReportFragment(), ReportView {
    private val TAG = ReportWeeklyFragment::class.java.name
    private var mReportPresenter: ReportPresenter? = null
    private var mHasLoadedOnce = false // your boolean field
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_weekly, container, false)
        mReportPresenter = ReportPresenter(this, ReportApi())
        initView(view)
        swipeRefreshLayout!!.setOnRefreshListener { setWeeklyDateReport() }
        return view
    }

    override fun setUserVisibleHint(isFragmentVisible: Boolean) {
        super.setUserVisibleHint(true)
        if (this.isVisible) { // we check that the fragment is becoming visible
            if (isFragmentVisible && !mHasLoadedOnce) {
                setWeeklyDateReport()
                mHasLoadedOnce = true
            }
        }
    }

    /**
     * Getting From and To date from Util class
     */
    private fun setWeeklyDateReport() {
        if (ConnectivityReceiver.isConnected) {
            val weekDates = SdkUtils.getLastWeekDates()
            //Set date on UI
            try { //SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy");
                val thedate = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(weekDates[0])
                val fromDate = DateFormat.getDateInstance(DateFormat.MEDIUM).format(thedate)
                val todate = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(weekDates[1])
                val toDate = DateFormat.getDateInstance(DateFormat.MEDIUM).format(todate)
                mTvDate!!.text = "$fromDate - $toDate"
            } catch (e: Exception) {
                Log.e(TAG, e.message)
            }
            showLoader(getString(R.string.please_wait))
            mReportPresenter!!.getReportApi(weekDates[0], weekDates[1])
        } else {
            showSnackBar(activity!!.findViewById(android.R.id.content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    override fun onReportSuccess(list: ReportResponse) {
        if (list != null && list.reports.size > 0) {
            bindHeader(list)
            bindAdapter(list)
        } else {
            tvNoData!!.visibility = View.VISIBLE
            mListParent!!.visibility = View.GONE
        }
        hideLoader()
    }

    override fun onReportFailure() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    override fun onReportError() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    companion object {
        fun newInstance(): ReportWeeklyFragment {
            return ReportWeeklyFragment()
        }
    }
}