package com.dreamorbit.walktalktrack.screens.consent

import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse

/**
 * Assent flow Contractore to communicate between Model and View with the help of presenter
 * Created by mujasam.bn on 10/6/2017.
 */
interface ConsenttViewContract {
    fun onErrorResponse(consentResponse: ConsentResponse?)
    fun onSuccessResponse(consentResponse: ConsentResponse?)
    fun onFailureResponse(consentResponse: ConsentResponse?)
    fun onDownloadComplete()
}