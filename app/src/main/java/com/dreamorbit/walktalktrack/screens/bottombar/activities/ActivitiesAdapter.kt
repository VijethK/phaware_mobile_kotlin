package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.pojo.activities.ActivityList
import java.util.*

class ActivitiesAdapter(private val context: Context) : RecyclerView.Adapter<ActivitiesViewHolder>() {
    private val inflater: LayoutInflater
    //Creating an arraylist of POJO objects
    private var listActivities: List<ActivityList>? = ArrayList()
    private var view: View? = null
    private var holder: ActivitiesViewHolder? = null
    var schedulerList: List<SchedulerModel>
    /**
     * fetch the fresh data from sqlite
     */
    fun refreshAdapter() {
        schedulerList = SchedulerRepository.instance!!.scheduler
        notifyDataSetChanged()
    }

    //This method inflates view present in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivitiesViewHolder {
        view = inflater.inflate(R.layout.row_item_activities, parent, false)
        holder = ActivitiesViewHolder(view!!, this, listActivities!!)
        return holder!!
    }

    //Binding the data using get() method of POJO object
    override fun onBindViewHolder(holder: ActivitiesViewHolder, position: Int) {
        val activities = listActivities!![position]
        holder.parentFrame.tag = activities.id
        holder.tvQuestion.tag = activities.id
        holder.tvQuestion.text = activities.name
        //holder.radio.setChecked(activities.getIsActivityAnswered() == 1);
        val model = schedulerList[position]
        if (model != null && !TextUtils.isEmpty(model.status) && model.status.equals(SchedulerModel.TEST_PENDING, ignoreCase = true)) {
            holder.radio.isChecked = false
        } else if (model != null && !TextUtils.isEmpty(model.status) && model.status.equals(SchedulerModel.TEST_TAKEN, ignoreCase = true)) {
            holder.radio.isChecked = true
        } else {
            holder.radio.isChecked = false
        }
    }

    //Setting the arraylist
    fun setListContent(list: List<ActivityList>?) {
        listActivities = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (listActivities == null) 0 else listActivities!!.size
    }

    init {
        inflater = LayoutInflater.from(context)
        schedulerList = SchedulerRepository.instance!!.scheduler
    }
}