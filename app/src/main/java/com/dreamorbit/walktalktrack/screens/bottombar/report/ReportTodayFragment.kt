package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ReportTodayFragment : BaseReportFragment(), ReportView {
    private var mReportPresenter: ReportPresenter? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_report_today, container, false)
        mReportPresenter = ReportPresenter(this, ReportApi())
        initView(view)
        setTodaysDateReport()
        swipeRefreshLayout!!.setOnRefreshListener { setTodaysDateReport() }
        return view
    }

    fun setTodaysDateReport() {
        if (ConnectivityReceiver.isConnected) {
            val dateFormat: DateFormat = SimpleDateFormat("MMMM dd")
            val cal = Calendar.getInstance()
            mTvDate!!.text = dateFormat.format(cal.time)
            showLoader(getString(R.string.please_wait))
            mReportPresenter!!.getReportApi(SdkUtils.getTodaysDates(), SdkUtils.getTodaysDates())
        } else {
            showSnackBar(activity!!.findViewById(android.R.id.content), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            swipeRefreshLayout!!.isRefreshing = false
        }
    }

    override fun onReportSuccess(list: ReportResponse) {
        if (list != null && list.reports.size > 0) {
            bindHeader(list)
            bindAdapter(list)
        } else {
            tvNoData!!.visibility = View.VISIBLE
            mListParent!!.visibility = View.GONE
        }
        hideLoader()
    }

    override fun onReportFailure() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    override fun onReportError() {
        tvNoData!!.visibility = View.VISIBLE
        mListParent!!.visibility = View.GONE
        hideLoader()
    }

    companion object {
        fun newInstance(): ReportTodayFragment {
            return ReportTodayFragment()
        }
    }
}