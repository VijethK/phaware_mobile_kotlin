package com.dreamorbit.walktalktrack.screens.eligibility

import android.util.Log
import com.dreamorbit.walktalktrack.pojo.invite.InviteToggleResponse
import com.dreamorbit.walktalktrack.screens.invite.InviteApi

import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response

class EligibilityPresenter(private val eligibilityView: EligibilityView, private val inviteApi: InviteApi) {
    fun performInviteCodeToggleApiCall() {
        inviteApi.validateInviteCodeOnOff()?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(object : SingleObserver<InviteToggleResponse?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(response: InviteToggleResponse) {
                        if (response != null) {
                            Log.e("Response:", "" + response)
                            getValidateSuccess(response.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        getError(e.toString())
                    }
                })
    }

    /**
     * If entered token is valid
     *
     * @param isToggleOn
     */
    fun getValidateSuccess(isToggleOn: Boolean) {
        eligibilityView.onValidationSuccess(isToggleOn)
    }

    /**
     * If entered token is not valid
     *
     * @param response
     */
    fun onFaiure(response: Response<ResponseBody?>?) {
        eligibilityView.onValidationFail(null)
    }

    /**
     * If api call fails
     *
     * @param error
     */
    fun getError(error: String?) {
        eligibilityView.onApiError(error)
    }

}