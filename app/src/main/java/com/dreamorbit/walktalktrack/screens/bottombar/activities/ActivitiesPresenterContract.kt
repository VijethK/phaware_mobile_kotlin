package com.dreamorbit.walktalktrack.screens.bottombar.activities

import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.walktalktrack.pojo.activities.Activities

/**
 * Created by mujasam.bn on 10/26/2017.
 */
interface ActivitiesPresenterContract {
    interface ActivityView {
        fun onActivitiesItemClick()
        fun onLoad(activityList: Activities?)
        fun onSurveyLoaded(questionList: QuestionList?)
        fun hideProgressBar()
    }
}