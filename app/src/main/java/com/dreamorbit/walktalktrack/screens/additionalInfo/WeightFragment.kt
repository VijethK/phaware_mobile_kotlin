package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance

/**
 * A simple [Fragment] subclass.
 */
class WeightFragment() : Fragment() {
    private var performAction: PerformAction? = null
    private var weight: EditText? = null
    private var next: TextView? = null
    private var back: ImageView? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_weight, container, false)
        initToolbar(view)
        weight = view.findViewById(R.id.etWeight)
        //Validating weight field to move to next page
        weight?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (weight?.getText().toString().trim { it <= ' ' }.isEmpty()) {
                    next!!.isEnabled = false
                    next!!.alpha = 0.5f
                } else {
                    next!!.isEnabled = true
                    next!!.alpha = 1.0f
                    instance!!.saveWeight(weight?.getText().toString().trim { it <= ' ' })
                }
            }
        })
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.weight)
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(View.OnClickListener { performAction!!.moveBack(2) })
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setEnabled(false)
        next?.setAlpha(0.5f)
        next?.setOnClickListener(View.OnClickListener { view1: View? -> performAction!!.moveToNext(2) })
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }
}