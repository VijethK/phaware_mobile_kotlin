package com.dreamorbit.walktalktrack.screens.assent

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreateRequest
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Service access layer to call Create Assent api
 * Created by mujasam.bn on 10/6/2017.
 */
class CreateAssentApi {
    private var mPresenter: AssentPresenter? = null
    fun createAssent(createRequest: AssentCreateRequest?, mPresenter: AssentPresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        this.mPresenter = mPresenter
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.createAssent(createRequest)
        call!!.enqueue(object : Callback<AssentCreatedResponse?> {
            override fun onResponse(call: Call<AssentCreatedResponse?>, response: Response<AssentCreatedResponse?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    mPresenter.postResult(response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), AssentCreatedResponse::class.java)
                        mPresenter.postResult(errorResponse)
                    } catch (e: Exception) {
                        val errorResponse = AssentCreatedResponse()
                        errorResponse.errorMessage = "Please try again."
                        mPresenter.postResult(errorResponse)
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<AssentCreatedResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val errorResponse = AssentCreatedResponse()
                errorResponse.errorMessage = "Registration failed. Please try again."
                mPresenter.postResult(errorResponse)
            }
        })
    }
}