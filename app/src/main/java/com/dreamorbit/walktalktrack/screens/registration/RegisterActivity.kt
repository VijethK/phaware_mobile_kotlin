package com.dreamorbit.walktalktrack.screens.registration

import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.joinstudy.StudyData
import com.dreamorbit.walktalktrack.database.registration.RegistartionData
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.register.RegisterRequest
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.dreamorbit.walktalktrack.service.PhGCMTaskService
import com.dreamorbit.walktalktrack.service.PublicKeyApi
import com.dreamorbit.walktalktrack.utilities.RSAEncryption
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : BaseActivity(), RegisterViewContract, View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private var mEtEmail: EditText? = null
    private var mEtUserName: EditText? = null
    private var mEtPassword: EditText? = null
    private var mRegisterPresenter: RegisterPresenter? = null
    private var mStudyDataLoaded: StudyData? = null
    private var mRegistartionData: RegistartionData? = null
    private var mEmailSHA: String? = null
    /**
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mRegisterPresenter = RegisterPresenter(this@RegisterActivity, RegisterApi(), PublicKeyApi())
        //toolbar_consent initialization
        initToolbar()
        //views initialization
        initViews()
        //register broadcast receiver for connectivity check for OS >= Nougat
        Utility.registerReciverForN(this)
    }

    /**
     * views initialization
     */
    private fun initViews() {
        mEtEmail = findViewById(R.id.email)
        mEtUserName = findViewById(R.id.username)
        mEtPassword = findViewById(R.id.password)
        mRegisterPresenter!!.studyDetail
        mRegistartionData = RegistartionData()
    }

    /*
     *toolbar_consent initialization
     */
    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val done = toolbar.findViewById<TextView>(R.id.done)
        done.setOnClickListener(this)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * on menu item click listeners
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Onclick listeners
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.done -> mRegisterPresenter!!.validation(Utility.isValidEmail(mEtEmail!!.text.toString().trim { it <= ' ' }), mEtUserName!!.text.toString().trim { it <= ' ' }, mEtPassword!!.text.toString().trim { it <= ' ' })
        }
    }

    /**
     * Showing validation errors.
     *
     * @param errors
     */
    override fun setValidationErrors(errors: String?) {
        if (errors.equals(getString(R.string.email), ignoreCase = true)) {
            mEtEmail!!.error = getString(R.string.valid_email)
        } else if (errors.equals(getString(R.string.user_name), ignoreCase = true)) {
            mEtUserName!!.error = getString(R.string.valid_username)
        } else if (errors.equals(getString(R.string.password), ignoreCase = true)) {
            mEtPassword!!.error = getString(R.string.valid_password)
        }
    }

    /**
     * Web request for registration.
     */
    override fun registerWebRequest() {
        Utility.hideKeyboard(findViewById(R.id.register_layout))
        val publicKey = SharedPrefSingleton.instance?.publicKey
        if (ConnectivityReceiver.isConnected) {
            if (TextUtils.isEmpty(publicKey)) {
                Utility.showProgressBar(this@RegisterActivity, getString(R.string.downloading_certificate))
                mRegisterPresenter!!.downloadCertificate()
            } else {
                val register = createRegisterRequest()
                Utility.showProgressBar(this@RegisterActivity, getString(R.string.registering))
                mRegisterPresenter!!.registerService(register)
            }
        } else {
            Utility.showSnackBar(findViewById(R.id.register_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    override fun onCertificateDownloadSuccess() {
        Utility.dismissProgressBar()
        Utility.showProgressBar(this@RegisterActivity, getString(R.string.registering))
        val register = createRegisterRequest()
        mRegisterPresenter!!.registerService(register)
    }

    /**
     * Creating the registration model.
     * request param mEtEmail should be in sha-256 format string appended with @phaware.org
     *
     * DOB format yyyy-mm-dd
     * Role will be hardcoded i.e "parrticipant"
     */
    private fun createRegisterRequest(): RegisterRequest { //Encrypting the mEtEmail to SHA-256
        mEmailSHA = Utility.getSHA256Hash(mEtEmail!!.text.toString().trim { it <= ' ' })
        val register = RegisterRequest()
        register.email = StringBuilder().append(mEmailSHA).append("@phaware.org").toString()
        register.password = mEtPassword!!.text.toString().trim { it <= ' ' }
        register.name = mEtUserName!!.text.toString().trim { it <= ' ' }
        register.role = "participant"
        register.device_token = SharedPrefSingleton.instance?.fcmToken
        register.device_type = AppConstants.DEVICE_TYPE
        try {
            register.actual_email = RSAEncryption.encryptText(mEtEmail!!.text.toString().trim { it <= ' ' })
            val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd")
            val cal = Calendar.getInstance()
            //Assign present day to end Date
            register.survey_end_date = dateFormat.format(cal.time)
            //Assign yesterday day to start Date
            cal.add(Calendar.DATE, -1)
            register.survey_start_date = dateFormat.format(cal.time)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //To store in db
        mRegistartionData!!.email = mEtEmail!!.text.toString().trim { it <= ' ' }
        mRegistartionData!!.email_sha = StringBuilder().append(mEmailSHA).append("@phaware.org").toString().trim { it <= ' ' }
        mRegistartionData!!.userName = mEtUserName!!.text.toString().trim { it <= ' ' }
        return register
    }

    /**
     * Initilize the internet broadcast listener
     */
    override fun onResume() {
        super.onResume()
        // register connection status listener
        PhAwareApplication().getInstance().setConnectivityListener(this)
    }

    /***
     * If error response during api call show the message in SnackBar
     * @param registerResponse
     */
    override fun onRegistrationFailure(registerResponse: RegisterResponse?) {
        Utility.dismissProgressBar()
        Utility.showSnackBar(findViewById(R.id.register_layout), registerResponse?.errorMessage, Snackbar.LENGTH_LONG)
    }

    /***
     * If success response during register api call. Store the access token in sared preference
     * @param registerResponse
     *
     * On Success response start periodic task to get refresh token in every 1 hour
     * Navigate to the assent flow
     */
    override fun onRegistrationSuccess(registerResponse: RegisterResponse?) {
        Utility.dismissProgressBar()
        if (registerResponse != null) {
            mRegisterPresenter!!.saveRegistartionDetails(mRegistartionData)
            Utility.showSnackBar(findViewById(R.id.register_layout), "Successfully Registered", Snackbar.LENGTH_LONG)
            SharedPrefSingleton.instance?.saveString(SharedPrefSingleton.ACCESS_TOKEN, registerResponse.accessToken)
            SharedPrefSingleton.instance?.saveString(SharedPrefSingleton.REFRESH_TOKEN, registerResponse.refreshToken)
            SharedPrefSingleton.instance?.saveUserEmail(StringBuilder().append(mEmailSHA).append("@phaware.org").toString().trim { it <= ' ' })
            //optimize this. May be not required
/* Intent intent = new Intent(PhAwareApplication.getMyAppContext(), BackGroundService.class);
            intent.putExtra(AppConstants.BACKGROUND_ACTION, String.valueOf(Utility.BackgroundAction.upload_survey));
            BackGroundService.enqueueBackgroundWork(PhAwareApplication.getMyAppContext(), intent);*/PhGCMTaskService.startPeriodicTask()
            studyConfigNavigation()
        } else {
            Utility.showSnackBar(findViewById(R.id.register_layout), getString(R.string.try_again), Snackbar.LENGTH_LONG)
        }
    }

    /**
     * If This user is partially registered earlier then navigate him to login screen
     *
     * @param response
     */
    override fun partialRegistration(response: RegisterResponse?) {
        Utility.dismissProgressBar()
        showPartialAlertDialog()
    }

    /***
     * Query the study data from database and result will be store in this callback
     * @param studyData
     */
    override fun onStudyDataLoaded(studyData: StudyData?) {
        mStudyDataLoaded = studyData
    }

    /***
     * If connectivity flickers user will get the message using this receiver
     * @param isConnected
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            Utility.dismissProgressBar()
            Utility.showSnackBar(findViewById(R.id.register_layout), if (isConnected) getString(R.string.internet_connected) else getString(R.string.internet_disconnected), Snackbar.LENGTH_LONG)
        }
    }

    /**
     * On successful registration and user ages is  greatet than 17 navigate to the Consent flow activity
     */
    fun showPartialAlertDialog() { //How to call the custom dialog sample
        showDialogOKCancel(getString(R.string.partial_registered), getString(R.string.ok), getString(R.string.cancel),
                DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> goToLoginActivity()
                        DialogInterface.BUTTON_NEGATIVE -> {
                        }
                    }
                })
    }

    override fun onPause() {
        super.onPause()
        if (Utility.receiver != null) Utility.unRegisterReciverForN(this)
    }
}