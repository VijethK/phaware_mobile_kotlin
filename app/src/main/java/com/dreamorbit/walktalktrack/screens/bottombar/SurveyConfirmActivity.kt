package com.dreamorbit.walktalktrack.screens.bottombar

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.researchaware.screentype.screens.survey.SurveyActivity
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity

/**
 * Created by mujasam.bn on 10/5/2017.
 */
class SurveyConfirmActivity : BaseActivity() {
    private var mTvSurveyTitle: TextView? = null
    private var mToolbar: Toolbar? = null
    private var mToolbarTitle: TextView? = null
    private var mToolbarCancel: TextView? = null
    private var mBtnStart: Button? = null
    private val dialog: Dialog? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_survey_confirm)
        initViews()
        initToolbar()
        initToolbarListeners()
    }

    private fun initViews() {
        mTvSurveyTitle = findViewById(R.id.tvActivityTitle)
        mBtnStart = findViewById(R.id.btnStart)
        mTvSurveyTitle?.setText(intent.getStringExtra(SdkConstant.SURVERY_TITLE))
    }

    private fun initToolbar() {
        mToolbar = findViewById(R.id.toolbar)
        mToolbarTitle = mToolbar?.findViewById(R.id.title)
        mToolbarTitle?.setPadding(30, 0, 0, 0)
        mToolbarCancel = mToolbar?.findViewById(R.id.cancel)
        mToolbarTitle?.setText(intent.getStringExtra(SdkConstant.SURVERY_TITLE))
        setSupportActionBar(mToolbar)
    }

    private fun initToolbarListeners() { // Set an OnMenuItemClickListener to handle menu item clicks
        mToolbarCancel!!.setOnClickListener { finish() }
        mBtnStart!!.setOnClickListener { view: View? ->
            val intent = Intent(this, SurveyActivity::class.java)
            intent.putExtra(SdkConstant.SURVEY_QUESTION_LIST, getIntent().getStringExtra(SdkConstant.SURVEY_QUESTION_LIST))
            intent.putExtra(SdkConstant.ACTIVITY_TITLE, getIntent().getStringExtra(SdkConstant.SURVERY_TITLE))
            startActivityForResult(intent, BottomBarActivity.SURVEY_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val isSurveyCompleted = data.getBooleanExtra(SdkConstant.IS_SURVEY_COMPLETED, false)
            val activityId = data.getIntExtra(SdkConstant.SURVERY_ID, -1)
            if (isSurveyCompleted) {
                val i = Intent(this, BottomBarActivity::class.java)
                i.putExtra(SdkConstant.SURVERY_ID, activityId)
                i.putExtra(SdkConstant.IS_SURVEY_COMPLETED, isSurveyCompleted)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i) //will trigger only myMethod in MainActivity
                finish()
            }
        } else {
            finish()
        }
    }
}