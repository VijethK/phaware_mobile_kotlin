package com.dreamorbit.walktalktrack.screens.additionalInfo

import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.additionalInfo.AdditionalInfoRequest
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import io.reactivex.Single
import okhttp3.ResponseBody

/**
 * Created by nareshkumar.reddy on 10/25/2017.
 */
/**
 * Service access layer to call Additional Info api
 */
class AdditionalInfoApi {
    fun sendAdditionalInfo(addInfoRequest: AdditionalInfoRequest?): Single<ResponseBody?>? {
        val retrofit = getClient(SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")) //? is added
        val service = retrofit!!.create(IApiRepo::class.java)
        return service.sendAdditionalInfo(addInfoRequest)
    }
}