package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.pojo.activities.ActivityList

/**
 * Created by Mujas on 26-10-2017.
 */
//View holder class, where all view components are defined
class ActivitiesViewHolder(itemView: View, private val adapter: ActivitiesAdapter, private val listActivities: List<ActivityList>) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var parentFrame: RelativeLayout
    var radio: RadioButton
    var tvQuestion: TextView
    override fun onClick(v: View) {}

    init {
        radio = itemView.findViewById(R.id.radio)
        tvQuestion = itemView.findViewById(R.id.tv_question)
        parentFrame = itemView.findViewById(R.id.parent_frame)
        val clickListener = View.OnClickListener { v: View? -> adapter.notifyItemChanged(adapterPosition) }
        //radio.setOnClickListener(clickListener);
        itemView.setOnClickListener(clickListener)
    }
}