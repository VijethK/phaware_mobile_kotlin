package com.dreamorbit.walktalktrack.screens.bottombar.profile

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by nareshkumar.reddy on 11/16/2017.
 */
class SignOutApi {
    fun signOut(email: String?, profilePresenter: ProfilePresenter) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.signOut(email, AppConstants.DEVICE_TYPE)
        call!!.enqueue(object : Callback<Void?> {
            override fun onResponse(call: Call<Void?>, response: Response<Void?>) {
                if (response != null && response.code() == 204) {
                    Log.e("Response:", "" + response.body())
                    profilePresenter.success()
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), RegisterResponse::class.java)
                        val failure = errorResponse.errorMessage
                        profilePresenter.faiure(failure)
                    } catch (e: Exception) {
                        profilePresenter.error("Please try again.")
                    }
                }
            }

            override fun onFailure(call: Call<Void?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val error = "SignOut failed. Please try again."
                profilePresenter.error(error)
            }
        })
    }
}