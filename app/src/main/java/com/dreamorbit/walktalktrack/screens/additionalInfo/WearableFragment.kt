package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance

/**
 * A simple [Fragment] subclass.
 */
class WearableFragment : Fragment(), View.OnClickListener {
    private var performAction: PerformAction? = null
    private var next: TextView? = null
    private var wearable: TextView? = null
    private var watch: ImageView? = null
    private var fitbit: ImageView? = null
    private var back: ImageView? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_wearable, container, false)
        initToolbar(view)
        watch = view.findViewById(R.id.watch)
        fitbit = view.findViewById(R.id.fitbit)
        wearable = view.findViewById(R.id.wearable)
        watch?.setOnClickListener(this)
        fitbit?.setOnClickListener(this)
        instance!!.saveWearable("")
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.sync_wearable)
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setOnClickListener(View.OnClickListener {
            val additionalInfo = instance!!.additionalInfo
            if (additionalInfo.gender != null && additionalInfo.height != null && additionalInfo.weigh != null && additionalInfo.diagnosed != null && additionalInfo.medication != null) {
                val intent = Intent(activity, AdditionalInfoSummary::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(activity, "Please fill all the mandatory fields.", Toast.LENGTH_SHORT).show()
            }
        })
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(View.OnClickListener { performAction!!.moveBack(5) })
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    override fun onClick(view: View) {
        next!!.isEnabled = true
        next!!.alpha = 1.0f
        when (view.id) {
            R.id.watch -> {
                wearable!!.setText(R.string.watch)
                watch!!.setBackgroundResource(R.drawable.gender_border)
                fitbit!!.setBackgroundColor(Color.WHITE)
                instance!!.saveWearable(wearable!!.text.toString().trim { it <= ' ' })
            }
            R.id.fitbit -> {
                wearable!!.setText(R.string.fitbit)
                fitbit!!.setBackgroundResource(R.drawable.gender_border)
                watch!!.setBackgroundColor(Color.WHITE)
                instance!!.saveWearable(wearable!!.text.toString().trim { it <= ' ' })
            }
        }
    }
}