package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance

/**
 * A simple [Fragment] subclass.
 */
class HeightFragment : Fragment(), View.OnClickListener {
    private var performAction: PerformAction? = null
    private var next: TextView? = null
    private var height: EditText? = null
    private var feet_count = 0
    private var inch_count = 0
    private var feet: TextView? = null
    private var inch: TextView? = null
    private var feet_up: ImageView? = null
    private var inch_up: ImageView? = null
    private var feet_down: ImageView? = null
    private var inch_down: ImageView? = null
    private var back: ImageView? = null
    private var heightDialog: AlertDialog? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_height, container, false)
        height = view.findViewById(R.id.height)
        initToolbar(view)
        height?.setOnClickListener(this)
        //Validating height field to move to next page
        height?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                if (height?.getText().toString().trim { it <= ' ' }.isEmpty()) {
                    next!!.isEnabled = false
                    next!!.alpha = 0.5f
                } else {
                    next!!.isEnabled = true
                    next!!.alpha = 1.0f
                    instance!!.saveHeight(height?.getText().toString().trim { it <= ' ' })
                }
            }
        })
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.height)
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(View.OnClickListener { performAction!!.moveBack(1) })
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setEnabled(false)
        next?.setAlpha(0.5f)
        next?.setOnClickListener(View.OnClickListener { performAction!!.moveToNext(1) })
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    private fun showDailog() {
        heightDialog = AlertDialog.Builder(activity!!).create()
        val factory = LayoutInflater.from(activity)
        val heightDialogView = factory.inflate(R.layout.height_dilaog, null)
        heightDialog?.setCancelable(false)
        heightDialog?.setView(heightDialogView)
        feet_up = heightDialogView.findViewById(R.id.feet_up)
        feet_down = heightDialogView.findViewById(R.id.feet_down)
        inch_up = heightDialogView.findViewById(R.id.inch_up)
        inch_down = heightDialogView.findViewById(R.id.inch_down)
        val cancel = heightDialogView.findViewById<Button>(R.id.height_cancel)
        val set = heightDialogView.findViewById<Button>(R.id.height_set)
        cancel.setOnClickListener(this)
        set.setOnClickListener(this)
        feet = heightDialogView.findViewById(R.id.feet)
        inch = heightDialogView.findViewById(R.id.inches)
        feet_up?.setOnClickListener(this)
        feet_down?.setOnClickListener(this)
        inch_up?.setOnClickListener(this)
        inch_down?.setOnClickListener(this)
        feet?.setOnClickListener(this)
        inch?.setOnClickListener(this)
        feet?.setText(getString(R.string.feet_count, feet_count))
        inch?.setText(getString(R.string.inch_count, inch_count))
        heightDialog?.show()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.height -> showDailog()
            R.id.feet -> {
            }
            R.id.inches -> {
            }
            R.id.feet_up -> if (feet_count < 9) {
                feet!!.text = getString(R.string.feet_count, ++feet_count)
                feet_up!!.alpha = 1.0f
                feet_down!!.alpha = 1.0f
            } else {
                feet_up!!.alpha = 0.5f
            }
            R.id.feet_down -> if (feet_count > 0) {
                feet!!.text = getString(R.string.feet_count, --feet_count)
                feet_up!!.alpha = 1.0f
                feet_down!!.alpha = 1.0f
            } else {
                feet_down!!.alpha = 0.5f
            }
            R.id.inch_up -> if (inch_count < 11) {
                inch!!.text = getString(R.string.inch_count, ++inch_count)
                inch_up!!.alpha = 1.0f
                inch_down!!.alpha = 1.0f
            } else {
                inch_up!!.alpha = 0.5f
            }
            R.id.inch_down -> if (inch_count > 0) {
                inch!!.text = getString(R.string.inch_count, --inch_count)
                inch_up!!.alpha = 1.0f
                inch_down!!.alpha = 1.0f
            } else {
                inch_down!!.alpha = 0.5f
            }
            R.id.height_cancel -> heightDialog!!.dismiss()
            R.id.height_set -> {
                height!!.setText(getString(R.string.ft_in, feet_count, inch_count))
                heightDialog!!.dismiss()
                instance!!.saveHeight(height!!.text.toString().trim { it <= ' ' })
            }
        }
    }
}