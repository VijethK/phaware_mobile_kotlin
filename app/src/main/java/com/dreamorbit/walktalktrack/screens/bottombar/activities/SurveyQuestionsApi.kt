package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.util.Log
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mujasam.bn on 10/6/2017.
 */
class SurveyQuestionsApi {
    /**
     * in Authorization header for this api send refresh token instead of access token
     */
    fun getSurveyQuestions(activitiesPresenter: ActivitiesPresenter, activityID: Int) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "") // SharedPrefSingleton.TEMP_TOKN;
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.getQuestionsList(activityID)
        call!!.enqueue(object : Callback<QuestionList?> {
            override fun onResponse(call: Call<QuestionList?>, response: Response<QuestionList?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    activitiesPresenter.onSurveySuccessResponse(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<QuestionList?>, t: Throwable) {
                Log.e("", "" + t.toString())
                activitiesPresenter.HideProgressBar()
            }
        })
    }
}