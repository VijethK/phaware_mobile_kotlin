package com.dreamorbit.walktalktrack.screens.splash

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.VideoView
import com.crashlytics.android.Crashlytics
import com.dreamorbit.walktalktrack.BuildConfig
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.assent.AssentActivity
import com.dreamorbit.walktalktrack.consent.ConsentActivity
import com.dreamorbit.walktalktrack.database.flow.FlowRepository.Companion.instance
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoActivity
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.screens.splash.SplashActivity
import com.dreamorbit.walktalktrack.service.PhGCMTaskService
import com.dreamorbit.walktalktrack.utilities.LocationHelper
import com.dreamorbit.walktalktrack.utilities.PermissionsUtility
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.welcome.WelcomeActivity
import io.fabric.sdk.android.Fabric

class SplashActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener, OnCompletionListener {
    private var mLocationManager: LocationManager? = null
    private var mPermissionsUtility: PermissionsUtility? = null
    //video view
    private var mContentView: VideoView? = null
    private var isVideoComplete = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        SharedPrefSingleton.instance?.saveBoolean(SharedPrefSingleton.IS_USA, true)
        Fabric.with(this, Crashlytics())
        initVideoView()
        initData()
        checkGPSEnabled()
    }

    private fun initData() {
        mPermissionsUtility = PermissionsUtility()
    }

    private fun initVideoView() {
        mContentView = findViewById(R.id.videoView)
        mContentView?.setOnCompletionListener(this)
        findViewById<View>(R.id.ta_btn_skip).setOnClickListener(mSkipClickListener)
        if (SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)!!) {
            findViewById<View>(R.id.ta_btn_skip).visibility = View.GONE
        }
    }

    private val mSkipClickListener = View.OnClickListener { view: View? ->
        if (SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)!!) {
            isVideoComplete = true
            navigationFlow()
            finish()
        } else { //stay in his page unless permission has given
            requestPermission()
        }
    }

    override fun onResume() {
        super.onResume()
        // register connection status listener
        Utility.registerReciverForN(this)
        PhAwareApplication().getInstance().setConnectivityListener(this)
    }

    private fun checkGPSEnabled() {
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) { //requestPermission();
        } else {
            showGPSDisabledAlertToUser()
        }
    }

    private fun requestPermission() {
        if (mPermissionsUtility!!.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            val locationHelper = LocationHelper(this@SplashActivity)
            locationHelper.startLocationUpdates()
        } else {
            if (mPermissionsUtility!!.shouldShowRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(findViewById(R.id.main_content), "Location permission is required to fetch location.",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                    // Request the permission
                    mPermissionsUtility!!.requestForLocationPermission(this@SplashActivity)
                }.show()
            } else {
                mPermissionsUtility!!.requestForLocationPermission(this)
            }
        }
    }

    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(resources.getString(R.string.gps_message))
                .setCancelable(false)
                .setPositiveButton(resources.getString(R.string.gps_enable)
                ) { dialog, id ->
                    val callGPSSettingIntent = Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(callGPSSettingIntent, REQUEST_CODE)
                }
        alertDialogBuilder.setNegativeButton(resources.getString(R.string.cancel)
        ) { dialog, id -> dialog.cancel() }
        alertDialogBuilder.show()
    }

    override fun onPause() {
        super.onPause()
        if (Utility.receiver != null) unregisterReceiver(Utility.receiver)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsUtility.PERMISSIONS_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val locationHelper = LocationHelper(this@SplashActivity)
                    locationHelper.startLocationUpdates()
                } else {
                    requestPermission()
                }
                return
            }
        }
    }

    fun navigationFlow() {
        if (!isVideoComplete) return  // dont go further unless video completes
        PhGCMTaskService.startPeriodicTask() // Refresh the Access_Token and Public key
        if (BuildConfig.IS_DENVER) {
            if (instance!!.getAssentStatus(AppConstants.ADDITIONAL_INFO_FLOW)) {
                goToSurveyActivity()
            } else if (instance!!.getAssentStatus(AppConstants.REGISTER_FLOW)) {
                gotoConsent() // Consent I Screen
            } else if (instance!!.getAssentStatus(AppConstants.CONSENT_ONE_FLOW)) { //Denver
                goToAssent() // Consent II screen
            } else if (instance!!.getAssentStatus(AppConstants.CONSENT_TWO_FLOW)) { //Denver
                goToAdditionalInfo()
            } else {
                gotoWelcome()
            }
        } else { //Stanford
            if (instance!!.getAssentStatus(AppConstants.ADDITIONAL_INFO_FLOW)) { // If Stanford use old flow after registration
                goToSurveyActivity()
            } else {
                gotoWelcome()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE -> {
                checkGPSEnabled()
            }
        }
    }

    fun gotoWelcome() {
        startActivity(Intent(this@SplashActivity, WelcomeActivity::class.java))
        finish()
    }

    private fun gotoConsent() {
        startActivity(Intent(this@SplashActivity, ConsentActivity::class.java))
        finish()
    }

    private fun goToAssent() {
        startActivity(Intent(this@SplashActivity, AssentActivity::class.java))
        finish()
    }

    private fun goToAdditionalInfo() {
        startActivity(Intent(this@SplashActivity, AdditionalInfoActivity::class.java))
        finish()
    }

    private fun goToSurveyActivity() {
        startActivity(Intent(this@SplashActivity, BottomBarActivity::class.java))
        finish()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) Utility.showSnackBar(findViewById(R.id.main_content), if (isConnected) getString(R.string.internet_connected) else getString(R.string.internet_disconnected), Snackbar.LENGTH_SHORT)
    }

    override fun onCompletion(mp: MediaPlayer) {
        isVideoComplete = true
        if (SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)!!) {
            navigationFlow()
            finish()
        } else { //stay in his page unless permission has given
            requestPermission()
        }
    }

    override fun onStart() {
        super.onStart()
        mContentView!!.setVideoURI(Uri.parse("android.resource://" + packageName + "/" + R.raw.ph_logo))
        mContentView!!.start()
    }

    companion object {
        private val TAG = SplashActivity::class.java.simpleName
        private const val REQUEST_CODE = 11
    }
}