package com.dreamorbit.walktalktrack.screens.bottombar.activities

import android.app.ActivityManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.annotation.WorkerThread
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.pedometer.PedoUtils
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.researchaware.model.survey.Questions
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton
//import com.dreamorbit.walktalktrack.BuildConfig
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.LAUNCH_WEAR
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.base.BaseFragment
import com.dreamorbit.walktalktrack.custom.RecyclerTouchListener
import com.dreamorbit.walktalktrack.custom.SimpleDividerItemDecoration
import com.dreamorbit.walktalktrack.database.questions.QuestionsRepository.Companion.instance
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.dialogs.AlertCallback
import com.dreamorbit.walktalktrack.dialogs.DialogClass.getAlertDialogOk
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.activities.Activities
import com.dreamorbit.walktalktrack.pojo.activities.ActivityList
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.screens.bottombar.SurveyConfirmActivity
import com.dreamorbit.walktalktrack.screens.bottombar.activities.ActivitiesPresenterContract.ActivityView
import com.dreamorbit.walktalktrack.screens.casting.CastingActivity
import com.dreamorbit.walktalktrack.screens.splash.WeatherApi
import com.dreamorbit.walktalktrack.service.BackGroundService
import com.dreamorbit.walktalktrack.utilities.Utility
import com.dreamorbit.walktalktrack.wear.MobileMessageSenderAsync
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.Node
import com.google.android.gms.wearable.Wearable
import com.google.android.wearable.intent.RemoteIntent
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutionException

class ActivitiesFragment : BaseFragment(), ActivityView {
    // Result from sending RemoteIntent to wear device(s) to open app in play/app store.
    private val mResultReceiver: ResultReceiver = object : ResultReceiver(Handler()) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            Log.d(TAG, "onReceiveResult: $resultCode")
            if (resultCode == RemoteIntent.RESULT_OK) {
                val toast = Toast.makeText(
                        activity,
                        "Play Store Request to Wear device successful.",
                        Toast.LENGTH_SHORT)
                toast.show()
            } else if (resultCode == RemoteIntent.RESULT_FAILED) {
                val toast = Toast.makeText(
                        activity, "Play Store Request Failed. Wear device(s) may not support Play Store, "
                        + " that is, the Wear device may be version 1.0.",
                        Toast.LENGTH_LONG)
                toast.show()
            } else {
                throw IllegalStateException("Unexpected result $resultCode")
            }
        }
    }
    private var mActivitiesPresenter: ActivitiesPresenter? = null
    //Adapter RecyclerView
    private var mActivitiesAdapter: ActivitiesAdapter? = null
    private var mActivityRecycler: RecyclerView? = null
    private lateinit  var mActivityIdArray: IntArray
    private var mActivityList: List<ActivityList>? = null
    private var mActivityIdClicked = 0
    private var mActivityName: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    /**
     * Check if Activities allready stored in database, if so fetch from db and show in list
     * Else call api to get the list of activities
     *
     * @param savedInstanceState
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        callActivityApi()
    }

    fun callActivityApi() {
       /* val listLocalDB = mActivitiesPresenter!!.localDBActivities
        if (listLocalDB.size > 0) {
            bindAdapter(listLocalDB)
        } else { //Calling api to get the Activities
            if (ConnectivityReceiver.isConnected) {
                showLoader(getString(R.string.please_wait))
                mActivitiesPresenter!!.activitiesApi
            } else {
                Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            }
        }
        //Send data to Wearable to enable or disable test taking job
        Utility.sendTestSchedulerToWear()*/
    }

    /**
     * Getting Questions for all activities
     */
    val activitiesQuestionsApi: Unit
        get() {
            if (ConnectivityReceiver.isConnected) {
                showLoader(getString(R.string.downloading_questions))

                    for (id in mActivityIdArray) {
                        mActivitiesPresenter!!.getSurveyQuestionsApi(id)
                    }

                hideLoader()
            } else {
                Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
            }
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mActivitiesPresenter = ActivitiesPresenter(this, ActivitiesApi(), SurveyQuestionsApi())
        val view = inflater.inflate(R.layout.fragment_bottom_activities, container, false)
        val date = view.findViewById<TextView>(R.id.tv_date)
        val dateFormat: DateFormat = SimpleDateFormat("MMMM dd")
        val cal = Calendar.getInstance()
        //Assign present day to end Date
        date.text = getString(R.string.current_date, dateFormat.format(cal.time))
        initRecyclerView(view)
        initToolbar(view)
        checkTestIsRunning()
        return view
    }

    private fun initToolbar(v: View) {
        val toolbar: Toolbar = v.findViewById(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.title)
        val imgProfile = toolbar.findViewById<TextView>(R.id.profile)
        title.setText(R.string.menu_bottom_activities)
        imgProfile.setOnClickListener { view: View? -> loadProfileActivity() }
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    private fun checkTestIsRunning() {
        if (SdkSharedPrefSingleton.getInstance().isWalkTestActive) {
            Log.e(TAG, "Running")
            val mngr = activity!!.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val taskList = mngr.getRunningTasks(20)
            if (!activity!!.isTaskRoot) { // Android launched another instance of the root activity into an existing task
// so just quietly finish and go away, dropping the user back into the activity
// at the top of the stack (ie: the last state of this task)
                activity!!.finish()
            }
        } else {
            Log.e(TAG, "Not Running")
        }
    }

    private fun initRecyclerView(view: View) {
        mActivityRecycler = view.findViewById(R.id.activity_recycler)
        mActivityRecycler?.setLayoutManager(LinearLayoutManager(this.activity))
        mActivityRecycler?.addItemDecoration(SimpleDividerItemDecoration(this.activity))
        mActivityRecycler?.addOnItemTouchListener(RecyclerTouchListener(this.activity,
                mActivityRecycler!!, object : IRecyclerItemClickListener {
            override fun onClick(view: View?, position: Int, activityId: Int) { // Removed ? from position: Int?, activityId: Int?
                mActivityIdClicked = activityId!!
                mActivityName = mActivityList!![position!!].name
                //boolean isCompleted = mActivityList.get(position).getIsActivityAnswered() == 1;
                val isCompleted = SchedulerRepository.instance?.getActivityStatus(activityId)
                //Disable button click if already test has taken
                if (mActivityName.equals("6 Minute Walk Test", ignoreCase = true) && walkTestSchedulerSetup(isCompleted!!)) {
                    Toast.makeText(context, getString(R.string.test_taken_message), Toast.LENGTH_SHORT).show()
                    return
                }
                /**
                 * dont launch the survey if already completed
                 */
                /**
                 * dont launch the survey if already completed
                 */
                if (isCompleted!!) {
                    initSurveySetup(activityId)
                }
            }

            override fun onLongClick(view: View?, position: Int) {}  // Added ? to View
        }))
    }

    private fun initSurveySetup(activityId: Int) {
        val questionList = mActivitiesPresenter!!.getSurveyQuestionsDb(activityId)
        if (questionList?.size == 0 && ConnectivityReceiver.isConnected) {
            showLoader(getString(R.string.downloading_questions))
            mActivitiesPresenter!!.getSurveyQuestionsApi(mActivityIdClicked)
        } else if (questionList?.size != 0) {
            val questionJson = careateQuestionJson(questionList)
            showActivityConfirmDialog(mActivityName, questionJson)
        } else {
            Toast.makeText(activity, "Connect with your internet. As the questions are not downloaded.", Toast.LENGTH_LONG).show()
        }
    }

    private fun walkTestSchedulerSetup(isCompleted: Boolean): Boolean {
        if (isCompleted) {
            return true //
        } else { /*
             * Get weather data before starting 6MWT
             */
            if (mActivityName.equals("6 Minute Walk Test", ignoreCase = true)) {
                val lat: Double = if (TextUtils.isEmpty(SdkSharedPrefSingleton.getInstance().latitude)) 0.0 else SdkSharedPrefSingleton.getInstance().latitude.toDouble()
                val longitude: Double = if (TextUtils.isEmpty(SdkSharedPrefSingleton.getInstance().longitude)) 0.0 else SdkSharedPrefSingleton.getInstance().longitude.toDouble()
                if (lat > 0 && ConnectivityReceiver.isConnected) WeatherApi().getLocation(lat, longitude)
            }
            /*
             * Casting value feature implementation
             * If Phone is connected with Wear device then take 6MWT in Wearable rather than phone
             * Show the castin value in Phone screen
             * Once the 6MWT is completes navigate to Activity screen and go to 3rd question in Wear device
             */if (mActivityName.equals("6 Minute Walk Test", ignoreCase = true)) {
                WearNodeAsync().execute()
            }
        }
        return false
    }

    /***************************************************
     * Casting tak implemntation if werable is connected
     */
    private fun performCastingTask() { //Laucch the Wear app from Phone
//1.Uncomment this to make it work
//2.Also uncomment In Wear project -> SurveyFlowFragment -> onActivityCreated()
//3.Uncomment PhWearApplication -> registerActivityLifecycleCallbacks()
        MobileMessageSenderAsync().execute(LAUNCH_WEAR)
        val intent = Intent(activity, CastingActivity::class.java)
        startActivity(intent)
    }

    private fun fitAlert(context: Context) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.six_min_walk)
                .setPositiveButton(android.R.string.ok) { dialog: DialogInterface?, which: Int ->
                    if (ConnectivityReceiver.isConnected) { //  Redirect to play store
                        val viewIntent = Intent("android.intent.action.VIEW",
                                Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.fitness"))
                        startActivity(viewIntent)
                    } else {
                        Utility.showSnackBar(view, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog: DialogInterface, which: Int -> dialog.dismiss() }
                .setCancelable(false)
                .show()
    }

    private fun appInstalledOrNot(uri: String): Boolean {
        val pm = activity!!.packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return false
    }

    private fun showActivityConfirmDialog(activityName: String?, questionJson: String) {
        var activityName = activityName
        if (activityName.equals(getString(R.string.who_fun_survey), ignoreCase = true)) {
            activityName = getString(R.string.who_fun_study)
        }
        SdkSharedPrefSingleton.getInstance().saveWalkTestActive(false)
        val intent = Intent(activity, SurveyConfirmActivity::class.java)
        intent.putExtra(SdkConstant.SURVEY_QUESTION_LIST, questionJson)
        intent.putExtra(SdkConstant.SURVERY_TITLE, activityName)
        startActivityForResult(intent, BottomBarActivity.SURVEY_REQUEST_CODE)
    }

    /**
     * Fetching the data from main application
     * data will be in json fomat related to all data regarding Assent and consent flow
     *
     * @param questionList
     * @return screenDataRoot
     */
    private fun careateQuestionJson(questionList: List<Questions?>?): String {
        val listType = object : TypeToken<List<Questions?>>() {}.type
        val gson = Gson()
        return gson.toJson(questionList, listType)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onActivitiesItemClick() {}
    override fun onLoad(result: Activities?) { //? added
        hideLoader()
        if (TextUtils.isEmpty(result?.errorMessage)) { //? added
            mActivitiesPresenter!!.saveActivitiesLocally(result?.activityList) //? added
            mActivitiesPresenter!!.saveSchedulerData(result!!) //!! added
            bindAdapter(result.activityList)
            activitiesQuestionsApi
        } else {
            Toast.makeText(activity, result?.errorMessage, Toast.LENGTH_LONG).show() //? added
        }
    }

    /**
     * Dont load if questionlist allready downloaded
     * Save questions and options
     *
     * @param questionList
     */
    override fun onSurveyLoaded(questionList: QuestionList?) {
        instance!!.saveQuestions(questionList)
        if (!TextUtils.isEmpty(mActivityName)) {
            val questionJson = careateQuestionJson(questionList!!.questions)
            showActivityConfirmDialog(mActivityName, questionJson)
            hideLoader()
            mActivityName = null
        }
    }

    /**
     * If api call failed
     */
    override fun hideProgressBar() {
        hideLoader()
    }

    /**
     * Bind the survey activity adapter
     *
     * @param activityList
     */
    private fun bindAdapter(activityList: List<ActivityList>?) {
        mActivityList = activityList
        mActivitiesAdapter = ActivitiesAdapter(this.activity!!)
        mActivitiesAdapter!!.setListContent(activityList)
        mActivityRecycler!!.adapter = mActivitiesAdapter
        //get array from Activity list to pass in Intent service to download respective questions
        var i = 0
        mActivityIdArray = IntArray(activityList!!.size)
        for (id in activityList) {
            mActivityIdArray[i] = id.id!!
            ++i
        }
    }

    /**
     * Mark survey as completed in local db and refresh the list
     *
     * @param activityId
     */
    fun updateSurveyStatusCompleted(activityId: Int) { //If no notification has been triggered yet for Scheduler dont disable the test taking task
//Update scheduler table against this survey or test ID
//Infinity test and survey action lock. Enable disable depend on flag. if true, do not update the db
        if (!PedoUtils.isContinuousSurvey()) {
            if (SchedulerRepository.instance?.scheduler != null) {
                SchedulerRepository.instance?.saveSchedulerStatus(SchedulerModel.TEST_TAKEN, SdkUtils.getCurrentUTCDateTimeWithMillSeconds(), activityId) //update the test taken flag as TAKEN. and disable the row item.
                mActivitiesAdapter!!.notifyDataSetChanged()
            }
        }
        // TODO: 26-02-2019 Need to optimize. Use single enum to upload both survey and test data
        val updated = mActivitiesPresenter!!.updateSurveyAsComplted(activityId)
        if (updated != -1L) {
           /* val listLocalDB = mActivitiesPresenter!!.localDBActivities
            bindAdapter(listLocalDB)
            if (ConnectivityReceiver.isConnected) {
                if (activityId != 4) { //"If not 6 Minute Walk Test"
                    startBackgroundServiceToUploadSurvey()
                } else {
                    startBackgroundServiceToUploadWalkTest()
                }
            } else {
                getAlertDialogOk(activity, getString(R.string.not_connected), getString(R.string.survey_upload_alert), getString(R.string.ok), false,
                        object : AlertCallback {
                            override fun PositiveMethod(dialog: DialogInterface?, id: Int) {}
                            override fun NegativeMethod(dialog: DialogInterface?, id: Int) {}
                        })
            }*/
        }
        saveSummaryDataInDatabaseFromPreference()
    }

    /**
     * Get summary from preference and upload to server as Report data
     */
    private fun saveSummaryDataInDatabaseFromPreference() {
        val gson = Gson()
        val summary = SdkSharedPrefSingleton.getInstance().testSummary
        val type: TypeToken<List<PostReportRequest>> = object : TypeToken<List<PostReportRequest>>() {} // Removed ? from  TypeToken<List<PostReportRequest?>?>()
        val summaryList = gson.fromJson<List<PostReportRequest>>(summary, type.type)
        if (summaryList != null && summaryList.size > 0) {
            mActivitiesPresenter!!.saveSummary(summaryList)
            SdkSharedPrefSingleton.getInstance().saveTestSummary(null) //clear once saved to database
            startBackgroundServiceUploadSummary()
        }
    }

    /**
     * Open Wear stand alone app from phone
     *
     * @param mAllConnectedNodes
     */
    //KOTLIN Error
  /*  private fun openPlayStoreOnWearDevicesWithoutApp(mAllConnectedNodes: List<Node>) {
        Log.d(TAG, "openPlayStoreOnWearDevicesWithoutApp()")
        val intent2 = activity!!.packageManager.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID)
        val intent = Intent(Intent.ACTION_VIEW)
                .addCategory(Intent.CATEGORY_BROWSABLE)
                .setData(Uri.parse(PLAY_STORE_APP_URI))
        for (node in mAllConnectedNodes) {
            RemoteIntent.startRemoteActivity(
                    activity,
                    intent2,
                    mResultReceiver,
                    node.id)
        }
    }*/

    private fun startBackgroundServiceToUploadWalkTest() {
        val intent = Intent(activity, BackGroundService::class.java)
        intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_walk_test.toString())
        BackGroundService.enqueueBackgroundWork(PhAwareApplication.context, intent)
    }

    /**
     * Start the upload survey background task
     */
    private fun startBackgroundServiceToUploadSurvey() {
        val intent = Intent(activity, BackGroundService::class.java)
        intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_survey.toString())
        BackGroundService.enqueueBackgroundWork(PhAwareApplication.context, intent)
    }

    /**
     * Start the upload survey background task
     */
    private fun startBackgroundServiceUploadSummary() {
        val intent = Intent(activity, BackGroundService::class.java)
        intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_summary_data.toString())
        BackGroundService.enqueueBackgroundWork(PhAwareApplication.context, intent)
    }

    fun refreshAdapter() {
        if (mActivitiesAdapter != null) mActivitiesAdapter!!.refreshAdapter()
    }

    /**
     * Check connected nodes
     */
    inner class WearNodeAsync : AsyncTask<Any?, Any?, Any?>() {
        override fun doInBackground(objects: Array<Any?>): Any? {
            return nodes
        }

        override fun onPostExecute(o: Any?) {
            super.onPostExecute(o)
            if (o != null) {
                if ((o as List<Node?>).size > 0) {
                    performCastingTask()
                }
            }
        }

        // Block on a task and get the result synchronously (because this is on a background
// thread).
        @get:WorkerThread
        protected val nodes: Collection<Node>?
            protected get() {
                var nodes: List<Node>? = null
                val nodeListTask = Wearable.getNodeClient(activity!!).connectedNodes
                try { // Block on a task and get the result synchronously (because this is on a background
// thread).
                    nodes = Tasks.await(nodeListTask)
                    for (node in nodes!!) {
                        nodes = if (node.isNearby) break else null
                    }
                } catch (exception: ExecutionException) {
                    Log.e(TAG, "Task failed: $exception")
                } catch (exception: InterruptedException) {
                    Log.e(TAG, "Interrupt occurred: $exception")
                }
                return nodes
            }
    }

    companion object {
        val TAG = ActivitiesFragment::class.java.simpleName
        // TODO: Replace with your links/packages.
        private const val PLAY_STORE_APP_URI = "market://details?id=com.dreamorbit.walktalktrack"

        @JvmStatic
        fun newInstance(): ActivitiesFragment {
            return ActivitiesFragment()
        }
    }
}