package com.dreamorbit.walktalktrack.screens.joinstudy

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.*
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
//import com.dreamorbit.walktalktrack.BuildConfig //Kotlin error
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.screens.eligibility.EligibilityActivity
import com.dreamorbit.walktalktrack.screens.joinstudy.JoinStudyActivity
import com.dreamorbit.walktalktrack.utilities.LocationHelper
import com.dreamorbit.walktalktrack.utilities.PermissionsUtility
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import com.rd.PageIndicatorView
import java.util.*

class JoinStudyActivity : BaseActivity(), View.OnClickListener {
    var radioYes: RadioButton? = null
    var radioNo: RadioButton? = null
    //ViewPager
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var mViewPager: ViewPager? = null
    //Date Picker to get DOB screen
    private var datePickerDialog: DatePickerDialog? = null
    private var etAge: EditText? = null
    private var txtAgeLabel: TextView? = null
    private var txtAgeValue: TextView? = null
    private var ageValue: String? = null
    private var DOBValue: String? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    //Country selection screen
    private var isCountrySelected: String? = null
    //Parent allow section
    private var isParentAllowing: String? = null
    private var permissionsUtility: PermissionsUtility? = null
    //Option menu
    private var mMenu: Menu? = null
    private var tvTtle: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_join_study)
        initToolbar()
        initViewPager()
        if (SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)!!) {
            requestPermission()
        }
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        tvTtle = toolbar.findViewById(R.id.title)
        tvTtle?.setText(R.string.age)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun requestPermission() {
        permissionsUtility = PermissionsUtility()
        if (permissionsUtility!!.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            val locationHelper = LocationHelper(this@JoinStudyActivity)
            locationHelper.startLocationUpdates()
        } else {
            if (permissionsUtility!!.shouldShowRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar.make(findViewById(R.id.main_content), "Location permission is required to fetch location.",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                    // Request the permission
                    permissionsUtility!!.requestForLocationPermission(this@JoinStudyActivity)
                }.show()
            } else {
                permissionsUtility!!.requestForLocationPermission(this)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        mMenu = menu
        val inflater = menuInflater
        inflater.inflate(R.menu.study_menu, menu)
        MenuNextButtonVisibility()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.next -> {
                performMenuNextClick()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Check in which screen you are and navigate to the screen on the screen mPosition
     */
    private fun performMenuNextClick() {
        if (mMenu != null) {
            when (mViewPager!!.currentItem) {
                0 -> mViewPager!!.currentItem = 1
                1 -> mViewPager!!.currentItem = 2
                2 -> validateEligibility()
            }
        }
    }

    /**
     * Check the eligibility and navigate to the next screen
     */
    private fun validateEligibility() {
        val intent = Intent(this@JoinStudyActivity, EligibilityActivity::class.java)
        val isConsent = Utility.AgeValidationCheck(ageValue!!.toInt())
        if (isConsent == Utility.AgeCheck.invalid || isCountrySelected == "No" || isParentAllowing == "No") {
            intent.putExtra(AppConstants.IS_ELIGIBLE, false)
        } else {
            intent.putExtra(AppConstants.IS_ELIGIBLE, true)
            intent.putExtra(AppConstants.YEAR, mYear)
            intent.putExtra(AppConstants.MONTH, mMonth)
            intent.putExtra(AppConstants.DAY, mDay)
            intent.putExtra(AppConstants.AGE, ageValue)
        }
        startActivity(intent)
    }

    /**
     * Show menu next button depend on the viewpager screen selection
     */
    private fun MenuNextButtonVisibility() {
        if (mMenu != null) {
            when (mViewPager!!.currentItem) {
                0 -> {
                    tvTtle!!.setText(R.string.age)
                    //disable next if DOB is not entered
                    if (TextUtils.isEmpty(ageValue)) {
                        mMenu!!.findItem(R.id.next).isEnabled = false
                        hideAge()
                    } else {
                        mMenu!!.findItem(R.id.next).isEnabled = true
                        showAge()
                    }
                }
                1 -> {
                    tvTtle!!.setText(R.string.country)
                    if (SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)!!) {
                        mMenu!!.findItem(R.id.next).isEnabled = true
                    } else if (TextUtils.isEmpty(isParentAllowing)) {
                        mMenu!!.findItem(R.id.next).isEnabled = false
                    } else if (!TextUtils.isEmpty(isParentAllowing) && isParentAllowing == "Yes" || isParentAllowing == "No") {
                        mMenu!!.findItem(R.id.next).isEnabled = true
                    }
                }
                2 -> {
                    tvTtle!!.setText(R.string.permission)
                    if (!TextUtils.isEmpty(ageValue) && !TextUtils.isEmpty(isCountrySelected) && !TextUtils.isEmpty(isParentAllowing)) {
                        mMenu!!.findItem(R.id.next).isEnabled = true
                    } else {
                        mMenu!!.findItem(R.id.next).isEnabled = false
                    }
                }
            }
        }
    }

    /**
     * Init Viewpager
     */
    private fun initViewPager() {
        mSectionsPagerAdapter = SectionsPagerAdapter()
        mViewPager = findViewById(R.id.container)
        mViewPager?.setAdapter(mSectionsPagerAdapter)
        mViewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                MenuNextButtonVisibility()
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
        val pageIndicatorView = findViewById<PageIndicatorView>(R.id.pageIndicatorView)
        pageIndicatorView.setViewPager(mViewPager)
    }

    //Show age screen if mPager mPosition is 1
    private fun inflateDOB(collection: ViewGroup): ViewGroup {
        val inflater = LayoutInflater.from(this@JoinStudyActivity)
        val layout = inflater.inflate(R.layout.join_study_age, collection, false) as ViewGroup
        etAge = layout.findViewById(R.id.etAge)
        etAge?.setOnClickListener(this)
        txtAgeLabel = layout.findViewById(R.id.txtAge)
        txtAgeValue = layout.findViewById(R.id.txtAgeValue)
        etAge?.setText(DOBValue)
        txtAgeValue?.setText(ageValue)
        return layout
    }

    /**
     * Show Country screen if mPager mPosition is 2
     */
    private fun inflateCountry(collection: ViewGroup): ViewGroup {
        val inflater = LayoutInflater.from(this@JoinStudyActivity)
        val layout = inflater.inflate(R.layout.join_study_country, collection, false) as ViewGroup
        val radioGroup = layout.findViewById<RadioGroup>(R.id.radioGroup)
        radioYes = layout.findViewById(R.id.radioYes)
        radioNo = layout.findViewById(R.id.radioNo)
        Utility.hideKeyboard(radioNo)
        radioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            if (checkedId == R.id.radioYes) {
                isCountrySelected = "Yes"
            } else if (checkedId == R.id.radioNo) {
                isCountrySelected = "No"
            }
            if (mMenu != null) mMenu!!.findItem(R.id.next).isEnabled = true
        }
        deviceLocality()
        return layout
    }

    fun deviceLocality() { //Checking the device locality
        val locale = SharedPrefSingleton.instance?.getBoolean(SharedPrefSingleton.IS_USA, false)
        if (locale!!) { //!! added
            radioYes!!.isEnabled = true
        } else {
            radioNo!!.isChecked = true
            radioYes!!.isEnabled = false
        }
    }

    /**
     * Inflate parent allow screen
     */
    private fun inflateParentAllow(collection: ViewGroup): ViewGroup {
        val inflater = LayoutInflater.from(this@JoinStudyActivity)
        val layout = inflater.inflate(R.layout.join_study_parentallowance, collection, false) as ViewGroup
        val radioGroup = layout.findViewById<RadioGroup>(R.id.radioGroup)
        val radioYes = layout.findViewById<RadioButton>(R.id.radioYes)
        val radioNo = layout.findViewById<RadioButton>(R.id.radioNo)
        Utility.hideKeyboard(radioNo)
        if (TextUtils.isEmpty(isParentAllowing)) {
        } else if (!TextUtils.isEmpty(isParentAllowing) && isParentAllowing == "Yes" || isParentAllowing == "No") {
            radioYes.isChecked = true
        }
        radioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            if (checkedId == R.id.radioYes) {
                isParentAllowing = "Yes"
            } else if (checkedId == R.id.radioNo) {
                isParentAllowing = "No"
            }
            if (mMenu != null) MenuNextButtonVisibility()
        }
        return layout
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.etAge -> FetchDateOfBirth()
        }
    }

    /**
     * Fetch DOB
     */
    private fun FetchDateOfBirth() {
        val MONTHS = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        // calender class's instance and get current date , month and year from calender
        val c = Calendar.getInstance()
        if (TextUtils.isEmpty(DOBValue)) {
            mYear = c[Calendar.YEAR] // current year
            mMonth = c[Calendar.MONTH] // current month
            mDay = c[Calendar.DAY_OF_MONTH] // current day
        } else { //value will get get from previous state
        }
        // date picker dialog
        datePickerDialog = DatePickerDialog(this@JoinStudyActivity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    //Saving the state of last selected DOB to prepolulate next time on date picker dialog
                    mYear = year
                    mMonth = monthOfYear + 1
                    mDay = dayOfMonth
                    etAge!!.setText("")
                    txtAgeValue!!.text = ""
                    etAge!!.setText(MONTHS[monthOfYear] + "-" + dayOfMonth + "-" + year)
                    txtAgeValue!!.text = getAge(mYear, mMonth, mDay)
                    DOBValue = etAge!!.text.toString()
                    ageValue = txtAgeValue!!.text.toString()
                    MenuNextButtonVisibility()
                }, mYear, mMonth, mDay)
        datePickerDialog!!.show()
        datePickerDialog!!.datePicker.maxDate = System.currentTimeMillis()
    }

    private fun showAge() {
        txtAgeLabel!!.visibility = View.VISIBLE
        txtAgeValue!!.visibility = View.VISIBLE
        txtAgeValue!!.text = ageValue
    }

    private fun hideAge() {
        txtAgeLabel!!.visibility = View.INVISIBLE
        txtAgeValue!!.visibility = View.INVISIBLE
    }

    fun updateLocation() {}
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsUtility.PERMISSIONS_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val locationHelper = LocationHelper(this@JoinStudyActivity)
                    locationHelper.startLocationUpdates()
                } else {
                    Snackbar.make(findViewById(R.id.main_content), "Location permission is denied go to settings to enable.",
                            Snackbar.LENGTH_INDEFINITE).setAction("OK") {
                        // Request the permission
/* Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(callGPSSettingIntent, 2);*/

                        //Kotlin error
                       // startActivityForResult(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID)), 2)
                    }.show()
                }
                return
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        requestPermission()
    }

    /**
     * A [PagerAdapter] that returns a layout corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter : PagerAdapter() {
        override fun getCount(): Int { // Show 3 total pages.
            return 3
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return `object` === view
        }

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            var layout: ViewGroup? = null
            when (position) {
                0 -> layout = inflateDOB(collection)
                1 -> layout = inflateCountry(collection)
                2 -> layout = inflateParentAllow(collection)
            }
            collection.addView(layout)
            return layout!!
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getItemPosition(`object`: Any): Int {
            return POSITION_NONE
        }
    }

    companion object {
        /**
         * Calculate age from the date picker selection
         *
         * @return ageS
         */
        fun getAge(year: Int, month: Int, day: Int): String {
            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()
            dob[year, month] = day
            val age = today[Calendar.YEAR] - dob[Calendar.YEAR]
            /* if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }*/
            var ageInt = age
            ageInt = if (ageInt < 0) 0 else ageInt
            return ageInt.toString()
        }
    }
}