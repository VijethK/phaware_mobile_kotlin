package com.dreamorbit.walktalktrack.screens.registration

import android.util.Log
import com.dreamorbit.walktalktrack.api.PhPublicKey
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository
import com.dreamorbit.walktalktrack.database.registration.RegistartionData
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.pojo.register.RegisterRequest
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.dreamorbit.walktalktrack.service.PublicKeyApi
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by mujasam on 12/06/2019.
 */
class RegisterPresenter
/**
 * Constructor to inialize the register contract and Register api to call service
 */(private val mRegisterView: RegisterViewContract, private val mRegisterApi: RegisterApi, private val publicKeyApi: PublicKeyApi) {
    /**
     * fetch the data from database using repository class
     */
    val studyDetail: Unit
        get() {
            mRegisterView.onStudyDataLoaded(StudyRepository.instance!!.studyData)
        }

    /**
     * On successfull registration. persist the register data in sqlite for future use
     */
    fun saveRegistartionDetails(registartionData: RegistartionData?) {
        RegistartionRepository.instance!!.deleteRegistrationData()
        RegistartionRepository.instance!!.saveRegistrationData(registartionData!!)
    }

    /**
     * Call the api to register the user using service layer object
     */
    fun registerService(registerRequest: RegisterRequest?) {
        mRegisterApi.register(registerRequest, this)
    }

    fun downloadCertificate() {
        publicKeyApi.presendUrl?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(object : SingleObserver<PhPublicKey?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(phPublicKey: PhPublicKey) {
                        if (phPublicKey != null) {
                            Log.e("Response:", "" + phPublicKey)
                            publicKeyApi.getPublicKey(phPublicKey.url)?.subscribeOn(Schedulers.io())
                                    ?.observeOn(AndroidSchedulers.mainThread())
                                    ?.subscribe(object : SingleObserver<String?> {
                                        override fun onSubscribe(disposable: Disposable) {}
                                        override fun onSuccess(s: String) {
                                            mRegisterView.onCertificateDownloadSuccess()
                                        }

                                        override fun onError(throwable: Throwable) {
                                            mRegisterView.onCertificateDownloadSuccess()
                                        }
                                    })
                        }
                    }

                    override fun onError(e: Throwable) {}
                })
    }

    /**
     * Validation of registration fields
     *
     * @param email
     * @param userName
     * @param password
     */
    fun validation(email: Boolean, userName: String, password: String) {
        if (!email) {
            mRegisterView.setValidationErrors("email")
        } else if (userName.trim { it <= ' ' }.isEmpty()) {
            mRegisterView.setValidationErrors("userName")
        } else if (password.trim { it <= ' ' }.isEmpty()) {
            mRegisterView.setValidationErrors("password")
        } else {
            mRegisterView.registerWebRequest()
        }
    }

    /**
     * Registration service result
     *
     * @param registerResponse
     */
    fun registerResult(registerResponse: RegisterResponse?) {
        if (registerResponse != null && registerResponse.accessToken != null) {
            saveStudyConfiguration(registerResponse)
            mRegisterView.onRegistrationSuccess(registerResponse)
        } else if (registerResponse != null) {
            mRegisterView.onRegistrationFailure(registerResponse)
        }
    }

    /***
     * if response.code() == 409 that means partially registered
     * @param registerResponse
     */
    fun OnPartialRegistered(registerResponse: RegisterResponse) {
        saveStudyConfiguration(registerResponse)
        mRegisterView.partialRegistration(registerResponse)
    }

    private fun saveStudyConfiguration(regResponse: RegisterResponse) {
        SharedPrefSingleton.instance?.saveStudyConfig(regResponse.studyConfiguration)
    }

}