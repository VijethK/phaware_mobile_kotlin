package com.dreamorbit.walktalktrack.screens.rating

import retrofit2.Response

interface RatingView {
    fun onRateChange(rate: Float)
    fun onSubmitPressed()
    fun onSuccess(response: Response<*>?)
    fun onFailure(failureMessage: Response<*>?)
    fun onError(errorMessage: Response<*>?)
}