package com.dreamorbit.walktalktrack.screens.registration

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.register.RegisterRequest
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

/**
 * Service access layer to call Register api
 * Created by nareshkumar.reddy on 9/26/2017.
 */
class RegisterApi {
    fun register(register: RegisterRequest?, registerPresenter: RegisterPresenter) {
        val retrofit = getClient("")
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.getRegister(register)
        call!!.enqueue(object : Callback<RegisterResponse?> {
            override fun onResponse(call: Call<RegisterResponse?>, response: Response<RegisterResponse?>) {
                if (response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    registerPresenter.registerResult(response.body())
                } else if (response.code() == 409) { // already exist
                    val gson = Gson()
                    try {
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), RegisterResponse::class.java)
                        registerPresenter.OnPartialRegistered(errorResponse)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), RegisterResponse::class.java)
                        registerPresenter.registerResult(errorResponse)
                    } catch (e: Exception) {
                        val registerResponse = RegisterResponse()
                        registerResponse.errorMessage = "Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<RegisterResponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
                val registerResponse = RegisterResponse()
                registerResponse.errorMessage = "Registration failed. Please try again."
                registerPresenter.registerResult(registerResponse)
            }
        })
    }
}