package com.dreamorbit.walktalktrack.screens.bottombar.report

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.report.Report
import com.dreamorbit.walktalktrack.R
import java.util.*

class ReportAdapter(private val context: Context) : RecyclerView.Adapter<ReportViewHolder>() {
    private val inflater: LayoutInflater
    private var reportList: List<Report>? = ArrayList()
    private var view: View? = null
    private var holder: ReportViewHolder? = null
    //This method inflates view present in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        view = inflater.inflate(R.layout.row_item_report, parent, false)
        holder = ReportViewHolder(view!!)
        return holder!! //added !! kotlin error
    }

    //Binding the data using get() method of POJO object
    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) {
        val report = reportList!![position]
        holder.tvTime.text = "At " + SdkUtils.serverTolocalDateTimeTodayReport(report.testTakenAt)
        holder.tvAvgHeartRate.text = "Avg " + report.averageHeartRate
        holder.tvMaxHeartRate.text = "Max " + report.maxHeartRate
        holder.tvDistance.text = "" + report.distance + " m"
        holder.tvSteps.text = "" + report.stepsCount
        holder.tvDuration.text = report.durationOfTest
    }

    //Setting the arraylist
    fun setListContent(list: List<Report>?) {
        reportList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (reportList == null) 0 else reportList!!.size
    }

    init {
        inflater = LayoutInflater.from(context)
    }
}