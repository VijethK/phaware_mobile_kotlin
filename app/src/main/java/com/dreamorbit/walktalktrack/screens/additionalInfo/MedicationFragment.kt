package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance
import com.dreamorbit.walktalktrack.utilities.Utility

/**
 * A simple [Fragment] subclass.
 */
class MedicationFragment : Fragment() {
    private var performAction: PerformAction? = null
    private var next: TextView? = null
    private var medicated: String? = null
    private var back: ImageView? = null
    private var mParentActivity: AdditionalInfoActivity? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
        mParentActivity = context as AdditionalInfoActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_medication, container, false)
        initToolbar(view)
        isMedicated(view)
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTitle = toolbar.findViewById<TextView>(R.id.title)
        tvTitle.setText(R.string.under_medication)
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(View.OnClickListener { v: View? -> performAction!!.moveBack(4) })
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setEnabled(false)
        next?.setAlpha(0.5f)
        //Goto DOB screen. Age has to take
        if (mParentActivity!!.isPartialRegistrationFlow) {
            next?.setOnClickListener(View.OnClickListener { v: View? -> performAction!!.moveToNext(4) })
        } else { //Directly goto review screen.
            next?.setOnClickListener(View.OnClickListener { v: View? ->
                val additionalInfo = instance!!.additionalInfo
                if (additionalInfo.gender != null && additionalInfo.height != null && additionalInfo.weigh != null && additionalInfo.diagnosed != null && additionalInfo.medication != null) {
                    val intent = Intent(activity, AdditionalInfoSummary::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(activity, "Please fill all the mandatory fields.", Toast.LENGTH_SHORT).show()
                }
            })
        }
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    /*
     * To check whether user is medicated or not by using radioGroup
     * @layout
     * */
    private fun isMedicated(layout: View) {
        val radioGroup = layout.findViewById<RadioGroup>(R.id.radioGroup)
        val radioYes = layout.findViewById<RadioButton>(R.id.radioYes)
        val radioNo = layout.findViewById<RadioButton>(R.id.radioNo)
        Utility.hideKeyboard(radioNo)
        radioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            next!!.isEnabled = true
            next!!.alpha = 1.0f
            if (checkedId == R.id.radioYes) {
                medicated = "Yes"
                instance!!.saveMedicated(medicated)
            } else if (checkedId == R.id.radioNo) {
                medicated = "No"
                instance!!.saveMedicated(medicated)
            }
        }
    }
}