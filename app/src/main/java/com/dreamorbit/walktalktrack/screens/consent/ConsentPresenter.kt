package com.dreamorbit.walktalktrack.screens.consent

import com.dreamorbit.walktalktrack.pojo.consent.ConsentCreateRequest
import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse
import com.dreamorbit.walktalktrack.screens.registration.DocumentDownloadApi

/**
 * Created by nareshkumar.reddy on 10/12/2017.
 */
class ConsentPresenter
/**
 * Constructor to initialize the contractor and api service
 */(private val mConsenttViewContract: ConsenttViewContract, private val mAssentPostApi: CreateConsentApi, var mDocumentDownloadApi: DocumentDownloadApi) {
    /**
     * Api call to create assent in server
     */
    fun createConsentService(createRequest: ConsentCreateRequest?) {
        mAssentPostApi.createConsent(createRequest, this)
    }

    /**
     * craete api Callback result will be navigated to Controller.
     */
    fun postResult(consentResponse: ConsentResponse?) {
        if (consentResponse != null && consentResponse.id != null) {
            mConsenttViewContract.onSuccessResponse(consentResponse)
        } else if (consentResponse != null) {
            mConsenttViewContract.onErrorResponse(consentResponse)
        }
    }

    fun downloadDocument() {
        val docsType = arrayOf("consent")
        for (type in docsType) {
            mDocumentDownloadApi.downloadDocument(this, null, type)
        }
    }

    fun onDownloadComplete() {
        mConsenttViewContract.onDownloadComplete()
    }

}