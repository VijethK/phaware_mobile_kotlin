package com.dreamorbit.walktalktrack.screens.bottombar.newsfeed

import com.dreamorbit.walktalktrack.pojo.feed.FeedItem

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
interface FeedView {
    fun onSuccess(feedList: List<FeedItem?>?)
    fun onFailure(message: String?)
    fun onError(message: String?)
}