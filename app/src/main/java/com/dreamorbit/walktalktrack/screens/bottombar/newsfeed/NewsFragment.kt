package com.dreamorbit.walktalktrack.screens.bottombar.newsfeed

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseFragment
import com.dreamorbit.walktalktrack.custom.SimpleDividerItemDecoration
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.feed.FeedItem
import com.dreamorbit.walktalktrack.utilities.Utility

class NewsFragment : BaseFragment(), FeedView, SwipeRefreshLayout.OnRefreshListener {
    private var recyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var parentLayout: FrameLayout? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bottom_news, container, false)
        initView(view)
        callPhAwareFeed()
        initToolbar(view)
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val title = view.findViewById<TextView>(R.id.title)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        title.text = getString(R.string.menu_bottom_listen)
        val imgProfile = toolbar.findViewById<TextView>(R.id.profile)
        imgProfile.setOnClickListener { v: View? -> loadProfileActivity() }
    }

    private fun initView(view: View) {
        recyclerView = view.findViewById(R.id.feed_view)
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh)
        parentLayout = view.findViewById(R.id.parent_feed)
        recyclerView?.setLayoutManager(LinearLayoutManager(activity))
        recyclerView?.addItemDecoration(SimpleDividerItemDecoration(this.activity))
        swipeRefreshLayout?.setOnRefreshListener(this)
    }

    private fun callPhAwareFeed() {
        if (ConnectivityReceiver.isConnected) {
            Utility.showProgressBarListen(activity, getString(R.string.please_wait))
            val feedPresenter = FeedPresenter(this, RssApi())
            feedPresenter.feed
        } else {
            Utility.showSnackBar(parentLayout, getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    override fun onSuccess(feedList: List<FeedItem?>?) {
        //Kotlin error
       // recyclerView!!.adapter = FeedAdapter(activity!!, feedList)
        Utility.dismissProgressBar()
    }

    override fun onFailure(message: String?) {
        Utility.dismissProgressBar()
    }

    override fun onError(message: String?) {
        Utility.dismissProgressBar()
    }

    override fun onRefresh() {
        callPhAwareFeed()
        swipeRefreshLayout!!.isRefreshing = false
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivitiesFragment.
         */
        @JvmStatic
        fun newInstance(): NewsFragment {
            return NewsFragment()
        }
    }
}