package com.dreamorbit.walktalktrack.screens.invite

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.invite.InviteToggleResponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InviteApi {
    fun validateInviteCode(invitePresenter: InvitePresenter, token: String?) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.validateInviteToken(token)
        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                if (response != null && response.code() == 200) {
                    Log.e("Response:", "" + response.body())
                    invitePresenter.getValidateSuccess(response)
                } else if (response != null && response.code() == 401) {
                    try {
                        invitePresenter.onFaiure(response)
                    } catch (e: Exception) {
                        invitePresenter.onFaiure(response)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.e("", "" + t.toString())
                invitePresenter.getError(t.toString())
            }
        })
    }

    fun validateInviteCodeOnOff(): Single<InviteToggleResponse?>? {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        return service.validateTokenOnOFF()
    }
}