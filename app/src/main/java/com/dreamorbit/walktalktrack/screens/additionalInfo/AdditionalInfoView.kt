package com.dreamorbit.walktalktrack.screens.additionalInfo

/**
 * Created by nareshkumar.reddy on 10/27/2017.
 */
interface AdditionalInfoView {
    fun onSuccess()
    fun onFailure()
    fun onError()
}