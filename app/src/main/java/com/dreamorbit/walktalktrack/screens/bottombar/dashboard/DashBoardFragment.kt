package com.dreamorbit.walktalktrack.screens.bottombar.dashboard

import android.content.Context
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseFragment
import java.util.*

class DashBoardFragment : BaseFragment() {
    private val imageArray = intArrayOf(R.drawable.look_icon1, R.drawable.look_icon2, R.drawable.look_icon3, R.drawable.look_icon4, R.drawable.look_icon5
            , R.drawable.look_icon6, R.drawable.look_icon7, R.drawable.look_icon8, R.drawable.look_icon9, R.drawable.look_icon10, R.drawable.look_icon11
            , R.drawable.look_icon12, R.drawable.look_icon13, R.drawable.look_icon14)
    private var i = 0
    private var timerTask: TimerTask? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_bottom_dashboard, container, false)
        val viewPager: ViewPager = view.findViewById(R.id.viewpager)
        viewPager.adapter = CustomPagerAdapter(activity)
        viewPager.setOnTouchListener { v: View?, event: MotionEvent? -> true }
        val mTimer = Timer()
        timerTask = object : TimerTask() {
            override fun run() { // As timer is not a Main/UI thread need to do all UI task on runOnUiThread
                activity!!.runOnUiThread { viewPager.currentItem = ++i % imageArray.size - 1 }
            }
        }
        mTimer.schedule(timerTask, 0, 3000)
        initToolbar(view)
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val title = view.findViewById<TextView>(R.id.title)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        title.text = getString(R.string.menu_bottom_look)
        val imgProfile = toolbar.findViewById<TextView>(R.id.profile)
        imgProfile.setOnClickListener { v: View? -> loadProfileActivity() }
    }

    override fun onPause() {
        super.onPause()
        timerTask!!.cancel()
    }

    internal inner class CustomPagerAdapter(var mContext: Context?) : PagerAdapter() {
        var mLayoutInflater: LayoutInflater
        override fun getCount(): Int {
            return imageArray.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = mLayoutInflater.inflate(R.layout.image_layout, container, false)
            val imageView = itemView.findViewById<ImageView>(R.id.image)
            imageView.setBackgroundResource(imageArray[position])
            container.addView(itemView)
            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as ImageView)
        }

        init {
            mLayoutInflater = mContext!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivitiesFragment.
         */
// TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(): DashBoardFragment {
            val fragment = DashBoardFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}