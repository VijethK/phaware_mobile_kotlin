package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository
import com.dreamorbit.walktalktrack.database.joinstudy.StudyData
import com.dreamorbit.walktalktrack.database.joinstudy.StudyRepository
import com.dreamorbit.walktalktrack.utilities.Utility
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DOBFragment : Fragment(), View.OnClickListener {
    //Date Picker to get DOB screen
    private var datePickerDialog: DatePickerDialog? = null
    private var etAge: EditText? = null
    private var txtAgeLabel: TextView? = null
    private var txtAgeValue: TextView? = null
    private var ageValue: String? = null
    private var DOBValue: String? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    private var next: TextView? = null
    private var back: ImageView? = null
    private var performAction: PerformAction? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_dob, container, false)
        etAge = view.findViewById(R.id.etAge)
        etAge?.setOnClickListener(this)
        txtAgeLabel = view.findViewById(R.id.txtAge)
        txtAgeValue = view.findViewById(R.id.txtAgeValue)
        etAge?.setText(DOBValue)
        txtAgeValue?.setText(ageValue)
        initToolbar(view)
        hideAge()
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.age)
        back = toolbar.findViewById(R.id.back)
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(View.OnClickListener { v: View? -> performAction!!.moveBack(5) })
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setEnabled(false)
        next?.setAlpha(0.5f)
        next?.setOnClickListener(View.OnClickListener { v: View? ->
            val additionalInfo = AdditionalInfoRepository.instance!!.additionalInfo
            val ageData = StudyRepository.instance!!.studyData
            if (ageData != null && ageData.age != null && additionalInfo.gender != null && additionalInfo.height != null && additionalInfo.weigh != null && additionalInfo.diagnosed != null && additionalInfo.medication != null) {
                val intent = Intent(activity, AdditionalInfoSummary::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(activity, "Please fill all the mandatory fields.", Toast.LENGTH_SHORT).show()
            }
        })
        tvTtle.setPadding(30, 0, 0, 0)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    override fun onClick(view: View) {
        next!!.isEnabled = true
        next!!.alpha = 1.0f
        when (view.id) {
            R.id.etAge -> FetchDateOfBirth()
        }
    }

    /**
     * Fetch DOB
     */
    private fun FetchDateOfBirth() {
        val MONTHS = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        // calender class's instance and get current date , month and year from calender
        val c = Calendar.getInstance()
        if (TextUtils.isEmpty(DOBValue)) {
            mYear = c[Calendar.YEAR] // current year
            mMonth = c[Calendar.MONTH] // current month
            mDay = c[Calendar.DAY_OF_MONTH] // current day
        } else { //value will get get from previous state
        }
        // date picker dialog
        datePickerDialog = DatePickerDialog(activity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    //Saving the state of last selected DOB to prepolulate next time on date picker dialog
                    mYear = year
                    mMonth = monthOfYear + 1
                    mDay = dayOfMonth
                    etAge!!.setText("")
                    txtAgeValue!!.text = ""
                    etAge!!.setText(MONTHS[monthOfYear] + "-" + dayOfMonth + "-" + year)
                    txtAgeValue!!.text = getAge(mYear, mMonth, mDay)
                    DOBValue = etAge!!.text.toString()
                    ageValue = txtAgeValue!!.text.toString()
                    val data = StudyData(mYear, mMonth, mDay, ageValue!!, true)
                    StudyRepository.instance!!.deleteStudyData()
                    StudyRepository.instance!!.saveStudyData(data)
                    showAge()
                }, mYear, mMonth, mDay)
        datePickerDialog!!.show()
        datePickerDialog!!.datePicker.maxDate = System.currentTimeMillis()
    }

    private fun showAge() {
        val isConsent = Utility.AgeValidationCheck(ageValue!!.toInt())
        if (isConsent == Utility.AgeCheck.invalid) {
            StudyRepository.instance!!.deleteStudyData()
            ageValue = ""
            etAge!!.setText("")
            Toast.makeText(activity, "Age should be less than 22.", Toast.LENGTH_SHORT).show()
        }
        txtAgeLabel!!.visibility = View.VISIBLE
        txtAgeValue!!.visibility = View.VISIBLE
        txtAgeValue!!.text = ageValue
    }

    private fun hideAge() {
        txtAgeLabel!!.visibility = View.INVISIBLE
        txtAgeValue!!.visibility = View.INVISIBLE
    }

    companion object {
        /**
         * Calculate age from the date picker selection
         *
         * @return ageS
         */
        fun getAge(year: Int, month: Int, day: Int): String {
            val dob = Calendar.getInstance()
            val today = Calendar.getInstance()
            dob[year, month] = day
            val age = today[Calendar.YEAR] - dob[Calendar.YEAR]
            /* if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }*/
            var ageInt = age
            ageInt = if (ageInt < 0) 0 else ageInt
            return ageInt.toString()
        }
    }
}