package com.dreamorbit.walktalktrack.screens.additionalInfo

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.database.additionalInformation.AdditionalInfoRepository.Companion.instance

/**
 * A simple [Fragment] subclass.
 */
class GenderFragment : Fragment(), View.OnClickListener {
    private var gender: TextView? = null
    private var next: TextView? = null
    private var boy: ImageView? = null
    private var girl: ImageView? = null
    private var performAction: PerformAction? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        performAction = context as PerformAction
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_gender, container, false)
        boy = view.findViewById(R.id.boy)
        girl = view.findViewById(R.id.girl)
        gender = view.findViewById(R.id.gender)
        boy?.setOnClickListener(this)
        girl?.setOnClickListener(this)
        initToolbar(view)
        return view
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        val tvTtle = toolbar.findViewById<TextView>(R.id.title)
        tvTtle.setText(R.string.gender)
        next = toolbar.findViewById(R.id.done)
        next?.setText(getString(R.string.next))
        next?.setEnabled(false)
        next?.setAlpha(0.5f)
        next?.setOnClickListener(View.OnClickListener { performAction!!.moveToNext(0) })
        tvTtle.setPadding(30, 0, 0, 0)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
    }

    override fun onClick(view: View) {
        next!!.isEnabled = true
        next!!.alpha = 1.0f
        when (view.id) {
            R.id.boy -> {
                gender!!.setText(R.string.boy)
                boy!!.setBackgroundResource(R.drawable.gender_border)
                girl!!.setBackgroundColor(Color.WHITE)
                instance!!.saveGender(gender!!.text.toString().trim { it <= ' ' })
            }
            R.id.girl -> {
                gender!!.setText(R.string.girl)
                girl!!.setBackgroundResource(R.drawable.gender_border)
                boy!!.setBackgroundColor(Color.WHITE)
                //Toast.makeText(getActivity(), ""+, Toast.LENGTH_SHORT).show();
                instance!!.saveGender(gender!!.text.toString().trim { it <= ' ' })
            }
        }
    }
}