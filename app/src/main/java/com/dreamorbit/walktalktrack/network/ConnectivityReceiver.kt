package com.dreamorbit.walktalktrack.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.service.BackGroundService
import com.dreamorbit.walktalktrack.utilities.Utility

/**
 * Created by nareshkumar.reddy on 9/22/2017.
 */
class ConnectivityReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, arg1: Intent) {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting)
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }
        //Check if any pending zip files available in app folder
        val directories = SdkUtils.getAllDirectories(context)
        if (isConnected && directories != null && directories.size > 0) {
            val intent = Intent(context, BackGroundService::class.java)
            intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_all_pending.toString())
            BackGroundService.enqueueBackgroundWork(context, intent)
        }
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
        val isConnected: Boolean
            get() {
                val cm = PhAwareApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork = cm.activeNetworkInfo
                return (activeNetwork != null
                        && activeNetwork.isConnectedOrConnecting)
            }
    }
}