package com.dreamorbit.walktalktrack.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by mujasam.bn on 11/14/2017.
 */
class PhPublicKey {
    @SerializedName("url")
    @Expose
    var url: String? = null

}