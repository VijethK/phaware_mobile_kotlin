package com.dreamorbit.walktalktrack.api

import android.content.Intent
import android.text.TextUtils
import android.util.Log
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.service.BackGroundService
import com.dreamorbit.walktalktrack.service.RefreshTokenApi
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by mujasam.bn on 9/6/2017.
 */
object ApiClient {
    private var sRetrofit: Retrofit? = null
    private const val RETROFIT_TIMEOUT: Long = 30
    fun getClient(refreshToken: String?): Retrofit? {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Accept", "application/json")
                    .addHeader("Token", ApiServiceSettings().getStudyToken())
                    .addHeader("Authorization", refreshToken)
                    .build()
            val response = chain.proceed(request)
            if (response != null && response.code() == 401) { //unauthorised
                Log.e("TOKEN", "***EXPIRED***" + response.code())
                val email = RegistartionRepository.instance?.registrationEmailSha // Take value from table
                if (!TextUtils.isEmpty(email)) RefreshTokenApi().refreshToken(email) // if email is null that means not yet logined. No need to get refresh token
                return@Interceptor response
            } else if (response != null && response.code() == 404) { //not found
                Utility.signOut()
                Utility.clearAlarms()
            }
            response
        }).addInterceptor(logging)
                .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
        val client = httpClient.build()
        sRetrofit = Retrofit.Builder()
                .baseUrl(ApiServiceSettings().getBaseApi())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
        return sRetrofit
    }

    /**
     * For AWS api call
     *
     * @return
     */
    @JvmStatic
    fun getGeoCodeClient(baseUrl: String?): Retrofit? {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)
                .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
        val client = httpClient.build()
        sRetrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
        return sRetrofit
    }//On fresh installation if AWS key once downloaded start upload pending files// set your desired log level

    /**
     * For AWS api call
     *
     * @return
     */
    @JvmStatic
    val awsCertificateClient: Retrofit?
        get() {
            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain: Interceptor.Chain ->
                val response = chain.proceed(chain.request())
                val key = response.body()!!.string()
                SharedPrefSingleton.instance?.savePublicKey(key)
                Log.e("Awst@Response", key)
                //On fresh installation if AWS key once downloaded start upload pending files
                if (Utility.isPendingUpload) {  //vijeth changed isPendingUpload()
                    Log.e("uploading Zips: ", "Started After fetching KEY")
                    val intent = Intent(PhAwareApplication().getMyAppContext(), BackGroundService::class.java)
                    intent.putExtra(AppConstants.BACKGROUND_ACTION, Utility.BackgroundAction.upload_survey.toString())
                    PhAwareApplication().getMyAppContext().startService(intent)
                }
                response
            }.addInterceptor(logging)
                    .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
            val client = httpClient.build()
            sRetrofit = Retrofit.Builder()
                    .baseUrl(ApiServiceSettings().getBaseApi())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .build()
            return sRetrofit
        }

    /**
     * Pass generic header if not all api call has same format of headers
     *
     * @param headers
     * @return
     */
    @JvmStatic
    fun getClient(headers: Map<String?, String?>): Retrofit? {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
            for ((key, value) in headers) {
                if (value != null) {
                    requestBuilder.header(key, value)
                }
            }
            requestBuilder.method(original.method(), original.body())
            val request = requestBuilder.build()
            chain.proceed(request)
        }.addInterceptor(logging)
                .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
        val client = httpClient.build()
        sRetrofit = Retrofit.Builder()
                .baseUrl(ApiServiceSettings().getBaseApi())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
        return sRetrofit
    }

    /**
     * For AWS api call
     *
     * @return
     */
    @JvmStatic
    val awsDocumentClient: Retrofit
        get() = Retrofit.Builder()
                .baseUrl(ApiServiceSettings().getBaseApi())
                .build()
}