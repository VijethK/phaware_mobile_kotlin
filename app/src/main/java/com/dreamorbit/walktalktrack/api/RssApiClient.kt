package com.dreamorbit.walktalktrack.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by nareshkumar.reddy on 11/21/2017.
 */
object RssApiClient {
    private var sRetrofit: Retrofit? = null
    private const val RETROFIT_TIMEOUT: Long = 30
    // set your desired log level
    @JvmStatic
    val client: Retrofit?
        get() {
            val logging = HttpLoggingInterceptor()
            // set your desired log level
            logging.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
                    .connectTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(RETROFIT_TIMEOUT, TimeUnit.SECONDS)
            val client = httpClient.build()
            sRetrofit = Retrofit.Builder()
                    .baseUrl("http://phawarepodcast.libsyn.com")
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .client(client)
                    .build()
            return sRetrofit
        }
}