package com.dreamorbit.walktalktrack.api

import android.content.Context
import android.content.res.Resources
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.PhAwareApplication.Companion.context

/**
 * Created by mujasam.bn on 9/6/2017.
 */
class ApiServiceSettings{

    private var sBaseApi: String? = null
    private var sStudyToken: String? = null

    constructor() {
        val resources = context.resources
        setBaseApi(resources)
        setStudyToken(resources)
    }

    fun getStudyToken(): String? {
        return sStudyToken
    }

    private fun setStudyToken(resources: Resources) {
        sStudyToken = resources.getString(R.string.study_token_server)
    }

    fun getBaseApi(): String? {
        return sBaseApi
    }

    private fun setBaseApi(resources: Resources) {
        sBaseApi = resources.getString(R.string.base_api)
    }
}