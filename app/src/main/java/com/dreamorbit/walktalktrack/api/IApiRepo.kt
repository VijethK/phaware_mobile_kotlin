package com.dreamorbit.walktalktrack.api

import com.dreamorbit.researchaware.model.fit.weather.WeatherResponse
import com.dreamorbit.researchaware.model.report.PostReportRequest
import com.dreamorbit.researchaware.model.report.ReportResponse
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.walktalktrack.pojo.activities.Activities
import com.dreamorbit.walktalktrack.pojo.additionalInfo.AdditionalInfoRequest
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreateRequest
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse
import com.dreamorbit.walktalktrack.pojo.consent.ConsentCreateRequest
import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse
import com.dreamorbit.walktalktrack.pojo.document.DocumentResponse
import com.dreamorbit.walktalktrack.pojo.fcm.FcmPutRequest
import com.dreamorbit.walktalktrack.pojo.feed.Feed
import com.dreamorbit.walktalktrack.pojo.geocode.GeoResponse
import com.dreamorbit.walktalktrack.pojo.invite.InviteToggleResponse
import com.dreamorbit.walktalktrack.pojo.login.SignInRequest
import com.dreamorbit.walktalktrack.pojo.login.SignInResponse
import com.dreamorbit.walktalktrack.pojo.rating.Rating
import com.dreamorbit.walktalktrack.pojo.refreshToken.RefreshTokenReponse
import com.dreamorbit.walktalktrack.pojo.register.RegisterRequest
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse
import com.dreamorbit.walktalktrack.pojo.upload.PhUploadRequest
import com.dreamorbit.walktalktrack.pojo.upload.PhUploadResponse
import com.dreamorbit.walktalktrack.pojo.userProfile.ProfileResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by mujasam.bn on 9/6/2017.
 */
//https://guides.codepath.com/android/Consuming-APIs-with-Retrofit
interface IApiRepo {
    @POST("/auth/signin")
    fun signIn(@Body register: SignInRequest?): Call<SignInResponse?>?

    @POST("/auth/signup")
    fun getRegister(@Body register: RegisterRequest?): Call<RegisterResponse?>?

    @POST("/study_assents")
    fun createAssent(@Body request: AssentCreateRequest?): Call<AssentCreatedResponse?>?

    @POST("/study_consents")
    fun createConsent(@Body request: ConsentCreateRequest?): Call<ConsentResponse?>?

    @FormUrlEncoded
    @POST("/auth/refresh_token")
    fun refreshToken(@Field("email") email: String?): Call<RefreshTokenReponse?>?

    @POST("/auth/UserProfile")
    fun sendAdditionalInfo(@Body request: AdditionalInfoRequest?): Single<ResponseBody?>?

    @get:GET("/activities")
    val activities: Call<Activities?>?

    @GET("/activities/{activity_id}/questions")
    fun getQuestionsList(@Path("activity_id") activityID: Int): Call<QuestionList?>?

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/auth/signout", hasBody = true)
    fun signOut(@Field("email") email: String?, @Field("device_type") deviceType: String?): Call<Void?>?

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/study_consents/withdraw", hasBody = true)
    fun consentWithdraw(@Field("email") email: String?): Call<Void?>?

    @get:GET("/rss")
    val phAwareFeed: Call<Feed?>?

    @get:GET("/study/certificate.json")
    val publicKey: Single<PhPublicKey?>?

    @GET
    fun getAwsPublicKey(@Url awsUrl: String?): Single<String?>?

    @POST("/uploads")
    fun upload(@Body phUploadRequest: PhUploadRequest?): Observable<PhUploadResponse?>?

    @PUT
    fun uploadPresigned(@Url awsUrl: String?, @Body file: RequestBody?, @HeaderMap headers: Map<String?, String?>?): Observable<ResponseBody?>?

    @PUT("/uploads/{id}")
    fun uploadConfirmation(@Path("id") uploadID: Int): Observable<ResponseBody?>?

    @get:GET("/auth/UserProfile")
    val userProfile: Call<ProfileResponse?>?

    @GET("json")
    fun getGeoLocation(@QueryMap options: Map<String?, Any?>?): Call<GeoResponse?>?

    @GET("weather")
    fun getWeather(@QueryMap options: Map<String?, Any?>?): Call<WeatherResponse?>?

    @POST("/ratings")
    fun ratings(@Body ratingRequest: Rating?): Call<Any?>?

    @POST("/reports")
    fun postReports(@Body reportRequest: PostReportRequest?): Call<Any?>?

    @GET("/reports")
    fun getReports(@QueryMap options: Map<String?, String?>?): Call<ReportResponse?>?

    @PUT("/auth/users/device_info")
    fun updateFcm(@Body fcmPutRequest: FcmPutRequest?): Call<Any?>?

    @GET("/study/verify_token")
    fun validateInviteToken(@Query("user_token") userToken: String?): Call<ResponseBody?>?

    @GET("/study/token_verification_required")
    fun validateTokenOnOFF(): Single<InviteToggleResponse?>?

    @GET(" /study_documents/get_latest_doc")
    fun getStudyDocument(@Query("doc_type") studyType: String?): Single<DocumentResponse?>?

    @GET
    fun downloadFromAws(@Url fileUrl: String?): Call<ResponseBody?>?
}