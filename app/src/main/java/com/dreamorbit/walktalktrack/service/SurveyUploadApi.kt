package com.dreamorbit.walktalktrack.service

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.ApiServiceSettings
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.pojo.upload.PhUploadRequest
import com.dreamorbit.walktalktrack.pojo.upload.PhUploadResponse
import com.dreamorbit.walktalktrack.service.alarm.NotificationHelper.showNotification
import com.dreamorbit.walktalktrack.utilities.FileHelper
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.io.File
import java.util.*

/**
 * Created by mujasam.bn on 10/6/2017.
 */
class SurveyUploadApi {
    private var mAwsUploadURL: String? = null
    private var mPhUploadResponse: PhUploadResponse? = null
    private val TAG = "SurveyUploadApi"
    /**
     * in Authorization header for this api send refresh token instead of access token
     */
    fun uploadSurvey(phUploadRequest: PhUploadRequest, zipFile: File?) {
        val phUploadHeaders = createPhUploadHeader()
        val retrofit = getClient(phUploadHeaders)
        val service = retrofit!!.create(IApiRepo::class.java)
        //AWS Client without any Token or Authorization headers
        val awsUploadHeaders = createAwsUploadHeader(phUploadRequest)
        val retrofitAws = getClient(awsUploadHeaders)
        val serviceAws = retrofitAws!!.create(IApiRepo::class.java)
        try {
            service.upload(phUploadRequest) //.subscribeOn(Schedulers.io()) // "work" on io thread
//.observeOn(AndroidSchedulers.mainThread()) // "listen" on UIThread
                    ?.subscribe(object : Observer<PhUploadResponse?> {
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(phUploadResponse: PhUploadResponse) {
                            mPhUploadResponse = phUploadResponse
                            mAwsUploadURL = phUploadResponse.url
                        }

                        override fun onError(e: Throwable) {}
                        override fun onComplete() {
                            Log.e(TAG, "UPLOAD meta data 1")
                            val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
                            //RequestBody fileBody = RequestBody.create(MediaType.parse("application/zip"), zipFile);
                            val header: MutableMap<String?, String?> = HashMap()
                            header[PhAwareApplication().getMyAppContext().getString(R.string.content_type)] = phUploadRequest.contentType
                            header[PhAwareApplication().getMyAppContext().getString(R.string.content_length)] = phUploadRequest.contentLength.toString()
                            header[PhAwareApplication().getMyAppContext().getString(R.string.md5)] = phUploadRequest.contentMd5
                            header[PhAwareApplication().getMyAppContext().getString(R.string.bridge_session)] = accessToken
                            //MultipartBody.Part filePart = MultipartBody.Part.create(fileBody);
                            val requestBody = RequestBody.create(MediaType.parse("application/zip"), zipFile)
                            //MultipartBody.Part filePart = MultipartBody.Part.create(requestBody);
                            serviceAws.uploadPresigned(mAwsUploadURL, requestBody, header)
                                    ?.subscribe(object : Observer<ResponseBody?> {
                                        override fun onSubscribe(d: Disposable) {}
                                        override fun onNext(s: ResponseBody) {
                                            Log.e(TAG, "AWS UPLOAD SUCCESS 2 $s")
                                            //Call final api to send the confirmation to PhAware server
                                            service.uploadConfirmation(mPhUploadResponse!!.id)?.subscribe(object : Observer<ResponseBody?> {
                                                override fun onSubscribe(d: Disposable) {}
                                                override fun onNext(s: ResponseBody) {
                                                    Log.e(TAG, "UPLOAD Confirmation 3 $s")
                                                    FileHelper.deleteRecursive(zipFile)
                                                    showNotification()
                                                    sendRatingMessageToActivityScreen()
                                                }

                                                override fun onError(e: Throwable) {}
                                                override fun onComplete() {}
                                            })
                                        }

                                        override fun onError(e: Throwable) {
                                            Log.e("*uploadPresigned Error*", e.toString())
                                        }

                                        override fun onComplete() {}
                                    })
                        }
                    })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createPhUploadHeader(): Map<String?, String?> {
        val header: MutableMap<String?, String?> = HashMap()
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        header[PhAwareApplication().getMyAppContext().getString(R.string.content_type)] = "application/x-www-form-urlencoded"
        header[PhAwareApplication().getMyAppContext().getString(R.string.accept)] = "application/json"
        header[PhAwareApplication().getMyAppContext().getString(R.string.token)] = ApiServiceSettings().getStudyToken()
        header[PhAwareApplication().getMyAppContext().getString(R.string.authorization)] = accessToken
        return header
    }

    private fun createAwsUploadHeader(phUploadRequest: PhUploadRequest): Map<String?, String?> {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        /*header.put(PhAwareApplication.getMyAppContext().getString(R.string.content_type),phUploadRequest.getContentType());
        header.put(PhAwareApplication.getMyAppContext().getString(R.string.content_length),String.valueOf(phUploadRequest.getContentLength()));
        header.put(PhAwareApplication.getMyAppContext().getString(R.string.md5),phUploadRequest.getContentMd5());
        header.put(PhAwareApplication.getMyAppContext().getString(R.string.bridge_session),accessToken);*/return HashMap()
    }

    private fun sendRatingMessageToActivityScreen() {
        Log.d("SurveyUploadApi", "send Rating Message To ActivityScreen ")
        val intent = Intent(AppConstants.RATE_APP)
        LocalBroadcastManager.getInstance(PhAwareApplication().getMyAppContext()).sendBroadcast(intent)
    }
}