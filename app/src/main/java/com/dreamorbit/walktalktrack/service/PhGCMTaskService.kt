package com.dreamorbit.walktalktrack.service

import android.util.Log
import com.dreamorbit.walktalktrack.api.PhPublicKey
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository.Companion.instance
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.GcmTaskService
import com.google.android.gms.gcm.PeriodicTask
import com.google.android.gms.gcm.TaskParams
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by mujasam.bn on 10/10/2017.
 */
class PhGCMTaskService : GcmTaskService() {
    override fun onInitializeTasks() {}
    override fun onRunTask(taskParams: TaskParams): Int {
        Log.d(TAG, "onRunTask: " + taskParams.tag)
        val tag = taskParams.tag
        // Default result is success.
        val result = GcmNetworkManager.RESULT_SUCCESS
        // Choose method based on the tag.
        if (AppConstants.TASK_TAG_PERIODIC == tag) {
            doPeriodicTask()
        }
        // Return one of RESULT_SUCCESS, RESULT_FAILURE, or RESULT_RESCHEDULE
        return result
    }

    /**
     * Fetch the registered email from preference and generate sha-256.
     */
    private fun doPeriodicTask() {
        val email = instance!!.registrationEmailSha // Take value from table
        RefreshTokenApi().refreshToken(email)
        //api call to get Public key to encrypt the zip file (SurveyResponse.json)
        downloadCertificate()
    }

    fun downloadCertificate() {
        val publicKeyApi = PublicKeyApi()
        publicKeyApi.presendUrl?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(object : SingleObserver<PhPublicKey?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(phPublicKey: PhPublicKey) {
                        if (phPublicKey != null) {
                            Log.e("Response:", "" + phPublicKey)
                            publicKeyApi.getPublicKey(phPublicKey.url)?.subscribeOn(Schedulers.io())
                                    ?.observeOn(AndroidSchedulers.mainThread())
                                    ?.subscribe(object : SingleObserver<String?> {
                                        override fun onSubscribe(disposable: Disposable) {}
                                        override fun onSuccess(s: String) {}
                                        override fun onError(throwable: Throwable) {}
                                    })
                        }
                    }

                    override fun onError(e: Throwable) {}
                })
    }

    companion object {
        private const val TAG = "MyTaskService"
        private const val MINUTES = 30L
        private const val SECONDS = 60L
        fun startPeriodicTask() {
            Log.d("Utility", "startPeriodicTask")
            val mGcmNetworkManager = GcmNetworkManager.getInstance(PhAwareApplication().getMyAppContext())
            // [START start_periodic_task]
            val task = PeriodicTask.Builder()
                    .setService(PhGCMTaskService::class.java)
                    .setTag(AppConstants.TASK_TAG_PERIODIC)
                    .setPeriod(MINUTES * SECONDS)
                    .build()
            mGcmNetworkManager.schedule(task)
        }
    }
}