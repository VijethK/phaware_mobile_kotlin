package com.dreamorbit.walktalktrack.service

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.refreshToken.RefreshTokenReponse
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mujasam.bn on 10/6/2017.
 */
class RefreshTokenApi {
    /**
     * in Authorization header for this api send refresh token instead of access token
     */
    fun refreshToken(email: String?) {
        val refreshToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.REFRESH_TOKEN, "")
        val retrofit = getClient(refreshToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.refreshToken(email)
        call!!.enqueue(object : Callback<RefreshTokenReponse?> {
            override fun onResponse(call: Call<RefreshTokenReponse?>, response: Response<RefreshTokenReponse?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Token Refreshed:", "" + response.body()!!.accessToken)
                    SharedPrefSingleton.instance?.saveString(SharedPrefSingleton.ACCESS_TOKEN, response.body()!!.accessToken) //Store the refresh token in Preferences
                    Utility.uploadPendingData()
                } else if (response != null && response.code() == 401 || response.code() == 404) { //unauthorised || not found
                    Utility.signOut()
                    Utility.clearAlarms()
                }
            }

            override fun onFailure(call: Call<RefreshTokenReponse?>, t: Throwable) {
                Log.e("", "" + t.toString())
            }
        })
    }
}