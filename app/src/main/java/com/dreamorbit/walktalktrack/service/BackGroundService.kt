package com.dreamorbit.walktalktrack.service

import android.content.Context
import android.content.Intent
import android.support.v4.app.JobIntentService
import android.text.TextUtils
import android.util.Log
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.model.survey.QuestionList
import com.dreamorbit.walktalktrack.api.PhPublicKey
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.database.questions.QuestionsRepository
import com.dreamorbit.walktalktrack.database.registration.RegistartionRepository
import com.dreamorbit.walktalktrack.database.report.ReportRepository
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.upload.PhUploadRequest
import com.dreamorbit.walktalktrack.screens.bottombar.activities.SurveyQuestionsApi
import com.dreamorbit.walktalktrack.screens.bottombar.report.ReportApi
import com.dreamorbit.walktalktrack.utilities.FileHelper
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File

/**
 * Created by mujasam.bn on 10/27/2017.
 */
class BackGroundService : JobIntentService() {
    //Get survey questions
    private val surveyQuestionsAPI = SurveyQuestionsApi()
    private lateinit var mActivityIdArray: IntArray
    //api repo to upload the survey answer
    private val surveyUploadAPI = SurveyUploadApi()
    //api repo to upload summary report
    private val reportApi = ReportApi()

    override fun onCreate() {
        super.onCreate()
    }

    override fun onHandleWork(intent: Intent) {
        val action = Utility.BackgroundAction.valueOf(intent.getStringExtra(AppConstants.BACKGROUND_ACTION))
        var publicKey: String? = null
        if (ConnectivityReceiver.isConnected) {
            when (action) {
                Utility.BackgroundAction.upload_survey -> {
                    //Check if PublicKey already downloaded from server lse dont start upload unless PublicKey downloaded
                    publicKey = SharedPrefSingleton.instance?.publicKey
                    if (TextUtils.isEmpty(publicKey)) {
                        downloadCertificate()
                    } else {
                        uploadSurveyAnswers()
                    }
                }
                Utility.BackgroundAction.upload_walk_test -> {
                    //Check if PublicKey allready downloaded from server lse dont start upload unless PublicKey downloaded
                    publicKey = SharedPrefSingleton.instance?.publicKey
                    if (TextUtils.isEmpty(publicKey)) {
                        downloadCertificate()
                    } else {
                        uploadWalkTestData()
                    }
                }
                Utility.BackgroundAction.upload_all_pending -> {
                    //Check if PublicKey allready downloaded from server lse dont start upload unless PublicKey downloaded
                    publicKey = SharedPrefSingleton.instance?.publicKey
                    if (TextUtils.isEmpty(publicKey)) {
                        downloadCertificate()
                    } else {
                        uploadWalkTestData()
                        uploadSurveyAnswers()
                        uploadSummaryData()
                    }
                    //Check if PublicKey allready downloaded from server lse dont start upload unless PublicKey downloaded
                    publicKey = SharedPrefSingleton.instance?.publicKey
                    if (TextUtils.isEmpty(publicKey)) {
                        downloadCertificate()
                    } else {
                        uploadWearWalkTestData()
                    }
                }
                Utility.BackgroundAction.upload_wear_data -> {
                    publicKey = SharedPrefSingleton.instance?.publicKey
                    if (TextUtils.isEmpty(publicKey)) {
                        downloadCertificate()
                    } else {
                        uploadWearWalkTestData()
                    }
                }
                Utility.BackgroundAction.upload_summary_data -> uploadSummaryData()
                Utility.BackgroundAction.reset_survey -> {
                }
            }
        }
    }

    /**
     * Upload Survey data single Survey Answers json file
     */
    private fun uploadSurveyAnswers() {
        val directories = SdkUtils.getAllDirectories(this)
        for (dir in directories) {
            if (dir.isDirectory) {
                val file = dir.listFiles() //get all files inside this directory
                val zipPath = filesDir.absolutePath + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + file[0].parentFile.name + ".zip" //get the folder name
                if (FileHelper.zip(file[0].toString(), zipPath)) { // deleting the old folder once zip is successfully done
                    FileHelper.deleteRecursive(dir)
                    //Now process starts for encrypting the zip file
                    val zipEncPath = filesDir.absolutePath + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + file[0].parentFile.name + "_Encrypted.zip" //get the folder name
                    FileHelper.encrypLargeZip(zipPath, zipEncPath)
                    // deleting the old Zip once Encryption zip is successfully done
                    val zipFile = File(zipPath)
                    FileHelper.deleteRecursive(zipFile)
                    //Start uploading the survey response to server
                    uploadAPI(zipEncPath)
                    Log.d("ZIP UPLOAD", "Success")
                } else {
                    Log.d("ZIP", "Failed")
                }
            } else if (dir.isFile && dir.name.endsWith(".zip")) { //Start uploading the survey response to server
                uploadAPI(dir.toString())
                Log.d("ZIP UPLOAD", "Success")
            }
        }
    }

    /**
     * Upload 6 minute walk test data
     */
    private fun uploadWalkTestData() {
        val directories = SdkUtils.getAllDirectories(this)
        for (dir in directories) {
            if (dir.isDirectory) {
                val files = dir.listFiles() //get all files inside this directory
                val zipPath = filesDir.absolutePath + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + files[0].parentFile.name + ".zip" //get the folder name
                FileHelper.zipFiles(files, zipPath) //Zipping multiple files
                //deleting the old folder once zip is successfully done
                FileHelper.deleteRecursive(dir)
                //Now process starts for encrypting the zip file
                val zipEncPath = filesDir.absolutePath + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + files[0].parentFile.name + "_Encrypted.zip" //get the folder name
                FileHelper.encrypLargeZip(zipPath, zipEncPath)
                // deleting the old Zip once Encryption zip is successfully done
                val zipFile = File(zipPath)
                FileHelper.deleteRecursive(zipFile)
                //Start uploading the survey response to server
                uploadAPI(zipEncPath)
                //Log.d("ZIP UPLOAD", "Success");
            } else if (dir.isFile && dir.name.endsWith(".zip")) { //Start uploading the survey response to server
                uploadAPI(dir.toString())
                //Log.d("ZIP UPLOAD", "Success");
            }
        }
    }

    /**
     * Upload 6 minute walk test data
     */
    private fun uploadWearWalkTestData() {
        val directories = SdkUtils.getAllDirectories(this)
        for (dir in directories) {
            if (!dir.name.contains("_Encrypted") && !dir.name.contains("AppEventsLogger.persistedevents")) {
                Log.e("BackGroundService", "uploadWearWalkTestData() " + dir.name)
                //Start uploading the survey response to server
//Now process starts for encrypting the zip file
                val zipEncPath = filesDir.absolutePath + File.separator + SdkConstant.ROOT_APP_FOLDER_NAME + File.separator + dir.name + "_Encrypted.zip" //get the folder name
                FileHelper.encrypLargeZip(dir.absolutePath, zipEncPath)
                // deleting the old Zip once Encryption zip is successfully done
                FileHelper.deleteRecursive(dir)
                //Start uploading the survey response to server
                uploadAPI(zipEncPath)
                //Log.d("ZIP UPLOAD", "Success");
            } else if (dir.isFile && dir.name.contains("_Encrypted")) { //Start uploading the survey response to server
                uploadAPI(dir.toString())
                //Log.d("ZIP UPLOAD", "Success");
            }
        }
    }

    private fun uploadAPI(zipEncPath: String) { //append username to file upload
//get name from email address
        val email = RegistartionRepository.instance!!.registrationEmail
        val userName = email!!.substring(0, email.indexOf("@"))
        // during first time upload check if rating already given
// if count is 0, app is first time
// if count is -1 already given the rating so dont give ratinf
        val count = SharedPrefSingleton.instance!!.rateCount
        if (count == 0) //first time
            SharedPrefSingleton.instance!!.saveRateCount(1)
        val zipFile = File(zipEncPath)
        val fileLength = zipFile.length()
        val name = zipFile.name.split("_").toTypedArray()
        val md5 = Utility.getMD5(zipFile)
        val phUploadRequest = PhUploadRequest(name[0] + "_" + userName + "_" + name[2] + ".zip", fileLength, md5, "application/zip", null)
        surveyUploadAPI.uploadSurvey(phUploadRequest, zipFile)
    }

    /**
     * Upload api for summary report
     */
    private fun uploadSummaryData() {
        val email = SharedPrefSingleton.instance?.userEmail
        val reportList = ReportRepository.instance!!.getReport(email)
        for (report in reportList!!) {
            reportApi.uploadSummary(report)
        }
    }

    private fun getSurveyQuestions(intent: Intent) {
        mActivityIdArray = intent.getIntArrayExtra(AppConstants.ACTIVITY_ID_ARRAY)
        for (id in mActivityIdArray) { //surveyAPI.getSurveyQuestions(id);
        }
    }

    fun onSuccessResponse(questions: QuestionList?) {
        QuestionsRepository.instance!!.saveQuestions(questions)
    }

    fun downloadCertificate() {
        val publicKeyApi = PublicKeyApi()
        publicKeyApi.presendUrl?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe(object : SingleObserver<PhPublicKey?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(phPublicKey: PhPublicKey) {
                        if (phPublicKey != null) {
                            Log.e("Response:", "" + phPublicKey)
                            publicKeyApi.getPublicKey(phPublicKey.url)?.subscribeOn(Schedulers.io())
                                    ?.observeOn(AndroidSchedulers.mainThread())
                                    ?.subscribe(object : SingleObserver<String?> {
                                        override fun onSubscribe(disposable: Disposable) {}
                                        override fun onSuccess(s: String) {}
                                        override fun onError(throwable: Throwable) {}
                                    })
                        }
                    }

                    override fun onError(e: Throwable) {}
                })
    }

    companion object {
        const val JOB_ID = 1000
        @JvmStatic
        fun enqueueBackgroundWork(context: Context, work: Intent) {  //Removed ? for Context and Intent
            enqueueWork(context, BackGroundService::class.java, JOB_ID, work)
        }
    }
}