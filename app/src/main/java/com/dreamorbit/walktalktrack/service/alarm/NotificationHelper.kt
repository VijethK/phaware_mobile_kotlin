package com.dreamorbit.walktalktrack.service.alarm

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_HOUR
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_MIN
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.service.alarm.AlarmReceiver
import java.util.*

/**
 * Created by mujasam.bn on 11/15/2017.
 */
object NotificationHelper {
    const val ALARM_TYPE_RTC_RESET = 100
    const val ALARM_TYPE_RTC_RATING = 200
    const val ALARM_RECEIVER_TYPE = "ALARM_RECEIVER"
    private const val NOTIFICATION_CHANNEL_ID = "my_notification_channel"
    private const val ACTION_ALARM_RECEIVER = "action_alarm_receiver"
    var ALARM_TYPE_ELAPSED = 101
    private const val TAG = "NotificationHelper"
    private var alarmManagerReset: AlarmManager? = null
    private var alarmManagerRating: AlarmManager? = null
    private var alarmIntentReset: PendingIntent? = null
    private var alarmIntentRating: PendingIntent? = null
    /**
     * This is the real time /wall clock time will trigger every day morning 7am
     *
     * @param context
     */
    fun scheduleResetRTCNotification(context: Context?, repeating: Boolean) { //checking if alarm is working with pendingIntent
        var repeating = repeating
        val intentCheck = Intent(PhAwareApplication().getMyAppContext(), AlarmReceiver::class.java) //the same as up
        intentCheck.action = ACTION_ALARM_RECEIVER //This is the trick
        val isActive = PendingIntent.getBroadcast(PhAwareApplication().getMyAppContext(), ALARM_TYPE_RTC_RESET, intentCheck, PendingIntent.FLAG_NO_CREATE) != null //just changed the flag
        if (isActive && !repeating) {
            Log.e(TAG, "Alarm is already active")
            return
        }
        //get calendar instance to be able to select what time notification should be scheduled
//Setting time of the day (7am here) when notification will be sent every day (default)
//Set the alarm to start at approximately 7:00 a.m.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = REPEAT_HOUR
        calendar[Calendar.MINUTE] = REPEAT_MIN
        //if time has already elapsed then repeating flag set to true
//So that alarm will not trigger immediately
//Set the time to next day
        val currentTime = System.currentTimeMillis()
        if (currentTime > calendar.timeInMillis) {
            repeating = true
        }
        //If calling this method to repeat the alarm this flag need to be true
//if daily add next day
        if (repeating) {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
        val intent = Intent(PhAwareApplication().getMyAppContext(), AlarmReceiver::class.java)
        intent.action = ACTION_ALARM_RECEIVER //my custom string action name
        intent.putExtra(ALARM_RECEIVER_TYPE, ALARM_TYPE_RTC_RESET)
        //Setting alarm pending intent
        alarmIntentReset = PendingIntent.getBroadcast(PhAwareApplication().getMyAppContext(), ALARM_TYPE_RTC_RESET,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        //getting instance of AlarmManager service
        alarmManagerReset = PhAwareApplication().getMyAppContext()!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        //Setting alarm to wake up device every day for clock time.
//AlarmManager.RTC_WAKEUP is responsible to wake up device for sure, which may not be good practice all the time.
// Use this when you know what you're doing.
//Use RTC when you don't need to wake up device, but want to deliver the notification whenever device is woke-up
//We'll be using RTC.WAKEUP for demo purpose only
// Hopefully your alarm will have a lower frequency than this!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // Wakes up the device in Doze Mode
            alarmManagerReset!!.setExactAndAllowWhileIdle(AlarmManager.RTC, calendar.timeInMillis, alarmIntentReset)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // Wakes up the device in Idle Mode
            alarmManagerReset!!.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntentReset)
        } else { // Old APIs
            alarmManagerReset!![AlarmManager.RTC, calendar.timeInMillis] = alarmIntentReset
        }
        Log.e("AlarmSet ", "Done")
    }

    /**
     * This is the real time /wall clock time triggers every week morning 8am
     *
     * @param context
     */
    fun scheduleRatingRTCNotification(context: Context?, hour: Int, min: Int) { //get calendar instance to be able to select what time notification should be scheduled
//Setting time of the day (7am here) when notification will be sent every day (default)
//Set the alarm to start at approximately 7:00 a.m.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR_OF_DAY] = hour + 1
        calendar[Calendar.MINUTE] = min
        val intent = Intent(PhAwareApplication().getMyAppContext(), AlarmReceiver::class.java)
        intent.putExtra(ALARM_RECEIVER_TYPE, ALARM_TYPE_RTC_RATING)
        //if notifcation time is less than equal to system time its true and reciever will work..but if not then nothing will happen...
//if (t <= calendar.getTimeInMillis()) {
//boolean isAlarmNull = (PendingIntent.getBroadcast(PhAwareApplication.getMyAppContext(), ALARM_TYPE_RTC_RESET, intent, PendingIntent.FLAG_UPDATE_CURRENT) == null);//just changed the flag
//Setting intent to class where Alarm broadcast message will be handled
//Setting alarm pending intent
        alarmIntentRating = PendingIntent.getBroadcast(PhAwareApplication().getMyAppContext(), ALARM_TYPE_RTC_RATING,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        //getting instance of AlarmManager service
        alarmManagerRating = PhAwareApplication().getMyAppContext()!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        //Setting alarm to wake up device every day for clock time.
//AlarmManager.RTC_WAKEUP is responsible to wake up device for sure, which may not be good practice all the time.
// Use this when you know what you're doing.
//Use RTC when you don't need to wake up device, but want to deliver the notification whenever device is woke-up
//We'll be using RTC.WAKEUP for demo purpose only
        alarmManagerRating!!.setRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY * 7, alarmIntentRating)
        Log.e("AlarmSet ", "Done")
    }

    @JvmStatic
    fun cancelRatingRTCNotification() {
        if (alarmManagerRating != null) {
            alarmManagerRating!!.cancel(alarmIntentRating)
        }
    }

    fun cancelResetRTCNotification() {
        if (alarmManagerReset != null) {
            alarmManagerReset!!.cancel(alarmIntentReset)
        }
    }

    fun getNotificationManager(context: Context): NotificationManager {
        return context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    /**
     * Enable boot receiver to persist alarms set for notifications across device reboots
     */
    fun enableBootReceiverReset(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.packageManager
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)
    }

    /**
     * Disable boot receiver when user cancels/opt-out from notifications
     */
    fun disableBootReceiver(context: Context) {
        val receiver = ComponentName(context, AlarmBootReceiver::class.java)
        val pm = context.packageManager
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP)
    }

    @JvmStatic
    fun showNotification() { //Intent to invoke app when click on notification.
//In this sample, we want to start/launch this sample app when user clicks on notification
        val intentToRepeat = Intent(PhAwareApplication().getMyAppContext(), BottomBarActivity::class.java)
        //set flag to restart/relaunch the app
        intentToRepeat.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        //Pending intent to handle launch of Activity in intent above
        val pendingIntent = PendingIntent.getActivity(PhAwareApplication().getMyAppContext(), ALARM_TYPE_RTC_RESET, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT)
        //Build notification
        val repeatedNotification = buildLocalNotification(PhAwareApplication().getMyAppContext(), pendingIntent).build()
        //Send local notification
        getNotificationManager(PhAwareApplication().getMyAppContext()).notify(ALARM_TYPE_RTC_RESET, repeatedNotification)
    }

    private fun buildLocalNotification(context: Context, pendingIntent: PendingIntent): NotificationCompat.Builder {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT)
            // Configure the notification channel.
            notificationChannel.description = "Channel description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setContentIntent(pendingIntent) //todo need to handle on tap of notifcaition launch last state.
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(context.getString(R.string.upload_title))
                .setContentText(context.getString(R.string.upload_message))
                .setAutoCancel(true)
    }
}