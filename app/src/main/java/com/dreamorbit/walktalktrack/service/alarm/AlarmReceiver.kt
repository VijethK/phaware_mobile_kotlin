package com.dreamorbit.walktalktrack.service.alarm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_HOUR
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_MIN
import com.dreamorbit.walktalktrack.database.activities.ActivitiesRepository
import com.dreamorbit.walktalktrack.database.questions.QuestionsRepository
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import java.util.*

/**
 * Created by mujasam.bn on 11/15/2017.
 */
class AlarmReceiver : BroadcastReceiver() {
    private val TAG = "AlarmReceiver"
    override fun onReceive(context: Context, intent: Intent) {
        val requestCode = intent.getIntExtra(NotificationHelper.ALARM_RECEIVER_TYPE, 0)
        if (intent != null && requestCode == NotificationHelper.ALARM_TYPE_RTC_RESET) {
            QuestionsRepository.instance!!.deleteQuestions()
            ActivitiesRepository.instance!!.deleteActivities()
            Log.d(TAG, "Deleted: " + true)
            //Intent to invoke app when click on notification.
//In this sample, we want to start/launch this sample app when user clicks on notification
            val intentToRepeat = Intent(context, BottomBarActivity::class.java)
            //set flag to restart/relaunch the app
            intentToRepeat.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            //Pending intent to handle launch of Activity in intent above
            val pendingIntent = PendingIntent.getActivity(context, NotificationHelper.ALARM_TYPE_RTC_RESET, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT)
            //Build notification
            val repeatedNotification = buildLocalNotification(context, pendingIntent, "Survey has been reset").build()
            //Send local notification
            NotificationHelper.getNotificationManager(context).notify(NotificationHelper.ALARM_TYPE_RTC_RESET, repeatedNotification)
            //Reset the alarm
            NotificationHelper.scheduleResetRTCNotification(context, true)
            //send inline observer to activity screen to update the row status
            sendResetStatusToActivityScreen(context)
        } else { //Rating
            val t = System.currentTimeMillis()
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            calendar[Calendar.HOUR_OF_DAY] = REPEAT_HOUR + 1
            calendar[Calendar.MINUTE] = REPEAT_MIN
            if (t <= calendar.timeInMillis) { // Rating notification
                val intentToRepeat = Intent(context, BottomBarActivity::class.java)
                //set flag to restart/relaunch the app
                intentToRepeat.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
                //Pending intent to handle launch of Activity in intent above
                val pendingIntent = PendingIntent.getActivity(context, NotificationHelper.ALARM_TYPE_RTC_RATING, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT)
                //Build notification
                val repeatedNotification = buildLocalNotification(context, pendingIntent, "Please rate the application.").build()
                //Send local notification
                NotificationHelper.getNotificationManager(context).notify(NotificationHelper.ALARM_TYPE_RTC_RATING, repeatedNotification)
            }
        }
    }

    fun buildLocalNotification(context: Context, pendingIntent: PendingIntent?, message: String?): NotificationCompat.Builder {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT)
            // Configure the notification channel.
            notificationChannel.description = "Channel description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("PhAware Notification")
                .setContentText(message)
                .setAutoCancel(true)
    }

    /**
     * Send an Intent with an action named "custom-event-name". The Intent sent should
     * be received by the ReceiverActivity.
     */
    private fun sendResetStatusToActivityScreen(context: Context) {
        Log.d(TAG, "Send Reset Status ")
        val intent = Intent(AppConstants.ACTIVITY_HAS_RESET)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "my_notification_channel"
    }
}