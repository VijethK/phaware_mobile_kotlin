package com.dreamorbit.walktalktrack.service.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_HOUR
import com.dreamorbit.walktalktrack.application.AppConstants.REPEAT_MIN
import com.dreamorbit.walktalktrack.firebase.MyFirebaseMessagingService
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton

/**
 * Created by mujasam.bn on 11/15/2017.
 */
class AlarmBootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            Log.e("AlarmBootReceiver ", "Received")
            if (!TextUtils.isEmpty(SharedPrefSingleton.instance?.userEmail)) { // Refresh the Survey activity everyday 7 a.m
//NotificationHelper.scheduleResetRTCNotification(context, false);
// Rating screen weekly 8 a.m
                NotificationHelper.scheduleRatingRTCNotification(context, REPEAT_HOUR, REPEAT_MIN)
                // Scheduler 6 minute walk test
                MyFirebaseMessagingService().initSchedulerAlarm(-1)
                //Enable Boot receiver
                NotificationHelper.enableBootReceiverReset(context)
            } else {
                Log.e("AlarmBootReceiver ", "Logout state")
            }
        }
    }
}