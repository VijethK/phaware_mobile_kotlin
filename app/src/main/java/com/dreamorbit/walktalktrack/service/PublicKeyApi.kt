package com.dreamorbit.walktalktrack.service

import com.dreamorbit.walktalktrack.api.ApiClient.awsCertificateClient
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.api.PhPublicKey
import io.reactivex.Single

/**
 * Created by mujasam.bn on 12/6/2019.
 */
class PublicKeyApi {
    val presendUrl: Single<PhPublicKey?>?
        get() {
            val retrofit = getClient("")
            val service = retrofit!!.create(IApiRepo::class.java)
            return service.publicKey
        }

    fun getPublicKey(awsURL: String?): Single<String?>? { //AWS Client without any Token or Authorization headers
        val retrofitAws = awsCertificateClient
        val serviceAws = retrofitAws!!.create(IApiRepo::class.java)
        return serviceAws.getAwsPublicKey(awsURL)
    }
}