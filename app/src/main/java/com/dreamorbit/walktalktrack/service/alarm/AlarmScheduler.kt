package com.dreamorbit.walktalktrack.service.alarm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.firebase.MyFirebaseMessagingService
import com.dreamorbit.walktalktrack.screens.bottombar.BottomBarActivity
import com.dreamorbit.walktalktrack.utilities.Utility

class AlarmScheduler : BroadcastReceiver() {
    private val TAG = AlarmScheduler::class.java.name
    override fun onReceive(context: Context, intent: Intent) {
        val surveyID = intent.getIntExtra(AppConstants.SURVEY_ID, -1)
        val surveyName = intent.getStringExtra(AppConstants.SURVEY_NAME)
        Log.e(TAG, "Scheduler Triggered")
        //Enable the test taken flag. User is allowed to take the test now.
        SchedulerRepository.instance?.saveSchedulerStatus(SchedulerModel.TEST_PENDING, SdkUtils.getCurrentDateTime(), surveyID)
        //Intent to invoke app when click on notification.
//In this sample, we want to start/launch this sample app when user clicks on notification
        val intentToRepeat = Intent(context, BottomBarActivity::class.java)
        //set flag to restart/relaunch the app
        intentToRepeat.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
        //Pending intent to handle launch of Activity in intent above
        val pendingIntent = PendingIntent.getActivity(context, NotificationHelper.ALARM_TYPE_RTC_RESET, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT)
        //Build notification
        val repeatedNotification = buildLocalNotification(context, pendingIntent, context.getString(R.string.please_take_test) + " " + surveyName).build()
        //Send local notification with unique ID to show each notification separately
        NotificationHelper.getNotificationManager(context).notify(surveyID, repeatedNotification)
        // Scheduler 6 minute walk test
        MyFirebaseMessagingService().initSchedulerAlarm(surveyID) // Set for next day or week
        //send notification to Wearable to enable or disable test taking job
        Utility.sendTestSchedulerToWear()
        //send inline observer to activity screen to update the row status
        sendSchedulerStatusToActivityScreen(context)
    }

    /**
     * Send an Intent with an action named "custom-event-name". The Intent sent should
     * be received by the ReceiverActivity.
     */
    private fun sendSchedulerStatusToActivityScreen(context: Context) {
        Log.d(TAG, "Send Scheduler Status ")
        val intent = Intent(AppConstants.ACTIVITY_HAS_RESET)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "scheduler_notification_channel"
        fun buildLocalNotification(context: Context, pendingIntent: PendingIntent?, message: String?): NotificationCompat.Builder {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_DEFAULT)
                // Configure the notification channel.
                notificationChannel.description = "Channel description"
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.RED
                notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
                notificationChannel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChannel)
            }
            return NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(message) //.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setAutoCancel(true)
        }
    }
}