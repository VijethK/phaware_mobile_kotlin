package com.dreamorbit.walktalktrack.firebase

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerModel
import com.dreamorbit.walktalktrack.database.scheduler.SchedulerRepository
import com.dreamorbit.walktalktrack.service.alarm.AlarmScheduler
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.sql.Time
import java.util.*

/**
 * Created by mujasam.bn on 9/13/2017.
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) { // TODO(developer): Handle FCM messages here.
// Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + remoteMessage.from)
        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            saveSchedulerMessage(remoteMessage)
            initSchedulerAlarm(-1) //initially repeating will be false
        }
        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body)
        }
    }

    /**
     * Parse the scheduler message received from FCM
     *
     * @param remoteMessage
     */
    private fun saveSchedulerMessage(remoteMessage: RemoteMessage) {
        var fcmSchedulerData: List<SchedulerModel?>? = null
        if (remoteMessage.data != null) {
            val data = JSONObject(remoteMessage.data)
            Log.e(TAG, "Message data payload: $data")
            val gson = Gson()
            try {
                val token: TypeToken<List<SchedulerModel>> = object : TypeToken<List<SchedulerModel>>() {} // TypeToken<List<SchedulerModel?>?>() removed ?
                fcmSchedulerData = gson.fromJson<List<SchedulerModel?>>(data.getString("scheduler_details"), token.type)
                Log.e(TAG, "Message data List: $fcmSchedulerData")
                SchedulerRepository.instance?.saveScheduler(fcmSchedulerData)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Set Scheduler alarm for 6 minute walk test and surveys
     * static method because need to call this method from Boot receiver class and onReceive method of
     *
     * @param surveyID
     * @see AlarmScheduler
     */
    fun initSchedulerAlarm(surveyID: Int) { //check if this method called from AlarmScheduler receiver to repeat
//the alarm for next day/week for a particular activity
        if (surveyID > 0) { //not -1 then repeat for next day
            val model = SchedulerRepository.instance?.getScheduler(surveyID)
            val calendar = setupCalendar(model!!)
            setRepeatAlarm(model, calendar)
        } else { //if -1 coming from FCM or first time, set alarm for all activities.
            val modelList = SchedulerRepository.instance?.scheduler
            for (i in modelList!!.indices) {
                var repeating = false
                val model = modelList!![i]
                val calendar = setupCalendar(model)
                //if time has already elapsed then repeating flag set to true
//So that alarm will not trigger immediately
//Set the alarm for next day
                val currentTime = System.currentTimeMillis()
                if (currentTime > calendar.timeInMillis) {
                    repeating = true
                }
                if (repeating) {
                    setRepeatAlarm(model, calendar)
                } else {
                    setAlarm(model, calendar)
                }
            }
        }
    }

    private fun setupCalendar(model: SchedulerModel): Calendar { // Get today's date and time.
        val cal = Calendar.getInstance()
        val time = model.scheduledAt?.split(" ")?.toTypedArray()
        cal.time = Time.valueOf(time!![0] + ":00") //append seconds
        val AM_PM = if (time!![1].equals("AM", ignoreCase = true)) 0 else 1 //if AM 0, PM 1
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        calendar[Calendar.HOUR] = cal[Calendar.HOUR] //12 hour format
        calendar[Calendar.MINUTE] = cal[Calendar.MINUTE]
        calendar[Calendar.AM_PM] = AM_PM
        //set daily or weekly in alarm calendar
        if (model.scheduleType.equals("weekly", ignoreCase = true)) {
            calendar[Calendar.DAY_OF_WEEK] = getDayOfWeek(model.day!!) // On which day, every week need to trigger the alarm
        } else { //if daily no need to set the day
        }
        return calendar
    }

    /**
     * Depend on the survey ID reset the alarm. If alaram repeat then repeat only the triggered alarm
     * Not all the surveys.
     *
     * @param model
     * @param calendar
     */
    private fun setRepeatAlarm(model: SchedulerModel, calendar: Calendar) {
        setAlarm(model, setNextDayOrWeek(model, calendar))
    }

    /**
     * If repeat alarm then set for next dy or week depend on the current trigger
     *
     * @param model
     * @param calendar
     * @return
     */
    private fun setNextDayOrWeek(model: SchedulerModel, calendar: Calendar): Calendar { //If calling this method to repeat the alarm this flag need to be true
//if daily add next day
//If weekly add 7 days from today's date
        if (model.scheduleType.equals("daily", ignoreCase = true)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
        } else if (model.scheduleType.equals("weekly", ignoreCase = true)) {
            calendar.add(Calendar.DAY_OF_MONTH, 7)
        }
        return calendar
    }

    /**
     * Set the alarm for the survey and 6MWT
     *
     * @param model
     * @param calendar
     */
    private fun setAlarm(model: SchedulerModel, calendar: Calendar) { //Get Alarm manager instance
        //Kotlin error
       // alarmMgr = PhAwareApplication.context.getSystemService(Context.ALARM_SERVICE)
        val intent = Intent(PhAwareApplication.context, AlarmScheduler::class.java)
        intent.putExtra(AppConstants.TEST_SEQUENCE, model.scheduleType)
        intent.putExtra(AppConstants.SURVEY_ID, model.id)
        intent.putExtra(AppConstants.SURVEY_NAME, model.name)
        alarmIntent = PendingIntent.getBroadcast(PhAwareApplication.context, model.id!!, intent, PendingIntent.FLAG_ONE_SHOT)
        Log.e("Alarm For ", model.name + " " + calendar.time)
        // Hopefully your alarm will have a lower frequency than this!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // Wakes up the device in Doze Mode
            alarmMgr!!.setExactAndAllowWhileIdle(AlarmManager.RTC, calendar.timeInMillis, alarmIntent)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { // Wakes up the device in Idle Mode
            alarmMgr!!.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, alarmIntent)
        } else { // Old APIs
            alarmMgr!![AlarmManager.RTC, calendar.timeInMillis] = alarmIntent
        }
    }

    /**
     * Get calendar day to set weekly alarm
     *
     * @param dayPosition
     * @return
     */
    private fun getDayOfWeek(dayPosition: String): Int {
        return when (dayPosition) {
            "Sunday" -> Calendar.SUNDAY
            "Monday" -> Calendar.MONDAY
            "Tuesday" -> Calendar.TUESDAY
            "Wednesday" -> Calendar.WEDNESDAY
            "Thursday" -> Calendar.THURSDAY
            "Friday" -> Calendar.FRIDAY
            "Saturday" -> Calendar.SATURDAY
            else -> Calendar.TUESDAY
        }
    }

    companion object {
        private val TAG = MyFirebaseMessagingService::class.java.simpleName
        // Set the alarm to start at as per notified time
        private var alarmMgr: AlarmManager? = null
        private var alarmIntent: PendingIntent? = null
        /**
         * Cancel the alarm on logout
         */
        @JvmStatic
        fun cancelSchedulerRTCNotification() {
            if (alarmMgr != null) {
                alarmMgr!!.cancel(alarmIntent)
            }
        }
    }
}