package com.dreamorbit.walktalktrack.firebase

import android.util.Log
import com.dreamorbit.walktalktrack.api.ApiClient.getClient
import com.dreamorbit.walktalktrack.api.IApiRepo
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse
import com.dreamorbit.walktalktrack.pojo.fcm.FcmPutRequest
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Service access layer to call Create Assent api
 * Created by mujasam.bn on 10/6/2017.
 */
class FirebasePutApi {
    fun updateFcmKey(fcmPutRequest: FcmPutRequest?) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "")
        val retrofit = getClient(accessToken)
        val service = retrofit!!.create(IApiRepo::class.java)
        val call = service.updateFcm(fcmPutRequest)
        call!!.enqueue(object : Callback<Any?> {//? added for kotlin error Type mismatch
            override fun onResponse(call: Call<Any?>, response: Response<Any?>) {
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("FirebasePutApi", "FCM Put Response: " + response.body())
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse = gson.fromJson(response.errorBody()!!.string(), AssentCreatedResponse::class.java)
                    } catch (e: Exception) {
                        val errorResponse = AssentCreatedResponse()
                        errorResponse.errorMessage = "Please try again."
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<Any?>, t: Throwable) {
                Log.e("FirebasePutApi", "" + t.toString())
            }
        })
    }
}