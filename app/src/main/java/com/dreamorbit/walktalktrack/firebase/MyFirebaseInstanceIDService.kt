package com.dreamorbit.walktalktrack.firebase

import android.text.TextUtils
import android.util.Log
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.pojo.fcm.FcmPutRequest
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

/**
 * Created by mujasam.bn on 9/13/2017.
 */
class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() { // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.e(TAG, "FCM token: $refreshedToken")
        SharedPrefSingleton.instance?.saveFcmToken(refreshedToken) //? added
        // If you want to send messages to this application instance or
// manage this apps subscriptions on the server side, send the
// Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken)
    }

    private fun sendRegistrationToServer(refreshedToken: String?) {
        val accessToken = SharedPrefSingleton.instance?.getString(SharedPrefSingleton.ACCESS_TOKEN, "") //? added
        Log.e(TAG, "Access token: $accessToken")
        if (!TextUtils.isEmpty(accessToken)) {
            val request = FcmPutRequest()
            request.device_token = refreshedToken
            request.device_type = AppConstants.DEVICE_TYPE
            request.email = SharedPrefSingleton.instance?.userEmail //? added
            val updateApi = FirebasePutApi()
            updateApi.updateFcmKey(request)
        }
    }

    companion object {
        private val TAG = MyFirebaseInstanceIDService::class.java.simpleName
    }
}