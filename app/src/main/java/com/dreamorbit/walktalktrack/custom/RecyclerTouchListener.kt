package com.dreamorbit.walktalktrack.custom

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.OnItemTouchListener
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import com.dreamorbit.walktalktrack.screens.bottombar.activities.IRecyclerItemClickListener

/**
 * Created by Mujas on 22-10-2017.
 */
/**
 * RecyclerView: Implementing single item click and long press (Part-II)
 *
 *
 * - creating an innerclass implementing RevyvlerView.OnItemTouchListener
 * - Pass clickListener interface as parameter
 */
class RecyclerTouchListener(context: Context?, recycleView: RecyclerView, private val clicklistener: IRecyclerItemClickListener?) : OnItemTouchListener {
    private val gestureDetector: GestureDetector
    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        val child = rv.findChildViewUnder(e.x, e.y)
        if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
            clicklistener.onClick(child, rv.getChildAdapterPosition(child), (child.tag as Int))
        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}
    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

    init {
        gestureDetector = GestureDetector(context, object : SimpleOnGestureListener() {
            override fun onSingleTapUp(e: MotionEvent): Boolean {
                return true
            }

            override fun onLongPress(e: MotionEvent) {
                val child = recycleView.findChildViewUnder(e.x, e.y)
                if (child != null && clicklistener != null) {
                    clicklistener.onLongClick(child, recycleView.getChildAdapterPosition(child))
                }
            }
        })
    }
}