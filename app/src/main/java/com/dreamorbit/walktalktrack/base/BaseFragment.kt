package com.dreamorbit.walktalktrack.base

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager.BadTokenException
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.screens.bottombar.profile.ProfileActivity

/**
 * Created by mujasam.bn on 6/29/2016.
 */
open class BaseFragment : Fragment() {
    var ploader: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        return null
    }

    fun loadProfileActivity() {
        startActivity(Intent(activity, ProfileActivity::class.java))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    override fun onDetach() {
        super.onDetach()
    }

    fun showLoader(message: String?) {
        activity!!.runOnUiThread(Runnable {
            if (ploader != null || activity!!.isFinishing) {
                return@Runnable
            }
            ploader = ProgressDialog(activity, R.style.MyProgressDialog)
            ploader!!.window.setBackgroundDrawable(ColorDrawable(0))
            ploader!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            ploader!!.setCancelable(false)
            ploader!!.isIndeterminate = true
            ploader!!.setMessage(message)
            try {
                ploader!!.show()
            } catch (e: BadTokenException) {
                ploader = null
            } catch (e: Exception) {
                ploader = null
            }
        })
    }

    fun hideLoader() {
        activity!!.runOnUiThread {
            if (ploader != null && ploader!!.isShowing) {
                ploader!!.dismiss()
            }
            ploader = null
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BaseFragment.
         */
// TODO: Rename and change types and number of parameters
        fun newInstance(param1: String?, param2: String?): BaseFragment {
            return BaseFragment()
        }
    }
}