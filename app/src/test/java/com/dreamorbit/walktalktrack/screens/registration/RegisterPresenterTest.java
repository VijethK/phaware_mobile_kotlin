package com.dreamorbit.walktalktrack.screens.registration;

import com.dreamorbit.walktalktrack.pojo.register.RegisterRequest;
import com.dreamorbit.walktalktrack.pojo.register.RegisterResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by nareshkumar.reddy on 9/26/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RegisterPresenterTest {

    @Mock
    RegisterViewContract registerView;
    @Mock
    RegisterApi registerApi;
    private RegisterPresenter registerPresenter;

    @Before
    public void setUp() {
        registerPresenter = new RegisterPresenter(registerView, registerApi);
    }

    @Test
    public void isRegisterwebServiceCalling() {
        RegisterRequest registerRequest = new RegisterRequest();

        registerPresenter.registerService(registerRequest);

        Mockito.verify(registerApi).register(registerRequest, registerPresenter);
    }

    @Test
    public void userIsRegisteredSuccessfully() {
        RegisterResponse registerResponse = new RegisterResponse("segsdg", 250, "d", "sdg", "fdg", null, "");

        registerPresenter.registerResult(registerResponse);

        Mockito.verify(registerView).onResultReceived(registerResponse);
    }

    @Test
    public void userErrorResponse() {
        RegisterResponse registerResponse = new RegisterResponse(null, null, null, null, null, null, "Boom");

        registerPresenter.registerResult(registerResponse);

        Mockito.verify(registerView).onErrorResponse(registerResponse);
    }


    @Test
    public void emailValidation() {
        String userName = "cfgjfg";
        String password = "Ghikjl+987#";

        registerPresenter.validation(false, userName, password);

        Mockito.verify(registerView).setValidationErrors("email");
    }

    @Test
    public void userNameValidation() {
        String userName = "";
        String password = "Ghikjl+987#";

        registerPresenter.validation(true, userName, password);

        Mockito.verify(registerView).setValidationErrors("userName");
    }

    @Test
    public void passwordValidation() {
        String userName = "cfgjfg";
        String password = "";

        registerPresenter.validation(true, userName, password);

        Mockito.verify(registerView).setValidationErrors("password");
    }

    @Test
    public void successfuldValidation() {
        String userName = "cfgjfg";
        String password = "Naresh+987#";

        registerPresenter.validation(true, userName, password);

        Mockito.verify(registerView).registerWebRequest();
    }

}