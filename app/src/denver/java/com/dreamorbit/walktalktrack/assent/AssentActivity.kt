package com.dreamorbit.walktalktrack.assent

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ViewTaskActivity
import com.dreamorbit.researchaware.utilities.AESCrypt
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.flow.FlowRepository.Companion.instance
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreateRequest
import com.dreamorbit.walktalktrack.pojo.assent.AssentCreatedResponse
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoActivity
import com.dreamorbit.walktalktrack.screens.assent.AssentPresenter
import com.dreamorbit.walktalktrack.screens.assent.AssentViewContract
import com.dreamorbit.walktalktrack.screens.assent.CreateAssentApi
import com.dreamorbit.walktalktrack.screens.registration.DocumentDownloadApi
import com.dreamorbit.walktalktrack.utilities.FileHelper.deleteRecursive
import com.dreamorbit.walktalktrack.utilities.FileHelper.encrypLargeZip
import com.dreamorbit.walktalktrack.utilities.RSAEncryption.Companion.encryptText
import com.dreamorbit.walktalktrack.utilities.Utility.convertHtmlPdf
import com.dreamorbit.walktalktrack.utilities.Utility.dismissProgressBar
import com.dreamorbit.walktalktrack.utilities.Utility.encodeFileToBase64Binary
import com.dreamorbit.walktalktrack.utilities.Utility.showProgressBar
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.UnsupportedEncodingException
import java.security.GeneralSecurityException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import javax.crypto.BadPaddingException
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException

/*Consent Two Screen*/
class AssentActivity : BaseActivity(), AssentViewContract {
    //View components
    private var mTvTitle: TextView? = null
    private var btnStartConsent: Button? = null
    private var mAssentPresenter: AssentPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consent)
        initToolbar()
        initViews()
        downloadDocuments()
    }

    /**
     * Initialize the mToolbar components
     * dynamic left margin to mToolbar title. Reusing the existing mToolbar
     */
    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        mTvTitle = toolbar.findViewById(R.id.title)
        val params = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        params.setMargins(50, 0, 0, 0)
        mTvTitle?.setLayoutParams(params)
        mTvTitle?.setText(R.string.consent_two)
    }

    /**
     * Initilize the layout views
     */
    private fun initViews() {
        mAssentPresenter = AssentPresenter(this, CreateAssentApi(), DocumentDownloadApi())
        btnStartConsent = findViewById(R.id.btnStartConsent)
        btnStartConsent?.setOnClickListener(View.OnClickListener {
            val directory = File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
            if (directory.exists() && directory.listFiles().size > 0) {
                navigateToAssent()
            } else {
                showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.please_download_documents), Snackbar.LENGTH_LONG)
                downloadDocuments()
            }
        })
    }

    /**
     * Show consent related message in dialog
     *
     * @param context
     */
    private fun consentAlert(context: Context) {
        val builder: AlertDialog.Builder
        builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.consent_review_parent)
                .setPositiveButton(android.R.string.ok) { dialog, which -> navigateToAssent() }
                .show()
    }

    private fun downloadDocuments() {
        if (ConnectivityReceiver.isConnected) {
            showProgressBar(this, getString(R.string.downloading_assent_documents))
            mAssentPresenter!!.downloadDocument()
        } else {
            showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    /**
     * Send the assent json data to ResearchAware sdk to start ConsentAssent flow
     */
    private fun navigateToAssent() {
        val assentJson = loadAssentJSONFromAsset()
        //String assentJson = loadConsentJSONFromAsset();
        val intent = Intent(this, ViewTaskActivity::class.java)
        intent.putExtra(SdkConstant.CONSENT_ASSENT_DATA, assentJson)
        startActivityForResult(intent, REQUEST_ASSENT)
    }

    /**
     * Get the result from assent flow. if cancel isAssentFlowDone will be false else it will be true
     * Call api to post assent data to server to create new assent.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val isAssentFlowDone = data.getBooleanExtra("TAG", false)
            if (isAssentFlowDone) {
                if (ConnectivityReceiver.isConnected) {
                    startMergingNameSignature()
                } else { //Utility.showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG);
                }
            }
        }
    }

    /**
     * Create assent data i.e user name and signature and save in server to create a assent.
     * First name and last name in sdk is encrypted. Decrypt it using sdk.
     *
     * @return request
     */
    private fun createAssentRequest(): AssentCreateRequest {
        val signature = SdkSharedPrefSingleton.getInstance().signature //Signature from shared preference
        var firstName: String? = null
        var lastName: String? = null
        try {
            firstName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().firstName) //getFirstName from shared preference
            lastName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().lastName) //getLastName from shared preference
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
        //raw pdf file path
        val directory = SdkUtils.getPdfDirectory(SdkConstant.PDF_DIRECTORY.App, PhAwareApplication().getMyAppContext())
        val filePath = File(directory, "assent_signature.pdf")
        //Encrypt
//Now process starts for encrypting the pdf file
        val pdfEncPath = File(directory, "encrypted_" + filePath.name)
        encrypLargeZip(filePath.toString(), pdfEncPath.toString())
        // deleting the raw pdf once Encryption is successfully done
        deleteRecursive(filePath)
        val request = AssentCreateRequest()
        request.assent_signature = signature
        request.assent_document = encodeFileToBase64Binary(pdfEncPath)
        //Deleting the encrypted pdf once converted to base 64
        deleteRecursive(pdfEncPath)
        try {
            request.name = encryptText("$firstName $lastName") //Firstname and Last name from preference
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        return request
    }

    override fun onDownloadComplete() {
        dismissProgressBar()
        showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.document_download_successful), Snackbar.LENGTH_LONG)
        //Check Consent I flow is done
        if (instance!!.getAssentStatus(AppConstants.CONSENT_ONE_FLOW) && !intent.getBooleanExtra(AppConstants.LOGIN, false)) {
            consentAlert(this)
        }
    }

    override fun onErrorResponse(assentResponse: AssentCreatedResponse?) {
        dismissProgressBar()
    }

    override fun onSuccessResponse(assentResponse: AssentCreatedResponse?) {
        dismissProgressBar()
        navigateNext()
    }

    private fun navigateNext() {
        instance!!.saveFlow(AppConstants.CONSENT_TWO_FLOW, true) // Consent II flow is done
        startActivity(Intent(this, AdditionalInfoActivity::class.java))
        finish()
    }

    private fun startMergingNameSignature() {
        showProgressBar(this, getString(R.string.generating_assent_pdf))
        Observable.fromCallable {
            //Utility.appendConsentNameToDoc("assent");
            val result = convertHtmlPdf("assent")
            result
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result: Boolean? ->
                    //Use result for something
                    dismissProgressBar()
                    val directory = File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
                    val assentHtml = File(directory, "assent.html")
                    assentHtml.delete()
                    if (ConnectivityReceiver.isConnected) {
                        showProgressBar(this, getString(R.string.please_wait))
                        mAssentPresenter!!.createAssentService(createAssentRequest())
                    } else {
                        showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                    }
                }
    }

    companion object {
        const val REQUEST_ASSENT = 1
        const val REQUEST_CONSENT = 2
        private const val REQUEST_SURVEY = 3
    }
}