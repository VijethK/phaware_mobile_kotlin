package com.dreamorbit.walktalktrack.welcome

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.screens.invite.InviteApi
import com.dreamorbit.walktalktrack.screens.joinstudy.JoinStudyActivity
import com.dreamorbit.walktalktrack.screens.login.LoginActivity
import com.dreamorbit.walktalktrack.utilities.PermissionsUtility
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.welcome.WelcomeActivity
import com.dreamorbit.walktalktrack.welcome.WelcomePresenter.WelcomePresenterContract
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener
import com.google.android.gms.common.api.Scope
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.result.DataReadResponse
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.rd.PageIndicatorView
import java.util.*
import java.util.concurrent.TimeUnit

class WelcomeActivity : BaseActivity(), WelcomePresenterContract, View.OnClickListener, ConnectionCallbacks, OnConnectionFailedListener {
    protected val REQUEST_OAUTH_REQUEST_CODE = 1
    //Google fit Auth
    protected var mGoogleApiClient: GoogleApiClient? = null
    protected var fitnessOptions: FitnessOptions? = null
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var mViewPager: ViewPager? = null
    private var mWelcomePresenter: WelcomePresenter? = null
    private var mBtnJoinStudy: Button? = null
    private var mTvAlreadyParticipating: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
      //  googleFitSetUp()
      //  initToolbar()
        initViews()
        initViewPager()
        //Reset the Rating value to 0, that means fresh installation
        SharedPrefSingleton.instance?.saveRateCount(0)
        //Initializing Presenter
        mWelcomePresenter = WelcomePresenter(this, this, InviteApi())
    }

    private fun initViews() {
        mBtnJoinStudy = findViewById(R.id.btnJoinStudy)
        mTvAlreadyParticipating = findViewById(R.id.already_participating)
        mBtnJoinStudy?.setOnClickListener(this)
        mTvAlreadyParticipating?.setOnClickListener(this)
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    private fun initViewPager() {
        mSectionsPagerAdapter = SectionsPagerAdapter()
        mViewPager = findViewById(R.id.container)
        mViewPager?.setAdapter(mSectionsPagerAdapter)
        val pageIndicatorView = findViewById<PageIndicatorView>(R.id.pageIndicatorView)
        pageIndicatorView.setViewPager(mViewPager)
    }

    /**
     * If Stanford does not required pre-qualifying questions include your gender, height, weight, ph diagnosis and whether or not you take medications.
     * does not require any pre-qualifying questions EXCEPT the invitation code, remove the qualifying questions regarding age, location and parent approval.
     * does not require a consent or an ascent form.
     *
     *
     * Else Denver keep all old flow as it is
     */
    override fun showJoinStudyScreen() {
        startActivity(Intent(this@WelcomeActivity, JoinStudyActivity::class.java))
    }

    override fun showError() {
        hideLoader()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnJoinStudy -> mWelcomePresenter!!.joinStudy()
            R.id.already_participating -> startActivity(Intent(this@WelcomeActivity, LoginActivity::class.java))
        }
    }

    override fun onConnected(bundle: Bundle?) {
        mBtnJoinStudy!!.isEnabled = true
        mTvAlreadyParticipating!!.isEnabled = true
        googleSignIn()
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onConnectionFailed(result: ConnectionResult) {
        Log.d(TAG, "onConnectionFailed$result")
        Log.i(TAG, "onConnectionFailed:" + result.errorCode + "," + result.errorMessage)
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR)
            } catch (e: SendIntentException) {
                mGoogleApiClient!!.connect()
            }
        }
    }

    protected fun googleFitSetUp() {
        if (mGoogleApiClient == null) { //----------------------API-CLIENT-INITIALIZATION----------
            mGoogleApiClient = GoogleApiClient.Builder(this)
                    .addApi(Fitness.SESSIONS_API)
                    .addApi(Fitness.SENSORS_API)
                    .addApi(Fitness.HISTORY_API)
                    .addApi(Fitness.RECORDING_API)
                    .addScope(Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                    .addScope(Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                    .addScope(Scope(Scopes.FITNESS_BODY_READ_WRITE)) //.useDefaultAccount() //Use the default account
                    .addConnectionCallbacks(this)
                    .enableAutoManage(this, 0, this)
                    .build()
            //---------RECORD-INITIALIZATION------------------
            fitnessOptions = FitnessOptions.builder()
                    .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                    .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                    .addDataType(DataType.TYPE_DISTANCE_CUMULATIVE)
                    .addDataType(DataType.TYPE_DISTANCE_DELTA)
                    .build()
            mGoogleApiClient?.connect()
        }
    }

    fun googleSignIn() {
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions!!)) {
            GoogleSignIn.requestPermissions(
                    this,
                    REQUEST_OAUTH_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions!!)
        } else {
            mBtnJoinStudy!!.isEnabled = true
            mTvAlreadyParticipating!!.isEnabled = true
            requestCallPermission(this)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
                if (!mGoogleApiClient!!.isConnecting && !mGoogleApiClient!!.isConnected) {
                    mGoogleApiClient!!.connect()
                } else if (mGoogleApiClient!!.isConnected) {
                    googleSignIn()
                    //Toast.makeText(this, "Google authentication successfully done.", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
                Toast.makeText(this, "Please sign in with google account.", Toast.LENGTH_SHORT).show()
                googleSignIn()
            } else {
                googleSignIn()
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Please sign in with google account.", Toast.LENGTH_SHORT).show()
            googleSignIn()
        }
    }

    private fun accessGoogleFit() {
        val cal = Calendar.getInstance()
        cal.time = Date()
        val endTime = cal.timeInMillis
        cal.add(Calendar.YEAR, -1)
        val startTime = cal.timeInMillis
        val readRequest = DataReadRequest.Builder()
                .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .bucketByTime(10, TimeUnit.SECONDS)
                .build()
        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .readData(readRequest)
                .addOnSuccessListener(OnSuccessListener<DataReadResponse> { o: Any? -> Log.e(TAG, "OnSuccess()") } as OnSuccessListener<in DataReadResponse>)
                .addOnFailureListener { e: Exception? -> Log.e(TAG, "onFailure()", e) }
                .addOnCompleteListener { task: Task<*>? -> Log.d(TAG, "onComplete()") }
    }

    override fun requestCallPermission(activity: Activity?) { //super.requestCallPermission(activity);
        val mPermissionsUtility = PermissionsUtility()
        if (mPermissionsUtility.hasPermissions(this, Manifest.permission.CALL_PHONE)) {
        } else {
            if (mPermissionsUtility.shouldShowRationale(this, Manifest.permission.CALL_PHONE)) {
                Snackbar.make(findViewById(R.id.main_content), "Call permission is required for emergency call",
                        Snackbar.LENGTH_INDEFINITE).setAction("OK") { view: View? ->
                    // Request the permission
                    mPermissionsUtility.requestForCallPermission(activity)
                }.show()
            } else {
                mPermissionsUtility.requestForCallPermission(this)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) { //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * A [PagerAdapter] that returns a layout corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter : PagerAdapter() {
        override fun getCount(): Int { // Show 5 total pages.
            return 5
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return `object` === view
        }

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(this@WelcomeActivity)
            val layout = inflater.inflate(R.layout.welcome_to_walk_talk, collection, false) as ViewGroup
            val title = layout.findViewById<TextView>(R.id.title)
            val description = layout.findViewById<TextView>(R.id.descrption)
            val swipeMore = layout.findViewById<TextView>(R.id.swipeMore)
            description.movementMethod = ScrollingMovementMethod()
            when (position) {
                0 -> {
                    title.text = getString(R.string.welcome_to_walktalktrack)
                    description.text = getString(R.string.welcome_to_walktalktrack_desc)
                }
                1 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.text = getString(R.string.what_is_walktalktrack)
                    description.text = getString(R.string.what_is_walktalktrack_desc)
                }
                2 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.text = getString(R.string.how_this_study_works)
                    description.text = getString(R.string.how_this_study_works_desc)
                }
                3 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.text = getString(R.string.who_can_participate)
                    description.text = getString(R.string.who_can_participate_desc)
                }
                4 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.text = getString(R.string.who_we_are)
                    description.text = getString(R.string.who_we_are_desc)
                }
            }
            collection.addView(layout)
            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }
    }

    override fun onResume() {
        super.onResume()
        val publicKey: String? = SharedPrefSingleton.instance?.publicKey
        if (TextUtils.isEmpty(publicKey) && ConnectivityReceiver.isConnected) {
            mWelcomePresenter!!.downloadCertificate()
        }
    }

    companion object {
        private const val TAG = "WelcomeActivity"
        private const val REQUEST_CODE_RESOLVE_ERR = 100
    }
}