package com.dreamorbit.walktalktrack.consent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.dreamorbit.researchaware.helper.SdkConstant
import com.dreamorbit.researchaware.helper.SdkUtils
import com.dreamorbit.researchaware.screentype.screens.assentconsent.ViewTaskActivity
import com.dreamorbit.researchaware.utilities.AESCrypt
import com.dreamorbit.researchaware.utilities.SdkSharedPrefSingleton
import com.dreamorbit.walktalktrack.R
import com.dreamorbit.walktalktrack.application.AppConstants
import com.dreamorbit.walktalktrack.application.PhAwareApplication
import com.dreamorbit.walktalktrack.assent.AssentActivity
import com.dreamorbit.walktalktrack.base.BaseActivity
import com.dreamorbit.walktalktrack.database.flow.FlowRepository.Companion.instance
import com.dreamorbit.walktalktrack.network.ConnectivityReceiver
import com.dreamorbit.walktalktrack.pojo.consent.ConsentCreateRequest
import com.dreamorbit.walktalktrack.pojo.consent.ConsentResponse
import com.dreamorbit.walktalktrack.pojo.register.StudyConfiguration
import com.dreamorbit.walktalktrack.screens.additionalInfo.AdditionalInfoActivity
import com.dreamorbit.walktalktrack.screens.consent.ConsentPresenter
import com.dreamorbit.walktalktrack.screens.consent.ConsenttViewContract
import com.dreamorbit.walktalktrack.screens.consent.CreateConsentApi
import com.dreamorbit.walktalktrack.screens.registration.DocumentDownloadApi
import com.dreamorbit.walktalktrack.utilities.FileHelper.deleteRecursive
import com.dreamorbit.walktalktrack.utilities.FileHelper.encrypLargeZip
import com.dreamorbit.walktalktrack.utilities.RSAEncryption
import com.dreamorbit.walktalktrack.utilities.RSAEncryption.Companion.encryptText
import com.dreamorbit.walktalktrack.utilities.SharedPrefSingleton
import com.dreamorbit.walktalktrack.utilities.Utility.convertHtmlPdf
import com.dreamorbit.walktalktrack.utilities.Utility.dismissProgressBar
import com.dreamorbit.walktalktrack.utilities.Utility.encodeFileToBase64Binary
import com.dreamorbit.walktalktrack.utilities.Utility.showProgressBar
import com.dreamorbit.walktalktrack.utilities.Utility.showSnackBar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.security.GeneralSecurityException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import javax.crypto.BadPaddingException
import javax.crypto.IllegalBlockSizeException
import javax.crypto.NoSuchPaddingException

class ConsentActivity : BaseActivity(), ConsenttViewContract {
    private var mTvTitle: TextView? = null
    private var mBtnStartConsent: Button? = null
    private var mConsentPresenter: ConsentPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_consent)
        initToolbar()
        initViews()
        downloadDocuments()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        mTvTitle = toolbar.findViewById(R.id.title)
        val params = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        params.setMargins(50, 0, 0, 0)
        mTvTitle?.setLayoutParams(params)
        mTvTitle?.setText(R.string.consent_one)
    }

    private fun initViews() {
        mConsentPresenter = ConsentPresenter(this, CreateConsentApi(), DocumentDownloadApi())
        mBtnStartConsent = findViewById(R.id.btnStartConsent)
        mBtnStartConsent?.setOnClickListener(View.OnClickListener {
            val directory = File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
            if (directory.exists() && directory.listFiles().size > 0) {
                navigateToConsent()
            } else {
                showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.please_download_documents), Snackbar.LENGTH_LONG)
                downloadDocuments()
            }
        })
    }

    private fun navigateToConsent() {
        val consentJson = loadConsentJSONFromAsset()
        val intent = Intent(this, ViewTaskActivity::class.java)
        intent.putExtra(SdkConstant.CONSENT_ASSENT_DATA, consentJson)
        startActivityForResult(intent, AssentActivity.REQUEST_CONSENT)
    }

    private fun consentRequest() {
        if (ConnectivityReceiver.isConnected) {
            showProgressBar(this, getString(R.string.please_wait))
            val request = createConsentRequest()
            /*testDecrypt();
            Utility.dismissProgressBar();*/mConsentPresenter!!.createConsentService(request)
        } else {
            showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    private fun downloadDocuments() {
        if (ConnectivityReceiver.isConnected) {
            showProgressBar(this, getString(R.string.downloading_consent_documents))
            mConsentPresenter!!.downloadDocument()
        } else {
            showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
        }
    }

    override fun onDownloadComplete() {
        dismissProgressBar()
        showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.document_download_successful), Snackbar.LENGTH_LONG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            val isConsentFlowDone = data.getBooleanExtra("TAG", false)
            if (isConsentFlowDone) {
                if (ConnectivityReceiver.isConnected) {
                    startMergingNameSignature()
                } else {
                    showSnackBar(findViewById(R.id.consent_parent_layout), getString(R.string.check_internet_connection), Snackbar.LENGTH_LONG)
                }
            }
        }
    }

    /**
     * Create assent data i.e user name and signature and save in server to create a assent.
     * First name and last name in sdk is encrypted. Decrypt it using sdk.
     *
     * @return request
     */
    private fun createConsentRequest(): ConsentCreateRequest {
        val signature = SdkSharedPrefSingleton.getInstance().signature //Signature from shared preference
        var firstName: String? = null
        var lastName: String? = null
        try {
            firstName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().firstName)
            lastName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().lastName)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
        //raw pdf file path
        val directory = SdkUtils.getPdfDirectory(SdkConstant.PDF_DIRECTORY.App, PhAwareApplication().getMyAppContext())
        val filePath = File(directory, "consent_signature.pdf")
        //Encrypt
//Now process starts for encrypting the zip file
        val pdfEncPath = File(directory, "encrypted_" + filePath.name)
        encrypLargeZip(filePath.toString(), pdfEncPath.toString())
        // deleting the raw pdf once Encryption is successfully done
        deleteRecursive(filePath)
        val request = ConsentCreateRequest()
        request.consent_signature = signature
        request.consent_document = encodeFileToBase64Binary(pdfEncPath)
        //Deleting the encrypted pdf once converted to base 64
        deleteRecursive(pdfEncPath)
        try {
            request.name = encryptText("$firstName $lastName") //Firstname and Last name from preference
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        } catch (e: IllegalBlockSizeException) {
            e.printStackTrace()
        } catch (e: BadPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        return request
    }

    override fun onErrorResponse(consentResponse: ConsentResponse?) {
        dismissProgressBar()
        showSnackBar(findViewById(R.id.consent_parent_layout), consentResponse!!.errorMessage, Snackbar.LENGTH_LONG)
    }

    override fun onFailureResponse(consentResponse: ConsentResponse?) {
        dismissProgressBar()
    }

    override fun onSuccessResponse(consentResponse: ConsentResponse?) {
        dismissProgressBar()
        navigateNext()
    }

    private fun navigateNext() { //depend on the study config navigate to the screen
        val studyConfig: StudyConfiguration? = SharedPrefSingleton.instance?.studyConfig
        if (studyConfig?.studyAssent!!) {
            instance!!.saveFlow(AppConstants.CONSENT_ONE_FLOW, true) // Consent I flow is done. Just to handle the login navigation, will treat this as Assent flow done
            startActivity(Intent(this, AssentActivity::class.java))
            finish()
        } else {
            instance!!.saveFlow(AppConstants.CONSENT_TWO_FLOW, true) // Consent I flow is done. Just to handle the login navigation, will treat this as Assent flow done
            startActivity(Intent(this, AdditionalInfoActivity::class.java))
            finish()
        }
    }

    private fun startMergingNameSignature() {
        showProgressBar(this, getString(R.string.generating_consent_pdf))
        Observable.fromCallable {
            //Utility.appendConsentNameToDoc("consent");
            val result = convertHtmlPdf("consent")
            result
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result: Boolean ->
                    //Use result for something
                    dismissProgressBar()
                    if (result) {
                        val directory = File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
                        val consentHtml = File(directory, "consent.html")
                        consentHtml.delete()
                        //Once name and signature is appended to pdf
//create request object to call the api
                        consentRequest()
                    }
                }
    }

    private fun testDecrypt() {
        val directory = SdkUtils.getPdfDirectory(SdkConstant.PDF_DIRECTORY.App, PhAwareApplication().getMyAppContext())
        val input = File(directory, "encrypted_consent_signature.pdf")
        val pdfDecPath = File(directory, "decrypted_consent_signature.pdf")
        try {
            val bytes = FileUtils.readFileToByteArray(input)
            RSAEncryption().decryptFile(bytes, pdfDecPath)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}