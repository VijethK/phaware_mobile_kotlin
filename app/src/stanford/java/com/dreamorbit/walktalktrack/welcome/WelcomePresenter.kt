package com.dreamorbit.walktalktrack.welcome

import android.content.Context
import android.util.Log
import com.dreamorbit.walktalktrack.api.PhPublicKey
import com.dreamorbit.walktalktrack.pojo.invite.InviteToggleResponse
import com.dreamorbit.walktalktrack.screens.invite.InviteApi
import com.dreamorbit.walktalktrack.service.PublicKeyApi
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by mujasam.bn on 9/5/2017.
 */
class WelcomePresenter(private val mListener: WelcomePresenterContract, private val context: Context, private val inviteApi: InviteApi) {
    fun joinStudy() {
        mListener.showJoinStudyOrInviteScreen()
    }

    fun inviteToggleIsOnOff() {
        inviteApi.validateInviteCodeOnOff()!!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<InviteToggleResponse?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(response: InviteToggleResponse) {
                        if (response != null) {
                            mListener.navigateInviteToggleSwitch(response.message)
                        } else {
                            mListener.showError()
                        }
                    }

                    override fun onError(e: Throwable) {
                        mListener.showError()
                    }
                })
    }

    fun downloadCertificate() {
        val publicKeyApi = PublicKeyApi()
        publicKeyApi.presendUrl!!.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<PhPublicKey?> {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onSuccess(phPublicKey: PhPublicKey) {
                        if (phPublicKey != null) {
                            Log.e("Response:", "" + phPublicKey)
                            publicKeyApi.getPublicKey(phPublicKey.url)!!.subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(object : SingleObserver<String?> {
                                        override fun onSubscribe(disposable: Disposable) {}
                                        override fun onSuccess(s: String) {}
                                        override fun onError(throwable: Throwable) {}
                                    })
                        }
                    }

                    override fun onError(e: Throwable) {}
                })
    }

    interface WelcomePresenterContract {
        fun showJoinStudyOrInviteScreen()
        fun navigateInviteToggleSwitch(inviteOn: Boolean)
        fun showError()
    }

}