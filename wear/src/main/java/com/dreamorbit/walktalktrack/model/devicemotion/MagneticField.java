package com.dreamorbit.walktalktrack.model.devicemotion;

/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class MagneticField {

    private Float y;

    private Float z;

    private Float x;

    private Float accuracy;

    public Float getY() {
        return y;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public Float getZ() {
        return z;
    }

    public void setZ(Float z) {
        this.z = z;
    }

    public Float getX() {
        return x;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public Float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Float accuracy) {
        this.accuracy = accuracy;
    }
}
