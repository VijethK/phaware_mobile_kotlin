/**
 * Created by mujasam.bn on 12/20/2017.
 */

package com.dreamorbit.walktalktrack.model.pedometer.steps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Pedometer {

    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setItem(Item item) {
        items.add(item);
    }
}