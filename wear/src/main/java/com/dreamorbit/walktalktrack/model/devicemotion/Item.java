package com.dreamorbit.walktalktrack.model.devicemotion;


/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class Item {

    private String timestamp;

    private RotationRate rotationRate;

    private UserAcceleration userAcceleration;

    private Gravity gravity;

    private MagneticField magneticField;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public RotationRate getRotationRate() {
        return rotationRate;
    }

    public void setRotationRate(RotationRate rotationRate) {
        this.rotationRate = rotationRate;
    }

    public UserAcceleration getUserAcceleration() {
        return userAcceleration;
    }

    public void setUserAcceleration(UserAcceleration userAcceleration) {
        this.userAcceleration = userAcceleration;
    }

    public Gravity getGravity() {
        return gravity;
    }

    public void setGravity(Gravity gravity) {
        this.gravity = gravity;
    }

    public MagneticField getMagneticField() {
        return magneticField;
    }

    public void setMagneticField(MagneticField magneticField) {
        this.magneticField = magneticField;
    }
}
