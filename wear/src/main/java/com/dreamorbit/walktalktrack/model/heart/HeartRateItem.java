package com.dreamorbit.walktalktrack.model.heart;

/**
 * Created by mujasam.bn on 2/9/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeartRateItem {

    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("HeartRate")
    @Expose
    private String heartRate;
    @SerializedName("startDate")
    @Expose
    private String startDate;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}