package com.dreamorbit.walktalktrack.model.scheduler;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SchedulerModel {

    public static final String TEST_TAKEN = "test_taken";
    public static final String TEST_PENDING = "pending";

    private String schedule_type;
    private List<String> scheduleDays = null;
    private String study_token;
    private String name;
    private String scheduled_at;
    private Integer id;

    //DB
    private String testTakenAt;
    private String status;
    private String day;

    public String getTestTakenAt() {
        return testTakenAt;
    }

    public void setTestTakenAt(String testTakenAt) {
        this.testTakenAt = testTakenAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getScheduleType() {
        return schedule_type;
    }

    public void setScheduleType(String scheduleType) {
        this.schedule_type = scheduleType;
    }

    public List<String> getScheduleDays() {
        return scheduleDays;
    }

    public void setScheduleDays(List<String> scheduleDays) {
        this.scheduleDays = scheduleDays;
    }

    public String getStudyToken() {
        return study_token;
    }

    public void setStudyToken(String studyToken) {
        this.study_token = studyToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScheduledAt() {
        return scheduled_at;
    }

    public void setScheduledAt(String scheduledAt) {
        this.scheduled_at = scheduledAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
