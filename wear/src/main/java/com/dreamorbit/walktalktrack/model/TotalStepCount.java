package com.dreamorbit.walktalktrack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mujasam.bn on 12/22/2017.
 */

public class TotalStepCount {

    @SerializedName("totalStepsInDay")
    @Expose
    private String totalStepsInDay;

    public String getTotalStepsInDay() {
        return totalStepsInDay;
    }

    public void setTotalStepsInDay(String totalStepsInDay) {
        this.totalStepsInDay = totalStepsInDay;
    }

}