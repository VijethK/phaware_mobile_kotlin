package com.dreamorbit.walktalktrack.model.devicemotion;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class GyroResponse {
    private List<Item> items = new ArrayList<>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {

        this.items = items;
    }

    public void addItems(Item item) {
        items.add(item);
    }
}
