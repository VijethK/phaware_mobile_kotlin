
/**
 * Created by mujasam.bn on 2/9/2018.
 */

package com.dreamorbit.walktalktrack.model.heart;

        import java.util.ArrayList;
        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class HeartRate {

    @SerializedName("items")
    @Expose
    private List<HeartRateItem> items = new ArrayList<>();

    public List<HeartRateItem> getItems() {
        return items;
    }

    public void setItems(HeartRateItem item) {
        this.items.add(item);
    }

}