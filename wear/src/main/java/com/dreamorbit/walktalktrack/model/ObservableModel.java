package com.dreamorbit.walktalktrack.model;

/**
 * Created by mujasam.bn on 2/21/2018.
 */

public class ObservableModel {
    private int heartRate;
    private double distance;

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}
