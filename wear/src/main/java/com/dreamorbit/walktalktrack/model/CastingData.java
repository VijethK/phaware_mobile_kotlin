package com.dreamorbit.walktalktrack.model;

/**
 * Created by mujasam.bn on 3/5/2018.
 */

public class CastingData {

    private int heartRate;
    private int step;
    private int avgHeartRate;
    private double distance;
    private String timer;
    private String locations;
    private boolean isCastStop;
    private boolean isResting;

    public CastingData(int heartRate, double distance, String timer, String locations, int avgHeartRate, boolean isCastStop,boolean isResting, int step) {
        this.heartRate = heartRate;
        this.distance = distance;
        this.timer = timer;
        this.locations = locations;
        this.isCastStop = isCastStop;
        this.avgHeartRate = avgHeartRate;
        this.isResting = isResting;
        this.step = step;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getLocatons() {return locations;}

    public void setLocatons(String locations) {this.locations = locations;}

    public void setCastStop(boolean isCastStop) {this.isCastStop = isCastStop;}

    public boolean isCastStop() {
        return isCastStop;
    }

    public boolean isResting() {
        return isResting;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}
