package com.dreamorbit.walktalktrack.application;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;

public final class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private static String TAG = MyActivityLifecycleCallbacks.class.getName();

    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.e(TAG, "onActivityCreated:" + activity.getComponentName().getClassName());
    }

    public void onActivityDestroyed(Activity activity) {
        Log.e(TAG, "onActivityDestroyed:" + activity.getComponentName().getClassName());
    }

    public void onActivityPaused(Activity activity) {
        Log.e(TAG, "onActivityPaused:" + activity.getComponentName().getClassName());
    }

    public void onActivityResumed(Activity activity) {
        Log.e(TAG, "onActivityResumed:" + activity.getComponentName().getClassName());
        WearPrefSingleton.getInstance().saveLastState(activity.getComponentName().getClassName());
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.e(TAG, "onActivitySaveInstanceState:" + activity.getComponentName().getClassName());
    }

    public void onActivityStarted(Activity activity) {
        Log.e(TAG, "onActivityStarted:" + activity.getComponentName().getClassName());
    }

    public void onActivityStopped(Activity activity) {
        Log.e(TAG, "onActivityStopped:" + activity.getComponentName().getClassName());
    }
}