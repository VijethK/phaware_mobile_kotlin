package com.dreamorbit.walktalktrack.application;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;

import io.fabric.sdk.android.Fabric;


/**
 * Created by mujasam.bn on 9/6/2017.
 */

public class PhWearApplication extends Application {

    private static Context context;
    private static PhWearApplication mInstance;

    public PhWearApplication() {
    }

    public static Context getMyAppContext() {
        return context;
    }

    public static synchronized PhWearApplication getInstance() {
        return mInstance;
    }

    public void onCreate() {

        Log.e("PhWearApplication", "called");
        super.onCreate();
        context = getApplicationContext();
        mInstance = this;
        Fabric.with(this, new Crashlytics());
        WearPrefSingleton.getInstance().initialize(this,"WearPreference");
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
    }

    @Override
    public void onTerminate (){
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged (Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }
}
