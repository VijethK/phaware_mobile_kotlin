package com.dreamorbit.walktalktrack.datalayer;

import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.dreamorbit.walktalktrack.application.PhWearApplication;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.model.CastingData;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by mujasam.bn on 2/22/2018.
 * <p>
 * 1 Ackonoledge Login success in Mobile
 * 2 Acknoledge Phone receiverd the zip file
 */

/*********************************************************************************
 *                                                                               *
 *                                                                               *
 *                                Wearable Communication Channel                                   *
 *                                                                               *
 *                                                                               *
 **********************************************************************************/


public class WearMessageSenderAsync extends AsyncTask {

    public static final String CASTING_MESSAGE = "/casting_message";
    public static final String EMERGENCY_CALL_MESSAGE = "/emergency_call_message";
    public static final String REPORT_SUMMARY_MESSAGE = "/report_summary_message";
    public static final String TEST_ENABLE_DISABLE = "/test_enable_disable";

    protected static final String TAG = "BottomBarActivity";

    @Override
    protected Object doInBackground(Object[] objects) {
        String task = "";
        CastingData castingData = null;

        if (objects.length == 2) {
            task = (String) objects[1]; // this call and condition check is for Emergency call
        } else if (objects.length > 0) {
            castingData = (CastingData) objects[0]; // this call to send the casting data to phone or to send user logined confirmation check in phone app
        }

        Collection<String> nodes = getNodes();
        for (String node : nodes) {
            if (task.equals(EMERGENCY_CALL_MESSAGE)) {
                sendEmergencyCallMessage(node);
            } else if (task.equals(REPORT_SUMMARY_MESSAGE)) {
                sendReportSummaryMessage(node);
            }else if (task.equals(TEST_ENABLE_DISABLE)) {
                String schedulerModel = (String) objects[0];
                sendTestEnableDisableStatus(node, schedulerModel);
            }  else {
                sendWalkTestData(node, castingData);
            }
        }
        return null;
    }

    private void sendTestEnableDisableStatus(String node, String schedulerModel) {
        Task<Integer> sendMessageTask =
                Wearable.getMessageClient(PhWearApplication.getMyAppContext()).sendMessage(node, TEST_ENABLE_DISABLE, schedulerModel.getBytes());
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            Integer result = Tasks.await(sendMessageTask);
            Log.e(TAG, "Test Wear sent: " + result);
        } catch (ExecutionException exception) {
            Log.e(TAG, "Test Wear failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Test Wear occurred: " + exception);
        }
    }

    private void sendWalkTestData(String node, CastingData castingData) {
        Gson gson = new Gson();
        String data = gson.toJson(castingData, CastingData.class);
        Task<Integer> sendMessageTask =
                Wearable.getMessageClient(PhWearApplication.getMyAppContext()).sendMessage(node, CASTING_MESSAGE, data.getBytes());
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            Integer result = Tasks.await(sendMessageTask);
            Log.e(TAG, "Casting sent: " + data);
        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
    }

    @WorkerThread
    protected void sendEmergencyCallMessage(String node) {

        Task<Integer> sendMessageTask =
                Wearable.getMessageClient(PhWearApplication.getMyAppContext()).sendMessage(node, EMERGENCY_CALL_MESSAGE, new byte[0]);
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            Integer result = Tasks.await(sendMessageTask);
            Log.d(TAG, "Message sent: " + result);
        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
    }

    @WorkerThread
    protected void sendReportSummaryMessage(String node) {
        String summary = WearPrefSingleton.getInstance().getTestSummary();
        Log.e(TAG, "Report Summary Sending.. " + summary);
        Task<Integer> sendMessageTask =
                Wearable.getMessageClient(PhWearApplication.getMyAppContext()).sendMessage(node, REPORT_SUMMARY_MESSAGE, summary.getBytes());
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            Integer result = Tasks.await(sendMessageTask);

            WearPrefSingleton.getInstance().saveTestSummary(null);
            Log.d(TAG, "Report Summary sent: " + result);
        } catch (ExecutionException exception) {
            Log.e(TAG, "Report failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Report Interrupt occurred: " + exception);
        }
    }

    @WorkerThread
    protected Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        Task<List<Node>> nodeListTask =
                Wearable.getNodeClient(PhWearApplication.getMyAppContext()).getConnectedNodes();
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            List<Node> nodes = Tasks.await(nodeListTask);
            for (Node node : nodes) {
                results.add(node.getId());
            }
        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }
        return results;
    }
}
