package com.dreamorbit.walktalktrack.datalayer;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.wearable.ChannelClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.dreamorbit.walktalktrack.helper.FileHelper;
import com.dreamorbit.walktalktrack.helper.WearUtils;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.dreamorbit.walktalktrack.datalayer.WearLayerListenerService.FILE_RECEIVED;

/**
 * Created by mujasam.bn on 10/27/2017.
 */

public class BackGroundWearService extends IntentService {

    private static final String TAG = "BackGroundWearService";
    private static final String PATH = "/path";

    public BackGroundWearService() {
        super("BackGroundWearService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        boolean isZiped = zipWalkTestData();
    }

    /**
     * Create a zip from all json files respective of walk test
     *
     * @return
     */
    private boolean zipWalkTestData() {
        File[] directories = WearUtils.getAllDirectories(this);
        for (File dir : directories) {
            if (dir.isDirectory()) {
                File files[] = dir.listFiles(); //get all files inside this directory
                String zipPath = getFilesDir().getAbsolutePath() + File.separator + files[0].getParentFile().getName() + ".zip"; //get the folder name
                FileHelper.zipFiles(files, zipPath); //Zipping multiple files
                //deleting the old folder once zip is successfully done
                FileHelper.deleteRecursive(dir);

                //Sync this zip file to Handheld device
                syncWearDataToPhone(zipPath);
            } else if (dir.isFile() && dir.getName().endsWith(".zip")) {
                syncWearDataToPhone(dir.toString());
                Log.e("Wear", "Sending Old data to phone");
            }
        }

        return true;
    }

    /**
     * Once zip is done sync this zip file to phone
     * Note: This zip file is not encrypted
     *
     * @param zipPath
     */
    private void syncWearDataToPhone(String zipPath) {
        ChannelClient client = null;
        File file = new File(zipPath);
        Collection<String> nodes = getNodes();
        for (String node : nodes) {
            client = Wearable.getChannelClient(this);
            client.openChannel(node, PATH).addOnCompleteListener(task ->
                    Wearable.getChannelClient(getApplicationContext()).sendFile(task.getResult(), Uri.fromFile(file)).addOnSuccessListener(task1 -> {
                        // Send from the Wearable, the zip file name to phone
                        // Phone once confirmed that file has been returned
                        // get back the acknoledgement to wear
                        Log.e(TAG, "Successfully File Sent to Phone: " + node);
                        Task<Integer> sendMessageTask =
                                Wearable.getMessageClient(getApplicationContext()).sendMessage(node, FILE_RECEIVED, zipPath.getBytes());

                    })
            );
        }
    }

    protected Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        Task<List<Node>> nodeListTask =
                Wearable.getNodeClient(getApplicationContext()).getConnectedNodes();
        try {
            // Block on a task and get the result synchronously (because this is on a background
            // thread).
            List<Node> nodes = Tasks.await(nodeListTask);

            for (Node node : nodes) {
                results.add(node.getId());
            }

        } catch (ExecutionException exception) {
            Log.e(TAG, "Task failed: " + exception);
        } catch (InterruptedException exception) {
            Log.e(TAG, "Interrupt occurred: " + exception);
        }

        return results;
    }
}
