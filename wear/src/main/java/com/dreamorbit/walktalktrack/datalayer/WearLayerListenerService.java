/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dreamorbit.walktalktrack.datalayer;

import android.content.Intent;
import android.util.Log;

import com.dreamorbit.walktalktrack.helper.FileHelper;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.screens.MainActivity;
import com.dreamorbit.walktalktrack.walktest.HeartRateSensorService;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.File;

/**
 * Listens to DataItems and Messages from the local node.
 */
public class WearLayerListenerService extends WearableListenerService {

    public static final String FILE_RECEIVED = "/file-received";
    private static final String TAG = "WearListenerService";
    private static final String START_ACTIVITY_PATH = "/start-activity";
    private static final String STOP_ACTIVITY_PATH = "/stop-activity";
    private static final String LAUNCH_WEAR_PATH = "/launch-wear";
    protected static final String TEST_ENABLE_DISABLE = "/test_enable_disable";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.e(TAG, "onMessageReceived: " + messageEvent);

        // Check to see if the message is to start an activity
        if (messageEvent.getPath().equals(START_ACTIVITY_PATH)) {
            Log.e(TAG, "onMessageReceived: " + "START");
            WearPrefSingleton.getInstance().saveLoginedStatus(true);
        } else if (messageEvent.getPath().equals(STOP_ACTIVITY_PATH)) {
            Log.e(TAG, "onMessageReceived: " + "STOP");
            WearPrefSingleton.getInstance().saveLoginedStatus(false);
            stopEverything();
        } else if (messageEvent.getPath().equals(FILE_RECEIVED)) {
            Log.e(TAG, "onFileReceived Acknowledgement: " + "Confirm");
            File file = new File(new String(messageEvent.getData()));
            FileHelper.deleteRecursive(file);
            Log.e(TAG, "onFileReceived: " + "Deleted: " + file);
        }else if (messageEvent.getPath().equals(LAUNCH_WEAR_PATH)) {
            Log.e(TAG, "onMessageReceived: " + "Launch Wear");
            Class<?> activityClass;
            try {
                activityClass = Class.forName(WearPrefSingleton.getInstance().getLastState());
                Log.e(TAG, "onMessageReceived: "+ activityClass);
            } catch(ClassNotFoundException ex) {
                activityClass = MainActivity.class;
            }

            Intent intent = new Intent(this,activityClass);
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            startActivity(intent);
        }else if (messageEvent.getPath().equals(TEST_ENABLE_DISABLE)) {
            String schedulerString = new String(messageEvent.getData());
            Log.e(TAG, "WearLayer New Model received From Mobile: " + schedulerString);
            WearPrefSingleton.getInstance().saveSchedulerModel(schedulerString);
        }
    }

    private void stopEverything() {

        //Launch to Main activity clearing all previous stack
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

        //stop service if running
        Intent intentSerivce = new Intent(getApplicationContext(), HeartRateSensorService.class);
        stopService(intentSerivce);

        //Deleted the folder if exist
        String path = WearPrefSingleton.getInstance().getWalkTestFolderPath();
        File file = new File(path);
        FileHelper.deleteFolder(file);
        WearPrefSingleton.getInstance().saveWalkTestFolderPath(null);

        //clear previous report data
        WearPrefSingleton.getInstance().saveTestSummary(null);

        Log.e(TAG, "Stoped Everything: "+ path);

    }
}