package com.dreamorbit.walktalktrack.walktest;

import android.Manifest;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.wearable.activity.ConfirmationActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.IPedometerCallback;
import com.dreamorbit.pedometer.SensorChecker;
import com.dreamorbit.pedometer.accelerometer.AcceleroMeterService;
import com.dreamorbit.pedometer.accelerometer.PedometerSettings;
import com.dreamorbit.pedometer.pojo.pedometer.Item;
import com.dreamorbit.pedometer.pojo.pedometer.Pedometer;
import com.dreamorbit.pedometer.stepsensor.StepSensorService;
import com.dreamorbit.walktalktrack.model.ObservableModel;
import com.google.gson.Gson;
import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;
import com.dreamorbit.walktalktrack.helper.PermissionUtil;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.CastingData;
import com.dreamorbit.walktalktrack.model.heart.HeartRate;
import com.dreamorbit.walktalktrack.model.heart.HeartRateItem;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.screens.SurveyActivity;
import com.dreamorbit.walktalktrack.screens.SurveyBaseFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonParseException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.dreamorbit.walktalktrack.walktest.WalkingFragment.isCompleteWalktTest;

public class RestingFragment extends SurveyBaseFragment implements HeartRateSensorService.OnChangeListener {

    protected final int REQUEST_CODE_PERMISSION_BODY_SENSOR = 0;
    private final String TAG = "RestingFragment";

    //Timer
    public CountDownTimer mCountDownTimer;
    public long seconds = 60000;
    public HeartRate heartRate = new HeartRate();
    //public long seconds = 10000;
    protected boolean mIsBound = false;
    protected ServiceConnection serviceConnection, serviceConnection2;
    protected long remainingTime;
    private Intent intent;
    private HeartRateSensorService mSensorService;
    //UI
    private TextView tvHeartRate;
    private TextView tvTimer;

    //To show in resting data summary screen
    private int mMaxHeartRate;
    private List<Integer> mHeartRateList = new ArrayList();
    private String mMinuteTimer;
    private int mCurrentHeartRate = 0;

    //Resting Pedometer
    private List<Item> mPedoItems = new ArrayList<>();
    private BaseSensorService mBaseService;

    private PedometerSettings mPedometerSettings;
    private boolean mIsRunning;

    private static final int STEPS_MSG = 1;
    private static final int DISTANCE_MSG = 3;
    private static final int SPEED_MSG = 4;
    private static final int CALORIES_MSG = 5;
    private static final int HEART_RATE_MSG = 6;
    public int mFinalSteps;
    public double mFinalDistance = 0;
    private List<com.dreamorbit.pedometer.pojo.pedometer.Item> mPedoItemsTemp = new ArrayList<>();

    private SensorChecker.SensorType nSensorType;
    private Class<?> activeService;
    //Pedometer data
    private Pedometer mPedometer = new Pedometer();

    /*
     * Bind to our service.
     * ComponentName heartService = startService(new Intent(WearActivity.this, HeartbeatService.class));
     */
    public static RestingFragment newInstance(List<Questions> question, int position, ViewPager viewPager, String activityTitle) {
        RestingFragment fragment = new RestingFragment();
        Bundle args = new Bundle();
        args.putInt(WearConstant.POSITION, position);
        args.putString(WearConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(WearConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = new Intent(getActivity(), HeartRateSensorService.class);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());

        if (serviceConnection == null) {
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder binder) {
                    Log.d(TAG, "connected to service.");
                    // set our change listener to get change events
                    ((HeartRateSensorService.HeartbeatServiceBinder) binder).setChangeListener(RestingFragment.this);
                    HeartRateSensorService.HeartbeatServiceBinder service = (HeartRateSensorService.HeartbeatServiceBinder) binder;
                    mSensorService = service.getService();
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {

                }
            };
        }
        if (serviceConnection2 == null) {
            serviceConnection2 = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder binder) {
                    Log.e(TAG, "connected to Step service 1");
                    mBaseService = ((BaseSensorService.StepBinder) binder).getService();
                    mBaseService.registerCallback(mCallback);
                    Log.e(TAG, "connected to Step service 2");
                }
                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    if (mBaseService != null) {
                        mBaseService.unregisterListener();
                    }
                    mBaseService = null;
                }
            };
        }
    }

    //Handler - update
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STEPS_MSG:
                    int mStepValue = msg.arg1;
                    mFinalSteps = mStepValue;
                    Log.e(TAG, "handleMessage STEPS_MSG >>> "+mFinalSteps);
                    break;

                case DISTANCE_MSG:
                    Log.e(TAG, "handleMessage DISTANCE_MSG >>> 1");
                    float mDistanceValue = msg.arg1;
                    if (mDistanceValue <= 0) {
                        Log.e(TAG, "handleMessage DISTANCE_MSG if >>> "+mDistanceValue);
                        onDistanceValueChanged(mDistanceValue);
                    } else {
                        String finalDistance = String.format("%.0f", mDistanceValue);
                        mFinalDistance = Double.parseDouble(finalDistance);
                        Log.e(TAG, "handleMessage DISTANCE_MSG else mFinalDistance >>> "+mFinalDistance);
                        onDistanceValueChanged(mFinalDistance);
                    }
                    break;

               /* case HEART_RATE_MSG:
                    onHearBeatValueChanged(msg.arg1);
                    break;*/

                default:
                    super.handleMessage(msg);
            }

            //todo remove after test
            com.dreamorbit.pedometer.pojo.pedometer.Item item = new com.dreamorbit.pedometer.pojo.pedometer.Item();
            item.setNumberOfSteps(mFinalSteps);
            item.setDistance(mFinalDistance);
            item.setStartDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
            mPedoItemsTemp.add(item);
        }
    };

    // TODO: unite all into 1 type of message
    private IPedometerCallback mCallback = new IPedometerCallback() {
        public void stepsChanged(int value) {
            Log.e(TAG, "IPedometerCallback stepsChanged >>> "+value);
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }
        public void distanceChanged(float value) {
            Log.e(TAG, "IPedometerCallback distanceChanged >>> "+value);
            mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG, (int) (value), 0));
        }
        public void onHearBeatValueChanged(int hearRate) {
            Log.e(TAG, "IPedometerCallback onHearBeatValueChanged >>> "+hearRate);
           // mHandler.sendMessage(mHandler.obtainMessage(HEART_RATE_MSG, hearRate, 0));
        }
        @Override
        public void onDistanceListFetched(List<com.dreamorbit.pedometer.pojo.pedometer.Item> items) {
            mPedoItems = items;
            Log.e(TAG, "IPedometerCallback onDistanceListFetched >>> "+mPedoItems);
            fetchTestData();
            //new FetchTotalStepsAsync().execute();
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_resting_pager, container, false);
        if (savedInstanceState != null) {
        } else {
            initViews(rootView);
        }
        return rootView;
    }

    private void initViews(View rootView) {
        tvHeartRate = rootView.findViewById(R.id.tvHeartRate);
        tvTimer = rootView.findViewById(R.id.tvTimer);
        ImageView imgHeart = rootView.findViewById(R.id.imgHeart);
        WearUtils.heartAnimator(imgHeart);
    }

    /*********************************
     * Register the heart rate sensor
     * get heart rate
     */

    public void registerSensors() {
        boolean hasPermission = PermissionUtil.hasPermission(getActivity(), Manifest.permission.BODY_SENSORS);
        if (hasPermission) {
            startTimer();
            WalkTestServiceStart();
            RestingPedomenterServiceStart();
        } else {
            PermissionUtil.requestPermission(getActivity(), REQUEST_CODE_PERMISSION_BODY_SENSOR, Manifest.permission.BODY_SENSORS, null);
        }
    }

    /*****************************************************************************************************************
     *
     *  Step Count Subscription and Reading the Data
     *
     *****************************************************************************************************************/

    public void startTimer() {
        mCountDownTimer = new CountDownTimer(seconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                remainingTime = millisUntilFinished;
                long secondsLimit = millisUntilFinished / 1000;
                mMinuteTimer = String.format("%02d", secondsLimit / 60) + ":" + String.format("%02d", secondsLimit % 60);
                Log.e("Time ", mMinuteTimer);
                tvTimer.setText(mMinuteTimer);

                //Start resting casting screen in phone
                CastingData castingData = new CastingData(mCurrentHeartRate, 0, mMinuteTimer, null, 0, true, true, 0);
                new WearMessageSenderAsync().execute(castingData);
            }

            @Override
            public void onFinish() {
                tvTimer.setText("00:00");
                RestingPedomenterServiceStop();
                stopSensorService();
            }
        };
        mCountDownTimer.start();
    }

    /*********************************
     *Resting Pedometer Service Starts here
     *********************************/

    public void RestingPedomenterServiceStart() {
        WearUtils.grantVibration(getActivity());
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPedometerSettings = new PedometerSettings(mSettings);
        mIsRunning = mPedometerSettings.isServiceRunning();

        // Start the service if this is considered to be an application start (last onPause was long ago)
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Started First time");
            checkSensor();
            startStepService();
            bindStepService();
           // initFitApiClient();
           // getStartTime();
        } else if (mIsRunning) {
            Log.i(TAG, "[SERVICE] Running already");
            bindStepService();
        }
    }

    private void checkSensor() {
        SensorChecker sensorChecker = new SensorChecker(getActivity());
        nSensorType = sensorChecker.getPedometerSensor();
        switch (nSensorType) {
            case ACCELEROMETER:
                activeService = AcceleroMeterService.class;
                break;
            case STEP:
                activeService = StepSensorService.class;
                break;
        }
    }

    private void startStepService() {
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Start");
            mIsRunning = true;
           // startLocationHelper(); // start the location fetch task
           // startDeviceMotion(); // start device motion
            getActivity().startService(new Intent(getActivity(), activeService));
        }
    }

    private void bindStepService() {
        Log.i(TAG, "[SERVICE] Bind");
        mIsBound = getActivity().bindService(new Intent(getActivity(), activeService), serviceConnection2, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }


    /*********************************
     *Heart Rate Service Starts here
     *********************************/
    public void WalkTestServiceStart() {

        //Start Resting cast screen in phone
        CastingData castingData = new CastingData(0, 0, mMinuteTimer, null, 0, true, true, 0 );
        new WearMessageSenderAsync().execute(castingData);

        WearUtils.grantVibration(getActivity());
        mIsBound = getActivity().bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
        getActivity().startService(intent);
    }

    /***************************************
     * Callback from PhSensorService.java for heart beat
     * @param newValue
     */
    @Override
    public void onHearBeatValueChanged(int newValue) {
        mCurrentHeartRate = newValue;
        tvHeartRate.setText("" + newValue);

        //to check the Max HearRate
        if (newValue != 0 && newValue > mMaxHeartRate) {
            mMaxHeartRate = newValue;
        }

        //to check the Average HeartRate
        mHeartRateList.add(newValue);

        setHeartRate(newValue);
    }

    /************************************
     * Create hearrate model for resting.json
     * @param arg
     */
    private void setHeartRate(int arg) {
         HeartRateItem item = new HeartRateItem();
        item.setStartDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
        item.setEndDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
        item.setHeartRate(arg + " BPM");
        heartRate.setItems(item);
    }

    /****************************************
     * Callback from PhSensorService.java for Distance
     * value change
     * @param totalDistance
     */
    public void onDistanceValueChanged(double totalDistance) {
        ObservableModel model = new ObservableModel();
        model.setDistance(totalDistance);
        model.setHeartRate(-1);
      //  childAdapter.mainObserver.valueUpdated(model);
        Log.e("onDistanceValueChanged ", "" + totalDistance);
    }

    /*********************************
     *Resting Pedometer Service Stops here
     *********************************/
    public void RestingPedomenterServiceStop() {
        Log.i(TAG, "[SERVICE] Stop");
        if (mBaseService != null) {
            Log.i(TAG, "[SERVICE] stopService");
            unbindStepService();
            getActivity().stopService(new Intent(getActivity(), activeService));
        }
        mIsRunning = false;
        mPedometerSettings.saveServiceRunningWithTimestamp(mIsRunning);

        //stop timer
      //  mFitFragment = ((WalkingFragment) childAdapter.getRegisteredFragment(1));
      //  mFitFragment.mCountDownTimer.cancel();
    }

    private void unbindStepService() {
        Log.i(TAG, "[SERVICE] Unbind");
        getActivity().unbindService(serviceConnection2);
    }

    /************************************
     * Callback from PhSensorService.java
     * to stop the service
     */
    @Override
    public void stopSensorService() {
        Intent intent = new Intent(getActivity(), HeartRateSensorService.class);
        getActivity().stopService(intent);
        getActivity().unbindService(serviceConnection);

        //Get summary of test and send to phone once resting is complete
        //make resting flag false.
        double distance = ((SurveyActivity) getActivity()).getDistanceInMeter();
        int maxHeartRate = ((SurveyActivity) getActivity()).getMaxHeartRate();
        int avgHeartRate = ((SurveyActivity) getActivity()).getAvgHeartRate();
        String timDuration = ((SurveyActivity) getActivity()).getTestDuration();
        String locations = ((SurveyActivity) getActivity()).getLocations();
        long step = ((SurveyActivity) getActivity()).getTotalStepsCount();

        CastingData castingData = new CastingData(maxHeartRate, distance, timDuration, locations, avgHeartRate, true, false, (int)step);
        new WearMessageSenderAsync().execute(castingData);

        //fetchTestData();
    }

    private void showActivityConfirm() {
        WearUtils.grantVibration(getActivity());
        WearUtils.playDefaultSound(getActivity());

        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                ConfirmationActivity.SUCCESS_ANIMATION);
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                getString(R.string.data_saved));
        startActivityForResult(intent, 200);
    }

    /**
     * Navigate to summary screen
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (isCompleteWalktTest) {
                mIOnNextListener.onNextPressed(WearUtils.ScreenName.question_three.getScreen());//show 3 rd question 4
            } else {
                mIOnNextListener.onNextPressed(WearUtils.ScreenName.summary.getScreen()); //show summary screen 5
            }
        }
    }

    /*************************************
     * Create background task to fetch data
     * and create json files
     * <p>
     * Reading all listeners data in the background
     * </p>
     */
    private void fetchTestData() {
        Observable.fromCallable(() -> {
            showLoader("Collecting data. Please wait...");
            Log.e(TAG, "fromCallable()..1");

            Gson gson = new Gson();

            ((SurveyActivity) getActivity()).setRestMaxHeartRate(mMaxHeartRate);
            ((SurveyActivity) getActivity()).setRestAvgHeartRate(WearUtils.calculateHeartAverage(mHeartRateList));
            ((SurveyActivity) getActivity()).setTotalStepsCount(mFinalSteps);
            //2 Get Heart Rate data
            String heartRateJson = gson.toJson(heartRate, HeartRate.class);
            String[] data = {heartRateJson};
            long totalStepsCount = ((SurveyActivity) getActivity()).getTotalStepsCount();
            Log.e(TAG,"fetchTestData >>> "+totalStepsCount);
            //5 Get Pedometer
            //If Pedometer object is 0 then add atlease one dummy object to it
            if (mPedoItems.size() == 0) {
                Item item = new Item();
                item.setNumberOfSteps(0);
                item.setDistance(0d);
                mPedoItems.add(item);
            }
            mPedometer.setItems(mPedoItems);
            String pedoStepsJson1 = gson.toJson(mPedometer, Pedometer.class);
            Log.e(TAG, "pedoStepsJson()..6");
            String[] pedoStepsJson = {pedoStepsJson1};

            saveWalkTestData(data);
            saveStepsInResting(pedoStepsJson);
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    Log.e(TAG, "onPostExecute 10");
                    hideLoader();
                    showActivityConfirm();
                });
    }

    /****************************************
     *Save jsons in folder and keep this
     *folder name in golbal to store SurveyAnswer.json in same folder
     */
    private void saveWalkTestData(String[] data) {
        File uniqueAppFolder;
        String folderPathWalkTest = WearPrefSingleton.getInstance().getWalkTestFolderPath();
        if (TextUtils.isEmpty(folderPathWalkTest)) {
            uniqueAppFolder = WearUtils.createUniqueAppFolder(getActivity());
        } else {
            uniqueAppFolder = new File(folderPathWalkTest);
        }
        WearPrefSingleton.getInstance().saveWalkTestFolderPath(uniqueAppFolder.toString());
        int i = 0;
        for (String content : data) {
            ++i;
            saveProcess(uniqueAppFolder, content, i);
        }
    }

    /****************************************
     *Save jsons in folder and keep this
     *folder name in golbal to store SurveyAnswer.json in same folder
     */
    private void saveStepsInResting(String[] data) {
        Log.e(TAG,"saveStepsInResting >> ");
        File uniqueAppFolder;
        String folderPathWalkTest = WearPrefSingleton.getInstance().getWalkTestFolderPath();
        Log.e(TAG,"saveStepsInResting folder path >> "+folderPathWalkTest);
        if (TextUtils.isEmpty(folderPathWalkTest)) {
            uniqueAppFolder = WearUtils.createUniqueAppFolder(getActivity());
        } else {
            uniqueAppFolder = new File(folderPathWalkTest);
        }
        WearPrefSingleton.getInstance().saveWalkTestFolderPath(uniqueAppFolder.toString());
        int i = 0;
        for (String content : data) {
            ++i;
            saveRestingPedometer(uniqueAppFolder, content, i);
        }
    }

    protected Boolean saveProcess(File uniqueAppFolder, String answerJason, int pos) {

        String fileName = "";
        if (pos == 1) {
            fileName = "RestingheartRate.json";
        }
        File filePath = new File(uniqueAppFolder, fileName);
        try {
            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(answerJason);
            bw.close();
            Log.e(filePath.toString(), " Saved");
            Log.e("Data ", answerJason);
            return true;
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            return false;
        }
    }

    protected Boolean saveRestingPedometer(File uniqueAppFolder, String answerJason, int pos) {

        String fileName = "";
        if (pos == 1) {
            fileName = "Pedometer.json";
        }
        Log.e(TAG,"Data Resting Pedometer >> "+answerJason);
        File filePath = new File(uniqueAppFolder, fileName);
        String strFileJson = null;
        String pedometerConcat = null;
       // JsonArray array = new JsonArray();
        try {
            strFileJson = getStringFromFile(filePath.toString());

           // Log.e(TAG,"Data Pedometer to Merge >> "+strFileJson);
           /* JsonObject o1    = (JsonObject) new JsonParser().parse(strFileJson);
            JsonObject o2 = (JsonObject) new JsonParser().parse(answerJason);
            array.add(o1);
            array.add(o2);*/

            pedometerConcat = strFileJson + answerJason;
            Log.e(TAG,"Data Merged Pedometer >> "+pedometerConcat);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(pedometerConcat);
            bw.close();
            return true;
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            return false;
        }
    }

    public static String getStringFromFile(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        Log.e("ReastingFragment ","JSON getStringFromFile >> "+ret);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        Log.e("ReastingFragment ","JSON convertStreamToString >> "+sb.toString());
        return sb.toString();
    }
}

