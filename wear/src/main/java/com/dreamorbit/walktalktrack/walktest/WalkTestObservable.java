package com.dreamorbit.walktalktrack.walktest;

import com.dreamorbit.walktalktrack.model.ObservableModel;

import java.util.Observable;

/**
 * Created by mujasam.bn on 2/8/2018.
 */

public class WalkTestObservable extends Observable
{
    public void valueUpdated(ObservableModel heartRate)
    {
        setChanged();
        notifyObservers(heartRate);
    }
}