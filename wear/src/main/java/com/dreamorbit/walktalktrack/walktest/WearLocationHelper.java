package com.dreamorbit.walktalktrack.walktest;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.dreamorbit.walktalktrack.model.location.Coordinate;
import com.dreamorbit.walktalktrack.model.location.LocationData;
import com.dreamorbit.walktalktrack.model.location.LocationItem;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.ContentValues.TAG;
import static com.dreamorbit.walktalktrack.walktest.HeartRateSensorService.mIsStop;

/**
 * Created by nareshkumar.reddy on 9/27/2017.
 * Updated by Mujasam on 07/03/2019.
 */

public class WearLocationHelper implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    /**
     * Constant used in the location settings dialog.
     */
    private Location mCurrentLocation;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;

    public static final String TAG = WearLocationHelper.class.getSimpleName();
    private Activity mContext;
    private LocationData locationData = new LocationData();
    private Boolean mRequestingLocationUpdates;


    public WearLocationHelper(Activity context) {
        this.mContext = context;
        initGoogleSetup();
    }

    private void initGoogleSetup() {
        if (!isGooglePlayServicesAvailable()) {
            return;
        }
        // we build google api client
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, mContext, 0).show();
            return false;
        }
    }

    public LocationData getLocationDataOf6minWalk() {
        return locationData;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "Location Connected.");
        initLocationManager();
        createLocationCallback();
        createLocationRequest();
    }

    private void initLocationManager() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        mRequestingLocationUpdates = false;
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                Log.i("WearLocationHelper", "Location: " + mCurrentLocation.getLatitude() + " " + mCurrentLocation.getLongitude());
                LocationItem item = new LocationItem();
                Coordinate coordinate = new Coordinate();
                coordinate.setLatitude(mCurrentLocation.getLatitude());
                coordinate.setLongitude(mCurrentLocation.getLongitude());
                item.setCoordinate(coordinate);
                item.setAltitude(mCurrentLocation.getAltitude());
                item.setHorizontalAccuracy(mCurrentLocation.getAccuracy());
                item.setSpeed(mCurrentLocation.getSpeed());
                item.setCourse(0.0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    item.setVerticalAccuracy(mCurrentLocation.getVerticalAccuracyMeters());
                } else {
                    item.setVerticalAccuracy(0.0f);
                }
                item.setTimestamp(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
                //mDistanceLocationCallback.handleNewLocation(mCurrentLocation);
                locationData.addItems(item);
            }
        };
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    public void startLocationPeriodicUpdates() {
        mRequestingLocationUpdates = true;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(mContext, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                    }
                });
    }

    public void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(mContext, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
        Log.d("", "Location update stopped .......................");
    }
}
