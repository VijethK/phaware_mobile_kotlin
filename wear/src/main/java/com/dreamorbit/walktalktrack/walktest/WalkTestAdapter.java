package com.dreamorbit.walktalktrack.walktest;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * Created by mujasam.bn on 2/6/2018.
 */

public class WalkTestAdapter extends FragmentPagerAdapter {
    private Context ctxt = null;
    public SparseArray<Fragment> registeredChildFragments = new SparseArray<Fragment>();
    public WalkTestObservable mainObserver;

    public WalkTestAdapter(Context ctxt, FragmentManager mgr) {
        super(mgr);
        this.ctxt = ctxt;
        mainObserver = new WalkTestObservable();
    }

    @Override
    public int getCount() {
        return (2);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = WalkTestControlFragment.newInstance();
                //mainObserver.addObserver((WalkTestControlFragment)fragment);
                break;

            case 1:
                fragment = WalkingFragment.newInstance();
                mainObserver.addObserver((WalkingFragment)fragment);
                break;

        }
        return fragment;
    }

    /**
     * On each Fragment instantiation we are saving the reference of that Fragment in a Map
     * It will help us to retrieve the Fragment by mPosition
     *
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredChildFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Get the Fragment by mPosition
     *
     * @param position tab mPosition of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredChildFragments.get(position);
    }
}