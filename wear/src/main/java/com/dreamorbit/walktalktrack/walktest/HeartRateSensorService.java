package com.dreamorbit.walktalktrack.walktest;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by mujasam.bn on 2/8/2018.
 */

public class HeartRateSensorService extends Service implements SensorEventListener {
    private static final String LOG_TAG = "MyHeart";
    private static final String TAG = "PhSensorService";
    //Google Fit
    public static boolean mIsStop;
    private SensorManager mSensorManager;
    private int currentValue = 0;
    private IBinder binder = new HeartbeatServiceBinder();
    private OnChangeListener onChangeListener;
    private float mStepsTotal;

    @Override
    public void onCreate() {
        super.onCreate();
        initHeartrate();
    }

    /****************************************
     * Disconnect the Fitness client api
     ***************************************/
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this);
        mIsStop = false;
        Log.d(LOG_TAG, " sensor unregistered");
    }

    /***************************************
     * programming interface that clients can use to interact with the service.
     * @param intent
     * @return
     ***************************************/
    @Override
    public IBinder onBind(Intent intent) {
        mIsStop = false;
        return binder;
    }

    /***************************************
     * Init Hear rate sensor
     ***************************************/
    private void initHeartrate() {
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        boolean res = mSensorManager.registerListener(this, mHeartRateSensor, 10000);
    }

    /***************************************
     * Callback on Heart rate sensor
     ***************************************/
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // is this a heartbeat event and does it have data?
        if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE && sensorEvent.values.length > 0) {
            int newValue = Math.round(sensorEvent.values[0]);
//            int newValue = 60;
            //Log.d(LOG_TAG,sensorEvent.sensor.getName() + " changed to: " + newValue);
            // only do something if the value differs from the value before and the value is not 0.
            if (currentValue != newValue && newValue != 0) {
                // save the new value
                currentValue = newValue;
                // send the value to the listener
                if (onChangeListener != null && !mIsStop) {
                    //Log.d(LOG_TAG, "sending new value to listener: " + newValue);
                    onChangeListener.onHearBeatValueChanged(newValue);
                }
            }
        }
    }

    /***************************************
     * on accuracy changes
     ***************************************/
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    // interface to pass a heartbeat value to the implementing class
    public interface OnChangeListener {
        void onHearBeatValueChanged(int hearRate);

        void stopSensorService();
    }

    /**
     * Binder for this service. The binding activity passes a listener we send the heartbeat to.
     */
    public class HeartbeatServiceBinder extends Binder {
        public void setChangeListener(OnChangeListener listener) {
            onChangeListener = listener;
            listener.onHearBeatValueChanged(currentValue);
        }

        HeartRateSensorService getService() {
            // Return this instance of LocalService so clients can call public methods
            return HeartRateSensorService.this;
        }
    }
}