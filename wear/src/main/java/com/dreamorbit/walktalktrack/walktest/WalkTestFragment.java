package com.dreamorbit.walktalktrack.walktest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.ProgressSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.IPedometerCallback;
import com.dreamorbit.pedometer.SensorChecker;
import com.dreamorbit.pedometer.accelerometer.AcceleroMeterService;
import com.dreamorbit.pedometer.accelerometer.PedometerSettings;
import com.dreamorbit.pedometer.accelerometer.PedometerUtils;
import com.dreamorbit.pedometer.pojo.pedometer.Item;
import com.dreamorbit.pedometer.pojo.pedometer.Pedometer;
import com.dreamorbit.pedometer.stepsensor.StepSensorService;
import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.CurrentTime;
import com.dreamorbit.walktalktrack.model.ObservableModel;
import com.dreamorbit.walktalktrack.model.TotalStepCount;
import com.dreamorbit.walktalktrack.model.devicemotion.GyroResponse;
import com.dreamorbit.walktalktrack.model.heart.HeartRate;
import com.dreamorbit.walktalktrack.model.location.LocationData;
import com.dreamorbit.walktalktrack.model.report.ReportRequest;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.screens.SurveyActivity;
import com.dreamorbit.walktalktrack.screens.SurveyBaseFragment;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.SessionInsertRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rd.PageIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync.REPORT_SUMMARY_MESSAGE;
import static com.dreamorbit.walktalktrack.walktest.WalkingFragment.isCompleteWalktTest;

public class WalkTestFragment extends SurveyBaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "WalkTestFragment";
    private static final int STEPS_MSG = 1;
    private static final int PACE_MSG = 2;
    private static final int DISTANCE_MSG = 3;
    private static final int SPEED_MSG = 4;
    private static final int CALORIES_MSG = 5;
    private static final int HEART_RATE_MSG = 6;
    protected final String AUTH_PENDING = "auth_state_pending";

    //Timer
    public WalkTestAdapter childAdapter;
    public double mFinalDistance = 0;
    public int mLastHeartRate;
    protected boolean mIsBound = false;
    protected ServiceConnection serviceConnection;
    protected long remainingTime;
    //Location Fetch
    protected WearLocationHelper mLocationHelper;
    //DeviceMotion
    protected DeviceMotion mDeviceMotion;
    private WalkingFragment mFitFragment;

    //Pedometer data
    private Pedometer mPedometer = new Pedometer();
    private List<Integer> mHeartRateList = new ArrayList();
    private int mMaxHeartRate;
    private ProgressSpinner mIndeterminateBar;
    private Map<String, Object> updates = new HashMap<String, Object>();
    private CountDownTimer mCountDownTimerJson;
    private List<Item> mPedoItems = new ArrayList<>();
    private String lastFetchedDate = "";
    private long startTime, stopTime;

    //Pedometer
    private PedometerUtils mUtils;
    private PedometerSettings mPedometerSettings;
    private boolean mIsRunning;
    private GoogleApiClient mGoogleApiClient;
    private Class<?> activeService;
    private SensorChecker.SensorType nSensorType;
    private BaseSensorService mBaseService;
    public int mFinalSteps;
    private List<com.dreamorbit.pedometer.pojo.pedometer.Item> mPedoItemsTemp = new ArrayList<>();

    //Handler - update
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STEPS_MSG:
                    int mStepValue = msg.arg1;
                    mFinalSteps = mStepValue;
                    break;

                case DISTANCE_MSG:
                    float mDistanceValue = msg.arg1;
                    if (mDistanceValue <= 0) {
                        onDistanceValueChanged(mDistanceValue);
                    } else {
                        String finalDistance = String.format("%.0f", mDistanceValue);
                        mFinalDistance = Double.parseDouble(finalDistance);
                        onDistanceValueChanged(mFinalDistance);
                    }
                    break;

                case HEART_RATE_MSG:
                    onHearBeatValueChanged(msg.arg1);
                    break;

                default:
                    super.handleMessage(msg);
            }

            //todo remove after test
            com.dreamorbit.pedometer.pojo.pedometer.Item item = new com.dreamorbit.pedometer.pojo.pedometer.Item();
            item.setNumberOfSteps(mFinalSteps);
            item.setDistance(mFinalDistance);
            item.setStartDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
            mPedoItemsTemp.add(item);
        }
    };

    // TODO: unite all into 1 type of message
    private IPedometerCallback mCallback = new IPedometerCallback() {
        public void stepsChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }

        public void distanceChanged(float value) {
            mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG, (int) (value), 0));
        }

        public void onHearBeatValueChanged(int hearRate) {
            mHandler.sendMessage(mHandler.obtainMessage(HEART_RATE_MSG, hearRate, 0));
        }

        @Override
        public void onDistanceListFetched(List<com.dreamorbit.pedometer.pojo.pedometer.Item> items) {
            mPedoItems = items;
            fetchTestData();
            //new FetchTotalStepsAsync().execute();
        }
    };

    /*
     * Bind to our service.
     * ComponentName heartService = startService(new Intent(WearActivity.this, HeartbeatService.class));
     */
    public static WalkTestFragment newInstance(List<Questions> question, int position, ViewPager viewPager, String activityTitle) {
        WalkTestFragment fragment = new WalkTestFragment();
        Bundle args = new Bundle();
        args.putInt(WearConstant.POSITION, position);
        args.putString(WearConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(WearConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
        if (serviceConnection == null) {
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder binder) {
                    Log.d(TAG, "connected to service.");
                    mBaseService = ((BaseSensorService.StepBinder) binder).getService();
                    mBaseService.registerCallback(mCallback);
                    //mService.reloadSettings();
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    if (mBaseService != null) {
                        mBaseService.unregisterListener();
                    }
                    mBaseService = null;
                }
            };
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_walk_pager, container, false);
        if (savedInstanceState != null) {
            //authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        } else {
            initViews(rootView);
            buildAdapter(rootView);
            initDeviceMotion();
            initLocationHelper();
            mUtils = PedometerUtils.getInstance();
        }
        return rootView;
    }

    private void initViews(View rootView) {
        mIndeterminateBar = rootView.findViewById(R.id.indeterminateBar);
    }

    /************************************
     * Nested child fragment and viewpager
     * to control 6MWT
     * @param rootView
     */
    private void buildAdapter(View rootView) {
        Log.e("WalkTestFragment ", "" + mPosition);
        ViewPager pager = rootView.findViewById(R.id.pager);
        childAdapter = new WalkTestAdapter(getActivity(), getChildFragmentManager());
        pager.setAdapter(childAdapter);
        PageIndicatorView pageIndicatorView = rootView.findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setViewPager(pager);
        pager.setCurrentItem(2);
        pageIndicatorView.setSelection(2);
    }

    /*********************************
     *Pedometer Service Starts here
     *********************************/

    public void WalkTestServiceStart() {
        WearUtils.grantVibration(getActivity());
        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPedometerSettings = new PedometerSettings(mSettings);
        mIsRunning = mPedometerSettings.isServiceRunning();

        // Start the service if this is considered to be an application start (last onPause was long ago)
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Started First time");
            mFitFragment = ((WalkingFragment) childAdapter.getRegisteredFragment(1));
            checkSensor();
            startStepService();
            bindStepService();
            initFitApiClient();
            getStartTime();
        } else if (mIsRunning) {
            Log.i(TAG, "[SERVICE] Running already");
            bindStepService();
        }
    }

    private void initDeviceMotion() {
        mDeviceMotion = new DeviceMotion(getActivity());
    }

    private void initLocationHelper() {
        mLocationHelper = new WearLocationHelper(getActivity());
    }

    private void checkSensor() {
        SensorChecker sensorChecker = new SensorChecker(getActivity());
        nSensorType = sensorChecker.getPedometerSensor();
        switch (nSensorType) {
            case ACCELEROMETER:
                activeService = AcceleroMeterService.class;
                break;
            case STEP:
                activeService = StepSensorService.class;
                break;
        }
    }

    private void startStepService() {
        if (!mIsRunning) {
            Log.i(TAG, "[SERVICE] Start");
            mIsRunning = true;
            startLocationHelper(); // start the location fetch task
            startDeviceMotion(); // start device motion
            getActivity().startService(new Intent(getActivity(), activeService));
        }
    }

    private void bindStepService() {
        Log.i(TAG, "[SERVICE] Bind");
        mIsBound = getActivity().bindService(new Intent(getActivity(), activeService), serviceConnection, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }

    public void walkTestServiceStop() {
        Log.i(TAG, "[SERVICE] Stop");
        if (mBaseService != null) {
            Log.i(TAG, "[SERVICE] stopService");
            unbindStepService();
            getActivity().stopService(new Intent(getActivity(), activeService));
        }
        mIsRunning = false;
        mPedometerSettings.saveServiceRunningWithTimestamp(mIsRunning);

        //stop timer
        mFitFragment = ((WalkingFragment) childAdapter.getRegisteredFragment(1));
        mFitFragment.mCountDownTimer.cancel();
    }

    private void unbindStepService() {
        Log.i(TAG, "[SERVICE] Unbind");
        getActivity().unbindService(serviceConnection);
    }

    private long getStartTime() {
        Calendar mCalendar = Calendar.getInstance();
        Date now = new Date();
        mCalendar.setTime(now);
        startTime = mCalendar.getTimeInMillis();
        Log.e("6MWT Start Time", " " + startTime);
        return startTime;
    }

    private long getStopTime() {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        stopTime = cal.getTimeInMillis();
        return stopTime;
    }

    /*********************************
     *6MWT Pause Resume controller click event
     *********************************/
    public void WalkTestServicePauseResume(boolean isPaused) {
        if (isPaused) {
            if (mIsBound) {
                mBaseService.pauseWalkTest();
                stopLocationHelper();
                stopDeviceMotion();
                mIsBound = false;
            }
        } else {
            mBaseService.resumeWalkTest();
            startLocationHelper();
            startDeviceMotion();
            mIsBound = true;
        }
    }

    /***************************************
     * Timer pause and stop implementation
     * @param isPaused
     **************************************/
    protected void handleTimer(boolean isPaused) {
        mFitFragment = ((WalkingFragment) childAdapter.getRegisteredFragment(1));
        if (isPaused) {
            mFitFragment.mCountDownTimer.cancel();
        } else {
            mFitFragment.seconds = remainingTime; //asign the last time pasused time to this variable
            mFitFragment.startTimer(); //restart the timer from last timer position
        }
    }

    public void onHearBeatValueChanged(int newValue) {
        ObservableModel model = new ObservableModel();
        model.setDistance(-1);
        model.setHeartRate(newValue);
        childAdapter.mainObserver.valueUpdated(model);

        //to check the Average HeartRate
        mHeartRateList.add(newValue);

        //to check the Max HearRate
        if (newValue != 0 && newValue > mMaxHeartRate) {
            mMaxHeartRate = newValue;
        }
        mLastHeartRate = newValue;
    }


    /****************************************
     * Callback from PhSensorService.java for Distance
     * value change
     * @param totalDistance
     */
    public void onDistanceValueChanged(double totalDistance) {
        ObservableModel model = new ObservableModel();
        model.setDistance(totalDistance);
        model.setHeartRate(-1);
        childAdapter.mainObserver.valueUpdated(model);
        Log.e("onDistanceValueChanged ", "" + totalDistance);
    }

    /*************************************
     *DeviceMotion Updated Starts here
     */
    public void startDeviceMotion() {
        mDeviceMotion.readDeviceMotion();
    }

    /*************************************
     *Stop DeviceMotion Updates
     */

    public void stopDeviceMotion() {
        mDeviceMotion.stopDeviceMotion();
    }

    /************************************
     *Location Updated starts
     */
    public void startLocationHelper() {
        mLocationHelper.startLocationPeriodicUpdates();
    }

    /************************************
     *Stop Location Updated
     */
    public void stopLocationHelper() {
        mLocationHelper.stopLocationUpdates();
    }

    private void initFitApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .useDefaultAccount() //Use the default account
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Save Summary data in preference
     */
    private void saveSummaryData() {

        //Get summary data
        double distance = ((SurveyActivity) getActivity()).getDistanceInMeter();
        String testDuration = ((SurveyActivity) getActivity()).getTestDuration();
        long totalStepsCount = ((SurveyActivity) getActivity()).getTotalStepsCount();
        int maxHeartRate = ((SurveyActivity) getActivity()).getMaxHeartRate();
        int avgHeartRate = ((SurveyActivity) getActivity()).getAvgHeartRate();
        int floorAscended = 0;

        ReportRequest request = new ReportRequest(null, avgHeartRate, maxHeartRate, distance, totalStepsCount, testDuration, WearUtils.getTestTakenDateTime(),floorAscended);

        //get previous summary data
        Gson gsonObj = new Gson();
        List<ReportRequest> summaryList = new ArrayList<>();
        String summary = WearPrefSingleton.getInstance().getTestSummary();
        if (!TextUtils.isEmpty(summary)) {
            Type type = new TypeToken<List<ReportRequest>>() {
            }.getType();
            summaryList = gsonObj.fromJson(summary, type);
        }

        summaryList.add(request);


        String summaryString = gsonObj.toJson(summaryList);
        Log.e(TAG, "Wear Summary " + summaryList);
        JSONArray testObject = null;
        JSONObject jsonObj = null;
        try {
            testObject = new JSONArray(summaryString);
            for (int i = 0; i < testObject.length(); i++)
            {
                jsonObj = testObject.getJSONObject(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonObj.remove("id");

        //Change json key value string data(report will not work if we change)
       /* try {
            jsonObj.put("Total floor ascended",jsonObj.get("floorAscended"));
            jsonObj.remove("floorAscended");
            jsonObj.put("Average heart rate",jsonObj.get("average_heart_rate"));
            jsonObj.remove("average_heart_rate");
            jsonObj.put("Total distance",jsonObj.get("distance"));
            jsonObj.remove("distance");
            jsonObj.put("Test duration",jsonObj.get("duration_of_test"));
            jsonObj.remove("duration_of_test");
            jsonObj.put("Max heart rate",jsonObj.get("max_heart_rate"));
            jsonObj.remove("max_heart_rate");
            jsonObj.put("Total number of steps",jsonObj.get("steps_count"));
            jsonObj.remove("steps_count");
            jsonObj.put("Test taken time",jsonObj.get("test_taken_at"));
            jsonObj.remove("test_taken_at");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG,testObject.toString());*/
        summaryString = testObject.toString();

        //Save the summary data in jason
        WearPrefSingleton.getInstance().saveTestSummary(summaryString);
        new WearMessageSenderAsync().execute(null, REPORT_SUMMARY_MESSAGE);

        //insertStepsToGoogleFit(totalStepsCount);
        insertSessionToGoogleFit(request);
    }

    private void insertSessionToGoogleFit(ReportRequest castingData) {

        Log.i(TAG, "Creating a new session for an 6 minute walk");
        Session mSession = new Session.Builder()
                .setName("6 minute walk test Wear")
                .setIdentifier(getString(R.string.app_name) + " " + System.currentTimeMillis())
                .setDescription("Walking Session Details")
                .setStartTime(startTime, TimeUnit.MILLISECONDS)
                .setEndTime(getStopTime(), TimeUnit.MILLISECONDS)
                .setActivity(FitnessActivities.WALKING)
                .build();

        //Distance
        DataSource distanceSegmentDataSource = new DataSource.Builder()
                .setAppPackageName(getActivity().getPackageName())
                .setDataType(DataType.AGGREGATE_DISTANCE_DELTA)
                .setName("Total Distance Covered Wear")
                .setType(DataSource.TYPE_RAW)
                .build();
        DataSet distanceDataSet = DataSet.create(distanceSegmentDataSource);

        DataPoint firstRunSpeedDataPoint = distanceDataSet.createDataPoint()
                .setTimeInterval(startTime, stopTime, TimeUnit.MILLISECONDS);
        firstRunSpeedDataPoint.getValue(Field.FIELD_DISTANCE).setFloat((float) (castingData.getDistance()));
        distanceDataSet.add(firstRunSpeedDataPoint);

        //Steps
        DataSource stepsDataSource = new DataSource.Builder()
                .setAppPackageName(getActivity().getPackageName())
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setName("Total Steps")
                .setType(DataSource.TYPE_RAW)
                .build();
        DataSet stepsDataSet = DataSet.create(stepsDataSource);
        DataPoint stepsDataPoint = stepsDataSet.createDataPoint()
                .setTimeInterval(startTime, stopTime, TimeUnit.MILLISECONDS);
        stepsDataPoint.getValue(Field.FIELD_STEPS).setInt((int) castingData.getSteps_count());
        stepsDataSet.add(stepsDataPoint);

        //Build SessionInsertRequest
        SessionInsertRequest insertRequest = new SessionInsertRequest.Builder()
                .setSession(mSession)
                .addDataSet(distanceDataSet)
                .addDataSet(stepsDataSet)
                .build();

        //Inserting to Fit
        Fitness.getSessionsClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity()))
                .insertSession(insertRequest)
                .addOnSuccessListener(aVoid -> {
                    // At this point, the session has been inserted and can be read.
                    Log.i(TAG, "Session insert was successful!");
                })
                .addOnFailureListener(e -> {
                    //failure
                    Log.i(TAG, "There was a problem inserting the session: " + e.getLocalizedMessage());
                });
        // [END insert_session]

        Log.e(TAG, "**Test Data Pushed to Wear Fit**");
    }

    private void insertStepsToGoogleFit(long step) {
        // Create a data source
        DataSource dataSource =
                new DataSource.Builder()
                        .setAppPackageName(getActivity())
                        .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .setStreamName("Steps")
                        .setType(DataSource.TYPE_RAW)
                        .build();

        // Create a data set
        DataSet dataSet = DataSet.create(dataSource);
        // For each data point, specify a start time, end time, and the data value -- in this case,
        // the number of new steps.
        DataPoint dataPoint =
                dataSet.createDataPoint().setTimeInterval(startTime, getStopTime(), TimeUnit.MILLISECONDS);
        dataPoint.getValue(Field.FIELD_STEPS).setInt((int) step);
        dataSet.add(dataPoint);
        Task response = Fitness.getHistoryClient(getActivity(), GoogleSignIn.getLastSignedInAccount(getActivity())).insertData(dataSet);
        Log.e(TAG, "**Step Pushed to Fit**");
    }

    private void showActivityConfirm() {
        WearUtils.grantVibration(getActivity());
        WearUtils.playDefaultSound(getActivity());

        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                ConfirmationActivity.SUCCESS_ANIMATION);
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                getString(R.string.data_saved));
        startActivityForResult(intent, 200);
    }

    /**
     * Navigate to summary screen
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            mIOnNextListener.onNextPressed(WearUtils.ScreenName.resting.getScreen());//show resting screen
        }
    }

    /****************************************
     *Save jsons in folder and keep this
     *folder name in golbal to to store SurveyAnswer.json in same folder
     */
    private void saveWalkTestData(String[] data) {
        File uniqueAppFolder = WearUtils.createUniqueAppFolder(getActivity());
        WearPrefSingleton.getInstance().saveWalkTestFolderPath(uniqueAppFolder.toString());
        int i = 0;
        for (String content : data) {
            ++i;
            Log.e(TAG, "saveWalkTestData(): " + i + "\n Path: " + uniqueAppFolder + "\n Data: " + content);
            saveProcess(uniqueAppFolder, content, i);
        }
    }

    protected Boolean saveProcess(File uniqueAppFolder, String answerJason, int pos) {

        String fileName = "";
        if (pos == 1) {
            fileName = "WalkingHeartRate.json";
        } else if (pos == 2) {
            fileName = "CurrentTime.json";
        } else if (pos == 3) {
            fileName = "DeviceMotion.json";
        } else if (pos == 4) {
            fileName = "Location.json";
        } else if (pos == 5) {
            fileName = "StepCountOfDay.json";
        } else if (pos == 6) {
            fileName = "Pedometer.json";
        }  else if (pos == 7) {
            fileName = "Summary.json";
        }

        File filePath = new File(uniqueAppFolder, fileName);
        try {
            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(answerJason);
            bw.close();
            Log.e(filePath.toString(), " Saved");
            return true;
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
            return false;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed" + connectionResult.toString());
        Log.i(TAG, "onConnectionFailed:" + connectionResult.getErrorCode() + "," + connectionResult.getErrorMessage());
    }

    private class FetchTotalStepsAsync extends AsyncTask<Object, Object, Long> {
        protected Long doInBackground(Object... params) {
            long total = 0;
            PendingResult<DailyTotalResult> result = Fitness.HistoryApi.readDailyTotal(mGoogleApiClient, DataType.TYPE_STEP_COUNT_DELTA);
            DailyTotalResult totalResult = result.await(30, TimeUnit.SECONDS);
            if (totalResult.getStatus().isSuccess()) {
                DataSet totalSet = totalResult.getTotal();
                if (totalSet != null) {
                    total = totalSet.isEmpty()
                            ? 0
                            : totalSet.getDataPoints().get(0).getValue(Field.FIELD_STEPS).asInt();

                    //Log.w(TAG, "Total Dyas Step." + total);
                    float mTotalStepForDay = total;
                }
            } else {
                Log.w(TAG, "There was a problem getting the step count.");
            }
            return total;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
            Log.i(TAG, "Total steps: " + aLong);
            fetchTestData();
        }
    }

    /*************************************
     * Create background task to fetch data
     * and create json files
     * <p>
     * Reading all listeners data in the background
     * </p>
     */
    private void fetchTestData() {
        Observable.fromCallable(() -> {
            //showLoader("Collecting data. Please wait...");
            Log.e(TAG, "fromCallable()..1");
            Gson gson = new Gson();

            //1 Get CurrentTime
            String dateTime = WearUtils.getCurrentUTCDateTimeWithMillSeconds();
            CurrentTime curentTime = new CurrentTime();
            curentTime.setCurrentTime(dateTime);
            String curentTimeJson = gson.toJson(curentTime, CurrentTime.class);
            Log.e(TAG, "dateTime()..2");

            //2 Get Heart Rate data
            String heartRateJson = gson.toJson(mFitFragment.heartRate, HeartRate.class);
            Log.e(TAG, "heartRateJson()..3");

            //3 Get Device Motion
            GyroResponse deviceMotion = mDeviceMotion.getDeviceMotionData();
            String deviceJson = gson.toJson(deviceMotion, GyroResponse.class);
            Log.e(TAG, "deviceMotion()..4");

            //4 Get Location data
            LocationData locationData = mLocationHelper.getLocationDataOf6minWalk();
            String locationJson = gson.toJson(locationData, LocationData.class);
            Log.e(TAG, "locationData()..5");

            //5 Get Pedometer
            //If Pedometer object is 0 then add atlease one dummy object to it
            if (mPedoItems.size() == 0) {
                Item item = new Item();
                item.setNumberOfSteps(0);
                item.setDistance(0d);
                mPedoItems.add(item);
            }
            mPedometer.setItems(mPedoItems);
            String pedoStepsJson = gson.toJson(mPedometer, Pedometer.class);
            Log.e(TAG, "pedoStepsJson()..6");

            //6 Get stepCount of the day
            String totalStepCountJson = saveTotalStepsForTheDay(); //Save step count for the day
            Log.e(TAG, "totalStepCount()..7" + totalStepCountJson);

            String timDuration;
            if (isCompleteWalktTest) {
                timDuration = "6m : " + "00s";
            } else {
                long secondsLimit = (360000 - remainingTime) / 1000;
                timDuration = String.format("%02d", secondsLimit / 60) + "m : " + String.format("%02d", secondsLimit % 60) + "s";
            }

            ((SurveyActivity) getActivity()).setDistanceInMeter(mFinalDistance);
            ((SurveyActivity) getActivity()).setMaxHeartRate(mMaxHeartRate);
            ((SurveyActivity) getActivity()).setAvgHeartRate(WearUtils.calculateHeartAverage(mHeartRateList));
            ((SurveyActivity) getActivity()).setTestDuration(timDuration);
            ((SurveyActivity) getActivity()).setLocations(locationJson);
            ((SurveyActivity) getActivity()).setTotalStepsCount(mFinalSteps);

            saveSummaryData();

            //Get Summary
            String summary = WearPrefSingleton.getInstance().getTestSummary();
            Log.e(TAG,"test summary >> "+summary);

            String[] data = {heartRateJson, curentTimeJson, deviceJson, locationJson, totalStepCountJson, pedoStepsJson, summary};
            saveWalkTestData(data);
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    Log.e(TAG, "onPostExecute 10");
                    hideLoader();
                    showActivityConfirm();
                });
    }

    // return the steps for the day
    private String saveTotalStepsForTheDay() {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Gson gson = new Gson();
        float totalSteps = mFinalSteps;

        //To save in preference use another model with date column
        String lastSavedData = WearPrefSingleton.getInstance().getTotalStepsForTheDay();
        TotalStepCountPreference lastPreferenceData = gson.fromJson(lastSavedData, TotalStepCountPreference.class);
        if (lastPreferenceData == null) {
            lastPreferenceData = new TotalStepCountPreference();
            lastPreferenceData.setDateStamp(fmt.format(new Date()));
            lastPreferenceData.setTotalStepsInDay(totalSteps);
        } else {
            try {
                //check if current date is same as last saved date
                //If yes get the last step value and add it with current steps value
                Date lastSavedDate = fmt.parse(lastPreferenceData.getDateStamp());
                Date currentDate = fmt.parse(fmt.format(new Date()));
                if (lastSavedDate.compareTo(currentDate) == 0) //check last saved date is not today
                    totalSteps = totalSteps + lastPreferenceData.getTotalStepsInDay();
                lastPreferenceData.setDateStamp(fmt.format(new Date()));
                lastPreferenceData.setTotalStepsInDay(totalSteps);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //Save the latest steps count in preference
        WearPrefSingleton.getInstance().saveTotalStepsForTheDay(gson.toJson(lastPreferenceData));

        //Finally return the actual steps count for the day value to save and upload to server
        TotalStepCount totalStepCount = new TotalStepCount();
        totalStepCount.setTotalStepsInDay("" + lastPreferenceData.getTotalStepsInDay());
        String totalStepCountJson = gson.toJson(totalStepCount, TotalStepCount.class);

        return totalStepCountJson;
    }

    private class TotalStepCountPreference {

        private String dateStamp;
        private float totalStepsInDay;

        public String getDateStamp() {
            return dateStamp;
        }

        public void setDateStamp(String dateStamp) {
            this.dateStamp = dateStamp;
        }

        public float getTotalStepsInDay() {
            return totalStepsInDay;
        }

        public void setTotalStepsInDay(float totalStepsInDay) {
            this.totalStepsInDay = totalStepsInDay;
        }
    }
}
