package com.dreamorbit.walktalktrack.walktest;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.devicemotion.Gravity;
import com.dreamorbit.walktalktrack.model.devicemotion.GyroResponse;
import com.dreamorbit.walktalktrack.model.devicemotion.Item;
import com.dreamorbit.walktalktrack.model.devicemotion.MagneticField;
import com.dreamorbit.walktalktrack.model.devicemotion.RotationRate;
import com.dreamorbit.walktalktrack.model.devicemotion.UserAcceleration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.dreamorbit.walktalktrack.walktest.HeartRateSensorService.mIsStop;

/**
 * Created by nareshkumar.reddy on 12/21/2017.
 */

public class DeviceMotion {

    SensorManager sensorManager;
    GyroResponse gyroResponse;
    public SensorEventListener gyroListener = new SensorEventListener() {

        private MagneticField magneticField = new MagneticField();
        private Gravity gravity = new Gravity();
        private UserAcceleration userAcceleration = new UserAcceleration();
        private RotationRate rotationRate = new RotationRate();

        public void onAccuracyChanged(Sensor sensor, int acc) {

        }

        public void onSensorChanged(SensorEvent event) {
            Sensor sensor = event.sensor;
            float pressure_value = 0.0f;
            float height = 0.0f;
           /* if( Sensor.TYPE_PRESSURE == event.sensor.getType() ) {
                pressure_value = event.values[0];
                height = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, pressure_value);
                Toast.makeText(PhWearApplication.getMyAppContext(), "" + height, Toast.LENGTH_SHORT).show();
            }*/
            if (sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
                rotationRate = new RotationRate();
                rotationRate.setX(event.values[0]);
                rotationRate.setY(event.values[1]);
                rotationRate.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                userAcceleration = new UserAcceleration();
                userAcceleration.setX(event.values[0]);
                userAcceleration.setY(event.values[1]);
                userAcceleration.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_GRAVITY) {
                gravity = new Gravity();
                gravity.setX(event.values[0]);
                gravity.setY(event.values[1]);
                gravity.setZ(event.values[2]);
            }
            if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                magneticField = new MagneticField();
                magneticField.setX(event.values[0]);
                magneticField.setY(event.values[1]);
                magneticField.setZ(event.values[2]);
            }

            // if (magneticField != null && gravity != null && userAcceleration != null && rotationRate != null) {

            if (!mIsStop) {
                Item item = new Item();
                item.setTimestamp(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
                item.setGravity(gravity);
                item.setMagneticField(magneticField);
                item.setRotationRate(rotationRate);
                item.setUserAcceleration(userAcceleration);
                gyroResponse.addItems(item);

                //logMotion(gravity,magneticField,rotationRate,userAcceleration);

            } else {
                //logMotion(gravity,magneticField,rotationRate,userAcceleration);
            }


            //}
        }
    };
    private ArrayList<Sensor> sensors = new ArrayList<>();

    public DeviceMotion(Context context) {

        gyroResponse = new GyroResponse();

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE));
    }

    public void readDeviceMotion() {
        for (Sensor sensor : sensors) {
            sensorManager.registerListener(gyroListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void stopDeviceMotion() {
        sensorManager.unregisterListener(gyroListener);
    }

    public GyroResponse getDeviceMotionData() {
        if (gyroResponse.getItems().size() > 100) {
            List<Item> list = gyroResponse.getItems().subList(0, 100);
            gyroResponse.setItems(list);
        }
        return gyroResponse;
    }

    private void logMotion(Gravity gravity, MagneticField magneticField, RotationRate rotationRate, UserAcceleration userAcceleration) {

        Log.i("MF", "" + magneticField.getX());
        Log.i("MF", "" + magneticField.getY());
        Log.i("MF", "" + magneticField.getZ());

        Log.i("UA", "" + userAcceleration.getX());
        Log.i("UA", "" + userAcceleration.getY());
        Log.i("UA", "" + userAcceleration.getZ());

        Log.i("GY", "" + gravity.getX());
        Log.i("GY", "" + gravity.getY());
        Log.i("GY", "" + gravity.getZ());

        Log.i("RV", "" + rotationRate.getX());
        Log.i("RV", "" + rotationRate.getY());
        Log.i("RV", "" + rotationRate.getZ());
    }

}
