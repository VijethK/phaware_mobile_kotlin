package com.dreamorbit.walktalktrack.walktest;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;
import com.dreamorbit.walktalktrack.helper.PermissionUtil;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.CastingData;
import com.dreamorbit.walktalktrack.model.ObservableModel;
import com.dreamorbit.walktalktrack.model.heart.HeartRate;
import com.dreamorbit.walktalktrack.model.heart.HeartRateItem;
import com.dreamorbit.walktalktrack.screens.SurveyActivity;
import com.dreamorbit.walktalktrack.screens.SurveyBaseFragment;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by mujasam.bn on 12/19/2017.
 */

public class WalkingFragment extends SurveyBaseFragment implements Observer {

    public static final String TAG = "WalkingFragment";
    public static boolean isCompleteWalktTest;
    protected final int REQUEST_CODE_PERMISSION_BODY_SENSOR = 0;
    public HeartRate heartRate = new HeartRate();
    //Timer
    public CountDownTimer mCountDownTimer;
    public long seconds = 360000;
    //public long seconds = 10000;
    //FitFragment Views
    private TextView tvDistance;
    private TextView tvHeartRate;
    private TextView tvTimer;
    private WalkTestFragment mParentFragment;
    private String mMinuteTimer;

    public static WalkingFragment newInstance() {
        WalkingFragment frag = new WalkingFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return (frag);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentFragment = (WalkTestFragment) getParentFragment();
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_walktest, container, false);
        Log.e("Fit Position ", "" + mPosition);
        initView(rootView);
        if (savedInstanceState != null) {
            //authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        } else {
        }
        return rootView;
    }

    private void initView(View rootView) {
        tvDistance = rootView.findViewById(R.id.tvDistance);
        tvHeartRate = rootView.findViewById(R.id.tvHeartRate);
        tvTimer = rootView.findViewById(R.id.tvTimer);
        ImageView imgHeart = rootView.findViewById(R.id.imgHeart);
        WearUtils.heartAnimator(imgHeart);
    }

    public void registerSensors() {
        boolean hasPermission = PermissionUtil.hasPermission(getActivity(), Manifest.permission.BODY_SENSORS);
        if (hasPermission) {
            startTimer();
            mParentFragment.WalkTestServiceStart();
            mParentFragment.startDeviceMotion(); // start device motion
            mParentFragment.startLocationHelper(); // start the location fetch task
        } else {
            PermissionUtil.requestPermission(getActivity(), REQUEST_CODE_PERMISSION_BODY_SENSOR, Manifest.permission.BODY_SENSORS, null);
        }
    }

    /*****************************************************************************************************************
     *
     *  Step Count Subscription and Reading the Data
     *
     *****************************************************************************************************************/

    public void startTimer() {
        mCountDownTimer = new CountDownTimer(seconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mParentFragment.remainingTime = millisUntilFinished;
                long secondsLimit = millisUntilFinished / 1000;
                mMinuteTimer = String.format("%02d", secondsLimit / 60) + ":" + String.format("%02d", secondsLimit % 60);
                Log.e("Time ", mMinuteTimer);
                tvTimer.setText(mMinuteTimer);

                //Create casting data model to send phone
                CastingData castingData = new CastingData(mParentFragment.mLastHeartRate, mParentFragment.mFinalDistance, mMinuteTimer, null, 0, false, false, mParentFragment.mFinalSteps);
                Log.e(TAG, castingData.toString());
                new WearMessageSenderAsync().execute(castingData);
            }

            @Override
            public void onFinish() {
                tvTimer.setText("00:00");
                mParentFragment.walkTestServiceStop();
                mParentFragment.stopLocationHelper();
                mParentFragment.stopDeviceMotion();
                isCompleteWalktTest = true;
            }
        };
        mCountDownTimer.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions[0].equals(Manifest.permission.BODY_SENSORS) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            registerSensors();
        }
    }

    @Override
    public void update(Observable observable, Object arg) {
        ObservableModel model = (ObservableModel) arg;
        if (model.getDistance() > -1) {
            System.out.println("Distance UI " + model.getDistance());
            tvDistance.setText("" + String.format("%.0f", model.getDistance()));
        } else if (model.getHeartRate() > -1) {
            System.out.println("Heart UI " + model.getHeartRate());
            tvHeartRate.setText("" + model.getHeartRate());
            setHeartRate(model.getHeartRate());
        }
    }

    private void setHeartRate(int arg) {
        HeartRateItem item = new HeartRateItem();
        item.setStartDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
        item.setEndDate(WearUtils.getCurrentUTCDateTimeWithMillSeconds());
        item.setHeartRate(arg + " BPM");
        heartRate.setItems(item);
    }
}
