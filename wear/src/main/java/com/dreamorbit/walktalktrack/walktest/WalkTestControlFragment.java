package com.dreamorbit.walktalktrack.walktest;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.screens.SurveyActivity;
import com.dreamorbit.walktalktrack.screens.SurveyBaseFragment;

import java.util.Observable;
import java.util.Observer;

import static com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync.EMERGENCY_CALL_MESSAGE;
import static com.dreamorbit.walktalktrack.helper.WearConstant.EMERGENCY_NUMBER;
import static com.dreamorbit.walktalktrack.walktest.WalkingFragment.isCompleteWalktTest;

/**
 * Created by mujasam.bn on 2/6/2018.
 */

public class WalkTestControlFragment extends SurveyBaseFragment implements Observer {

    private static final String KEY_POSITION = "position";
    private TextView mBtnEnd;
    private Button mBtnCall;
    private ToggleButton mBtnPauseResume;
    private WalkTestFragment mParentFragment;

    public static WalkTestControlFragment newInstance() {
        WalkTestControlFragment frag = new WalkTestControlFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return (frag);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentFragment = (WalkTestFragment) getParentFragment();
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_walk_controller, container, false);
        Log.e("Position ", "" + mPosition);
        initViews(result);
        initClickListener();
        return result;
    }

    private void initViews(View rootView) {
        mBtnEnd = rootView.findViewById(R.id.btnEnd);
        mBtnPauseResume = rootView.findViewById(R.id.btnPauseResume);
        mBtnCall = rootView.findViewById(R.id.btnCall);
    }

    private void initClickListener() {
        mBtnEnd.setOnClickListener(view -> {
            showLoader("Collecting data. Please wait...");
            isCompleteWalktTest = false;
            mParentFragment.walkTestServiceStop();
            mParentFragment.stopLocationHelper();
            mParentFragment.stopDeviceMotion();
            mParentFragment.handleTimer(true);
        });
        mBtnPauseResume.setOnCheckedChangeListener((toggleButton, isChecked) -> {
            mParentFragment.WalkTestServicePauseResume(isChecked);
            mParentFragment.handleTimer(isChecked);
        });
        mBtnCall.setOnClickListener(view -> {
            WearUtils.showDialogOKCancel(getActivity(), EMERGENCY_NUMBER,
                    (dialog, which) -> {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                new WearMessageSenderAsync().execute(null, EMERGENCY_CALL_MESSAGE);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    });

        });
    }

    @Override
    public void update(Observable observable, Object arg) {
        System.out.println("HearRate changed: " + arg);
    }
}
