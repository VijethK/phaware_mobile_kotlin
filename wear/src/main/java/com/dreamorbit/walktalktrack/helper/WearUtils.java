package com.dreamorbit.walktalktrack.helper;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync.TEST_ENABLE_DISABLE;


/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class WearUtils {

    private final static String TAG = "WearUtils";
    private static AlertDialog dialog = null;

    /**
     * Get the custom drawables from application
     *
     * @param context
     * @param resource_name
     * @return
     */
    public static Drawable getDrawable(Context context, String resource_name) {
        try {
            int resId = context.getResources().getIdentifier(resource_name, "drawable", context.getPackageName());
            if (resId != 0) {
                return context.getResources().getDrawable(resId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Snackbar utility method
     */
    public static void showSnackBar(View view, String msg, int duration) {
        Snackbar mySnackbar = Snackbar.make(view,
                msg, duration);

        mySnackbar.show();
    }

    /***
     * Create a unique app folder to store Wear 6 minute walk test data/files
     * @param context
     * @return
     */

    public static File createUniqueAppFolder(Context context) {
        String folderName = getAndroidId(context) + "_" + System.currentTimeMillis();
        String file = context.getFilesDir().getAbsolutePath() + File.separator + WearConstant.ROOT_APP_FOLDER_NAME + File.separator + folderName;
        File projDir = new File(file);
        if (!projDir.exists()) {
            boolean created = projDir.mkdirs();
            Log.e(TAG, "createUniqueAppFolder() Not exist Created : " + created);
        }

        Log.e(TAG, "createUniqueAppFolder()" + projDir);

        return projDir;
    }

    public static boolean createDirectory(Context context) {
        boolean created;
        File myDirectory = new File(context.getFilesDir().getAbsolutePath() + File.separator + WearConstant.ROOT_APP_FOLDER_NAME);
        if (!myDirectory.exists()) {
            created = myDirectory.mkdir();
            Log.e(TAG, "createDirectory() Not exist Created : " + created);
        } else {
            Log.e(TAG, "createDirectory() Already Exist");
            created = true;
        }
        Log.e(TAG, "createDirectory()" + myDirectory);

        return created;
    }

    public static File[] getAllDirectories(Context context) {
        File myDirectory = new File(context.getFilesDir().getAbsolutePath() + File.separator + WearConstant.ROOT_APP_FOLDER_NAME);
        if (!myDirectory.exists())
            myDirectory.mkdir();
        File[] files = myDirectory.listFiles();
        return files;
    }

    public static String getAndroidId(Context context) {
        String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        return androidId;
    }

    public static String getCurrentDateTime() {
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    public static String getCurrentUTCDateTime() {
        SimpleDateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss:SSSZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    public static String getCurrentUTCDateTimeWithMillSeconds() {
        SimpleDateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSSZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date currentDate = new Date();
        return dateFormatterR.format(currentDate);
    }

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @return String representing date in specified format
     */
    public static String getDateTime(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return dateFormatterR.format(calendar.getTime());
    }

    public static void grantVibration(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(500);// Vibrate for 500 milliseconds
    }

    public static int calculateHeartAverage(List<Integer> heartRate) {
        Integer sum = 0;
        if (!heartRate.isEmpty()) {
            for (Integer mark : heartRate) {
                sum += mark;
            }
            return sum.intValue() / heartRate.size();
        }
        return sum;
    }

    public static void playDefaultSound(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***********************************************
     * Show alert dialog with Ok and Cancel
     * @param message
     * @param okListener
     **********************************************/
    public static void showDialogOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {

        if (dialog != null) {
            dialog.dismiss();
        }
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        mBuilder.setMessage(message);
        mBuilder.setPositiveButton("Call", okListener);
        mBuilder.setNegativeButton("Cancel", okListener);
        dialog = mBuilder.create();
        dialog.show();
    }

    public static void heartAnimator(View view) {
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(view,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(500);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();
    }

    /**
     * Test taken at date time 24 format upload
     * As per Karthik no need to send UTC format. he is handling it in backend
     *
     * @return
     */
    public static String getTestTakenDateTime() {
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = dateFormatterR.format(new Date());
        return utcTime;
    }

    /**
     * Get start date for test
     */
    public static String[] getStartEndDates() {
        String dates[] = new String[2];
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Calendar calendar = Calendar.getInstance();
        dates[0] = dateFormatterR.format(calendar.getTime());
        calendar.add(Calendar.SECOND, -10);
        dates[1] = dateFormatterR.format(calendar.getTime());
        return dates;
    }

    public static void playCountDownTimer(Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.countdown);
        mediaPlayer.start();
    }

    public static void sendTestSchedulerToWear() {
        if(WearPrefSingleton.getInstance().isLogined()) {
            String currentScheduler = WearPrefSingleton.getInstance().getSchedulerModel();
            if (!TextUtils.isEmpty(currentScheduler))
                new WearMessageSenderAsync().execute(currentScheduler, TEST_ENABLE_DISABLE);
            else
                new WearMessageSenderAsync().execute();// re-sync the data between phone and wear
        }else{
            WearPrefSingleton.getInstance().saveSchedulerModel("");
        }
    }

    public enum ScreenName {
        timer(1),
        walk_test(2),
        resting(3),
        question_three(4),
        question_four(5),
        summary(6);

        int position;

        ScreenName(int p) {
            position = p;
        }

        public int getScreen() {
            return position;
        }
    }
}
