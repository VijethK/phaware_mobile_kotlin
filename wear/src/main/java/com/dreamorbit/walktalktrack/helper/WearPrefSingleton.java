package com.dreamorbit.walktalktrack.helper;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mujasam on 6/28/2016.
 */

public class WearPrefSingleton {

    //Mujasam

    public static final String IS_LOGIN = "is_login";
    public static final String WALK_TEST_FOLDER_PATH = "wear_folder_path";
    public static final String LAST_STATE = "last_state";
    public static final String LAST_STATE_POS = "last_state_pos";
    public static final String SUMMARY = "summary";
    private static final String SCHEDULER_MODEL = "scheduler_model";
    private static final String TOTAL_STEPS_DAY = "total_steps_day" ;

    private static WearPrefSingleton sInstance;
    private SharedPreferences mSharedPreferences;

    private WearPrefSingleton() {
    }

    public static WearPrefSingleton getInstance() {
        if (sInstance == null) {
            sInstance = new WearPrefSingleton();
        }
        return sInstance;
    }

    public WearPrefSingleton initialize(Context context, String sharedPreferenceName) {
        mSharedPreferences = context.getSharedPreferences(sharedPreferenceName, MODE_PRIVATE);
        return sInstance;
    }

    private SharedPreferences.Editor getEditor() {
        return mSharedPreferences.edit();
    }

    public void saveLoginedStatus(boolean isLogined) {
        getEditor().putBoolean(IS_LOGIN, isLogined).apply();
    }

    public boolean isLogined() {
        return mSharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void saveWalkTestFolderPath(String path) {
        getEditor().putString(WALK_TEST_FOLDER_PATH, path).apply();
    }

    public String getWalkTestFolderPath() {
        return mSharedPreferences.getString(WALK_TEST_FOLDER_PATH, "");
    }

    public void saveLastState(String name) {
        getEditor().putString(LAST_STATE, name).apply();
    }

    public String getLastState() {
        return mSharedPreferences.getString(LAST_STATE, "");
    }

    public void saveFragmentLastStatePosition(int position) {
        getEditor().putInt(LAST_STATE_POS, position).apply();
    }

    public void saveTestSummary(String summary) {
        getEditor().putString(SUMMARY, summary).apply();
    }

    public String getTestSummary() {
        return mSharedPreferences.getString(SUMMARY, "");
    }

    public int getFragmentLastStatePosition() {
        return mSharedPreferences.getInt(LAST_STATE_POS, 0);
    }

    public void clearActiveResearchPrefs(@TAG String... tags) {
        for (String tag : tags) {
            getEditor().remove(tag).apply();
        }
    }

    public void saveSchedulerModel(String schedulerString) {
        getEditor().putString(SCHEDULER_MODEL, schedulerString).apply();
    }

    public String getSchedulerModel() {
        return mSharedPreferences.getString(SCHEDULER_MODEL,"");
    }

    public String getTotalStepsForTheDay() {
        return mSharedPreferences.getString(TOTAL_STEPS_DAY, "");
    }
    public void saveTotalStepsForTheDay(String totalSteps) {
        getEditor().putString(TOTAL_STEPS_DAY, totalSteps).apply();
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface TAG {
    }
}
