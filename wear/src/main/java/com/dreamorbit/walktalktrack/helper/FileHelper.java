package com.dreamorbit.walktalktrack.helper;

/**
 * Created by mujasam.bn on 11/14/2017.
 */


import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileHelper {
    private static final int BUFFER_SIZE = 8192;//2048;
    private static String TAG = FileHelper.class.getName();
    private static String parentPath = "";

    /**
     * Zip multiple files
     * For 6 Minute Walk Test json files
     *
     * @param files
     * @param zipFileName
     */

    public static void zipFiles(File[] files, String zipFileName) {

        FileOutputStream fos = null;
        ZipOutputStream zipOut = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream(zipFileName);
            zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
            for (File filePath : files) {
                File input = new File(filePath.toString());
                fis = new FileInputStream(input);
                ZipEntry ze = new ZipEntry(input.getName());
                System.out.println("Zipping the file: " + input.getName());
                zipOut.putNextEntry(ze);
                byte[] tmp = new byte[BUFFER_SIZE];
                int size = 0;
                while ((size = fis.read(tmp)) != -1) {
                    zipOut.write(tmp, 0, size);
                }
                zipOut.flush();
                fis.close();
            }
            zipOut.close();
            System.out.println("Done... Zipped the files...");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) fos.close();
            } catch (Exception ex) {

            }
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static void deleteFolder(File folder) {
        if (folder.isDirectory()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                Log.e("DeleteFolder", "file Path: "+ file +" Deleted: " + file.delete());
            }

            files = folder.listFiles();
            if (files.length == 0) {
                Log.e("Folder Name ", "" + folder);
                boolean isFolderDeleted = folder.delete();
                Log.e("Folder deleted ", "" + isFolderDeleted);
            }
        }
    }
}
