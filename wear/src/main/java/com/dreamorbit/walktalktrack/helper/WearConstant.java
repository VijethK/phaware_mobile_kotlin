package com.dreamorbit.walktalktrack.helper;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class WearConstant {


    public static final String POSITION = "mPosition";

    //Survey Activity screen
    public static final String IS_SURVEY_COMPLETED = "is_survey_completed";
    public static final String SURVERY_ID = "survey_id";

    //Survey screen
    public static final String QUESTION = "question";
    public static final String ACTIVITY_TITLE = "activity_title";

    //Wear start/stop constant values
    public static final int APP_LOGINED = 0;
    public static final int APP_LOGOUT = 1;
    public static final String QUESTION_LIST = "questions_list";
    public static String KEY = "ASDFGHJKL";

    public static final String ROOT_APP_FOLDER_NAME = "Phaware";
    public static final String EMERGENCY_NUMBER = "911";//"919986790";
}
