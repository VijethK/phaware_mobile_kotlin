package com.dreamorbit.walktalktrack.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.dreamorbit.walktalktrack.R;


/**
 * Created by mujasam.bn on 6/29/2016.
 */

public class BaseFragment extends Fragment {

    static ProgressDialog ploader;


    public BaseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BaseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BaseFragment newInstance(String param1, String param2) {
        BaseFragment fragment = new BaseFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void showLoader(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ploader != null || getActivity().isFinishing()) {
                    return;
                }

                ploader = new ProgressDialog(getActivity(), R.style.MyProgressDialog);
                ploader.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                ploader.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                ploader.setCancelable(false);
                ploader.setIndeterminate(true);
                ploader.setMessage(message);
                try {
                    ploader.show();
                } catch (WindowManager.BadTokenException e) {
                    ploader = null;
                } catch (Exception e) {
                    ploader = null;
                }
            }
        });
    }

    public void hideLoader() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (ploader != null && ploader.isShowing()) {
                    ploader.dismiss();
                }
                ploader = null;
            }
        });
    }

}
