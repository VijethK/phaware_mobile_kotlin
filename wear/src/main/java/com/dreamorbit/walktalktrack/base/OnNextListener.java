package com.dreamorbit.walktalktrack.base;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public interface OnNextListener {
    void onNextPressed(int position);
}
