package com.dreamorbit.walktalktrack.adapter;

import android.support.wear.widget.WearableRecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dreamorbit.walktalktrack.model.survey.Options;
import com.dreamorbit.walktalktrack.R;

import java.util.List;


/**
 * Created by Mujas on 22-10-2017.
 */

//View holder class, where all view components are defined
public class QuestionViewHolder extends WearableRecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvOption;
    public ImageView imgOption;
    private QuestionsAdapter adapter;
    private List<Options> mOptions;

    public QuestionViewHolder(View itemView, final QuestionsAdapter adapter, List<Options> options) {
        super(itemView);
        this.mOptions = options;
        this.adapter = adapter;

        tvOption = itemView.findViewById(R.id.tvOption);
        imgOption = itemView.findViewById(R.id.imgOption);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.mSelectedItem = getAdapterPosition();
                adapter.notifyItemRangeChanged(0, mOptions.size());
            }
        };

        itemView.setOnClickListener(clickListener);
        //radio.setOnClickListener(clickListener);
        imgOption.setOnClickListener(clickListener);
    }

    @Override
    public void onClick(View v) {

    }
}
