package com.dreamorbit.walktalktrack.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.wear.widget.WearableRecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.survey.Options;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.R;


public class QuestionsAdapter extends  WearableRecyclerView.Adapter<QuestionViewHolder>   {

    private final LayoutInflater inflater;
    public int mSelectedItem = -1;
    //Creating an arraylist of POJO objects
    private Questions mQuestions;
    private View view;
    private QuestionViewHolder holder;
    private Context context;


    public QuestionsAdapter(Context context, Questions question) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        mQuestions = question;
    }

    //This method inflates view present in the RecyclerView
    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = inflater.inflate(R.layout.row_item_questions, parent, false);
        holder = new QuestionViewHolder(view, this, mQuestions.getOptions());
        return holder;
    }

    //Binding the data using get() method of POJO object
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final QuestionViewHolder holder, int position) {
        Options option = mQuestions.getOptions().get(position);
        holder.tvOption.setText(option.getName());
        holder.imgOption.setImageDrawable(WearUtils.getDrawable(context, option.getImage()));
        int color = position == mSelectedItem ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.white);
        int typeface = position == mSelectedItem ? Typeface.BOLD : Typeface.NORMAL;
        holder.tvOption.setTypeface(null, typeface);
        holder.tvOption.setTextColor(color);
    }

    @Override
    public int getItemCount() {
        return mQuestions.getOptions() == null ? 0 : mQuestions.getOptions().size();
    }
}
