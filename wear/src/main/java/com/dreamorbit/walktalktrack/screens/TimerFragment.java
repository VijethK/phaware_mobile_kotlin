package com.dreamorbit.walktalktrack.screens;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dreamorbit.walktalktrack.base.OnNextListener;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class TimerFragment extends SurveyBaseFragment {

    private static OnNextListener mIOnNextListener;
    public CountDownTimer mCountDownTimer;
    protected int mPosition;
    private int count = 4;
    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private ImageButton mBtnBack;

    public TimerFragment() {
        // Required empty public constructor
    }

    public static TimerFragment newInstance(List<Questions> question, int position, String activityTitle) {
        TimerFragment consentFragment = new TimerFragment();
        Bundle args = new Bundle();
        args.putInt(WearConstant.POSITION, position);
        args.putString(WearConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(WearConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        consentFragment.setArguments(args);
        return consentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(WearConstant.POSITION);
        mIOnNextListener = ((SurveyActivity) getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_timer, container, false);

        initTimerView(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    /**
     * ON a count of 5 launch the 6 minute walk test
     *
     * @param rootView
     */
    private void initTimerView(View rootView) {

        TextView textTimer = rootView.findViewById(R.id.tvTimer);
        mCountDownTimer = new CountDownTimer(6000, 1000) {
            // 1000 means, onTick function will be called at every 1000 milliseconds
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                if (count < 0) {
                    return;
                }
                //start countdown music
                Log.e("TICK ", "" + count);
                if (count == 3)
                    WearUtils.playCountDownTimer(getActivity());

                //set the count in UI
                //textTimer.setText("" + count);
                switch (count) {
                    case 3:
                        textTimer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.timer_3));
                        break;
                    case 2:
                        textTimer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.timer_2));
                        break;
                    case 1:
                        textTimer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.timer_1));
                        break;
                    case 0:
                        textTimer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.timer_0));
                        break;

                }
                --count;
            }

            @Override
            public void onFinish() {
                mIOnNextListener.onNextPressed(mPosition);
            }
        };
    }

    /**
     * Init toolbar and navigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        mBtnBack = mToolbar.findViewById(R.id.btnBack);
        mTvCancel.setText(R.string.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText("Step " + (mPosition + 1) + " of 6");
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        //Done button
        mBtnBack.setOnClickListener(view -> {
                    mCountDownTimer.cancel();
                    navigateToActivityScreenCancelled();
                }
        );
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}