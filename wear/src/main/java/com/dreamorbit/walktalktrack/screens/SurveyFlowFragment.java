package com.dreamorbit.walktalktrack.screens;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dreamorbit.walktalktrack.base.CustomViewPager;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.walktest.WalkingFragment;
import com.dreamorbit.walktalktrack.walktest.RestingFragment;
import com.dreamorbit.walktalktrack.walktest.WalkTestFragment;

import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SurveyFlowFragment extends Fragment {

    public CustomViewPager mPager;
    public SurveyFragmentPagerAdapter mAdapter;
    private String activityTitle;
    private List<Questions> mRoot;

    public SurveyFlowFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SurveyFlowFragment(List<Questions> root, String activityTitle) {
        this.mRoot = root;
        this.activityTitle = activityTitle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_root, container, false);
        mPager = rootView.findViewById(R.id.vp_pages);


        return rootView;
    }

    /**
     * Launching the last state activity and fragment state
     * @param savedInstanceState
     */

    /*@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Getting the last fragment state poistion
        int fragmentPosition = WearPrefSingleton.getInstance().getFragmentLastStatePosition();
        String activityName = WearPrefSingleton.getInstance().getLastState();

        // Note that we are passing childFragmentManager, not FragmentManager
        mAdapter = new SurveyFragmentPagerAdapter(mPager, getResources(), getChildFragmentManager(), mRoot, activityTitle);
        mPager.setPagingEnabled(false);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(mRoot.size());

        //check if last state if SurveyActivty from Lifecycle Dispatcher
        //show the last fragment state or else show default 0
        if (activityName.contains("SurveyActivity"))
            mPager.setCurrentItem(fragmentPosition);
        else
            mPager.setCurrentItem(0);

        initPageChangeListener();
    }*/

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Note that we are passing childFragmentManager, not FragmentManager
        mAdapter = new SurveyFragmentPagerAdapter(mPager, getResources(), getChildFragmentManager(), mRoot, activityTitle);
        mPager.setPagingEnabled(false);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(mRoot.size());
        mPager.setCurrentItem(0);

        initPageChangeListener();
    }

    /**
     * Start the 6 minute walk test timer when the fragment is visible
     * Handle the viewpager onload lifecycler (timer was starting before the screen was visible)
     */
    public void initPageChangeListener() {
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {//&& !SharedPrefSingleton.getInstance().isWalkTestActive()) {
                    Fragment fragment = mAdapter.getRegisteredFragment(position);
                    if (fragment != null) {
                        ((TimerFragment) fragment).mCountDownTimer.start();
                    }
                } else if (position == 3) {//&& !SharedPrefSingleton.getInstance().isWalkTestActive()) {
                    Fragment fragment = mAdapter.getRegisteredFragment(position);
                    if (fragment != null) {
                        Fragment fitFragment = ((WalkTestFragment) fragment).childAdapter.getRegisteredFragment(1);
                        if (fitFragment != null) {
                            ((WalkingFragment) fitFragment).registerSensors();
                        }
                    }
                } else if (position == 4) {//&& !SharedPrefSingleton.getInstance().isWalkTestActive()) {
                    Fragment fragment = mAdapter.getRegisteredFragment(position);
                    if (fragment != null) {
                        ((RestingFragment) fragment).registerSensors();
                    }
                } else if (position == 7) {
                    Fragment fragment = mAdapter.getRegisteredFragment(position);
                    if (fragment != null) {
                        ((SummaryFragment) fragment).updateSummaryData();
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}