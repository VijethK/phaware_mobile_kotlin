package com.dreamorbit.walktalktrack.screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.dreamorbit.walktalktrack.base.BaseFragment;
import com.dreamorbit.walktalktrack.base.OnBackPressListener;
import com.dreamorbit.walktalktrack.base.OnNextListener;
import com.dreamorbit.walktalktrack.datalayer.BackGroundWearService;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.model.survey.Answers;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.dreamorbit.walktalktrack.R;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by mujasam.bn on 11/6/2017.
 */

public class SurveyBaseFragment extends BaseFragment {

    /**
     * Save answers in json format {"answer" :"Yes", "answer":"no"}
     */

    //listener for navigations next and back
    protected OnNextListener mIOnNextListener;
    protected OnBackPressListener mIOnBackPressListener;

    //Declare the Adapter, AecyclerView and our custom ArrayList
    protected TextView tvQuestion;
    protected List<Questions> mQuestionList;
    protected int mPosition;
    protected String mActivityTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuestionList = getArguments().getParcelableArrayList(WearConstant.QUESTION);
        mActivityTitle = getArguments().getString(WearConstant.ACTIVITY_TITLE);
        mPosition = getArguments().getInt(WearConstant.POSITION);
    }

    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreenCancelled() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(WearConstant.IS_SURVEY_COMPLETED, false);
        i.putExtra(WearConstant.SURVERY_ID, -1);
        getActivity().setResult(RESULT_CANCELED, i);
        getActivity().finish();
    }


    /**
     * Final navigation logic on completion of questionaire task
     */
    protected void navigateToActivityScreen() {
        Intent i = new Intent();  // or // Intent i = getIntent()
        i.putExtra(WearConstant.IS_SURVEY_COMPLETED, true);
        i.putExtra(WearConstant.SURVERY_ID, mQuestionList.get(0).getActivityId());
        getActivity().setResult(RESULT_OK, i);
        getActivity().finish();
    }

    protected void showCancelAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are sure wanna cancel?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        navigateToActivityScreenCancelled();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog

                    }
                });
        // Create the AlertDialog object and return it
        builder.setCancelable(false);
        builder.show();
    }

    /**
     * Save the question answer in json format
     */
    protected void saveAnswerJsonInAppFolder() {

        List<Answers> answersList = new ArrayList<>();
        Iterator<Questions> itr = mQuestionList.iterator();

        while (itr.hasNext()) {
            Questions question = itr.next();
            if (TextUtils.isEmpty(question.getQuestion())) {
                continue;
            } else {
                Answers answer = new Answers(); //root
                answer.setQuestions(question.getQuestion());
                List<String> answers = new ArrayList<>();
                answers.add(question.getAnswer());
                answer.setAnswers(answers);
                answer.setDate(question.getCurrentTime());
                answersList.add(answer);
            }
        }

        Type listType = new TypeToken<List<Answers>>() {
        }.getType();
        Gson gson = new Gson();
        String answerJason = gson.toJson(answersList, listType);

        //check if survey completed on 6 minute walk test then check for allready saved folder path
        //And save SurveyAnswers in same folder which allready contains pedometer,stepcount etc..
        File uniqueAppFolder;
        String folderPathWalkTest = WearPrefSingleton.getInstance().getWalkTestFolderPath();
        if (TextUtils.isEmpty(folderPathWalkTest)) {
            uniqueAppFolder = WearUtils.createUniqueAppFolder(getActivity());
        } else {
            uniqueAppFolder = new File(folderPathWalkTest);
        }
        //save this json in app folder
        saveProcess(uniqueAppFolder, answerJason, "SurveyResponse.json");

    }

    /**
     * Saving the json file in app folder
     *
     * @param uniqueAppFolder
     * @param answerJason
     * @param jsonName
     */
    private void saveProcess(File uniqueAppFolder, String answerJason, String jsonName) {

        File fileName = new File(uniqueAppFolder, jsonName);
        try {
            FileWriter fw = new FileWriter(fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(answerJason);
            bw.close();
            Log.e("Saved: ", answerJason);
            Log.e("Save Path ", uniqueAppFolder.toString());
            WearPrefSingleton.getInstance().saveWalkTestFolderPath(null);

            String folderPathWalkTest = WearPrefSingleton.getInstance().getWalkTestFolderPath();
            Log.e("Save Path ", folderPathWalkTest);
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Start the back ground thread to perform zip and sync data with phone
     */
    protected void startWearSyncService() {
        Intent intent = new Intent(getActivity(), BackGroundWearService.class);
        getActivity().startService(intent);
    }
}
