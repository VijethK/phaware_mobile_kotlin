package com.dreamorbit.walktalktrack.screens;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamorbit.pedometer.PedoUtils;
import com.google.gson.Gson;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.scheduler.SchedulerModel;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.R;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment extends SurveyBaseFragment {

    private static final String TAG = SummaryFragment.class.getName();
    protected TextView mTvDone;
    private TextView tvTotalDistance, tvAverageHeartRate, tvMaxHeartRate, tvTestDuration, tvRestAverageHeartRate, tvRestMaxHeartRate;
    private Toolbar mToolbar;

    public SummaryFragment() {
        // Required empty public constructor
    }

    public static SummaryFragment newInstance(List<Questions> question, int position, ViewPager viewPager, String activityTitle) {
        SummaryFragment fragment = new SummaryFragment();
        Bundle args = new Bundle();
        args.putInt(WearConstant.POSITION, position);
        args.putString(WearConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(WearConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fit_summary, container, false);
        initViews(view);
        initToolBar(view);
        return view;
    }

    private void initViews(View view) {
        tvAverageHeartRate = view.findViewById(R.id.tvAverageHeartRate);
        tvMaxHeartRate = view.findViewById(R.id.tvMaxHeartRate);
        tvTotalDistance = view.findViewById(R.id.tvDistance);
        tvTestDuration = view.findViewById(R.id.tvTime);
        tvRestAverageHeartRate = view.findViewById(R.id.tvRestAverageHeartRate);
        tvRestMaxHeartRate = view.findViewById(R.id.tvRestMaxHeartRate);
    }

    public void updateSummaryData() {
        tvTotalDistance.setText("" + ((SurveyActivity) getActivity()).getDistanceInMeter() + "m");
        tvTestDuration.setText(((SurveyActivity) getActivity()).getTestDuration());
        tvAverageHeartRate.setText("" + ((SurveyActivity) getActivity()).getAvgHeartRate());
        tvMaxHeartRate.setText("" + ((SurveyActivity) getActivity()).getMaxHeartRate());
        tvRestAverageHeartRate.setText("" + ((SurveyActivity) getActivity()).getRestAvgHeartRate());
        tvRestMaxHeartRate.setText("" + ((SurveyActivity) getActivity()).getRestMaxHeartRate());

        //Newly added once Resting is done sync the data to phone
        saveAnswerJsonInAppFolder();
        startWearSyncService();
        //Infinity test and survey action lock. Enable disable depend on flag. if true, do not update the db
        if (!PedoUtils.isContinuousSurvey()) {
            updateAndSendSchedulerData();
        }
    }

    /**
     * Updating the test taken data in Wear
     */
    private void updateAndSendSchedulerData() {

        //first get the existing model
        String currentScheduler = WearPrefSingleton.getInstance().getSchedulerModel();
        if (!TextUtils.isEmpty(currentScheduler)) {
            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            TypeToken<List<SchedulerModel>> token = new TypeToken<List<SchedulerModel>>() {
            };
            List<SchedulerModel> modelList = gson.fromJson(WearPrefSingleton.getInstance().getSchedulerModel(), token.getType());
            for (SchedulerModel model : modelList) {
                if (model.getName().equalsIgnoreCase(getString(R.string.six_minute_walk_test).trim())) {
                    //update the model
                    model.setStatus(SchedulerModel.TEST_TAKEN);
                    model.setTestTakenAt(WearUtils.getCurrentDateTime());//we got the test model here
                    break;
                }
            }

            String updatedScheduler = gson.toJson(modelList);
            WearPrefSingleton.getInstance().saveSchedulerModel(updatedScheduler);

            //send the status to phone
            WearUtils.sendTestSchedulerToWear();
            Log.e(TAG, "Wear sent updated model to Mobile " + updatedScheduler);
        }
    }

    protected void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvDone = mToolbar.findViewById(R.id.done);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvDone.setOnClickListener(view -> {
            //John changed the sync time scenario
            //saveAnswerJsonInAppFolder();
            //startWearSyncService();
            navigateToActivityScreen();
        });
    }
}
