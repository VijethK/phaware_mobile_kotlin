package com.dreamorbit.walktalktrack.screens;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.dreamorbit.walktalktrack.base.OnBackPressListener;
import com.dreamorbit.walktalktrack.base.OnNextListener;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.survey.QuestionList;
import com.google.gson.Gson;
import com.dreamorbit.walktalktrack.R;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class SurveyActivity extends AppCompatActivity implements OnNextListener, OnBackPressListener {

    //walk test data storage
    private static double mDistanceMeter;
    private static int mAvgHeartRate;
    private static int mMaxHeartRate;
    private static String mTestDuration;
    private static String mLocations;
    private static int mRestAvgHeartRate;
    private static int mRestMaxHeartRate;
    private static long totalStepsCount;

    private SurveyFlowFragment mSurveyFlowFragment;
    private QuestionList mRootQuestionList;
    private Bundle mSavedInatsnce;

    public static double getDistanceInMeter() {
        return mDistanceMeter;
    }

    public static void setDistanceInMeter(double meter) {
        mDistanceMeter = meter;
    }

    public static int getAvgHeartRate() {
        return mAvgHeartRate;
    }

    public static void setAvgHeartRate(int avgHeartRate) {
        mAvgHeartRate = avgHeartRate;
    }

    public static int getMaxHeartRate() {
        return mMaxHeartRate;
    }

    public static void setMaxHeartRate(int maxHeartRate) {
        mMaxHeartRate = maxHeartRate;
    }

    public static String getTestDuration() {
        return mTestDuration;
    }

    public static void setTestDuration(String testDuration) {
        mTestDuration = testDuration;
    }

    public static String getLocations() {
        return mLocations;
    }

    public static void setLocations(String locations) {
        mLocations = locations;
    }

    public static void setRestMaxHeartRate(int maxHeartRate) { mRestMaxHeartRate = maxHeartRate;}

    public static int getRestMaxHeartRate() { return mRestMaxHeartRate; }

    public static void setRestAvgHeartRate(int avgHeartRate) { mRestAvgHeartRate = avgHeartRate;}

    public static int getRestAvgHeartRate() { return mRestAvgHeartRate; }

    public static long getTotalStepsCount() {
        return totalStepsCount;
    }

    public static void setTotalStepsCount(long stepsCount) {
        totalStepsCount = stepsCount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_task);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mSavedInatsnce = savedInstanceState;
        if (mSavedInatsnce == null) {
            // withholding the previously created fragment from being created again
            // On orientation change, it will prevent fragment recreation
            // its necessary to reserving the fragment stack inside each tab
            mRootQuestionList = getSurveyQuestion();
            if (mRootQuestionList == null) {
                finish();
            }
            initScreen();
        } else {
            // restoring the previously created fragment
            // and getting the reference
            mSurveyFlowFragment = (SurveyFlowFragment) getSupportFragmentManager().getFragments().get(0);
        }
    }

    /**
     * @return
     */
    private QuestionList getSurveyQuestion() {
        String questionListString = getIntent().getStringExtra(WearConstant.QUESTION_LIST);
        Gson gson = new Gson();
        QuestionList screenDataRoot = gson.fromJson(questionListString, QuestionList.class);
        if (screenDataRoot.getQuestions().size() == 0) {
            finish();
            return null;
        }

        return screenDataRoot;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initScreen() {
        // Creating the ViewPager container fragment once
        mSurveyFlowFragment = new SurveyFlowFragment(mRootQuestionList.getQuestions(), "");
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, mSurveyFlowFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        backNavigation();
    }

    @Override
    public void onNextPressed(int position) {
        mSurveyFlowFragment.mPager.setCurrentItem(position + 1);
    }

    @Override
    public void onFragmentBackPressed(int position) {
        backNavigation();
    }

    /**
     * Navigation back implementation depend on the
     * if 6 minute walk test dont allow to go back once Walk test has beens started
     */
    private void backNavigation() {
        if (mSurveyFlowFragment.mPager.getCurrentItem() != 0) {
            if (mSurveyFlowFragment.mPager.getCurrentItem() == WearUtils.ScreenName.question_four.getScreen() + 1) {
                mSurveyFlowFragment.mPager.setCurrentItem(WearUtils.ScreenName.question_three.getScreen() + 1, false);
            } else if (mSurveyFlowFragment.mPager.getCurrentItem() > 2) {
                return;
            } else {
                mSurveyFlowFragment.mPager.setCurrentItem(mSurveyFlowFragment.mPager.getCurrentItem() - 1, false);
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
