package com.dreamorbit.walktalktrack.screens;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.wearable.activity.WearableActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.model.survey.QuestionList;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.dreamorbit.walktalktrack.R;
import com.dreamorbit.walktalktrack.datalayer.BackGroundWearService;
import com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync;
import com.dreamorbit.walktalktrack.helper.PermissionUtil;
import com.dreamorbit.walktalktrack.helper.WearPrefSingleton;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.scheduler.SchedulerModel;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static com.dreamorbit.walktalktrack.datalayer.WearMessageSenderAsync.REPORT_SUMMARY_MESSAGE;

/**
 * Launcher activity to start the Phaware app
 */
public class MainActivity extends WearableActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int AUTH_REQUEST = 1;
    /* some trivial constants, at the end of the file*/
    private static final String TAG = MainActivity.class.getName();
    private static final int REQUEST_CODE_RESOLVE_ERR = 100;

    private boolean isGoogleApiConnected;
    private GoogleApiClient mGoogleApiClient;

    private ProgressDialog ploader;
    private String mRootQuestionListString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logFabricUser();
        launchLastState();
        mRootQuestionListString = getSurveyQuestion();

        boolean isCreated = WearUtils.createDirectory(this);
        Log.e(TAG, "onCreate() " + isCreated);
    }


    private void logFabricUser() {
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("12345");
        Crashlytics.setUserEmail("");
        Crashlytics.setUserName("Stanford Wear User");
        //throw new RuntimeException("This is a crash");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume()");

        setCastStatusToPhone();
        setAmbientEnabled();
        sendTestStatusToPhone();

        if (isPlayServices()) {
            initFitApiClient();
            checkPermissions();
            checkGPS();
        }
    }

    private void sendTestStatusToPhone() {
        WearUtils.sendTestSchedulerToWear();
    }

    private void launchLastState() {
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
                return;
            }
        }
    }

    /**
     * Check whether Google Play Services are available.
     * <p>
     * If not, then display dialog allowing user to update Google Play Services
     *
     * @return true if available, or false if not
     */
    private boolean isPlayServices() {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                gApi.getErrorDialog(this, resultCode, 100).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }


    private void initFitApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .useDefaultAccount() //Use the default account
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

        showLoader("Please wait...");
    }

    /*****************************************
     * Google Fit Client Connection callback
     * Register the Source Listener to fetch Distance and Steps
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
        hideLoader();
        isGoogleApiConnected = true;
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended " + i);
        isGoogleApiConnected = false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(TAG, "onConnectionFailed" + result.toString());
        Log.i(TAG, "onConnectionFailed:" + result.getErrorCode() + "," + result.getErrorMessage());
        hideLoader();
        isGoogleApiConnected = true;
        /*if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        }*/
    }


    /*********************************************
     * Check does Wearable supports GPS
     *********************************************/

    private void checkGPS() {
        if (!hasGps()) {
            Log.w(TAG, "This hardware doesn't have GPS.");
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.gps_not_available))
                    .setPositiveButton(R.string.ok, (dialog, id) -> dialog.cancel())
                    .setOnDismissListener(dialog -> dialog.cancel())
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    /**********************************************
     * Returns {@code true} if this device has the GPS capabilities.
     **********************************************/
    private boolean hasGps() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

    /**********************************************
     * Check Runtime permission for Bodysensor and Location
     **********************************************/
    private boolean checkPermissions() {
        String[] permission = {Manifest.permission.BODY_SENSORS, Manifest.permission.ACCESS_FINE_LOCATION};
        boolean hasPermission = PermissionUtil.hasPermissions(this, permission);
        if (hasPermission) {
            return true;
        } else {
            String[] PERMISSIONS = {Manifest.permission.BODY_SENSORS, Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, PERMISSIONS, PermissionUtil.LOCATION_PERMISSIONS_REQUEST);
            return false;
        }
    }

    /***********************************************
     * Show alert dialog if user deny the permission
     * @param message
     * @param okListener
     **********************************************/
    private void showDialogOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setCancelable(false)
                .create()
                .show();
    }

    /***********************************************
     * onclick show the Survey questions activity
     * Disable the test if already taken
     *
     * @param view
     ***********************************************/
    public void startWalk(View view) {
        boolean isStartWalk = true;

        //Check if Phone app is logined or not
        if (!WearPrefSingleton.getInstance().isLogined()) {
            isStartWalk = false;
            showDialogOK("Please login the PhAware Mobile application.", (dialog, which) -> {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        checkPermissions();
                        break;
                }
            });

            return; //don't check for further conditions
        }

        //check if scheduler data is received from phone or not
        SchedulerModel testModel = null;
        Gson gson = new Gson();
        TypeToken<List<SchedulerModel>> token = new TypeToken<List<SchedulerModel>>() {
        };
        List<SchedulerModel> modelList = gson.fromJson(WearPrefSingleton.getInstance().getSchedulerModel(), token.getType());
        if (modelList != null && modelList.size() > 0) {
            for (SchedulerModel model : modelList) {
                if (model.getName().equalsIgnoreCase(getString(R.string.six_minute_walk_test).trim())) {
                    testModel = model; //we got the test model here
                    break;
                }
            }

            if (testModel != null && !TextUtils.isEmpty(testModel.getStatus()) && testModel.getName().equalsIgnoreCase(getString(R.string.six_minute_walk_test).trim()) && testModel.getStatus().equalsIgnoreCase(SchedulerModel.TEST_TAKEN)) {
                //String schedulerSequence = (model != null && model.getSequence().equalsIgnoreCase("daily")) ? "day." : "week.";
                Toast.makeText(this, getString(R.string.test_taken_message), Toast.LENGTH_SHORT).show();
                return;
            }

            //check if body sensor and location service is enable
            if (!checkPermissions()) {
                isStartWalk = false;
                showDialogOK("Please enable the permissions in application setting.", (dialog, which) -> {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            checkPermissions();
                            break;
                    }
                });
            }

            //check if Google api client is connected
            if (!isGoogleApiConnected) {
                isStartWalk = false;
                showDialogOK("Please check the connectivity and retry.", (dialog, which) -> {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            checkPermissions();
                            break;
                    }
                });
            }

            //if none of the condition fails start the task
            if (isStartWalk) {
                Intent intent = new Intent(this, SurveyActivity.class);
                intent.putExtra(WearConstant.QUESTION_LIST, mRootQuestionListString);
                startActivity(intent);
            }
        } else {
            Toast.makeText(this, getString(R.string.please_relaunch_phoneapp), Toast.LENGTH_SHORT).show();
        }
    }

    /**********************************************
     * Runtime permission callback
     **********************************************/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPermissionGranted = true;
        switch (requestCode) {
            case PermissionUtil.LOCATION_PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = false;
                }
                if (grantResults.length > 0 && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    isPermissionGranted = false;
                }

                if (!isPermissionGranted) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BODY_SENSORS)) {
                        showDialogOKCancel("Body Sensor and Location permission required for this app",
                                (dialog, which) -> {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkPermissions();
                                            break;
                                        case DialogInterface.BUTTON_NEGATIVE:
                                            // proceed with logic by disabling the related features or quit the app.
                                            break;
                                    }
                                });
                    } else {
                        Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
                    }
                } else {
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult Code: " + requestCode);
    }

    private void setCastStatusToPhone() {
        if (!WearPrefSingleton.getInstance().isLogined()) {
            new WearMessageSenderAsync().execute();
            new WearMessageSenderAsync().execute(null, REPORT_SUMMARY_MESSAGE);

        }
    }

    /**
     * Start the back ground thread to perform zip and sync data with phone
     */
    protected void startWearSyncService() {
        Intent intent = new Intent(this, BackGroundWearService.class);
        startService(intent);
    }

    /**
     * Show progressbar
     *
     * @param message
     */
    public void showLoader(final String message) {
        runOnUiThread(() -> {
            if (ploader != null || isFinishing()) {
                return;
            }

            ploader = new ProgressDialog(MainActivity.this, R.style.MyProgressDialog);
            ploader.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            ploader.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            ploader.setCancelable(false);
            ploader.setIndeterminate(true);
            ploader.setMessage(message);
            try {
                ploader.show();
            } catch (WindowManager.BadTokenException e) {
                ploader = null;
            } catch (Exception e) {
                ploader = null;
            }
        });
    }

    public void hideLoader() {
        runOnUiThread(() -> {
            if (ploader != null && ploader.isShowing()) {
                ploader.dismiss();
            }
            ploader = null;
        });
    }

    /**
     * @return
     */
    private String getSurveyQuestion() {

        String surveyQuestion = loadJSONFromAsset();

        return surveyQuestion;
    }

    /**
     * Fetch the assent flow json from asset and pass it it to sdk
     *
     * @return assentJson
     */
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("questions.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}


