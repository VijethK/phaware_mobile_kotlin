package com.dreamorbit.walktalktrack.screens;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.support.wearable.activity.ConfirmationActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dreamorbit.walktalktrack.adapter.ClickListeners;
import com.dreamorbit.walktalktrack.adapter.QuestionsAdapter;
import com.dreamorbit.walktalktrack.adapter.RecyclerTouchListener;
import com.dreamorbit.walktalktrack.adapter.ScalingScrollLayoutCallback;
import com.dreamorbit.walktalktrack.base.OnBackPressListener;
import com.dreamorbit.walktalktrack.base.OnNextListener;
import com.dreamorbit.walktalktrack.helper.WearConstant;
import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.survey.Options;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.R;

import java.util.ArrayList;
import java.util.List;

public class SurveyQuestionsFragment extends SurveyBaseFragment {

    private static final String TAG = "SurveyQuestionsFragment";
    //listener for navigations next and back
    protected OnNextListener mIOnNextListener;
    protected OnBackPressListener mIOnBackPressListener;
    //Declare the Adapter, RecyclerView and our custom ArrayList
    private TextView tvQuestion;
    private WearableRecyclerView mWearableRecyclerView;
    private QuestionsAdapter mQuestionAdapter;
    private int mOptionPosition;

    //Toolbar
    private Toolbar mToolbar;
    private TextView mTvTitle, mTvCancel;
    private ImageButton mBtnBack;

    public SurveyQuestionsFragment() {
        // Required empty public constructor
    }

    public static SurveyQuestionsFragment newInstance(List<Questions> question, int position, String activityTitle) {
        SurveyQuestionsFragment consentFragment = new SurveyQuestionsFragment();
        Bundle args = new Bundle();
        args.putInt(WearConstant.POSITION, position);
        args.putString(WearConstant.ACTIVITY_TITLE, activityTitle);
        args.putParcelableArrayList(WearConstant.QUESTION, (ArrayList<? extends Parcelable>) question);
        consentFragment.setArguments(args);
        return consentFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mIOnNextListener = ((SurveyActivity) getActivity());
        mIOnBackPressListener = ((SurveyActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_survey_list, container, false);
        initViews(rootView);
        setUpWearableRecyclerView(rootView);
        initToolBar(rootView);
        initNavigationListeners(rootView);
        return rootView;
    }

    /**
     * Init screen views
     *
     * @param rootView
     */
    private void initViews(View rootView) {
        tvQuestion = rootView.findViewById(R.id.tvQuestion);
        tvQuestion.setText(mQuestionList == null ? "NA" : mQuestionList.get(mPosition).getQuestion());
    }

    private void setUpWearableRecyclerView(View rootView) {
        mWearableRecyclerView = rootView.findViewById(R.id.recycleView);
        // Aligns the first and last items on the list vertically centered on the screen.
        mWearableRecyclerView.setEdgeItemsCenteringEnabled(true);

        // Customizes scrolling so items farther away form center are smaller.
        ScalingScrollLayoutCallback scalingScrollLayoutCallback = new ScalingScrollLayoutCallback(); // it will work same as Circle in Square device
        mWearableRecyclerView.setLayoutManager(
                new WearableLinearLayoutManager(getActivity()));

        // Improves performance because we know changes in content do not change the layout size of
        // the RecyclerView.
        mWearableRecyclerView.setHasFixedSize(true);
        mQuestionAdapter = new QuestionsAdapter(getActivity(), mQuestionList.get(mPosition));
        mWearableRecyclerView.setAdapter(mQuestionAdapter);

        /**
         * RecyclerView: Implementing single item click and long press (Part-II)
         * */
        mWearableRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                mWearableRecyclerView, new ClickListeners() {
            @Override
            public void onClick(View view, final int position) {
                mOptionPosition = position;
                updateAnswerForThisQuestion(position);
            }

            @Override
            public void onLongClick(View view, int position) {
                mOptionPosition = position;
                updateAnswerForThisQuestion(position);
            }
        }));
    }

    /**
     * Init toolbar and ncavigation listeners
     *
     * @param rootView
     */
    private void initToolBar(View rootView) {
        mToolbar = rootView.findViewById(R.id.toolbar);
        mTvTitle = mToolbar.findViewById(R.id.title);
        mBtnBack = mToolbar.findViewById(R.id.btnBack);
        mTvCancel = mToolbar.findViewById(R.id.cancel);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        mTvTitle.setText("Step " + (mPosition + 1) + " of 6");

        //dont show back button if walk test screen and in further survey question's screen.
        if (mPosition == (WearUtils.ScreenName.question_four.getScreen() + 1)) {
            mBtnBack.setVisibility(View.VISIBLE);
        } else if (mPosition > (WearUtils.ScreenName.walk_test.getScreen())) {
            mBtnBack.setVisibility(View.INVISIBLE);
        } else {
            mBtnBack.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Overriding metod to perform ConsentScreen action Visual info type
     * #1 mToolbar back navigation action
     * #2 mToolbar Cancel action
     * #2 screen Next button action
     *
     * @param rootView
     */

    private void initNavigationListeners(final View rootView) {
        rootView.findViewById(R.id.tvNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 *If Yes close the activity else if No then go to next question
                 * first condition check if you are in last but one screen (Questionaire's last but one screen)
                 * second condition to check whether user has selected "no"*/

                if (mPosition == (WearUtils.ScreenName.question_four.getScreen() + 1) && checkIsAnswerSelectedforThisQuestion()) { //last question position
                    showActivityConfirm();
                } else if (checkIsAnswerSelectedforThisQuestion()) {
                    mIOnNextListener.onNextPressed(mPosition);
                } else {
                    WearUtils.showSnackBar(rootView.findViewById(R.id.fragment_mainLayout), getString(R.string.please_select_answer), Snackbar.LENGTH_LONG);
                }
            }
        });

        /*   //Yes or No Screen
        if (mPosition == mQuestionList.size() - 2 && mActivityTitle.equalsIgnoreCase("Quality of Life Survey")) {
            rootView.findViewById(R.id.tvNext).setVisibility(View.INVISIBLE);
        }*/
        rootView.findViewById(R.id.tvSkip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAnswerSkipped(); // update the answer to null if allready selected any
                if (mPosition == (WearUtils.ScreenName.question_four.getScreen() + 1)) {  //last question position
                    showActivityConfirm();
                } else {
                    mIOnNextListener.onNextPressed(mPosition);
                }
            }
        });


        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnBackPressListener.onFragmentBackPressed(mPosition);
            }
        });

        mTvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIOnBackPressListener.onFragmentBackPressed(mPosition);
            }
        });

        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancelAlert();
            }
        });
    }

    private void showActivityConfirm() {
        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                ConfirmationActivity.SUCCESS_ANIMATION);
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                getString(R.string.activity_cmplete));
        startActivityForResult(intent,100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 100)
        mIOnNextListener.onNextPressed(mPosition);
    }

    /**
     * Upadate the answer for each questions
     *
     * @param position
     */
    private void updateAnswerForThisQuestion(int position) {
        List<Options> optionsList = mQuestionList.get(mPosition).getOptions(); // get Option list fof this question from main list
        mQuestionList.get(mPosition).setAnswer(optionsList.get(position).getName()); // select the answer from options and update in in main mQuestionList
        String dateTime = WearUtils.getCurrentUTCDateTimeWithMillSeconds();
        optionsList.get(position).setDateTime(dateTime);
        mQuestionList.get(mPosition).setCurrentTime(optionsList.get(position).getDateTime());
    }

    /**
     * Upadate the answer for each questions
     */
    private void updateAnswerSkipped() {
        mQuestionList.get(mPosition).setAnswer(null); // select the answer from options and update in in main mQuestionList
        String dateTime = WearUtils.getCurrentUTCDateTimeWithMillSeconds();
        mQuestionList.get(mPosition).setCurrentTime(dateTime);
        mQuestionAdapter.mSelectedItem = -1; //deselecting the last selected position
        mQuestionAdapter.notifyItemChanged(mOptionPosition); // refresh the radio button selection to uncheck
    }

    /**
     * on next click check whether answer is selected allready
     *
     * @return
     */
    private boolean checkIsAnswerSelectedforThisQuestion() {
        String answer = mQuestionList.get(mPosition).getAnswer();
        return !TextUtils.isEmpty(answer);
    }

    /**
     * on next click check if you are in Meication screen
     * If Yes got to next questionaire
     * else if No finish the activity and mark it as completed.
     *
     * @return
     */
    private boolean checkIsNoSelected() {
        if (mQuestionList.get(mPosition).getAnswer() != null) {
            String answer = mQuestionList.get(mPosition).getAnswer().toLowerCase();
            return !TextUtils.isEmpty(answer) && answer.equalsIgnoreCase("no");
        } else {
            return false;
        }
    }

    /**
     * Create final answer json to send to server
     * save this in preference for offline use
     */
    private void fetchAllAnswers() {
    }

    /**
     * Show dialog for Complete fragment
     * List<Questions> question, int position, String activityTitle
     */
  /*  private void showActivityConfirmDialog() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ActivityCompleteFragment surveyConfirmDialogFragment = ActivityCompleteFragment.newInstance(mQuestionList, mPosition, mActivityTitle);
        surveyConfirmDialogFragment.show(fm, "ACTIVITY_COMPLETE_FRAGMENT");
    }*/
}
