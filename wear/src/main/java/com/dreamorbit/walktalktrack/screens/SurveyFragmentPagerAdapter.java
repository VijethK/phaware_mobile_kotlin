package com.dreamorbit.walktalktrack.screens;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.survey.Questions;
import com.dreamorbit.walktalktrack.walktest.RestingFragment;
import com.dreamorbit.walktalktrack.walktest.WalkTestFragment;

import java.util.List;

/**
 * Created by mujasam.bn on 10/3/2017.
 */

public class SurveyFragmentPagerAdapter extends FragmentPagerAdapter {

    private final Resources resources;
    private final String activityTitle;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    private List<Questions> root;
    private ViewPager viewPager;

    /**
     * Create mPager mAdapter
     *
     * @param resources
     * @param fm
     * @param root
     * @param activityTitle
     */
    public SurveyFragmentPagerAdapter(ViewPager viewPager, final Resources resources, FragmentManager fm, List<Questions> root, String activityTitle) {
        super(fm);
        this.resources = resources;
        this.root = root;
        this.activityTitle = activityTitle;
        this.viewPager = viewPager;

        root.add((WearUtils.ScreenName.timer.getScreen()+1), new Questions()); //Timer screen 2
        root.add((WearUtils.ScreenName.walk_test.getScreen()+1), new Questions()); //Fit screen 3
        root.add((WearUtils.ScreenName.resting.getScreen()+1), new Questions()); //Resting screen 4
        root.add(root.size(), new Questions()); //Summary screen 7

    }

    /**
     * Call a factory to initialize the fragment.
     * Show the screen depend on the ui
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        Fragment result = null;  //getRegisteredFragment(position);
        Log.e("POS"," "+ position);
        if (position == (WearUtils.ScreenName.timer.getScreen()+1) ) { //2
            result = TimerFragment.newInstance(root, position, activityTitle);
        } else if (position == (WearUtils.ScreenName.walk_test.getScreen()+1) ) { //3
            result = WalkTestFragment.newInstance(root, position, viewPager, activityTitle);
        } else if (position == (WearUtils.ScreenName.resting.getScreen()+1) ) { //4
            result = RestingFragment.newInstance(root, position, viewPager, activityTitle);
        } else if(position == (WearUtils.ScreenName.summary.getScreen()+1) ){ //7
            result = SummaryFragment.newInstance(root,position, viewPager,activityTitle);
        } else { // Show questions fragment for position 0,1,5,6
            result = SurveyQuestionsFragment.newInstance(root, position, activityTitle);
        }

        return result;
    }

    /**
     * ViewPagerAdapter will loop depend on this count.
     *
     * @return
     */
    @Override
    public int getCount() {
        //int count = root.size() + 1; //adding one fragment to show Activity completed
        int count = root.size(); //adding one fragment to show Activity completed
        return count;
    }

    /**
     * Title for the fragment
     *
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(final int position) {
        return root.get(position).getQuestion();
    }

    /**
     * On each Fragment instantiation we are saving the reference of that Fragment in a Map
     * It will help us to retrieve the Fragment by mPosition
     *
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Get the Fragment by mPosition
     *
     * @param position tab mPosition of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}