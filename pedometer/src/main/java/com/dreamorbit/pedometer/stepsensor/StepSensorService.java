package com.dreamorbit.pedometer.stepsensor;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.PedoUtils;
import com.dreamorbit.pedometer.pojo.pedometer.Item;

import java.util.List;

/**
 * The main application service. Does stuff.
 */
public class StepSensorService extends BaseSensorService implements SensorEventListener {

    private static final String TAG = "StepSensorService";
    public static final String ACTION_FLUSH_SENSOR = "net.frakbot.FStepService.FlushSensorData";
    private Sensor mStepCounter, mStepDetector;
    private Handler mUiHandler;
    private int mLastCount, mInitialCount;
    private boolean mInitialCountInitialized;
    private PendingIntent mSensorUpdatePIntent;
    private IntentFilter mIntentFilter;

    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_FLUSH_SENSOR.equals(intent.getAction())) {
                Log.i(TAG, "Flushing the sensor's data");
                mSensorManager.flush(StepSensorService.this);
            }
        }
    };

    /**
     * Calculates the maximum sensor report interval, based on the
     * hardware sensor events buffer size, to avoid dropping steps.
     *
     * @param stepCounter The Step Counter sensor
     * @return Returns the optimal update interval, in milliseconds
     */
    private static int calcSensorReportInterval(Sensor stepCounter) {
        // We assume that, normally, a person won't do more than
        // two steps in a second (worst case: running)
        final int fifoSize = stepCounter.getFifoReservedEventCount();
        if (fifoSize > 1) {
            return (fifoSize / 2) * 1000;
        }

        // In this case, the device seems not to have an HW-backed
        // sensor events buffer. We're assuming that there's no
        // batching going on, so we don't really need the alarms.
        return 0;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "Creating the service 1");

        //PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TAG");
        PackageManager packageManager = getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER) && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)) {
            initHeartrate();
            registerStepListener();
            startJsonCreatorTimer();
        } else {
            Toast.makeText(this, "This phone does not supports required sensor", Toast.LENGTH_SHORT).show();
        }

        Log.e(TAG, "Creating the service 2");
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(ACTION_FLUSH_SENSOR);
        registerReceiver(mUpdateReceiver, mIntentFilter);
    }

    private void registerStepListener() {
        mStepCounter = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        mStepDetector = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        mUiHandler = new Handler(Looper.getMainLooper());
        mSensorUpdatePIntent = PendingIntent.getService(this, 1, new Intent(ACTION_FLUSH_SENSOR), 0);

        // Batching for the step counter doesn't make sense (the buffer holds
        // just one step counter event anyway, as it's not a continuous event)
        mSensorManager.registerListener(this, mStepCounter, SensorManager.SENSOR_DELAY_NORMAL);

        // We do instead use batching for the step detector sensor
        final int reportInterval = calcSensorReportInterval(mStepDetector);
        mSensorManager.registerListener(this, mStepDetector, SensorManager.SENSOR_DELAY_NORMAL,
                reportInterval * 1000 /*  micro seconds */);

        if (reportInterval > 0) {
            Log.i(TAG, "Setting up batched data retrieval every " + reportInterval + " ms");
            setupSensorUpdateAlarm(reportInterval);
        } else {
            Log.w(TAG, "This device doesn't support events batching!");
        }
    }

    /***************************************
     * Init Hear rate sensor
     ***************************************/
    private void initHeartrate() {
        Sensor mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        boolean res = mSensorManager.registerListener(this, mHeartRateSensor, 10000);
    }

    /**
     * Sets up a wakelock-based alarm that allows this service
     * to retrieve sensor events before they're dropped out of
     * the FIFO buffer.
     */
    private void setupSensorUpdateAlarm(int interval) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval,
                interval, mSensorUpdatePIntent);
    }

    public IBinder onBind(Intent intent) {
        Log.v(TAG, "Binding the service");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "Destroying the service");
        mIsStop = false;

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(mSensorUpdatePIntent);
        mSensorManager.unregisterListener(this);
        mPedoItems = addLastFetchedData();
        mCallback.onDistanceListFetched(mPedoItems);
        mCountDownTimerJson.cancel();
        unregisterReceiver(mUpdateReceiver);
    }

    private List<Item> addLastFetchedData() {
        if (updates.get("Distance") != null && updates.get("Steps") != null) {
            Item item = new Item();
            item.setDistance(Double.parseDouble(updates.get("Distance").toString()));
            item.setNumberOfSteps(Integer.parseInt(updates.get("Steps").toString()));
            item.setStartDate(PedoUtils.getStartEndDates()[1]);
            item.setEndDate(PedoUtils.getStartEndDates()[0]);
            item.setFloorsAscended(0);
            item.setFloorsDescended(0);
            mPedoItems.add(item);
            Log.d("Step Log: ", item.toString());
        }
        return mPedoItems;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final int type = event.sensor.getType();
        if (type == Sensor.TYPE_STEP_COUNTER) {
            if (!mInitialCountInitialized) {
                Log.e(TAG, "Initializing initial steps count: " + (int) event.values[0]);
                mInitialCount = (int) event.values[0];
                mInitialCountInitialized = true;
            }
            if (mIsStop) {
                mInitialCount = (int) event.values[0];
                mStopCount = mLastCount; //store the last count
                Log.e(TAG, "***Stop*** Step InitialCount: " + mInitialCount);
                Log.e(TAG, "***Stop*** Step StopCount: " + mStopCount);
            } else {
                Log.e(TAG, "***Resume***" +"NowLive: "+ (int) event.values[0] +" LastSensor: "+ mInitialCount +" LastStep: "+ mStopCount);
                mLastCount = ((int) event.values[0] - mInitialCount) + mStopCount;
                Log.e(TAG, "***Resume*** Final count: " + mLastCount);
                postSensorChange(mLastCount, Sensor.TYPE_STEP_COUNTER);
                //PedoUtils.insertStepDataToGoogleFit(mLastCount,StepSensorService.this);
            }
        } else {
            //is this a heartbeat event and does it have data?
            if (event.sensor.getType() == Sensor.TYPE_HEART_RATE && event.values.length > 0) {
                int newValue = Math.round(event.values[0]);
                //Log.d(LOG_TAG,sensorEvent.sensor.getName() + " changed to: " + newValue);
                // only do something if the value differs from the value before and the value is not 0.
                if (currentValue != newValue && newValue != 0) {
                    // save the new value
                    currentValue = newValue;
                    // send the value to the listener
                    if (mCallback != null && !mIsStop) {
                        //Log.d(LOG_TAG, "sending new value to listener: " + newValue);
                        mCallback.onHearBeatValueChanged(newValue);
                        PedoUtils.insertHeartRateDataToGoogleFit(newValue,StepSensorService.this);
                    }
                }
            }
        }/*else(type == Sensor.TYPE_STEP_DETECTOR) {
            mLastDetectorCount++;
            Log.v("StepSensorService", "New step detector event. Updated count: " + mLastDetectorCount);
            postSensorChange((int) mLastDetectorCount, Sensor.TYPE_STEP_DETECTOR);
        }*/
    }

    /**
     * Posts a step sensor event, both for the counter and the detector,
     * to the registered listeners.
     *
     * @param value The event value, if any
     * @param type  The sensor type
     */
    private void postSensorChange(final int value, final int type) {
        if (Looper.getMainLooper().equals(Looper.myLooper())) {
            // UI thread
            if (mCallback != null) {
                if (type == Sensor.TYPE_STEP_COUNTER) {
                    float distance = getDistanceRun(value);
                    mCallback.stepsChanged(value);
                    mCallback.distanceChanged(distance);
                    updates.put("Steps", value);
                    updates.put("Distance", distance);
                } else if (type == Sensor.TYPE_STEP_DETECTOR) {
                    //l.onStep(value);
                }
            }
        } else {
            // Non-UI thread
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mCallback != null) {
                        if (type == Sensor.TYPE_STEP_COUNTER) {
                            float distance = getDistanceRun(value);
                            mCallback.stepsChanged(value);
                            mCallback.distanceChanged(getDistanceRun(value));
                            updates.put("Steps", value);
                            updates.put("Distance", distance);
                        } else if (type == Sensor.TYPE_STEP_DETECTOR) {
                            //l.onStep(value);
                        }
                    }
                }
            });
        }
    }


    //function to determine the distance run in kilometers using average step length for men and number of steps
    public float getDistanceRun(int steps) {
        float distance = (((float) (steps * 78) / (float) 100000) * 1000);
        return distance;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Log.v(TAG, "Sensor accuracy changed. New value: " + accuracy);
    }

    //region Sensor data capture


}
