package com.dreamorbit.pedometer;

import android.app.Service;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import com.dreamorbit.pedometer.pojo.pedometer.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseSensorService extends Service {

    private static final String TAG = "BaseSensorService";
    protected final IBinder mBinder = new StepBinder();
    protected IPedometerCallback mCallback;
    public static boolean mIsStop;
    public CountDownTimer mCountDownTimerJson;
    protected List<Item> mPedoItems = new ArrayList<>();
    protected SensorManager mSensorManager;
    protected int mStopCount = 0;

    //Storing the data
    protected Map<String, Object> updates = new HashMap<String, Object>();

    //heart rate
    protected int currentValue = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    }

    /*
     * programming interface that clients can use to interact with the service.
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        mIsStop = false;
        return mBinder;
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class StepBinder extends Binder {
        public BaseSensorService getService() {
            return BaseSensorService.this;
        }
    }

    public void registerCallback(IPedometerCallback cb) {
        mCallback = cb;
    }

    /**
     * Store every 10 seconds data
     */
    public void startJsonCreatorTimer() {
        mCountDownTimerJson = new CountDownTimer(360000, 10000) {
            public void onTick(long millisUntilFinished) {

                long secondsLimit = millisUntilFinished / 1000;
                String minute = String.format("%02d", secondsLimit / 60) + ":" + String.format("%02d", secondsLimit % 60);
                Log.e(TAG, "startJsonCreatorTimer " + minute);

                if (updates.get("Distance") != null && updates.get("Steps") != null) {
                    Item item = new Item();
                    item.setDistance(Double.parseDouble(updates.get("Distance").toString()));
                    item.setNumberOfSteps(Integer.parseInt(updates.get("Steps").toString()));
                    item.setStartDate(PedoUtils.getStartEndDates()[1]);
                    item.setEndDate(PedoUtils.getStartEndDates()[0]);
                    item.setFloorsAscended(0);
                    item.setFloorsDescended(0);
                    mPedoItems.add(item);
                    Log.d("Step Log: ", item.toString());
                }
            }

            public void onFinish() {
            }
        };

        mCountDownTimerJson.start();
    }

    public void unregisterListener() {
        Log.v("BaseSensorService", "Unregistering a listener");
        mCallback = null;
    }

    /***************************************
     * Pause the walk test
     ***************************************/
    public void pauseWalkTest() {
        mIsStop = true;
        mCountDownTimerJson.cancel();
    }

    /***************************************
     * Resume the walk test
     ***************************************/
    public void resumeWalkTest() {
        mIsStop = false;
        startJsonCreatorTimer();
    }
}
