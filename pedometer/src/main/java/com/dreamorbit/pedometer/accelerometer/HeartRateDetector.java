package com.dreamorbit.pedometer.accelerometer;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import static com.dreamorbit.pedometer.accelerometer.AcceleroMeterService.mIsStop;

/**
 * Detects heart and notifies all listeners
 */
public class HeartRateDetector implements SensorEventListener {
    private final static String TAG = "StepDetector";
    private int currentValue = 0;

    private StepListener mStepListener;

    public HeartRateDetector() {
    }

    public void addStepListener(StepListener sl) {
        mStepListener = sl;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        synchronized (this) {
            // is this a heartbeat event and does it have data?
            if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE && sensorEvent.values.length > 0) {
                int newValue = Math.round(sensorEvent.values[0]);
//            int newValue = 60;
                //Log.d(LOG_TAG,sensorEvent.sensor.getName() + " changed to: " + newValue);
                // only do something if the value differs from the value before and the value is not 0.
                if (currentValue != newValue && newValue != 0) {
                    // save the new value
                    currentValue = newValue;
                    // send the value to the listener
                    if (mStepListener != null && !mIsStop) {
                        //Log.d(LOG_TAG, "sending new value to listener: " + newValue);
                        mStepListener.passHeartRate(newValue);
                    }
                }
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}