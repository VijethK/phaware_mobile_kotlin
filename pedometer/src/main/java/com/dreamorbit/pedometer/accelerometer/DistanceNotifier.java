package com.dreamorbit.pedometer.accelerometer;

public class DistanceNotifier implements StepListener, SpeakingTimer.Listener {

    float mDistance = 0;
    PedometerSettings mSettings;
    PedometerUtils mUtils;
    boolean mIsMetric;
    float mStepLength;
    private Listener mListener;
    public DistanceNotifier(Listener listener, PedometerSettings settings, PedometerUtils utils) {
        mListener = listener;
        mUtils = utils;
        mSettings = settings;
        reloadSettings();
    }

    public void setDistance(float distance) {
        mDistance = distance;
        notifyListener();
    }

    public void reloadSettings() {
        mIsMetric = mSettings.isMetric();
        mStepLength = mSettings.getStepLength();
        notifyListener();
    }

    public void onStep() {

        if (mIsMetric) {
            mDistance += (float) (// kilometers
                    mStepLength // centimeters
                    / 100000.0); // centimeters/kilometer
        } else {
            mDistance += (float) (// miles
                    mStepLength // inches
                     / 63360.0); // inches/mile
        }

        notifyListener();
    }


    private void notifyListener() {
        mListener.valueChanged(convertMilesToMeter(mDistance));
    }

    private float convertMilesToMeter(float value) {
        int distance = (int)(value*1000);
        if (distance <= 0) {
            return 0;
        }else {
            float distanceDiv = distance / 1000f;
            String mileDistance = ("" + (distanceDiv + 0.000001f)).substring(0, 5);
            float meterDistance = (((Float.parseFloat(mileDistance))*1609.344f)); //mile to meter convert
            return meterDistance;
        }
    }

    public void passValue() {
        // Callback of StepListener - Not implemented
    }

    @Override
    public void passHeartRate(int heartRate) {
    }

    public void speak() {
        if (mSettings.shouldTellDistance()) {
            if (mDistance >= .001f) {
                mUtils.say(("" + (mDistance + 0.000001f)).substring(0, 4) + (mIsMetric ? " kilometers" : " miles"));
                // TODO: format numbers (no "." at the end)
            }
        }
    }

    public interface Listener {
        void valueChanged(float value);

        void passValue();
    }


}

