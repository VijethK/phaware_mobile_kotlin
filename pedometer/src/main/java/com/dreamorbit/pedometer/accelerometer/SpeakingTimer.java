package com.dreamorbit.pedometer.accelerometer;

import java.util.ArrayList;

/**
 * Call all listening objects repeatedly. 
 * The interval is defined by the user settings.
 */
public class SpeakingTimer implements StepListener {

    PedometerSettings mSettings;
    PedometerUtils mUtils;
    boolean mShouldSpeak;
    float mInterval;
    long mLastSpeakTime;
    
    public SpeakingTimer(PedometerSettings settings, PedometerUtils utils) {
        mLastSpeakTime = System.currentTimeMillis();
        mSettings = settings;
        mUtils = utils;
        reloadSettings();
    }
    public void reloadSettings() {
        mShouldSpeak = mSettings.shouldSpeak();
        mInterval = mSettings.getSpeakingInterval();
    }
    
    public void onStep() {
        long now = System.currentTimeMillis();
        long delta = now - mLastSpeakTime;
        
        if (delta / 60000.0 >= mInterval) {
            mLastSpeakTime = now;
            notifyListeners();
        }
    }

    @Override
    public void passHeartRate(int heartRate) {
        notifyListeners();
    }

    public void passValue() {
    }
    //-----------------------------------------------------
    // Listener
    
    public interface Listener {
        void speak();
    }
    private ArrayList<Listener> mListeners = new ArrayList<>();

    public void addListener(Listener l) {
        mListeners.add(l);
    }
    public void notifyListeners() {
        mUtils.ding();
        for (Listener listener : mListeners) {
            listener.speak();
        }
    }

    //-----------------------------------------------------
    // Speaking
    
    public boolean isSpeaking() {
        return mUtils.isSpeakingNow();
    }
}

