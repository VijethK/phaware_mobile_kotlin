package com.dreamorbit.pedometer.accelerometer;

public class HeartRateNotifier implements StepListener, SpeakingTimer.Listener {

    public interface Listener {
        void valueChanged(float value);
        void passValue();
    }
    private Listener mListener;
    private int mHeartRate = 0;

    PedometerSettings mSettings;
    PedometerUtils mUtils;

    public HeartRateNotifier(Listener listener, PedometerSettings settings, PedometerUtils utils) {
        mListener = listener;
        mUtils = utils;
        mSettings = settings;
        reloadSettings();
    }
    public void setHeartRate(int heartRate) {
        mHeartRate = heartRate;
        notifyListener();
    }

    public void reloadSettings() {
        notifyListener();
    }
    public void resetValues() {
        mHeartRate = 0;
    }

    public void onStep() {
        notifyListener();
    }

    @Override
    public void passHeartRate(int heartRate) {
        mHeartRate = heartRate;
        notifyListener();
    }

    private void notifyListener() {
        mListener.valueChanged(mHeartRate);
    }
    
    public void passValue() {
    }

    public void speak() {
        if (mSettings.shouldTellCalories()) {
            if (mHeartRate > 0) {
                mUtils.say("" + mHeartRate + " Heart rate");
            }
        }
    }
}

