package com.dreamorbit.pedometer.accelerometer;

import java.util.ArrayList;

/**
 * Counts steps provided by StepDetector and passes the current
 * step count to the activity.
 */
public class StepDisplayer implements StepListener, SpeakingTimer.Listener {

    PedometerSettings mSettings;
    PedometerUtils mUtils;
    private int mCount = 0;
    private ArrayList<Listener> mListeners = new ArrayList<Listener>();

    public StepDisplayer(PedometerSettings settings, PedometerUtils utils) {
        mUtils = utils;
        mSettings = settings;
        notifyListener();
    }

    public void setUtils(PedometerUtils utils) {
        mUtils = utils;
    }

    public void setSteps(int steps) {
        mCount = steps;
        notifyListener();
    }

    public void onStep() {
        mCount++;
        notifyListener();
    }

    public void reloadSettings() {
        notifyListener();
    }

    //-----------------------------------------------------
    // Listener

    public void passValue() {
    }

    public void addListener(Listener l) {
        mListeners.add(l);
    }

    public void notifyListener() {
        for (Listener listener : mListeners) {
            listener.stepsChanged(mCount);
        }
    }

    @Override
    public void passHeartRate(int heartRate) {
    }

    public void speak() {
        if (mSettings.shouldTellSteps()) {
            if (mCount > 0) {
                mUtils.say("" + mCount + " steps");
            }
        }
    }

    //-----------------------------------------------------
    // Speaking

    public interface Listener {
        void stepsChanged(int value);

        void passValue();
    }


}
