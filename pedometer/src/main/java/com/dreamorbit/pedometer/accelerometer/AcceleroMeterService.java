package com.dreamorbit.pedometer.accelerometer;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.PedoUtils;
import com.dreamorbit.pedometer.R;
import com.dreamorbit.pedometer.pojo.pedometer.Item;

import java.util.List;

public class AcceleroMeterService extends BaseSensorService {
    private static final String TAG = "StepService";
    private SharedPreferences mSettings;
    private PedometerSettings mPedometerSettings;
    private SharedPreferences mState;
    private SharedPreferences.Editor mStateEditor;
    private PedometerUtils mUtils;
    private SensorManager mSensorManager;
    private SensorManager mHeartSensorManager;
    private Sensor mSensor;
    private StepDetector mStepDetector;
    private HeartRateDetector mHeartRateDetector;
    private StepDisplayer mStepDisplayer;
    private DistanceNotifier mDistanceNotifier;
    private SpeakingTimer mSpeakingTimer;
    private HeartRateNotifier mHeartRateNotifier;

    private PowerManager.WakeLock wakeLock;
    private NotificationManager mNM;

    private int mSteps;
    private int mPace;
    private float mDistance;
    private float mSpeed;
    private float mCalories;
    private int mHeartRate;

    @Override
    public void onCreate() {
        Log.i(TAG, "[SERVICE] onCreate");
        super.onCreate();

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //showNotification();

        // Load settings
        mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        mPedometerSettings = new PedometerSettings(mSettings);
        mState = getSharedPreferences("state", 0);

        mUtils = PedometerUtils.getInstance();
        mUtils.setService(this);
        mUtils.initTTS();

        acquireWakeLock();
        registerDetector();
        initHeartrate();
        startJsonCreatorTimer();

        // Register our receiver for the ACTION_SCREEN_OFF action. This will make our receiver
        // code be called whenever the phone enters standby mode.
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, filter);

        mStepDisplayer = new StepDisplayer(mPedometerSettings, mUtils);
        mStepDisplayer.setSteps(mSteps = mState.getInt("steps", 0));
        mStepDisplayer.addListener(mStepListener);
        mStepDetector.addStepListener(mStepDisplayer);

        mDistanceNotifier = new DistanceNotifier(mDistanceListener, mPedometerSettings, mUtils);
        mDistanceNotifier.setDistance(mDistance = mState.getFloat("distance", 0));
        mStepDetector.addStepListener(mDistanceNotifier);

        mHeartRateNotifier = new HeartRateNotifier(mHeartRateListener, mPedometerSettings, mUtils);
        mHeartRateNotifier.setHeartRate(mHeartRate);
        mHeartRateDetector.addStepListener(mHeartRateNotifier);

        mSpeakingTimer = new SpeakingTimer(mPedometerSettings, mUtils);
        mSpeakingTimer.addListener(mStepDisplayer);
        mSpeakingTimer.addListener(mDistanceNotifier);
        mSpeakingTimer.addListener(mHeartRateNotifier);

        mStepDetector.addStepListener(mSpeakingTimer);
        // Used when debugging:
        // mStepBuzzer = new StepBuzzer(this);
        // mStepDetector.addStepListener(mStepBuzzer);

        // Start voice
        reloadSettings();
        resetValues();

        // Tell the user we started.
        //Toast.makeText(this, getText(R.string.started), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        mPedoItems = addLastFetchedData();
        mCallback.onDistanceListFetched(mPedoItems);

        Log.i(TAG, "[SERVICE] onDestroy");
        mUtils.shutdownTTS();

        // Unregister our receiver.
        unregisterReceiver(mReceiver);
        unregisterDetector();

        mStateEditor = mState.edit();
        mStateEditor.putInt("steps", mSteps);
        mStateEditor.putInt("pace", mPace);
        mStateEditor.putFloat("distance", mDistance);
        mStateEditor.putFloat("speed", mSpeed);
        mStateEditor.putFloat("calories", mCalories);
        mStateEditor.commit();

        mNM.cancel(R.string.app_name);
        wakeLock.release();

        // Stop detecting
        mSensorManager.unregisterListener(mStepDetector);
        mHeartSensorManager.unregisterListener(mHeartRateDetector);

        // Tell the user we stopped.
        //Toast.makeText(this, getText(R.string.stopped), Toast.LENGTH_SHORT).show();
    }

    private List<Item> addLastFetchedData() {
        if (updates.get("Distance") != null && updates.get("Steps") != null) {
            Item item = new Item();
            item.setDistance(Double.parseDouble(updates.get("Distance").toString()));
            item.setNumberOfSteps(Integer.parseInt(updates.get("Steps").toString()));
            item.setStartDate(PedoUtils.getStartEndDates()[1]);
            item.setEndDate(PedoUtils.getStartEndDates()[0]);
            item.setFloorsAscended(0);
            item.setFloorsDescended(0);
            mPedoItems.add(item);
            Log.d("Step Log: ", item.toString());
        }
        return mPedoItems;
    }

    private void registerDetector() {
        // Start detecting
        mStepDetector = new StepDetector();
        mHeartRateDetector = new HeartRateDetector();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mHeartSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(mStepDetector, mSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    /***************************************
     * Init Hear rate sensor
     ***************************************/
    private void initHeartrate() {
        mHeartSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mHeartRateSensor = mHeartSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        boolean res = mHeartSensorManager.registerListener(mHeartRateDetector, mHeartRateSensor, 10000);
    }

    private void unregisterDetector() {
        mSensorManager.unregisterListener(mStepDetector);
        mHeartSensorManager.unregisterListener(mHeartRateDetector);
    }

    /***************************************
     * programming interface that clients can use to interact with the service.
     * @param intent
     * @return
     ***************************************/
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "[SERVICE] onBind");
        mIsStop = false;
        return mBinder;
    }

    /***************************************
     * Pause the walk test
     ***************************************/
    public void pauseWalkTest() {
        mIsStop = true;
    }

    /***************************************
     * Resume the walk test
     ***************************************/
    public void resumeWalkTest() {
        mIsStop = false;
    }

    public void reloadSettings() {
        mSettings = PreferenceManager.getDefaultSharedPreferences(this);

        if (mStepDetector != null) {
            mStepDetector.setSensitivity(
                    Float.valueOf(mSettings.getString("sensitivity", "10"))
            );
        }

        if (mStepDisplayer != null) mStepDisplayer.reloadSettings();
        if (mDistanceNotifier != null) mDistanceNotifier.reloadSettings();
        if (mSpeakingTimer != null) mSpeakingTimer.reloadSettings();
    }

    public void resetValues() {
        mStepDisplayer.setSteps(0);
        mDistanceNotifier.setDistance(0);
    }

    /**
     * Forwards pace values from PaceNotifier to the activity.
     */
    private StepDisplayer.Listener mStepListener = new StepDisplayer.Listener() {
        public void stepsChanged(int value) {
            mSteps = value;
            passValue();
        }

        public void passValue() {
            if (mCallback != null) {
                updates.put("Steps", mSteps);
                mCallback.stepsChanged(mSteps);
                PedoUtils.insertStepDataToGoogleFit(mSteps,AcceleroMeterService.this);
            }
        }
    };

    /**
     * Forwards distance values from DistanceNotifier to the activity.
     */
    private DistanceNotifier.Listener mDistanceListener = new DistanceNotifier.Listener() {
        public void valueChanged(float value) {
            mDistance = value;
            passValue();
        }

        public void passValue() {
            if (mCallback != null) {
                updates.put("Distance", mDistance);
                mCallback.distanceChanged(mDistance); //converting miles to meter
            }
        }
    };

    /**
     * Forwards Heart rate values from HeartRateNotifier to the activity.
     */
    private HeartRateNotifier.Listener mHeartRateListener = new HeartRateNotifier.Listener() {
        public void valueChanged(float value) {
            mHeartRate = (int) value;
            passValue();
        }

        public void passValue() {
            if (mCallback != null) {
                mCallback.onHearBeatValueChanged(mHeartRate);
                PedoUtils.insertHeartRateDataToGoogleFit(mHeartRate,AcceleroMeterService.this);
            }
        }
    };


    /**
     * Show a notification while this service is running.
     */
    /*private void showNotification() {
        CharSequence text = getText(R.string.app_name);
        Notification notification = new Notification(R.drawable.ic_notification, null,
                System.currentTimeMillis());
        notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        Intent pedometerIntent = new Intent();
        pedometerIntent.setComponent(new ComponentName(this, AcceleroMeterService.class));
        pedometerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                pedometerIntent, 0);
        mNM.notify(R.string.app_name, notification);
    }*/


    // BroadcastReceiver for handling ACTION_SCREEN_OFF.
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Check action just to be on the safe side.
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                // Unregisters the listener and registers it again.
                AcceleroMeterService.this.unregisterDetector();
                AcceleroMeterService.this.registerDetector();
                if (mPedometerSettings.wakeAggressively()) {
                    wakeLock.release();
                    acquireWakeLock();
                }
            }
        }
    };

    @SuppressLint("InvalidWakeLockTag")
    private void acquireWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        int wakeFlags;
        if (mPedometerSettings.wakeAggressively()) {
            wakeFlags = PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP;
        } else if (mPedometerSettings.keepScreenOn()) {
            wakeFlags = PowerManager.SCREEN_DIM_WAKE_LOCK;
        } else {
            wakeFlags = PowerManager.PARTIAL_WAKE_LOCK;
        }
        wakeLock = pm.newWakeLock(wakeFlags, TAG);
        wakeLock.acquire();
    }
}

