package com.dreamorbit.pedometer;

import com.dreamorbit.pedometer.pojo.pedometer.Item;

import java.util.List;

public interface IPedometerCallback {
    void stepsChanged(int value);

    void distanceChanged(float value);

    void onHearBeatValueChanged(int hearRate);

    void onDistanceListFetched(List<Item> items);
}
