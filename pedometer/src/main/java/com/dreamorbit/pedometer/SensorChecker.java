package com.dreamorbit.pedometer;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentActivity;

public class SensorChecker {

    private Context mContext;

    public SensorChecker(FragmentActivity mContext) {
            this.mContext = mContext;
    }

    public void SensorChecker(Context mContext) {
        this.mContext = mContext;
    }

    public SensorType getPedometerSensor() {
        PackageManager packageManager = mContext.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER) && packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)) {
            return SensorType.STEP;
        } else {
            return SensorType.ACCELEROMETER;
        }
    }

    public enum SensorType {
        FIT, STEP, ACCELEROMETER
    }

}


