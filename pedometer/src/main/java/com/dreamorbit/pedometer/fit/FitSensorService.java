package com.dreamorbit.pedometer.fit;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;
import com.dreamorbit.pedometer.BaseSensorService;
import com.dreamorbit.pedometer.IPedometerCallback;
import com.dreamorbit.pedometer.PedoUtils;
import com.dreamorbit.pedometer.pojo.pedometer.DistanceItem;
import com.dreamorbit.pedometer.pojo.pedometer.Item;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*import com.dreamorbit.walktalktrack.helper.WearUtils;
import com.dreamorbit.walktalktrack.model.pedometer.distance.DistanceItem;
import com.dreamorbit.walktalktrack.model.pedometer.distance.PedoDistanceRequest;
import com.dreamorbit.walktalktrack.model.pedometer.steps.Item;
import com.dreamorbit.walktalktrack.model.pedometer.steps.Pedometer;*/

/**
 * Created by mujasam.bn on 10/17/2018.
 */

public class FitSensorService extends BaseSensorService implements SensorEventListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String LOG_TAG = "MyHeart";
    private static final String TAG = "FitSensorService";
    //Google Fit
    public static boolean mIsStop;
    private SensorManager mSensorManager;
    private int currentValue = 0;
    private IPedometerCallback iPedometerCallback;
    private GoogleApiClient mGoogleApiClient;
    private OnDataPointListener onDataPointListener;
    private float mStepsTotal;
    private long mStartTime;
    private double mFinalDistance = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        // register us as a sensor listener
        mStartTime = getStartTime();
        initFitApiClient();
    }

    /*****************************************
     * Google Fit Client Connection callback
     * Register the Source Listener to fetch Distance and Steps
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mStartTime = getStartTime();
        initHeartrate();
        subscribeRecording();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("onConnectionSuspended", "" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(LOG_TAG, "onConnectionFailed" + connectionResult.toString());
        Log.i(LOG_TAG, "onConnectionFailed:" + connectionResult.getErrorCode() + "," + connectionResult.getErrorMessage());
    }

    /****************************************
     * Disconnect the Fitness client api
     ***************************************/
    @Override
    public void onDestroy() {
        super.onDestroy();
        mSensorManager.unregisterListener(this);
        mIsStop = false;
        if (this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
        Log.d(LOG_TAG, " sensor unregistered");
    }

    /***************************************
     * programming interface that clients can use to interact with the service.
     * @param intent
     * @return
     ***************************************/
    @Override
    public IBinder onBind(Intent intent) {
        mIsStop = false;
        return mBinder;
    }

    /***************************************
     * Fetch the 6 minute walk test
     * Fetch Daiy total step and between range
     ***************************************/
    public void fetchWalkTestData() {
        getSeptsBetweenTimeStamp();
        getPedometerDistanceList();
    }


    /***************************************
     * Init Hear rate sensor
     ***************************************/
    private void initHeartrate() {
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor mHeartRateSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        boolean res = mSensorManager.registerListener(this, mHeartRateSensor, 10000);
    }

    /***************************************
     * Callback on Heart rate sensor
     ***************************************/
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // is this a heartbeat event and does it have data?
        if (sensorEvent.sensor.getType() == Sensor.TYPE_HEART_RATE && sensorEvent.values.length > 0) {
            int newValue = Math.round(sensorEvent.values[0]);
//            int newValue = 60;
            //Log.d(LOG_TAG,sensorEvent.sensor.getName() + " changed to: " + newValue);
            // only do something if the value differs from the value before and the value is not 0.
            if (currentValue != newValue && newValue != 0) {
                // save the new value
                currentValue = newValue;
                // send the value to the listener
                if (iPedometerCallback != null && !mIsStop) {
                    //Log.d(LOG_TAG, "sending new value to listener: " + newValue);
                    iPedometerCallback.onHearBeatValueChanged(newValue);
                }
            }
        }
    }

    /***************************************
     * on accuracy changes
     ***************************************/
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    /**
     * author's note :
     * earlier one could use the  broader  Fitness.API -- as in the now deprecated hellofit example --
     * However since March 2015/ Google Play Services V 7.x onwards,
     * changes to GoogleApiClient mandate use of the more spefic
     * Fitness API such as  Fitness.BLE_API SENSORS_API  Fitness.RECORDING_API
     * Fitness.HISTORY_API Fitness.SESSIONS_API Fitness.CONFIG_API No longer can one use Fitness.API
     */
    private synchronized void initFitApiClient() {
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                //.addApi(Fitness.BLE_API) //heart rate
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .useDefaultAccount() //Use the default account
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        this.mGoogleApiClient.connect();
    }

    /************************************************************
     * Subscribes Recording API to Total Steps Count for the day.
     ***********************************************************/
    private void subscribeRecording() {
        Fitness.RecordingApi.subscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setResultCallback(status -> {
                    if (status.isSuccess()) {
                        if (status.getStatusCode() == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                            Log.i(TAG, "Existing subscription for activity detected.");
                            registerSubscriptions();
                        } else {
                            Log.i(TAG, "Subscription registered.");
                            registerSubscriptions();
                        }
                    } else {
                        Log.i(TAG, "There was a problem subscribing.");
                    }
                });
    }

    /***************************************
     * Register required datatype for Sensor api
     ***************************************/
    private void registerSubscriptions() {
        //registerDataSourceListener(DataType.TYPE_STEP_COUNT_DELTA);
        registerDataSourceListener(DataType.TYPE_STEP_COUNT_CUMULATIVE);
        registerDataSourceListener(DataType.TYPE_DISTANCE_CUMULATIVE);
        //registerDataSourceListener(DataType.TYPE_DISTANCE_DELTA);
    }

    /****************************************
     * Register the Sensor API
     ***************************************/
    private void registerDataSourceListener(DataType dataType) {
        DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
                .setDataTypes(dataType)
                .setDataSourceTypes(DataSource.TYPE_DERIVED)
                .build();

        ResultCallback<DataSourcesResult> dataSourcesResultCallback = dataSourcesResult -> {
            for (DataSource dataSource : dataSourcesResult.getDataSources()) {
                Fitness.SensorsApi.add(mGoogleApiClient, new SensorRequest.Builder()
                                .setDataSource(dataSource)
                                .setDataType(dataType)
                                .setSamplingRate(10, TimeUnit.SECONDS)
                                .setAccuracyMode(SensorRequest.ACCURACY_MODE_HIGH)
                                .build(),
                        onDataPointListener)
                        .setResultCallback(status -> {
                            if (status.isSuccess()) {
                                Log.d(TAG, "onDataPointListener  registered good");
                            } else {
                                Log.d(TAG, "onDataPointListener failed to register bad");
                            }
                        });
            }
        };

        Fitness.SensorsApi.findDataSources(mGoogleApiClient, dataSourceRequest)
                .setResultCallback(dataSourcesResultCallback);

        onDataPointListener = dataPoint -> {
            if (dataPoint == null) {
                Log.d(TAG, "registerDataSourceListener onDataPoint = " + dataPoint);
                return;
            }
            //Toast.makeText(this, "Field: "+ dataPoint.getDataType(), Toast.LENGTH_SHORT).show();
            if (dataPoint.getDataType().equals(DataType.TYPE_DISTANCE_CUMULATIVE)) {
                for (Field field : dataPoint.getDataType().getFields()) {
                    Value value = dataPoint.getValue(field);

                    DistanceItem distanceItem = new DistanceItem();
                    distanceItem.setDistance((double) value.asFloat());
                    distanceItem.setStartDate(PedoUtils.getDateTime(dataPoint.getStartTime(TimeUnit.MILLISECONDS)));
                    distanceItem.setEndDate(PedoUtils.getDateTime(dataPoint.getEndTime(TimeUnit.MILLISECONDS)));
                    if (mStartTime <= dataPoint.getStartTime(TimeUnit.MILLISECONDS) && !mIsStop) {
                        float mDistance = value.asFloat();
                        Log.d("New Log: ", distanceItem.toString());
                        //pedoDistanceRequest.addItems(distanceItem); //handled duplicate distance data using Treeset comparator with startDate compareTo check
                        /*for (DistanceItem elem : pedoDistanceRequest.getItems()) {
                            mDistance = mDistance + elem.getDistance();
                        }
                        mFinalDistance = mDistance;*/

                        //String.format("%.0f", mFinalDistance) + "(m)");
                        iPedometerCallback.distanceChanged(mDistance);

                    } else {
                        //Log.d("Old Distance Log: ", distanceItem.toString());
                    }

                    Log.d(TAG, "onDataPoint Name= " + field.getName());
                    Log.d(TAG, "onDataPoint value=" + value);

                    // lets write cumulative steps
                    if (field.getName().contains("step")) {
                        Toast.makeText(this, "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Log.d(TAG, "else " + dataPoint.getDataType().getName());
            }
        };
    }


    /*****************************************************
     * Get all Subscription for Recording api to cancel it
     ****************************************************/
    public void cancelRecordingApi() {
        Fitness.RecordingApi.listSubscriptions(mGoogleApiClient)
                .setResultCallback(listSubscriptionsResult -> cancelSubscriptions(listSubscriptionsResult));
    }

    /****************************************
     * Cancel Subscription for Recording api
     **************************************
     * @param listSubscriptionsResult*/
    private void cancelSubscriptions(ListSubscriptionsResult listSubscriptionsResult) {

        Fitness.RecordingApi.unsubscribe(mGoogleApiClient, DataType.TYPE_DISTANCE_DELTA)
                .setResultCallback(status -> {
                    if (status.isSuccess()) {
                        Log.e("RecordingAPI", "Canceled DataType.TYPE_DISTANCE_DELTA");
                    } else {
                        // Subscription not removed
                        Log.e("RecordingAPI", "Failed to cancel subscriptions");
                    }
                    unregisterFitnessDataListener();
                });
    }

    /****************************************
     * Unregister the Sensor api
     ***************************************/
    private void unregisterFitnessDataListener() {
        if (this.onDataPointListener == null) {
            return;
        }

        Fitness.SensorsApi.remove(mGoogleApiClient, onDataPointListener)
                .setResultCallback(status -> {
                    if (status.isSuccess()) {
                        Log.d(TAG, "unregisterFitnessDataListener()Fitness.SensorsApi.remove isSuccess true ");
                    } else {
                        Log.d(TAG, "unregisterFitnessDataListener()Fitness.SensorsApi.remove isSuccess false");
                    }
                });

    }

    /**********************************************
     * Read the Step count between timestamp using
     * History api
     *********************************************/
    private void getSeptsBetweenTimeStamp() {
        //Get last 6 minute step data
        DataSource ESTIMATED_STEP_DELTAS = new DataSource.Builder()
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setType(DataSource.TYPE_DERIVED)
                .setStreamName("estimated_steps")
                .setAppPackageName("com.google.android.gms")
                .build();
        DataReadRequest readRequest = new DataReadRequest.Builder()
                .aggregate(ESTIMATED_STEP_DELTAS, DataType.AGGREGATE_STEP_COUNT_DELTA)
                .bucketByTime(10, TimeUnit.SECONDS)
                .setTimeRange(mStartTime, System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .build();

        // Invoke the History API to fetch the data with the query
        Fitness.HistoryApi.readData(mGoogleApiClient, readRequest).setResultCallback(dataReadResult -> printData(dataReadResult));
    }

    /**********************************************
     * Print data by fetching Buckets
     *********************************************/
    public void printData(DataReadResult dataReadResult) {
        // [START parse_read_data_result]
        // If the DataReadRequest object specified aggregated data, dataReadResult will be returned
        // as buckets containing DataSets, instead of just DataSets.
        if (dataReadResult.getBuckets().size() > 0) {
            Log.i(TAG, "Number of returned buckets of DataSets is: " + dataReadResult.getBuckets().size());
            for (Bucket bucket : dataReadResult.getBuckets()) {
                List<DataSet> dataSets = bucket.getDataSets();
                for (DataSet dataSet : dataSets) {
                    dumpDataSet(dataSet);
                }
            }
        } else if (dataReadResult.getDataSets().size() > 0) {
            Log.i(TAG, "Number of returned DataSets is: " + dataReadResult.getDataSets().size());
            for (DataSet dataSet : dataReadResult.getDataSets()) {
                dumpDataSet(dataSet);
            }
        }

        //onChangeListener.getStepBetweenRange(mPedometer);
    }

    /**********************************************
     * Print data by fetching Buckets
     *********************************************/
    private void dumpDataSet(DataSet dataSet) {
        Log.i(TAG, "Data returned for Data type: " + dataSet.getDataType().getName());
        for (DataPoint dataPoint : dataSet.getDataPoints()) {
            for (Field field : dataPoint.getDataType().getFields()) {
                Log.i(TAG, "Name: " + field.getName());
                if (field.getName().equalsIgnoreCase("steps")) {
                    Item item = new Item();
                    item.setDistance(0.0);
                    item.setStartDate(PedoUtils.getDateTime(dataPoint.getStartTime(TimeUnit.MILLISECONDS)));
                    item.setEndDate(PedoUtils.getDateTime(dataPoint.getEndTime(TimeUnit.MILLISECONDS)));
                    item.setFloorsAscended(0);
                    item.setFloorsDescended(0);
                    item.setNumberOfSteps(dataPoint.getValue(field).asInt());
                    mPedoItems.add(item);

                    Log.d(TAG, "Step History Frame: " + item.toString());
                    Log.i(TAG, "Steps: " + item.toString());

                    //Toast.makeText(this, item.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /**********************************************
     * Get pedometer list items distance 5 seconds buckets
     *********************************************/
    private void getPedometerDistanceList() {
        //onChangeListener.getDistanceList(pedoDistanceRequest);
    }


    private long getStartTime() {
        Calendar mCalendar = Calendar.getInstance();
        Date now = new Date();
        mCalendar.setTime(now);
        long startTime = mCalendar.getTimeInMillis();
        Log.e("6MWT Start Time", " " + startTime);
        return startTime;
    }
}