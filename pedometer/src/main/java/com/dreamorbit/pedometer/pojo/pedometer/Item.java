package com.dreamorbit.pedometer.pojo.pedometer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mujasam.bn on 12/20/2017.
 */

public class Item {

    @SerializedName("floorsAscended")
    @Expose
    private Integer floorsAscended;
    @SerializedName("floorsDescended")
    @Expose
    private Integer floorsDescended;
    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("numberOfSteps")
    @Expose
    private Integer numberOfSteps;
    @SerializedName("distance")
    @Expose
    private Double distance;

    public Integer getFloorsAscended() {
        return floorsAscended;
    }

    public void setFloorsAscended(Integer floorsAscended) {
        this.floorsAscended = floorsAscended;
    }

    public Integer getFloorsDescended() {
        return floorsDescended;
    }

    public void setFloorsDescended(Integer floorsDescended) {
        this.floorsDescended = floorsDescended;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Integer getNumberOfSteps() {
        return numberOfSteps;
    }

    public void setNumberOfSteps(Integer numberOfSteps) {
        this.numberOfSteps = numberOfSteps;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Item{" +
                "floorsAscended=" + floorsAscended +
                ", floorsDescended=" + floorsDescended +
                ", endDate='" + endDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", numberOfSteps=" + numberOfSteps +
                ", distance=" + distance +
                '}';
    }
}