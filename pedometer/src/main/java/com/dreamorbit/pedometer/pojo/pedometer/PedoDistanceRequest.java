package com.dreamorbit.pedometer.pojo.pedometer;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by nareshkumar.reddy on 12/27/2017.
 */

public class PedoDistanceRequest {

    private SortedSet<DistanceItem> items = new TreeSet<>(new StartDate());

    public SortedSet<DistanceItem> getItems() {
        return items;
    }

    public void addItems(DistanceItem item) {
        items.add(item);
    }
}
