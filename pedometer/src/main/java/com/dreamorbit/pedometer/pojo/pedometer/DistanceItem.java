package com.dreamorbit.pedometer.pojo.pedometer;

import java.util.Comparator;

/**
 * Created by nareshkumar.reddy on 12/27/2017.
 */

public class DistanceItem {
    private String endDate;
    private String startDate;
    private Double distance;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "DistanceItem{" +
                "endDate='" + endDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", distance=" + distance +
                '}';
    }

    class EndDate implements Comparator<DistanceItem> {
        @Override
        public int compare(DistanceItem e1, DistanceItem e2) {
            return e1.getEndDate().compareTo(e2.getEndDate());
        }
    }

    class Distance implements Comparator<DistanceItem>{
        @Override
        public int compare(DistanceItem e1, DistanceItem e2) {
            return e1.getDistance().compareTo(e2.getDistance());
        }
    }
}
