package com.dreamorbit.pedometer.pojo.pedometer;

import java.util.Comparator;

/**
 * Created by mujasam.bn on 1/19/2018.
 */


public class StartDate implements Comparator<DistanceItem> {
    @Override
    public int compare(DistanceItem e1, DistanceItem e2) {
        return e1.getStartDate().compareTo(e2.getStartDate());
    }
}