package com.dreamorbit.pedometer;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.tasks.Task;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class PedoUtils {
    /**
     * Get start date for test
     */
    public static String[] getStartEndDates() {
        String dates[] = new String[2];
        SimpleDateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSSZ");
        dateFormatterR.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance();
        dates[0] = dateFormatterR.format(calendar.getTime());
        calendar.add(Calendar.SECOND, -10);
        dates[1] = dateFormatterR.format(calendar.getTime());
        return dates;
    }

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @return String representing date in specified format
     */
    public static String getDateTime(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat dateFormatterR = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return dateFormatterR.format(calendar.getTime());
    }

    /**
     * Inserting the heart rate data to google fit
     *
     * @param heartRate
     */

    public static void insertHeartRateDataToGoogleFit(int heartRate, Context context) {
        // Set a start and end time for our data, using a start time of 1 hour before this moment.
        Log.e("PedoUtils", heartRate + "**Heart rate Pushing to Fit**");
        try {
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            cal.add(Calendar.SECOND, 10);
            long startTime = cal.getTimeInMillis();
            cal.add(Calendar.SECOND, 10);
            long endTime = cal.getTimeInMillis();

            // Create a data source
            DataSource dataSource =
                    new DataSource.Builder()
                            .setAppPackageName(context)
                            .setDataType(DataType.TYPE_HEART_RATE_BPM)
                            .setStreamName("Heart Rate")
                            .setType(DataSource.TYPE_RAW)
                            .build();

            // Create a data set
            DataSet dataSet = DataSet.create(dataSource);
            // For each data point, specify a start time, end time, and the data value -- in this case,
            // the number of new steps.
            DataPoint dataPoint =
                    dataSet.createDataPoint().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
            dataPoint.getValue(Field.FIELD_BPM).setFloat((float) heartRate);
            dataSet.add(dataPoint);
            Task response = Fitness.getHistoryClient(context, GoogleSignIn.getLastSignedInAccount(context)).insertData(dataSet);
            Log.e("PedoUtils", "**Heart Rate Pushed to Fit**");
        } catch (IllegalArgumentException e) {
            Log.e("PedoUtils Heart ", e.toString());
        }
    }

    /**
     * Inserting the Step data to google fit
     *
     * @param step
     */

    public static void insertStepDataToGoogleFit(int step, Context context) {
        // Set a start and end time for our data, using a start time of 1 hour before this moment.
        Log.e("PedoUtils", step + "**Step Pushing to Fit**");
        try {
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            cal.add(Calendar.SECOND, 10);
            long startTime = cal.getTimeInMillis();
            cal.add(Calendar.SECOND, 10);
            long endTime = cal.getTimeInMillis();

            DataSource dataSource =
                    new DataSource.Builder()
                            .setAppPackageName(context)
                            .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                            .setStreamName("Steps")
                            .setType(DataSource.TYPE_RAW)
                            .build();

            // Create a data set
            DataSet dataSet = DataSet.create(dataSource);
            // For each data point, specify a start time, end time, and the data value -- in this case,
            // the number of new steps.
            DataPoint dataPoint =
                    dataSet.createDataPoint().setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
            dataPoint.getValue(Field.FIELD_STEPS).setInt(step);
            dataSet.add(dataPoint);
            Task response = Fitness.getHistoryClient(context, GoogleSignIn.getLastSignedInAccount(context)).insertData(dataSet);
            Log.e("PedoUtils", "**Mobile Step Pushed to Fit**");
        } catch (IllegalArgumentException e) {
            Log.e("PedoUtils Step ", e.toString());
        }
    }

    /**
     * Infinity test and survey action lock.
     * Enable disable depend on flag. if true, do not update the db
     *
     * @return flag
     */
    public static boolean isContinuousSurvey() {
        return true;
    }
}
